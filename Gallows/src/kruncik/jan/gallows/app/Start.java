package kruncik.jan.gallows.app;

import javax.swing.SwingUtilities;

/**
 * Tato třída slouží puze jako třída pro spuštění samotné aplikace (spuštěcí
 * třída).
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Start {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(Login::new);
	}
}
