package kruncik.jan.gallows.app;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.dialogs.RegistrationForm;
import kruncik.jan.gallows.dialogs.RestoreAccountForm;
import kruncik.jan.gallows.dialogs.SourceDialog;
import kruncik.jan.gallows.gallowsApp.App;

/**
 * Tato třída slouží jako dialog pro přihlášení uživatele. Uživatel zde
 * má možnosti přihlásit se, vytvořit si nový účet (pouze jako hráč, ne jako admin) nebo
 * zavřit aplikaci.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Login extends SourceDialog implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	

	/**
	 * Proměnná pro volání operací nad DB. V tomto případě se jedná pouze
	 * o získání uživatelů pro kontrolu
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	/**
	 * Textové pole pro zadání uživatelského jména pro přihlášení uživatele
	 */
	private JTextField txtPseudonym;
	
	/**
	 * Textové pole pro zadání uživatelského hesla pro přihlášení uživatele
	 */
	private JPasswordField txtPassword;	
	
	
	// btnLogin - Tlačítko pro přihlášení uživatele
	// btnRegistraion - Tlačítko pro registraci nového uživatele - s vyjímkou admina
	// btnClose - Tlačítko pro ukončení apliikace
	private final JButton btnLogin, btnRegistration, btnClose, btnRestoreAccount;
	
	
	/**
	 * Tlačítko, které slouží pro nastavení cesty k DB pro aplikaci.
	 */
	private final JButton btnSetPathToDb;
	
	
	
	
	public Login() {
		super("Přihlášení");
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 270, "/icons/LoginForm.png", true);				
		
		
		createKeyAdapter();
		
		
		final GridBagConstraints gbc = createGbc();
		
		add(new JLabel("Uživatelské jméno: "), gbc);
		
		
		gbc.gridx = 1;
		txtPseudonym = new JTextField(25);		
		txtPseudonym.addKeyListener(keyAdapter);
		add(txtPseudonym, gbc);
		
		
		setGbc(gbc, 0, 1);
		add(new JLabel("Heslo: "), gbc);
		
		
		gbc.gridx = 1;
		txtPassword = new JPasswordField(25);
		txtPassword.addKeyListener(keyAdapter);
		add(txtPassword, gbc);
		
				
		setGbc(gbc, 0, 2);
		gbc.gridwidth = 2;
		
		btnLogin = new JButton("Přihlásit");
		btnLogin.addActionListener(this);
		
		btnRegistration = new JButton("Registrovat");
		btnRegistration.addActionListener(this);
		
		btnClose = new JButton("Zavřít");
		btnClose.addActionListener(this);
		
		btnRestoreAccount = new JButton("Obnovit účet");
		btnRestoreAccount.addActionListener(this);
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 0));
		pnlButtons.add(btnLogin);		
		pnlButtons.add(btnRegistration);
		pnlButtons.add(btnRestoreAccount);
		pnlButtons.add(btnClose);
		
		add(pnlButtons, gbc);
		
		
		

		// Tlačítko pro nastavení DB:
		final JPanel pnlSetPathToDbButton = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btnSetPathToDb = new JButton("Nastavit DB");
		btnSetPathToDb.addActionListener(this);
		
		pnlSetPathToDbButton.add(btnSetPathToDb);
		
		setGbc(gbc, 0, 3);
		add(pnlSetPathToDbButton, gbc);
		
		
		
		prepareWindow();
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * Metoda, která otestuje zadaná data a případně přihláší uživatele a spustí
		 * aplikaci, nebo vyhodí nějakou chybu, ke které došlo, například neexistuje
		 * zadaný uživatel apod.
		 * 
		 * Postup: Otestuji, zda jsou vyplněna pole, uživatelské jméno odpovídá
		 * regulárnímu výrazu a uživatel se zadaným jménem a heslem je v DB. Dle toho se
		 * uživatel buď přihlásí, nebo se zobrazí chybové hlášení dle chybi viz výše.
		 */
		
		final String password = new String(txtPassword.getPassword());

		if (txtPseudonym.getText().isEmpty() || password.length() == 0) {
			JOptionPane.showMessageDialog(this, "Nejsou zadány údaje pro přihlášení!", "Prázdná pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (txtPseudonym.getText().length() > 30 || password.length() > 35) {
			JOptionPane.showMessageDialog(this,
					"Alespoň jedno ze zadaných údajů obsahuje větší počet znaků, než je povolen!",
					"Překročen počet znaků", JOptionPane.ERROR_MESSAGE);
			return;
		}
		

		if (!txtPseudonym.getText().matches(REG_EX_PSEUDONYM)) {
			JOptionPane.showMessageDialog(null, "Zadané uživatelské jméno neodpovídá požadované syntaxi!",
					"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
			return;
		}

		final User user = DATA_MANAGER.getUserByPseudonymAndPassword(txtPseudonym.getText(), password);

		if (user != null) {
			/*
			 * Zde se načetl uživatel, tak akorát otestuji, zda není zablokovaný (nemá ban)
			 * a zda je to admin nebo "běžný uživatel" (- není to admin), abych věděl, zda
			 * se mají zobrazit možnosti pro admina nebo ne.
			 */
			if (user.getBan() == '1') {
				JOptionPane.showMessageDialog(this, "Tento účet je blokován!", "Účet blokován",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			
			else if (user.getDeleted() == '1') {
				if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this,
						"Uživatelský učet byl smazán, přejete si jej obnovit?", "Obnovit účet",
						JOptionPane.YES_NO_OPTION))
					new RestoreAccountForm();
				return;
			}

			// Zde mohu spostit aplikaci Šibenice a zrušit tento přihlašovací dialog.
			SwingUtilities.invokeLater(() -> new App(user));
			dispose();
		}

		else
			JOptionPane.showMessageDialog(this, "Uživatel se zadanými údaji se v DB nenachází!", "Uživatel nenalazen",
					JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	





	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			if (e.getSource() == btnLogin)
				checkData();

			else if (e.getSource() == btnRegistration)
				/*
				 * Otevře se formular pro registraci, kdyz se uzivatel zaregistruje, pak se mu
				 * ukaze hlaska, ze se uspesne zaregistroval a muze se zde prihlasit.
				 */
				new RegistrationForm(false, true);

			else if (e.getSource() == btnRestoreAccount)
				new RestoreAccountForm();
			
			else if (e.getSource() == btnClose)
				System.exit(0);
			
			
			else if (e.getSource() == btnSetPathToDb)
				DATA_MANAGER.setDbPath();
		}
	}
}