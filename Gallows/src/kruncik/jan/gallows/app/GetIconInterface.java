package kruncik.jan.gallows.app;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * Rozhraní, které obsahuje pouze metodu pro zísání obrázku a pro změnu velikosi
 * obrázku.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public interface GetIconInterface {

	/**
	 * Metoda, která načte obrázek s nějakého adresáře a buď ho vrátí nebo před tím
	 * upraví jeho rozměry (toto v případě obrázku pro šibenici).
	 * 
	 * @param path
	 *            - cesta k obrázku
	 * @param kindOfPicture
	 *            - výštový typ obrázku, kdle kterého se pozná, na jakou velikost se
	 *            má obrázek zvětšit / změnšít (transformoat)
	 * 
	 * @return ImageIcon nebo null, pokud nebude obrázek nalezen.
	 */
	static  ImageIcon getImage(final String path, final KindOfPicture kindOfPicture) {
		final URL url = GetIconInterface.class.getResource(path);

		if (url != null) {
			if (kindOfPicture == KindOfPicture.GALLOWS) {
				/*
				 * Zde se má vrátit obrázek do šibenice - jeden z obrázků šibenice, tak mu
				 * upravím velikost, tak aby pasoval do okna aplikace.
				 * 
				 * (Ale pouze v případě, že se rozměry obrázku již nenachází v požadovaných rozměrech.)
				 */
				final ImageIcon loadedIcon = new ImageIcon(url);

				if (loadedIcon.getIconWidth() != 590 || loadedIcon.getIconHeight() != 615)
					return new ImageIcon(new ImageIcon(url).getImage().getScaledInstance(590, 615, Image.SCALE_SMOOTH));

				return loadedIcon;
			}


			
			else if (kindOfPicture == KindOfPicture.APP_ICON)
				// Zde se jedná o "klasicou" ikonu do nějakého okna (dialogu, framu, ... tu
				// nebudu níjak upravovat
				return new ImageIcon(url);

			
			else if (kindOfPicture == KindOfPicture.TOOLBAR_OR_MENU) {
				// Zde se má obrázek zmenšit na velikost pro menu nebo toolbar:
				final ImageIcon loadedImg = new ImageIcon(GetIconInterface.class.getResource(path));
				return new ImageIcon(loadedImg.getImage().getScaledInstance(20, 20, Image.SCALE_SMOOTH));
			}
			
			else if (kindOfPicture == KindOfPicture.AUTOR_ICON)
				return new ImageIcon(new ImageIcon(url).getImage().getScaledInstance(150, 150, Image.SCALE_SMOOTH));
		}

		return null;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * metoda, která vrátí obrázek se změněnou velikostí na zadanou šířku a výšku.
	 * 
	 * @param image
	 *            - obrázek, kterému se má změit velikost na width a height
	 * 
	 * @param width
	 *            - šířka, která se má nastavit obrázku image
	 * @param height
	 *            - výška, která se má nastavit obrázku image
	 * 
	 * @return obrázek image o rozměrech width a height.
	 */
	static ImageIcon getResizedImage(final ImageIcon image, final int width, final int height) {
		return new ImageIcon(image.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
	}
}
