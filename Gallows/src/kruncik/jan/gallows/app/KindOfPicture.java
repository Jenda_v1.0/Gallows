package kruncik.jan.gallows.app;

/**
 * Tato třída - výčet obsahuje výčtové typy, které definují velikost obrázku, na
 * kterou se má načtený obrázek nastavit.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfPicture {

	/**
	 * Tento obrázek se má zvětšit / změnšit na vlikost, pro zobrazení "hlavním"
	 * okně aplikace - ro šibenici
	 */
	GALLOWS,

	/**
	 * Tento typ obrázku se měnit nijak nemusí, alespoň jeho velikost ne, apliace si
	 * jej sama přizpůsobí. Jedná se o ikony v horní liště aplikace - dialogu apod.
	 */
	APP_ICON,

	/**
	 * Tento typ obrázků se má přizpůsobit velikosti tak, aby se vešel a optimálně
	 * zobrazil v menu nebo v liště toolbar v "hlavním" okně aplikace.
	 */
	TOOLBAR_OR_MENU,
	
	/**
	 * Tento typ znamená, že se jedná o zmenšení obrázku na "normální" velikost tak,
	 * aby se vešla do dialogu o autorovi aplikace (AboutAuthorForm)
	 */
	AUTOR_ICON
}
