package kruncik.jan.gallows.db_objects;

import java.io.File;

import javax.swing.ImageIcon;

/**
 * Třída, která slouží jako objekt Uživatel, do tohoto objektu se vkládají
 * objekty z tabulky Users v DB.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class User {

	private final int id;
	
	private int psc, telephoneNumber;
	/**
	 * Tato proměnná značí, zda je uživatel blokován nebo ne,
	 * pokud je hodnota 1, pak je uživatel blokován, pokud je nula - 0, pak
	 * je uživatel povolen.
	 */
	private char ban;
	
	/**
	 * Prměnná typu char, která značí, zda byl uživatel smazaán nebo ne. Pokud se
	 * jedná o hodnotu 0 (nula), pak je to v pořádku, pokud je to hodnota 1 (jedna),
	 * pak byl uživatel smazán.
	 */
	private char deleted;
	
	
	private String firstName, lastName, street, email, kindOfUser, password, pseudonym;

	/**
	 * Tento obrázek je načten z DB, pokud jej uživatel nemá, pak je null
	 */
	private ImageIcon picture;

	/**
	 * Tato proměnná se použije pouze v případě, že uživatel chce vložit nějaký nový
	 * obrázek do DB, tak si nějaký obrázek vybere (nějaký označí v PC) a ten se
	 * uloží do této proměnné a při vkládání do DB se načte.
	 */
	private File fileImage;

	
	
	/**
	 * Výchozí konstruktor, který se používá pro vytvoření uživatele v aplikaci a
	 * následné vložení do DB, v tomto případě není potřeba ID, to se v DB při
	 * vložení vygeneruje.
	 */
	public User() {
		super();

		id = -1;
		deleted = '0';
	}
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param id
	 *            - ID uživatele
	 * @param psc
	 *            - PSC města, ve kterém bydlí
	 * @param telephoneNumber
	 *            - Telefonní číslo
	 * @param ban
	 *            - 0 - uživatel je OK, 1 - uživatel je blokován.
	 * @param firstName
	 *            - Jméno uživatele
	 * @param lastName
	 *            - příjmení uživatele
	 * @param street
	 *            - Ulice, kde uživatel bydlí
	 * @param email
	 *            - Email na uživatele
	 * @param kindOfUser
	 *            - Typ uživatele (admin, hráč, ...)
	 * @param password
	 *            - Heslo k účtu
	 * @param pseudonym
	 *            - Uživatelské jméno pro přihlášení
	 * @param picture
	 *            - obrázek načtený z DB (pokud nějaký je, jinak bude null)
	 * @param deleted
	 *            - proměnná, která značí, zda byl uživatel smazán nebo ne, nula -
	 *            OK, jedna - Smazán.
	 */
	public User(int id, int psc, int telephoneNumber, char ban, String firstName, String lastName, String street,
			String email, String kindOfUser, String password, String pseudonym, ImageIcon picture, char deleted) {
		super();

		this.id = id;
		
		/*
		 * Pokud je psč 0, tj. není zvolené žádné, pak nastavím psč na -1, abych při
		 * ukládání poznal, že se nemá vkládat psč.
		 */
		if (psc == 0)
			this.psc = -1;
		else this.psc = psc;
		
		this.telephoneNumber = telephoneNumber;
		this.ban = ban;
		this.firstName = firstName;
		this.lastName = lastName;
		this.street = street;
		this.email = email;
		this.kindOfUser = kindOfUser;
		this.password = password;
		this.pseudonym = pseudonym;
		this.picture = picture;
		this.deleted = deleted;
	}

	
	
	public File getFileImage() {
		return fileImage;
	}

	public void setFileImage(File fileImage) {
		this.fileImage = fileImage;
	}

	public int getPsc() {
		return psc;
	}

	public void setPsc(int psc) {
		this.psc = psc;
	}

	public int getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(int telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public char getBan() {
		return ban;
	}

	public void setBan(char ban) {
		this.ban = ban;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getKindOfUser() {
		return kindOfUser;
	}

	public void setKindOfUser(String kindOfUser) {
		this.kindOfUser = kindOfUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPseudonym() {
		return pseudonym;
	}

	public void setPseudonym(String pseudonym) {
		this.pseudonym = pseudonym;
	}

	public ImageIcon getPicture() {
		return picture;
	}

	public void setPicture(ImageIcon picture) {
		this.picture = picture;
	}

	public int getId() {
		return id;
	}

	public char getDeleted() {
		return deleted;
	}

	public void setDeleted(char deleted) {
		this.deleted = deleted;
	}
}