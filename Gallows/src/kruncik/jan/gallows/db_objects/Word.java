package kruncik.jan.gallows.db_objects;

/**
 * Tato třída slouží pro manipulaci s objekty v DB v tabulce Words
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Word {

	/**
	 * Proměnná, která slouží jako volitelný komentář ke slovu word, například
	 * význam slova apod.
	 */
	private String comment;

	/**
	 * Tato proměnná je hádané slovo v Šibenici
	 */
	private String word;

	
	/**
	 * Tato proměnná bude naplněna pouze v případě, že se má změnit dané slovo word
	 * (hádané slovo). Jinak tato proměnná nemá význam, bude naplněna pouze v
	 * případě, že se má slovo změnit, jinak nebude tato proměnná potřeba.
	 */
	private String newWord;
	
	
	
	/**
	 * Tato proměnná slouží jako název skupiny slov, kam patří slovo word
	 */
	private int groupId;

	
	
	
	
	/**
	 * Konstruktor této třídy slouží jak pro načítání dat z DB, tak i pro vkládání,
	 * nic se níjak neliší nebo zvlášť negeneruje (ID - čka apod.)
	 * 
	 * @param comment
	 *            - volitelný komentář ke hádanému / hledanému slovu word v ŠIbenici
	 * @param word
	 *            - hádané / hledané slovo v Šibenici
	 * @param groupId
	 *            - ID skupiny slov, do které patří dané slovo.
	 */
	public Word(String comment, String word, int groupId) {
		super();

		this.comment = comment;
		this.word = word;
		this.groupId = groupId;
	}

	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getNewWord() {
		return newWord;
	}

	public void setNewWord(String newWord) {
		this.newWord = newWord;
	}
}