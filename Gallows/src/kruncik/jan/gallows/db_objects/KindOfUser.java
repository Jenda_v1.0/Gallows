package kruncik.jan.gallows.db_objects;

/**
 * Tato třída slouží pro manipulaci s hodnotami v tabulce KindsOfUser
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfUser extends KindsOfAncestor {

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param kind
	 *            - typ uživatele.
	 * @param comment
	 *            - komentář k danému typu uživatele.
	 */
	public KindOfUser(final String kind, final String comment) {
		super();
		
		this.kind = kind;
		this.comment = comment;
	}

	
	
	@Override
	public String toString() {
		return kind;
	}
}