package kruncik.jan.gallows.db_objects;

/**
 * Tato třída slouží pro "manipulaci" s objekty v DB v tabulce Groups.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Group {

	
	private final int id;
	
	
	/**
	 * jedná se o název skupiny, ke které patří nějaká slova.
	 */
	private String groupName;
	
	
	
	
	/**
	 * Volitelný komentář k této skupině slov.
	 */
	private String comment;

	
	

	/**
	 * Konstruktor této třídy. Konstruktor pro získávání dat z DB-
	 * 
	 * @param id
	 *            - ID dané skupiny
	 * @param groupName
	 *            - Název skupiny
	 * @param comment
	 *            - komentář k této skupině.
	 */
	public Group(int id, String groupName, String comment) {
		super();

		this.id = id;
		this.groupName = groupName;
		this.comment = comment;
	}
	
	
	/**
	 * Konstruktor pro vytváření skupin pomocí aplikace a případné vkládání do DB.
	 * Zde není potřeba žádné ID, to se vytvoří až v DB:
	 * 
	 * @param groupName
	 *            - Název skupiny
	 * @param comment
	 *            - komentář k této skupině.
	 */
	public Group(String groupName, String comment) {
		super();

		this.id = -1;
		this.groupName = groupName;
		this.comment = comment;
	}

	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getId() {
		return id;
	}


	@Override
	public String toString() {
		if (comment != null && !comment.isEmpty())
			return groupName + " - " + comment;

		else
			return groupName;			
	}
}