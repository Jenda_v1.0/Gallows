package kruncik.jan.gallows.db_objects;

/**
 * Tato třída slouží pro manipulaci s objekty z tabulky KindsOfGame.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfGame extends KindsOfAncestor {

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param kind
	 *            - typ hry.
	 * @param comment
	 *            - komentář k danému typu hry.
	 */
	public KindOfGame(final String kind, final String comment) {
		super();

		this.kind = kind;
		this.comment = comment;
	}
	
	
	
	
	@Override
	public String toString() {
		if (comment != null && !comment.isEmpty())
			return kind + " - " + comment;

		else
			return kind;
	}
}