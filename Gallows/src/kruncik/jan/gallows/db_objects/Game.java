package kruncik.jan.gallows.db_objects;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Tato třída slouží pro manipulaci s objekty v tabulce Games v DB.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class Game {	
	
	private final int id;

	
	/**
	 * Proměnná, která reprezentuje životy. Tato hodnota bude naplněna pouze v
	 * případě, že se jedná o hru typu Countdown, v takovém případě může uživatel
	 * skončít tak, že hru zastaví a může pokračovat přístě, pokud má nějaé životy a
	 * čas mu ještě nevypršel. Proto potřebuji znát počet životů.
	 */
	private int lives;
	
	/**
	 * Proměnná, která obsahuje datum, kdy byla hra vytvořena. Konkrétně, kdy
	 * uživatel "prohrál", tj. hrál hru šibenici a prohrál, tak čas a datum kdy
	 * prohrál je v této proměnné.
	 */
	private Date date;

	// Comment = nějaký volitelný komentář k prohrané hře
	// KindOfGame - typ hry, na meřený čas, nebo se mu jen stopuje apod.
	private String comment, kindOfGame;

	/**
	 * groupName - ID skupiny, ze které se brali slovíčka
	 */
	private int groupId; 
	
	/**
	 * Toto jsou normální odehrané hodnoty, pro oba typy her budou to hodnoty
	 * odehrané při ukončení hry při Measurement hře a bude to čas při ukončení hry
	 * typu Countdown
	 */
	private int hh, mm, ss, score, userId;
	
	/**
	 * Toto jsou oroginální hodnoty, které se naplní v případě typu hry Countdown,
	 * jedná se o výchozí hodnoty, například když uživatel nastaví, že chce hrát 5
	 * minut, tak toto bude těch 5 minut.
	 */
	private int original_hh, original_mm, original_ss;

	
	/**
	 * Tento konstruktor slouží pro vytváření instancí této třídy když se načítají
	 * data z DB.
	 * 
	 * @param id
	 *            - ID dané hry
	 * @param date
	 *            - datum, kdy se daná hra hrála
	 * @param comment
	 *            - volitelný komentář ke hře
	 * @param kindOfGame
	 *            - typ hry
	 * @param hh
	 *            - hodnota značí jak dlouho hrál uživatel hru v hodinách
	 * @param mm
	 *            - hodnota značí jak dlouho hrál uživatel hru v minutách
	 * @param ss
	 *            - hodnota značí jak dlouho hrál uživatel hru v sekundách
	 * @param score
	 *            - skóre, kterého uživatel dosáhl
	 * @param userId
	 *            - ID uživatele, který hrál tuto hru a uložil ji
	 * @param groupId
	 *            - ID skupiny, ze které se brali slovíčka pro hru
	 * @param original_hh
	 *            - zbývající hodiny v případě, že se jedná o typ hry Countdown.
	 * @param original_mm
	 *            - zbývající minuty v případě, že se jedná o typ hry Countdown.
	 * @param original_ss
	 *            - zbývající sekundy v případě, že se jedná o typ hry Countdown.
	 * @param lives
	 *            - počet životů - tento parametr se týká pouze hry typu Countdown.
	 */
	public Game(int id, Date date, String comment, String kindOfGame, int hh, int mm, int ss, int score, int userId,
			int groupId, int original_hh, int original_mm, int original_ss, int lives) {
		super();

		this.id = id;
		this.date = date;
		this.comment = comment;
		this.kindOfGame = kindOfGame;
		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
		this.score = score;
		this.userId = userId;
		this.groupId = groupId;
		this.original_hh = original_hh;
		this.original_mm = original_mm;
		this.original_ss = original_ss;
		this.lives = lives;
	}
	
	
	/**
	 * Tento konstruktor slouží pro vytváření instancí této třídy když se tato hra
	 * vytváří, tj. když uživatel prohrál / dohrál a chce si uložit danou hru.
	 * 
	 * Dále se v tento konstruktor použije, pokud se jedná o typ hry Measurement,
	 * tj. měření času, ne časovač (Countdown).
	 * 
	 * 
	 * Jedná se o to, že není třeba vytvářet ID, to se neukládá do DB, to se při
	 * vložení hry vytvoří "samo", resp. DB si jej vytvoří.
	 * 
	 * @param date
	 *            - datum, kdy se daná hra hrála
	 * @param comment
	 *            - volitelný komentář ke hře
	 * @param kindOfGame
	 *            - typ hry
	 * @param hh
	 *            - hodnota značí jak dlouho hrál uživatel hru v hodinách
	 * @param mm
	 *            - hodnota značí jak dlouho hrál uživatel hru v minutách
	 * @param ss
	 *            - hodnota značí jak dlouho hrál uživatel hru v sekundách
	 * @param score
	 *            - skóre, kterého uživatel dosáhl
	 * @param userId
	 *            - ID uživatele, který hrál tuto hru a uložil ji
	 * @param groupId
	 *            - ID skupiny, ze které se brali slovíčka pro hru
	 * @param lives
	 *            - počet životů (pokud bude větší než nula, pak je možné ve hře pokračovat)
	 */
	public Game(Date date, String comment, String kindOfGame, int hh, int mm, int ss, int score, int userId,
			int groupId, int lives) {
		super();

		id = -1;
		this.date = date;
		this.comment = comment;
		this.kindOfGame = kindOfGame;
		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
		this.score = score;
		this.userId = userId;
		this.groupId = groupId;
		this.lives = lives;
	}
	
	
	
	
	
	
	/**
	 * Tento konstruktor slouží pro vytváření instancí této třídy když se tato hra
	 * vytváří, tj. když uživatel prohrál / dohrál a chce si uložit danou hru.
	 * 
	 * Dále se v tento konstruktor použije, pokud se jedná o typ hry Countdown, tj.
	 * časovač, ne meření času (Measurement). Protože si chci uložit dalši hodnoty a
	 * sice, ten původní nastavený čas hry, například pět minut, ale uživatel mohl
	 * odehrát třeba jen 3 minuty a pak si hru uložit, tak potřebuji ještě uložit ty
	 * 3 odehrané minuty, aby v tom mohl pokračovat příště a životy - pokud mu
	 * nějaké zbyly, jinak je jasné, že již prohrál.
	 * 
	 * Jedná se o to, že není třeba vytvářet ID, to se neukládá do DB, to se při
	 * vložení hry vytvoří "samo", resp. DB si jej vytvoří.
	 * 
	 * @param date
	 *            - datum, kdy se daná hra hrála
	 * @param comment
	 *            - volitelný komentář ke hře
	 * @param kindOfGame
	 *            - typ hry
	 * @param hh
	 *            - hodnota značí jak dlouho hrál uživatel hru v hodinách
	 * @param mm
	 *            - hodnota značí jak dlouho hrál uživatel hru v minutách
	 * @param ss
	 *            - hodnota značí jak dlouho hrál uživatel hru v sekundách
	 * @param score
	 *            - skóre, kterého uživatel dosáhl
	 * @param userId
	 *            - ID uživatele, který hrál tuto hru a uložil ji
	 * @param groupId
	 *            - ID skupiny, ze které se brali slovíčka pro hru
	 * @param original_hh
	 *            - zbývající hodiny v případě, že se jedná o typ hry Countdown.
	 * @param original_mm
	 *            - zbývající minuty v případě, že se jedná o typ hry Countdown.
	 * @param original_ss
	 *            - zbývající sekundy v případě, že se jedná o typ hry Countdown.
	 * @param lives
	 *            - počet životů (pokud bude větší než nula a čas ještě nevypršel, pak je možné ve hře pokračovat)
	 */
	public Game(Date date, String comment, String kindOfGame, int hh, int mm, int ss, int score, int userId,
			int groupId, int original_hh, int original_mm, int original_ss, int lives) {
		super();

		id = -1;
		this.date = date;
		this.comment = comment;
		this.kindOfGame = kindOfGame;
		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
		this.score = score;
		this.userId = userId;
		this.groupId = groupId;
		this.original_hh = original_hh;
		this.original_mm = original_mm;
		this.original_ss = original_ss;
		this.lives = lives;
	}
	
	
	
	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getKindOfGame() {
		return kindOfGame;
	}

	public int getHh() {
		return hh;
	}

	public void setHh(int hh) {
		this.hh = hh;
	}

	public int getMm() {
		return mm;
	}

	public void setMm(int mm) {
		this.mm = mm;
	}

	public int getSs() {
		return ss;
	}

	public void setSs(int ss) {
		this.ss = ss;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getId() {
		return id;
	}

	public int getUserId() {
		return userId;
	}

	public int getGroupId() {
		return groupId;
	}

	public int getOriginal_hh() {
		return original_hh;
	}

	public int getOriginal_mm() {
		return original_mm;
	}

	public int getOriginal_ss() {
		return original_ss;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	
	/**
	 * Metoda, která zformátuje zadaný datum na formát de.měsíc.rok hh:mm:ss a tento
	 * formát vrátí.
	 * 
	 * @return - datum ve výše uvedeném formátu.
	 */
	public final String getFormatedDate() {
		if (date != null) // Nemělo by nastat
			return new SimpleDateFormat("dd.MM.yyyy").format(date);

		return null;
	}
	
	
	/**
	 * Metoda, která zformátuje a vrátí čas hry. Pokud je to typ hry Measurement,
	 * pak se vrátí čas hry, dokud uživatel hrál, než prohrál (neuhodl slovo). Pokud
	 * se jedná o typ hry Countdown, pak se jedná čas, který uplynul. například
	 * pokud uživatel nastavil oroginální čas 5 minut, ale hrál jen tři minuty a pak
	 * prohrál, nebo zastavil hru a uložil jí, pak tento čas budou zbývající dvě
	 * minuty.
	 * 
	 * @return jeden z výše uvedených časů.
	 */
	public final String getFormatedTime() {
		final String s_hh, s_mm, s_ss;
		
		if (hh < 10)
			s_hh = "0" + hh;
		else s_hh = String.valueOf(hh);
		
		if (mm < 10)
			s_mm = "0" + mm;
		else s_mm = String.valueOf(mm);
		
		if (ss < 10)
			s_ss = "0" + ss;
		else s_ss = String.valueOf(ss);
				
		return s_hh + " : " + s_mm + " : " + s_ss;
	}
	
	/**
	 * metoda, která zformátuje a vrátí originální čas hry, tj. uživatel na začátku
	 * typu této hry definoval nějaký čas, a pro časovač - jak dlouho chce hrát, tak
	 * to je tento čas. Uživatel mohl například nastavit 5 minut apod.
	 * 
	 * @return výše uvedený čas.
	 */
	public final String getFormattedOriginalTime() {
		final String s_hh, s_mm, s_ss;
		
		if (original_hh < 10)
			s_hh = "0" + original_hh;
		else s_hh = String.valueOf(original_hh);
		
		if (original_mm < 10)
			s_mm = "0" + original_mm;
		else s_mm = String.valueOf(original_mm);
		
		if (original_ss < 10)
			s_ss = "0" + original_ss;
		else s_ss = String.valueOf(original_ss);
				
		return s_hh + " : " + s_mm + " : " + s_ss;
	}
}