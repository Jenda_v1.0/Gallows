package kruncik.jan.gallows.db_objects;

/**
 * Tato třída slouží pro "ukládání" objektů z DB typu City, do instancí této
 * třídy se vkládají hodnoty z tabulky Cities.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class City {

	/**
	 * Proměnná, do které se vkládá hodnota PSC coby PSČ nějakého města.
	 */
	private int psc;
	
	/**
	 * Proměnná, do které se vkládá hodnota název města s daným PSČ
	 */
	private String name;

	
	/**
	 * Tato proměnná bude naplněna pouze v případě, že se má aktualizovat PSČ.
	 */
	private int newPsc; 
	
	
	
	
	public City(final int psc, final String name) {
		super();
		
		this.psc = psc;
		this.name = name;
		newPsc = -1;
	}


	public int getPsc() {
		return psc;
	}

	public void setPsc(int psc) {
		this.psc = psc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNewPsc() {
		return newPsc;
	}

	public void setNewPsc(int newPsc) {
		this.newPsc = newPsc;
	}
	
	
	/**
	 * Metoda, která vrátí psč ve tvaru xxx xx. Případně jinak, ale za 3. číslem
	 * bude mezera.
	 * 
	 * @return text s psč ve výše uvedeném formátu.
	 */
	public String getPscWithSpace() {
		final String stPsc = String.valueOf(psc);
		return stPsc.substring(0, 3) + " " + stPsc.substring(3);
	}
}