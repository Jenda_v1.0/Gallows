package kruncik.jan.gallows.db_objects;

/**
 * Tato třída slouží pouze jako předek pro řádky v tabulkách KindsOfGames a
 * KindsOfUsers, protože mají stejné atributy.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindsOfAncestor {

	/**
	 * Proměnný, která značí typ hry nebo typ uživatele. Například, zda se jedná o
	 * hru typu Countdown nebo o uživatele typu admin apod.
	 */
	protected String kind;

	/**
	 * Proměnná, která značí komentář k danému typu hry nebo uživateli (je
	 * volitelný).
	 */
	protected String comment;

	/**
	 * Tato proměnná se naplní pouze v případě, že se má aktualizovat i typ hry nebo
	 * uživatele
	 */
	private String newKind;

	/**
	 * Vrátí typ hry nebo uživatele.
	 * 
	 * @return - typ hry nebo uživatele.
	 */
	public String getKind() {
		return kind;
	}

	/**
	 * Nastaví se typu hry nebo uživatele.
	 * 
	 * @param kind
	 *            - typ hry ebo uživatele.
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	 * Vrátí komentář
	 * 
	 * @return - komentář k typu uživatele nebo hry.
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Nastaví se komentář k typu hry nebo uživatele
	 * 
	 * @param comment
	 *            - Komentář k typu hry nebo uživatele.
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Setr pro nastavení proměnní newKind, která když bude naplněna aktualizuje se
	 * v DB i typ hry nebo uživatele.
	 * 
	 * @param newKind
	 *            - nový typ hry nebo uživatele, který má přepsat ten původní (kind)
	 */
	public void setNewKind(String newKind) {
		this.newKind = newKind;
	}

	/**
	 * Getr na proměnnou newKind, která obsahuje nový název typu hry nebo uživatele.
	 * 
	 * @return nový typ hry nebo uživatele.
	 */
	public String getNewKind() {
		return newKind;
	}
}
