package kruncik.jan.gallows.db;

import java.util.List;

import kruncik.jan.gallows.db_objects.City;
import kruncik.jan.gallows.db_objects.Game;
import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.db_objects.KindOfUser;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.db_objects.Word;

/**
 * Toto obsahuje metody, které lze provést nad DB.
 * 
 * Hlavně (pro každou tabulku):
 * - Přidání prvku do DB
 * - Odebrání prvku z DB
 * - Aktualizace prvku v DB
 * - Získání prvku v DB
 * - Získání prvku s nějakým ID daného prvku
 * 
 * Pak následují metody pro ostatní kombinace
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public interface DataManagerImpl {

	/**
	 * Metoda, která nastaví cestu k databázi pro aplikaci.
	 * 
	 * Otevře se dialogové okno, kde se vybere adresář, který obsahuje databázi pro
	 * aplikaci, po vybrání adresáře se daná cesta k adresáři nastaví do aplikace.
	 * 
	 * Je nezbytné, aby databáze obsahovala stejné tabulky a některé hodnoty v
	 * tabulkách KindsOfGames a KindsOfUsers.
	 */
	void setDbPath();
	
	
	
	// Operace nad tabulkou Users:

	/**
	 * Metoda, která přidá do DB uživatele.
	 * 
	 * @param user
	 *            - uživatel, který se přidá do DB
	 *            
	 * @return true, pokud se přidání povedlo v pohodě, false, pokud došlo k nějaké
	 *         vyjímce a nepodařilo se přidat uživatele
	 */
	boolean addUser(final User user);
	
	/**
	 * metoda, která upraví uživatele v DB dle ID uživatele.
	 * 
	 * @param user
	 *            - uživatel, který se má upravit v DB (dle ID)
	 * @return true, pokud se uživatel upraví v pořádku, jinak false
	 */
	boolean updateUser(final User user);

	/**
	 * Metoda, která získá uživatele z DB dle ID uživatele.
	 *
	 * @param id
	 *            - ID uživatele, který se získá z DB.
	 *
	 * @return uživatel s daným ID nebo null, pokud se v DB takový uživatel
	 *         nenachází.
	 */
	User getUser(final int id);

	/**
	 * Metoda, která získá z DB veškeré uživatele.
	 * 
	 * @return list uživatelů v DB
	 */
	List<User> getAllUsers();
	
	/**
	 * Metoda, která vymaže uživatelel s daným ID i se všemi daty, které k němu
	 * patří - jeho hry apod..
	 * 
	 * @param id
	 *            - ID uživatele, který se má z DB vymazat.
	 * 
	 * @return true, pokud se operace povede, jinak false.
	 */
	boolean deleteUserWithAllData(final int id);
	/**
	 * Tato metoda nastaví ve sloupci Deleted v řádku tabulky, kde se shodují ID -
	 * čka uživatele na 1, jako, že je účet smazaný.
	 * 
	 * Kvůli tomu, aby zůstaly výsledky v DB, tak nebudu mazat uživatele, který jich
	 * dosáhl, musel bych pak smazat i výsledky, což nechci.
	 * 
	 * @param id
	 *            - ID uživatele, u kterého se má nastavit sloupec Deleted na 1
	 *            ("Smazáno").
	 * 
	 * @return true, pokud se výše popsaná operace povedla, jinak false.
	 */
	boolean deleteBlockUser(final int id);
	
	// Další metody nad tabulkou User:
	/**
	 * Metoda, která získá uživatele (User) u kterého se shoduje pseudonym a heslo.
	 * 
	 * @param pseudonym
	 *            - pseudonym uživatele
	 * @param password
	 *            - heslo uživatele
	 * @return User, kde se shoduje pseudonym a heslo, nebo null, pokud nebude
	 *         takový uživatel nalezen.
	 */
	User getUserByPseudonymAndPassword(final String pseudonym, final String password);
	
	/**
	 * Metoda, která z DB vrátí uživatele, u kterého se shoduje uživatelské jméno,
	 * email a heslo. Tato metoda se používá pro získání uživatele dle těchté údajů
	 * za účelem obnovení účtu, pokud byl smazán (nastaven atribut Deleted na 1, ne
	 * úplné vymazání z DB)
	 * 
	 * @param pseudonym
	 *            - uživatelské jméno pro daný účet
	 * 
	 * @param email
	 *            - email pro daný účet
	 * @param password
	 *            - heslo daného účtu
	 * 
	 * @return uživatele s výše uvedenými daty (pseudonym, email a heslo) nebo null,
	 *         pokud se v DB takový uživatel nenachází.
	 */
	User getUserByPseudonymEmailPassword(final String pseudonym,final String email, final String password);
	
	/**
	 * Metoda, která "obnoví" uživatelský účet. V podsatě pouze aktualizuje řádek v
	 * tabulce Users, kde se shodují ID - čka uživatele, tak tam nastaví ve sloupci
	 * Deleted nulu (tj. účet není smazaný)
	 * 
	 * @param userId
	 *            - ID uživatele, který se má obnovit
	 * @return true, pokud se výše popsaná operace povede, jinak false
	 */
	boolean restoreUser(final int userId);
	
	
	
	
	
	
	
	
	
	
	
	
	// Operace nad tabulkou Cities:
	
	/**
	 * Metoda, která přidá do DB nové město.
	 * 
	 * @param city
	 *            - město, které se přidá do DB
	 *            
	 * @return true, pokud se přidání povedlo v pohodě, false, pokud došlo k nějaké
	 *         vyjímce a nepodařilo se přidat město
	 */
	boolean addCity(final City city);

	/**
	 * Metoda, která získá město z DB dle PSC města.
	 * 
	 * @param psc
	 *            - psc města, které se získá z DB.
	 * 
	 * @return město s daným psc nebo null, pokud se v DB takové město s daným psc
	 *         nenachází.
	 */
	City getCity(final int psc);

	/**
	 * metoda, která upraví / aktualizuje město v DB dle ID města.
	 * 
	 * @param city
	 *            - město, které se má upravit v DB (dle ID)
	 * @return true, pokud se město upraví v pořádku, jinak false
	 */
	boolean updateCity(final City city);
	
	/**
	 * Metoda, která získá z DB veškerá města.
	 * 
	 * @return list měst v DB
	 */
	List<City> getAllCities();

	/**
	 * Metoda, která vymaže město s daným psc.
	 * 
	 * @param psc
	 *            - psc města, které se má z DB vymazat.
	 *            
	 * @return true, pokud se operace povede, jinak false.
	 */
	boolean deleteCity(final int psc);
	
	/**
	 * Metoda, která získá z DB veškerá PSC všech měst.
	 * 
	 * @return list PSC
	 */
	List<Integer> getAllPsc();
	
	/**
	 * Metoda, která vymaže veškerá města v DB.
	 * 
	 * @return true, pokud se podaří všechna města smazat v pořádku, jinak false.
	 */
	boolean deleteAllCities();
	
	
	
	
	
	
	
	
	
	
	
	
	// Operace nad tabulkou Games:
	
	/**
	 * Metoda, která přidá do DB novou hru.
	 * 
	 * @param game
	 *            - hra, která se přidá do DB
	 * 
	 * @return true, pokud se přidání povedlo v pohodě, false, pokud došlo k nějaké
	 *         vyjímce a nepodařilo se přidat hru
	 * @param withOriginalTime
	 *            - logická hodnota o tom, zda se má vložit do DB i původní
	 *            nastavený čas v případě odehrané hry typu Countdown, pak se vloží
	 *            do DB i nastavený čas pro časovač hry.
	 */
	boolean addGame(final Game game, final boolean withOriginalTime);

	/**
	 * Metoda, která získá hru z DB dle ID dané hry.
	 * 
	 * @param id
	 *            - ID hry, která se získá z DB.
	 * 
	 * @return hru s daným ID nebo null, pokud se v DB taková hra nenachází.
	 */
	Game getGame(final int id);
	
	/**
	 * Metoda, která získá z DB veškeré hry, které odehrál uživatel s daným ID.
	 * 
	 * @param id
	 *            - ID uživatele jehož hry se mají načíst.
	 * @return list her uživatele s daným ID.
	 */
	List<Game> getAllGamesByUserId(final int id);
	
	/**
	 * metoda, která upraví / aktualizuje hru v DB dle ID hry.
	 * 
	 * @param game
	 *            - hra, která se má upravit v DB (dle ID)
	 * @return true, pokud se hra upraví v pořádku, jinak false
	 * @param withOriginalTime
	 *            - logická hodnota o tom, zda se má aktualizovat v DB i původní
	 *            nastavený čas v případě odehrané hry typu Countdown, pak se vloží
	 *            do DB i nastavený čas pro časovač hry.
	 */
	boolean updateGame(final Game game, final boolean withOriginalTime);

	/**
	 * Metoda, která získá z DB veškeré odehrané hry všech uživatelů.
	 * 
	 * @return list odehraných her všech uživatelů v DB
	 */
	List<Game> getAllGames();

	/**
	 * Metoda, která vymaže hru s daným ID.
	 * 
	 * @param id
	 *            - ID hry, které se má z DB vymazat.
	 * 
	 * @return true, pokud se operace povede, jinak false.
	 */
	boolean deleteGame(final int id);
	
	/**
	 * Metoda, která aktualizuje komentář v nějaké hře (v řádku v tabulce Games).
	 * 
	 * @param id
	 *            - ID hry, kde se má aktualizovat komentář.
	 * @param comment
	 *            - nový komentář, který má nahradit ten původní.
	 */
	void updateCommentInGame(final int id, final String comment);
	
	/**
	 * Metoda, která vymaže veškeré hry z tabulky Games, jejíchž typ je kindOfGame.
	 * Vymažou se veškeré hry typu kindOfGame.
	 * 
	 * @param kindOfGame
	 *            - Typ hry, které se mají vymazat.
	 * @return true, pokud se hry vymažou v pořádku, jinak false, pokud dojde k
	 *         nějaké chybě.
	 */
	boolean deleteGamesByKindOfGame(final String kindOfGame);
	
	/**
	 * Metoda, která vymaže veškeré hry v DB (v tabulce Games). (Vymaže všechna data
	 * v tabulce Games.)
	 * 
	 * @return true, pokud se data vymažou v pořádku, jinak false.
	 */
	boolean deleteAllGames();
	
	
	
	
	
	
	
	
	
	
	// Operace nad tabulkou KindsOfGames:

	/**
	 * Metoda, která přidá do DB nový typ hry.
	 * 
	 * @param kindOfGame
	 *            - typ hry, která se přidá do DB
	 * 
	 * @return true, pokud se přidání povedlo v pohodě, false, pokud došlo k nějaké
	 *         vyjímce a nepodařilo se přidat typ hry
	 */
	boolean addKindOfGame(final KindOfGame kindOfGame);

	/**
	 * Metoda, která získá hru z DB dle názvu / typu dané hry.
	 * 
	 * @param kind
	 *            - typ hry, který se získá z DB.
	 * 
	 * @return typ hry s daným názvem / typem nebo null, pokud se v DB takový typ
	 *         nenachází.
	 */
	KindOfGame getKindOfGame(final String kind);

	/**
	 * metoda, která upraví / aktualizuje typ hry v DB typu hry.
	 * 
	 * @param kindOfGame
	 *            - typ hry, který se má upravit v DB (dle ID)
	 * @return true, pokud se typ hry upraví v pořádku, jinak false
	 */
	boolean updateKindOfGame(final KindOfGame kindOfGame);
	
	/**
	 * Metoda, která získá z DB veškeré typy her. (Tj. typy her pro tu šibenici - na
	 * odpočitávání času, na měření apod.)
	 * 
	 * @return typy her v DB
	 */
	List<KindOfGame> getAllKindsOfGames();

	/**
	 * Metoda, která vymaže typ hry s daným názvem / typem.
	 * 
	 * @param kind
	 *            - typ hry, které se má z DB vymazat.
	 * 
	 * @return true, pokud se operace povede, jinak false.
	 */
	boolean deleteKindOfGame(final String kind);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Operace nad tabulkou KindsOfUser:	
	
	/**
	 * Metoda, která přidá do DB nový typ uživatele.
	 * 
	 * @param kindOfUser
	 *            - typ uživatele, který se přidá do DB
	 * 
	 * @return true, pokud se přidání povedlo v pohodě, false, pokud došlo k nějaké
	 *         vyjímce a nepodařilo se přidat typ uživatele
	 */
	boolean addKindOfUser(final KindOfUser kindOfUser);

	/**
	 * Metoda, která získá typ uživatele z DB dle názvu / typu daného uživatele.
	 *
	 * @param kind
	 *            - typ uživatele, který se získá z DB.
	 *
	 * @return typ uživatele s daným názvem / typem nebo null, pokud se v DB takový typ
	 *         nenachází.
	 */
	KindOfUser getKindOfUser(final String kind);

	/**
	 * metoda, která upraví / aktualizuje typ uživatele v DB dle typu uživatele.
	 * 
	 * @param kindOfUser
	 *            - typ uživatele, který se má upravit v DB (dle ID)
	 * @return true, pokud se typ uživatele aktualizuje v pořádku, jinak false
	 */
	boolean updateKindOfUser(final KindOfUser kindOfUser);
	
	/**
	 * Metoda, která získá z DB veškeré typy uživatele. (Veškeré typy, kterých může
	 * uživatel nabývat, tj. admin, hráč apod.)
	 * 
	 * @return list typů uživatelů v DB
	 */
	List<KindOfUser> getAllKindsOfUsers();

	/**
	 * Metoda, která vymaže typ uživatele s daným názvem / typem.
	 * 
	 * @param kind
	 *            - typ uživatele, které se má z DB vymazat.
	 * 
	 * @return true, pokud se operace povede, jinak false.
	 */
	boolean deleteKindOfUser(final String kind);
	
	/**
	 * Metoda, která vrátí všechny typy uživatelů, ale pouze toto (typy).
	 * 
	 * @return list typu String, který obsahuje pouze typy uživatelů, ne objekty,
	 *         kde je navíc i případný komentář.
	 */
	List<String> getAllKindOfUser();
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Operace nad tabulkou Groups:
	
	/**
	 * Metoda, která přidá do DB novou skupinu slov - Group.
	 * 
	 * @param group
	 *            - Skupina, která se přidá do DB
	 * 
	 * @return true, pokud se přidání povedlo v pohodě, false, pokud došlo k nějaké
	 *         vyjímce a nepodařilo se přidat skupinu
	 */
	boolean addGroup(final Group group);

	/**
	 * Metoda, která získá skupinu z DB dle Id dané skupiny
	 *
	 * @param groupId
	 *            - ID skupiny, který se získá z DB.
	 *
	 * @return skupinu s daným ID nebo null, pokud se v DB skupina
	 *         nenajde.
	 */
	Group getGroup(final int groupId);

	/**
	 * metoda, která upraví / aktualizuje skupinu v DB dle názvu skupiny..
	 * 
	 * @param group
	 *            - skupina, která se má upravit v DB (dle názvu skupiny)
	 * @return true, pokud se skupina aktualizuje v pořádku, jinak false
	 */
	boolean updateGroup(final Group group);
	
	/**
	 * Metoda, která získá z DB veškeré skupiny slov.
	 * 
	 * @return list skupin slov v DB
	 */
	List<Group> getAllGroups();

	/**
	 * Metoda, která vymaže skupinu s daným ID.
	 * 
	 * @param groupId
	 *            - ID skupiny, která se má z DB vymazat.
	 * 
	 * @return true, pokud se operace povede, jinak false.
	 */
	boolean deleteGroup(final int groupId);
	
	
	/**
	 * Metoda, která vymaže veškeré skupiny, tj. vymaže veškerá data v tabulce
	 * Groups.
	 * 
	 * @return true, pokud se vymažou všechna data v tabulce Groups, jinak false,
	 *         pokud dojde k nějaké chybě.
	 */
	boolean deleteAllGroups();
	
	
	
	
		
	
	
	
	
	
	
	
	// Operace nad tabulkou Words:
	
	
	/**
	 * Metoda, která přidá do DB nové slovo pro hádání v Šibenici.
	 * 
	 * @param word
	 *            - slovo, které se přidá do DB
	 * 
	 * @return true, pokud se přidání povedlo v pohodě, false, pokud došlo k nějaké
	 *         vyjímce a nepodařilo se přidat nové slovo do DB.
	 */
	boolean addWord(final Word word);

	/**
	 * Metoda, která získá slovo z DB dle jeho názvu (název slova)
	 * 
	 * @param word
	 *            - slovo, které se získá z DB.
	 * 
	 * @return hledané slovo nebo null, pokud se v DB takové slovo nenachází
	 */
	Word getWord(final String word);

	/**
	 * metoda, která upraví / aktualizuje slovo v DB dle daného slova.
	 * 
	 * @param word
	 *            - slovo, které se má upravit v DB
	 * @return true, pokud se dané slovo upraví v pořádku, jinak false
	 */
	boolean updateWord(final Word word);
	
	/**
	 * Metoda, která získá z DB veškerá slova.
	 * 
	 * @return list slov v DB.
	 */
	List<Word> getAllWords();

	/**
	 * Metoda, která vymaže nějaké slovo k hádání z DB..
	 * 
	 * @param word
	 *            - slovo, které se má z DB vymazat.
	 * 
	 * @return true, pokud se operace povede, jinak false.
	 */
	boolean deleteWord(final String word);
	
	/**
	 * Metoda, která z DB získá všechna slova, která patří k nějaké skupině
	 * (groupName).
	 * 
	 * @param groupId
	 *            - ID skupiny, ke které patří nějaká slova a tato slova z této
	 *            skupiny se mají vrátit z DB
	 * @return list slov z DB, které patří do skupiny slov (groupname)
	 */
	List<Word> getWordsByGroupId(final int groupId);
	
	/**
	 * Metoda, která vymaže veškerá slovíčka v tabulce Words, resp. vymaže veškerá
	 * data v tabulce Words.
	 * 
	 * @return true, pokud se úspěšně vymažou veškeré záznamy z tabuluky Words,
	 *         jinak false, pokud dojde k nějaké chybě.
	 */
	boolean deleteAllWords();
	
	/**
	 * Metoda, která vymaže všechna slovíčka, která mají groupID groupId v parametru
	 * této metody.
	 * 
	 * @param groupId
	 *            - ID skupiny. Značí, jaká slovíčka se mají smazat (z dané
	 *            skupiny).
	 * 
	 * @return true, pokud se všechna slovíčka s daným groupId vymažou v pořádku,
	 *         jinak false.
	 */
	boolean deleteWordsByGroupId(final int groupId);
}