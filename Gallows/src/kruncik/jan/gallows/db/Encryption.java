package kruncik.jan.gallows.db;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Tato třída slouží pro šifrování.
 * 
 * Jsou zde pouze dvě metody, jedna pro zašifrování zadaného textu (metoda
 * encrypt) a druhá pro rozšifrování zadaného textu coby zašifrovaného hesla
 * (metoda decrypt).
 * 
 * Info: 
 * Nikam není třeba nic ukládat, při zašifrování se potrebné IV, SALT a to
 * heslo vloží do jednoho zašifrovaného textu a při rozšifrování se tyto data z
 * toho zašifrovaného textu zase vezmou.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

class Encryption {

	
	/**
	 * Konstanta, která slouží jako heslo pro šifrování a dešofrování.
	 */
	private static final String PASSWORD = "Gallows password 123!";
	
	
	
	
	
	/**
	 * Metoda, která zašifruje zadaný text v parametru metody (word). Vytvoří se
	 * "šifra", která obsahuje zašifrované slovo word, IV A SALT pro dešifrování.
	 * 
	 * @param word
	 *            - Slovo, které se ma zašifrovat.
	 * @return zašifrované heslo word a potřebné údaje k jeho rozšifrování v jednom. 
	 */
	static String encrypt(final String word) {
		try {
			/* you can give whatever you want for password. This is for testing purpose */
			final SecureRandom random = new SecureRandom();
			final byte bytes[] = new byte[20];
			random.nextBytes(bytes);
			byte[] saltBytes = bytes;

			
			// Derive the key
			final SecretKeySpec secret = getSecrecKeySpec(saltBytes);

			
			// encrypting the word
			final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			final AlgorithmParameters params = cipher.getParameters();
			final byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
			final byte[] encryptedTextBytes = cipher.doFinal(word.getBytes("UTF-8"));

			
			// prepend salt and vi
			byte[] buffer = new byte[saltBytes.length + ivBytes.length + encryptedTextBytes.length];
			
			System.arraycopy(saltBytes, 0, buffer, 0, saltBytes.length);
			System.arraycopy(ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
			System.arraycopy(encryptedTextBytes, 0, buffer, saltBytes.length + ivBytes.length,
					encryptedTextBytes.length);

			
			// return new Base64().encodeToString(buffer);
			return Base64.getEncoder().encodeToString(buffer);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidParameterSpecException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// v případě, že dojde k nějaké chybě se vrátí původní text:
		return word;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která dešifruje zašifrovaný text (encryptedText).
	 * 
	 * Metoda si ze zadaného textu (encryptedText) vezme IV, SALT a zašifrovaný
	 * text, který pomocí získaných dat (mimo jiné) rozšifruje a vrátí původní text.
	 * 
	 * @param encryptedText
	 *            - zašifrovaný text s SALT a IV
	 * @return rozšifrovaný text.
	 */
	static String decrypt(final String encryptedText) {
		try {
			final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

			// strip off the salt and iv
			final ByteBuffer buffer = ByteBuffer.wrap(Base64.getDecoder().decode(encryptedText));

			
			final byte[] saltBytes = new byte[20];
			buffer.get(saltBytes, 0, saltBytes.length);

			final byte[] ivBytes1 = new byte[cipher.getBlockSize()];
			buffer.get(ivBytes1, 0, ivBytes1.length);

			final byte[] encryptedTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes1.length];

			buffer.get(encryptedTextBytes);

			
			// Deriving the key
			final SecretKeySpec secret = getSecrecKeySpec(saltBytes);

			cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes1));

			
			byte[] decryptedTextBytes = null;
			try {
				decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			} catch (BadPaddingException e) {
				e.printStackTrace();
			}

			return new String(decryptedTextBytes);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException e1) {
			e1.printStackTrace();
		}

		// V případě, že dojde k nějaké chybě vrátím původní text, resp. zašifrovaný text:
		return encryptedText;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která si vezme defaultní zakódování a vytvoří se klíč pro AES.
	 * 
	 * @param saltBytes
	 *            - pole s náhodnými bity, které slouží jako doplňující vstup. Je to
	 *            z toho důvodu, aby výstup měl více možných variant
	 * @return vytvořený klíč pro AES (vezme si defaultní zakódování a vytvoří se
	 *         klíč pro AES -> ten se vrátí)
	 */
	private static SecretKeySpec getSecrecKeySpec(final byte[] saltBytes) {
		try {
			/*
			 * bude sloužit jako továrna na vytvoření klíče. Parametrem je název algoritmu
			 * kterým se to bude šifrovat.
			 */
			final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");

			/*
			 * vytvoří nám jakýsi předpis pro náš klíč, specifikaci, kde zkombinujeme námi
			 * zadaný klíč, salt, počet proměn a finální délku
			 */
			final PBEKeySpec spec = new PBEKeySpec(PASSWORD.toCharArray(), saltBytes, 65556, 256);

			/*
			 * pomocí továrny a specifikace klíče se nám vytvoří klíč pomocí defaultních
			 * algoritmů
			 */
			final SecretKey secretKey = factory.generateSecret(spec);

			/*
			 * vezme se defaultní zakódování a vytvoří se klíč pro AES
			 */
			return new SecretKeySpec(secretKey.getEncoded(), "AES");
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}

		return null;
	}
}
