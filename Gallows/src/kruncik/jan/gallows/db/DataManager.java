package kruncik.jan.gallows.db;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.ImageIcon;

import kruncik.jan.gallows.db_objects.City;
import kruncik.jan.gallows.db_objects.Game;
import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.db_objects.KindOfUser;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.db_objects.Word;
import kruncik.jan.gallows.dialogs.dataForDialogs.MyFileChooser;

/**
 * Tato třída obsahuje implementované metody pro operace s DB, viz rozhraní
 * DataManagerImpl.
 * 
 * Note: 
 * Původně se neměl měnio měnit psč u měst, ale na konec jsem si to rozmyslel.
 * 
 * updateKindOfGame - Zde jde upravit pouze komentář, typ hry je pevně dán v
 * aplikaci, původně ani neměl být v DB, jen aby se to dalo dát na výběr pak v
 * aplikaci a aby se s tím lépe pracovalo. Kdyby se to dalo měnit bylo by to k
 * ničemu, aplikace by s tím neuměla pracovat, proto je možné měnit pouze
 * komentář u typu hry.
 * 
 * updateKindOfUser - Zde se jedná o stejný důvod jako výše, také lze upravit
 * pouze komentář, ale přidávání dalších typů nemá smysl. Aplikace umí pracovat
 * puze s těma dvěma.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class DataManager implements DataManagerImpl {
	
	// Údaje k DB:
	private static final String DRIVER_JDBC = "jdbc:derby:";
	private static String URL = DRIVER_JDBC + "DB";
	private static final String USER = "Gallows";
	private static final String PASSWORD = "Gallows_123";
	
	
	/**
	 * Proměnná, která "obsahuje" připojení k DB
	 */
	private static Connection connection;
	
	
	
	
	// Sql příkazy pro manipulaci s DB:
	
	// Sql pro tabulku Users
	private static final String SQL_GET_ALL_USERS = "select * from Users";
	private static final String SQL_UPDATE_USER = "update Users set Street = ?, Ban = ?, Email = ?, FirstName = ?, KindOfUser = ?, LastName = ?, Password = ?, Pseudonym = ?, TelephoneNumber = ?, Deleted = ?";
	private static final String SQL_GET_USER_BY_ID = "select * from Users where ID = ";
	private static final String SQL_DELETE_USER_BY_ID = "delete from Users where id = ";
	private static final String SQL_RESTORE_USER = "update Users set Deleted = ? where ID = ?";	
	private static final String SQL_DELETE_BLOCK_USER = "update Users set Deleted = '1' where ID = ?";
	
	// Sql pro tabulku Cities
	private static final String SQL_AD_CITY = "insert into Cities (PSC, Name) values (?, ?)";
	private static final String SQL_GET_CITY_BY_PSC = "select * from Cities where PSC = ";
	private static final String SQL_UPDATE_CITY_BY_PSC = "update Cities set Name = ? where PSC = ?";
	private static final String SQL_GET_ALL_CITIES = "select * from Cities";
	private static final String SQL_DELETE_CITY_BY_PSC = "delete from Cities where PSC = "; 
	private static final String SQL_GET_ALL_PSC = "select PSC from Cities";
	private static final String SQL_DELETE_ALL_CITIES = "delete from Cities";
	
	
	// Sql pro tabulku Games:
	
	// Tento Sql příkaz slouží pro přidání nové odehrané hry typu Measurement, není
	// potřeba vložit nastavený čas pro časovač,
	private static final String SQL_ADD_GAME_MEASUREMENT = "insert into Games (Comment, Date, HH, KindOfGame, MM, Score, SS, UserID, GroupID, Lives) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	// Sql příkaz pro vložení nové odehrané hry typu Countdown, tam je potřeba navíc
	// vložit originální čas - resp. čas hry nastavený uživatelem.
	private static final String SQL_ADD_GAME_COUNTDOWN = "insert into Games (Comment, Date, HH, KindOfGame, MM, Score, SS, UserID, GroupID, Lives, Original_HH, Original_MM, Original_SS) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_GET_GAME_BY_ID = "select * from Games where id = ";
	private static final String SQL_GET_ALL_GAMES = "select * from Games";
	private static final String SQL_DELETE_GAME_BY_ID = "delete from Games where id = ";
	private static final String SQL_UPDATE_COMMENT_IN_GAME_BY_ID = "update Games set Comment = ? where ID = ?";
	private static final String SQL_DELETE_GAME_BY_KIND_OF_GAME = "delete from Games where KindOfGame = '";
	private static final String SQL_DELETE_ALL_GAMES = "delete from Games";
	private static final String SQL_DELETE_GAME_BY_USER_ID = "delete from Games where UserID = ";
	
	// Sql pro tabulku KindsOfGames:
	private static final String SQL_ADD_KINDOFGAME = "insert into KindsOfGames (Kind, Comment) values (?, ?)";
	private static final String SQL_GET_KINDOFGAME_BY_KIND = "select * from KindsOfGames where Kind = '";
	private static final String SQL_UPDATE_KINDSOFGAMES = "update KindsOfGames set Comment = ? where Kind = ?";
	private static final String SQL_GET_ALL_KINDSOFGAMES = "select * from KindsOfGames";
	private static final String SQL_DELETE_KINDOFGAME_BY_KIND = "delete from KindsOfGames where Kind = '";
	private static final String SQL_UPDATE_KIND_OF_GAME_ALL_ROW = "update Games set KindOfGame = ? where KindOfGame = ?";
	
	// Sql pro tabulku KindsOfUsers:
	private static final String SQL_ADD_KINDOFUSER = "insert into KindsOfUsers (Kind, Comment) values (?, ?)";
	private static final String SQL_GET_KINDOFUSER_BY_KIND = "select * from KindsOfUsers where Kind = '";
	private static final String SQL_UPDATE_KIND_OF_USER = "update KindsOfUsers set Comment = ? where Kind = ?";
	private static final String SQL_UPDATE_USERS_BY_NEW_KIND_OF_USER = "update Users set KindOfUser = ? where KindOfUser = ?";
	private static final String SQL_GET_ALL_KINDSOFUSERS = "select * from KindsOfUsers";
	private static final String SQL_DELETE_KINDOFUSER_BY_KIND = "delete from KindsOfUsers where kind = '";
	private static final String SQL_GET_ALL_KINDS_OF_USER_ONLY_KINDS = "select Kind from KindsOfUsers";

	
	// Sql pro tabulku Groups:
	private static final String SQL_ADD_GROUP = "insert into Groups (GroupName, Comment) values (?, ?)";
	private static final String SQL_GET_GROUP_BY_GROUPID = "select * from Groups where ID = ";
	private static final String SQL_UPDATE_GROUP_BY_ID = "update Groups set GroupName = ?, Comment = ? where ID = ?";
	private static final String SQL_GET_ALL_GROUPS = "select * from Groups";
	private static final String SQL_DELETE_GROUP_BY_GROUPID = "delete from Groups where ID = ";
	private static final String SQL_DELETE_ALL_GROUPS = "delete from Groups";
	
	// Sql pro tabulku Words:
	private static final String SQL_ADD_WORD = "insert into Words (Comment, Word, GroupID) values (?, ?, ?)";
	private static final String SQL_UPDATE_WORD_INCLUDE_WORD = "update Words set Comment = ?, Word = ?, GroupID = ? where Word = ?";
	private static final String SQL_UPDATE_WORD_WITHOUT_WORD = "update Words set Comment = ?,  GroupID = ? where Word = ?";
	private static final String SQL_GET_WORD_BY_WORD = "select * from Words where Word = '";
	private static final String SQL_GET_ALL_WORDS = "select * from Words";
	private static final String SQL_DELETE_WORD_BY_WORD = "delete from Words where Word = '";
	private static final String SQL_GET_WORDS_BY_GROUP_ID = "select * from Words where GroupID = ";
	private static final String SQL_DELETE_ALL_WORDS = "delete from Words";
	private static final String SQL_DELETE_WORDS_BY_GROUP_ID = "delete from Words where GroupID = ";	
	
	
	
	
	
	
	@Override
	public void setDbPath() {
		final String path = new MyFileChooser().getPathToDbDir();
		
		if (path != null)
			URL = DRIVER_JDBC + path;	
	}
	
	
	
	
	
	/**
	 * Metoda, která vytvoří spojení s DB.
	 * "Připojí se k DB"
	 */
	private static void createConnection() {
		try{
			// Pokud již spojení existuje, není třeba jej znovu vytvářet:
//			if (connection == null)
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Metoda, která ukončí spojenís DB.
	 * "Odpojení od DB"
	 */
	private static void closeConnection() {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vykoná nějaký sql příkaz nad DB a vrátí
	 * výsledek ve formě proměnné typu ResultSet.
	 * 
	 * @return ResultSet, který obsahuje výsledek z provedení Sql 
	 * příkazu.
	 */
	private static ResultSet executeQuerySqlCommand(final String sqlCommand) {
		/*
		 * Pokud se z nějakého důvodu nevytvořilo připojení s DB, tak se musí
		 * skončit, nelze dále pokračovat:
		 */
		if (connection == null)
			return null;
		
		try {
			return connection.prepareStatement(sqlCommand).executeQuery();
			
//			PreparedStatement ps = connection.prepareStatement(sqlCommand);
			
//			return ps.executeQuery();				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vykoná příkaz, který nic nevrací, tj. volá se pro provedení
	 * příkazu metoda execute. Například pro smazání řádku z tabulky apod.
	 * 
	 * Na konec metoda vrátí logickou hodnotu dle úspěšného neboneúspěšného
	 * provedení operace.
	 * 
	 * @param sqlCommand
	 *            - Příkaz, který se má vykonat, například smazání řádku z tabulky
	 *            apod. Je to Sql příkaz, který z Db nic nevrací
	 *            
	 * @return true, pokud vše dopadne v pořádku, tj. příkaz se v pořádku vykoná,
	 *         jinak false, pokud dojde k nějaké chybě, například neexistuje spojení
	 *         s DB, nebo dojde k chybě při vykonávání příkazu apod.
	 */
	private static boolean executeSqlCommand(final String sqlCommand) {
		/*
		 * Pokud se z nějakého důvodu nevytvořilo připojení s DB, tak se musí skončit,
		 * nelze dále pokračovat:
		 */
		if (connection == null)
			return false;

		try {
//			return connection.prepareStatement(sqlCommand).execute();
			connection.prepareStatement(sqlCommand).execute();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která převede obrázek typu pole byte na obrázek typu ImageIcon.
	 * Pole byte je získáno z DB.
	 * 
	 * @param image - jednorozměrné pole typu byte (obrázek z DB)
	 * 
	 * @return obrázek typu ImageIcon načtený z DB nebo null, pokud se obrázek
	 * v DB nenachází.
	 */
	private static ImageIcon getBlobFromD(final byte[] image) {
		if (image != null) {
			final Image img = Toolkit.getDefaultToolkit().createImage(image);
			return new ImageIcon(img);	
		}
		// else - Obrázek je null - není v DB
		
		return null;
	}
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
		
	
	
	





	@Override
	public boolean addUser(final User user) {
		createConnection();
				
		try {
			/*
			 * Do této proměnné si vložím hodnotu true pokud uživatel načetl nějaký obrázek 
			 * a ten obrázek ještě existuje (nesmazal jej)
			 */
			final boolean insertPicture = user.getFileImage() != null && user.getFileImage().exists();
			
			/*
			 * Proměnná, kam vložím sql příkaz pro vložení uživatele do DB.
			 * 
			 * Potřebuji ale otestovat, zda se bude vkládat i s obrázkem nebo bez a podobně
			 * pro PSČ:
			 */
			String s1 = "insert into Users (Street, Ban, Email, FirstName, KindOfUser, LastName, Password, Pseudonym, TelephoneNumber, Deleted";
			String s2 = "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?";
			
			
			if (user.getPsc() > -1) {
				s1 += ", PSC";
				s2 += ", ?";
			}
			
			if (insertPicture) {
				s1 += ", Picture";
				s2 += ", ?";
			}				
			
			s1 += ") ";
			s2 += ")";
			
			
			final PreparedStatement ps = connection.prepareStatement(s1 + s2);
						
			
			ps.setString(1, user.getStreet());
			ps.setString(2, String.valueOf(user.getBan()));
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getFirstName());
			ps.setString(5, user.getKindOfUser());
			ps.setString(6, user.getLastName());
			// Heslo se zašifruje:
			ps.setString(7, Encryption.encrypt(user.getPassword()));
			ps.setString(8, user.getPseudonym());
			ps.setInt(9, user.getTelephoneNumber());
			ps.setString(10, String.valueOf(user.getDeleted()));
			
			if (user.getPsc() > -1)
				ps.setInt(11, user.getPsc());
			
			

			// Obrázek - Začátek
			FileInputStream fis = null;
			
			if (insertPicture) {
				fis = new FileInputStream(user.getFileImage());
				
				// Zde si musím pohlídat index, pokud se nemá PSČ přidat, pak je třeba nastavit
				// index 10, pokud se má přidat, pak je třeba nastavit index 11
				if (user.getPsc() > -1)
					ps.setBinaryStream(12, fis, (int) user.getFileImage().length());	// Má se přidat PSČ
				else ps.setBinaryStream(11, fis, (int) user.getFileImage().length()); 	// Nemá se přidat PSČ
			}
			// Obrázek - Konec	

			
			
			ps.execute();
			
			if (fis != null)
				fis.close();
		} catch (SQLException | IOException e1) {
			e1.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}		
		
		closeConnection();
		
		return true;
	}

	
	
	
	@Override
	public boolean updateUser(final User user) {
		createConnection();
		
		try {
			/*
			 * Do této proměnné si vložím hodnotu true pokud uživatel načetl nějaký obrázek 
			 * a ten obrázek ještě existuje (nesmazal jej)
			 */
			final boolean insertPicture = user.getFileImage() != null && user.getFileImage().exists();
			
			/*
			 * Proměnná, kam vložím sql příkaz pro vložení uživatele do DB.
			 * 
			 * Potřebuji ale otestovat, zda se bude vkládat i s obrázkem nebo bez a podobně
			 * pro PSČ:
			 */
			// update Users set street = 'edited 123' where id = 2;
			String s1 = SQL_UPDATE_USER;
			
			
			
			/*
			 * Pokud bude PSČ větší než -1, pak je zvoleno nějaké konkrétní číslo v DB, ale
			 * když bude -1, pak se nastavilo to, že si uživatel nezvolil zádné město, tj.
			 * nechtěl vyplnit tuto hodnotu, che ji nechat prázdnou, tak ji v DB musím
			 * nastavit na null
			 */
			if (user.getPsc() > -1)
				s1 += ", PSC = ?";
			else
				s1 += ", PSC = null"; 
			
			
			if (insertPicture) {
				s1 += ", Picture = ?";
			}				
			
			s1 += " where ID = " + user.getId();
			
			
			final PreparedStatement ps = connection.prepareStatement(s1);
						
			
			ps.setString(1, user.getStreet());
			ps.setString(2, String.valueOf(user.getBan()));
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getFirstName());
			ps.setString(5, user.getKindOfUser());
			ps.setString(6, user.getLastName());
			// Zašifruji heslo:
			ps.setString(7, Encryption.encrypt(user.getPassword()));
			ps.setString(8, user.getPseudonym());
			ps.setInt(9, user.getTelephoneNumber());
			ps.setString(10, String.valueOf(user.getDeleted()));
			
			
			/*
			 * Zde jen pokud se jedná o zvolené psč, tak se nastaví ta hodnota, jinak se
			 * výše nastavila null hodnota.
			 */
			if (user.getPsc() > -1)
				ps.setInt(11, user.getPsc());
			
			

			// Obrázek - Začátek
			FileInputStream fis = null;
			
			if (insertPicture) {
				fis = new FileInputStream(user.getFileImage());

				if (user.getPsc() > -1)
					ps.setBinaryStream(12, fis, (int) user.getFileImage().length());
				else
					ps.setBinaryStream(11, fis, (int) user.getFileImage().length()); 
			}
			// Obrázek - Konec	

			
			
			ps.execute();
			
			if (fis != null)
				fis.close();
		} catch (SQLException | IOException e1) {
			e1.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}		
		
		closeConnection();
		
		return true;
	}
	
	

	
	

	@Override
	public User getUser(final int id) {
		User user = null;
		
		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_USER_BY_ID + id);
		
		if (rs != null) {
			try {
				while (rs.next())
					user = new User(rs.getInt("ID"), rs.getInt("PSC"), rs.getInt("TelephoneNumber"),
							rs.getString("Ban").charAt(0), rs.getString("FirstName"), rs.getString("LastName"),
							rs.getString("Street"), rs.getString("Email"), rs.getString("KindOfUser"),
							Encryption.decrypt(rs.getString("Password")), rs.getString("Pseudonym"),
							getBlobFromD(rs.getBytes("Picture")), rs.getString("Deleted").charAt(0));					
				
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return user;
	}




	@Override
	public List<User> getAllUsers() {
		final List<User> usersList = new ArrayList<>();
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_USERS);
		
		/*
		 * Zde je třeba otestovat, zda se nevrátilo null, pokud ano, tak se buď 
		 * nevytvořilo spojení s DB, nebo se nepodařilo provést příkaz, případně
		 * obsahuje null apod.
		 */
		
		if (rs != null) {
			try {
				while (rs.next())
					usersList.add(new User(rs.getInt("ID"), rs.getInt("PSC"), rs.getInt("TelephoneNumber"),
							rs.getString("Ban").charAt(0), rs.getString("FirstName"), rs.getString("LastName"),
							rs.getString("Street"), rs.getString("Email"), rs.getString("KindOfUser"),
							Encryption.decrypt(rs.getString("Password")), rs.getString("Pseudonym"),
							getBlobFromD(rs.getBytes("Picture")), rs.getString("Deleted").charAt(0)));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		

		closeConnection();

		return usersList;
	}




	@Override
	public boolean deleteUserWithAllData(final int id) {

		try {
			createConnection();

			// Nejprve vymazu vsechny hry, kde je dané ID uživatele:
			final PreparedStatement ps = connection.prepareStatement(SQL_DELETE_GAME_BY_USER_ID + id);

			ps.execute();
			ps.close();

			// Nyní smažu daného uživatele:
			final boolean deleted = executeSqlCommand(SQL_DELETE_USER_BY_ID + id);

			closeConnection();

			return deleted;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	
	
	@Override
	public boolean deleteBlockUser(final int id) {
		createConnection();

		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_DELETE_BLOCK_USER);

			ps.setInt(1, id);

			ps.execute();

			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();

			return false;
		}

		closeConnection();

		return true;
	}
	
	
	

	@Override
	public User getUserByPseudonymAndPassword(final String pseudonym, final String password) {
		/*
		 * Jelikož se hesla šifrují a pří každém zašifrování hesla se vygeneruje unikátní znak, nelze jej porovnávat v DB,
		 * musím si daného uživatele načíst, rozšifrovat heslo a porovnat jej se zadaným heslem v parametru metody.
		 */

		try {
			return getAllUsers().stream()
					.filter(u -> u.getPseudonym().equals(pseudonym) && u.getPassword().equals(password)).findFirst()
					.get();
		} catch (NoSuchElementException e) {
			/**
			 * Tato vyjímka nastane v případě, že se nenajde žádný uživatel, který splňuje
			 * podmínky pseudonymu a hesla, například uživatel zadal špatné heslo apod. Pak
			 * musím vrátit null hodnotu:
			 */
			return null;
		}
	}
	
	
	
	
	
	
	@Override
	public User getUserByPseudonymEmailPassword(final String pseudonym,final String email, final String password) {
		try {
			return getAllUsers().stream().filter(u -> u.getPseudonym().equals(pseudonym) && u.getEmail().equals(email)
					&& u.getPassword().equals(password)).findFirst().get();
		} catch (NoSuchElementException e) {
			/*
			 * Tato vyjimka nastane v případě, že nebude žádný uživatel dle uvedených
			 * podmínek nalazen (nebude nalezen uživatel s danym uživatelským jmenem,
			 * emailem a heslem). Pak msím vrátit hdnotu null.
			 */
			return null;
		}
	}
	
	
	
	
	
	
	
	
	@Override
	public boolean restoreUser(final int userId) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_RESTORE_USER);

			ps.setString(1, "0");
			ps.setInt(2, userId);

			ps.execute();

			ps.close();
		} catch (SQLException e) {			
			e.printStackTrace();
			
			return false;
		}
		
		closeConnection();
		
		return true;
	}
	
	
	



	@Override
	public boolean addCity(final City city) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_AD_CITY);
			
			ps.setInt(1, city.getPsc());
			ps.setString(2, city.getName());			
			
			ps.execute();
			
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}				
		
		closeConnection();
		
		return true;
	}




	@Override
	public City getCity(final int psc) {
		City city = null;
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_CITY_BY_PSC + psc);
		
		if (rs != null) {
			try {
				while (rs.next())
					city = new City(rs.getInt("PSC"), rs.getString("Name"));
				
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return city;
	}


	


	@Override
	public boolean updateCity(final City city) {
		
		try {
			/*
			 * PSČ se bude měnit tak, že se vytvoří nové, pak se přepíšou všechna psč
			 * uživatelů na to nové (tam kde bylo původní) a pak se to původní psč smaže.
			 * 
			 * Info: 
			 * Původně jsem to měl tak, že se nemělo PSČ, ale pak jsem se rozhodl to
			 * povolit.
			 */
			if (city.getNewPsc() > -1) {
				// Připravím si výraz pro aktualizuaci PSČ
				final boolean created = addCity(new City(city.getNewPsc(), city.getName()));
				
				if (created) {
					// Zde projdu všechny uživatele a aktualizuji jim PSČ:
					getAllUsers().stream().filter(u -> u.getPsc() == city.getPsc()).forEach(u -> {
						// Přepíšu uživateům psč a aktualizuji je v DB:
						u.setPsc(city.getNewPsc());
						updateUser(u);
					});
					
					createConnection();
					
					// Nyní mohu smazat původní PSČ:
					executeSqlCommand(SQL_DELETE_CITY_BY_PSC + city.getPsc());
					
					closeConnection();
				}
			}

			else {
				createConnection();
				
				// Zde se pouze přepíše město u daného PSČ:
				final PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_CITY_BY_PSC);

				ps.setString(1, city.getName());
				ps.setInt(2, city.getPsc());

				ps.execute();

				ps.close();
				
				closeConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}				
		
		return true;
	}
	
	


	@Override
	public List<City> getAllCities() {
		final List<City> citiesList = new ArrayList<>();
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_CITIES);
		
		if (rs != null) {
			try {
				while (rs.next())
					citiesList.add(new City(rs.getInt("PSC"), rs.getString("Name")));
				
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return citiesList;
	}




	@Override
	public boolean deleteCity(final int psc) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_CITY_BY_PSC + psc);
		
		closeConnection();
		
		return deleted;
	}

	
	
	@Override
	public List<Integer> getAllPsc() {
		final List<Integer> pscList = new ArrayList<Integer>();
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_PSC);
		
		if (rs != null) {
			try {
				while (rs.next())
					pscList.add(rs.getInt("PSC"));
				
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return pscList;
	}
	
	
	
	@Override
	public boolean deleteAllCities() {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_ALL_CITIES);
		
		closeConnection();
		
		return deleted;
	}
	
	
	
	
	
	
	
	
	
	



	@Override
	public boolean addGame(final Game game, final boolean withOriginalTime) {
		createConnection();
		
		try {
			final PreparedStatement ps;
			
			if (withOriginalTime)
				ps = connection.prepareStatement(SQL_ADD_GAME_COUNTDOWN);
			else
				ps = connection.prepareStatement(SQL_ADD_GAME_MEASUREMENT);

			ps.setString(1, game.getComment());
			ps.setDate(2, game.getDate());
			ps.setInt(3, game.getHh());
			ps.setString(4, game.getKindOfGame());
			ps.setInt(5, game.getMm());
			ps.setInt(6, game.getScore());
			ps.setInt(7, game.getSs());
			ps.setInt(8, game.getUserId());
			ps.setInt(9, game.getGroupId());
			ps.setInt(10, game.getLives());

			// Vložím i výchozí hodnoty pro časovaš, kdyby se mohla hra obnovit
			if (withOriginalTime) {
				ps.setInt(11, game.getOriginal_hh());
				ps.setInt(12, game.getOriginal_mm());
				ps.setInt(13, game.getOriginal_ss());
			}
			
			ps.execute();

			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}
		
		closeConnection();
		
		return true;
	}




	@Override
	public Game getGame(final int id) {
		Game game = null;
		
		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_GAME_BY_ID + id);		

		if (rs != null) {
			try {
				while (rs.next())
					game = new Game(rs.getInt("ID"), rs.getDate("Date"), rs.getString("Comment"),
							rs.getString("KindOfGame"), rs.getInt("HH"), rs.getInt("MM"), rs.getInt("SS"),
							rs.getInt("Score"), rs.getInt("UserID"), rs.getInt("GroupID"), rs.getInt("Original_HH"),
							rs.getInt("Original_MM"), rs.getInt("Original_SS"), rs.getInt("Lives"));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return game;
	}

	
	
	
	@Override
	public List<Game> getAllGamesByUserId(final int id) {
		final List<Game> gamesList = new ArrayList<Game>();
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand("select * from Games where UserID = " + id);		

		if (rs != null) {
			try {
				while (rs.next())
					gamesList.add(new Game(rs.getInt("ID"), rs.getDate("Date"), rs.getString("Comment"),
							rs.getString("KindOfGame"), rs.getInt("HH"), rs.getInt("MM"), rs.getInt("SS"),
							rs.getInt("Score"), rs.getInt("UserID"), rs.getInt("GroupID"), rs.getInt("Original_HH"),
							rs.getInt("Original_MM"), rs.getInt("Original_SS"), rs.getInt("Lives")));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return gamesList;
	}
	
	
	
	
	
	@Override
	public boolean updateGame(final Game game, final boolean withOriginalTime) {
		createConnection();
		
		try {
			final PreparedStatement ps;
			
			if (withOriginalTime)
				ps = connection.prepareStatement(
						"update Games set Comment = ?, Date = ?, HH = ?, KindOfGame = ?, MM = ?, Score = ?, SS = ?, UserID = ?, GroupID = ?, Lives = ?, Original_HH = ?, Original_MM = ?, Original_SS = ? where ID = ?");
			else
				ps = connection.prepareStatement(
						"update Games set Comment = ?, Date = ?, HH = ?, KindOfGame = ?, MM = ?, Score = ?, SS = ?, UserID = ?, GroupID = ?, Lives = ? where ID = ?");

			ps.setString(1, game.getComment());
			ps.setDate(2, game.getDate());
			ps.setInt(3, game.getHh());
			ps.setString(4, game.getKindOfGame());
			ps.setInt(5, game.getMm());
			ps.setInt(6, game.getScore());
			ps.setInt(7, game.getSs());
			ps.setInt(8, game.getUserId());
			ps.setInt(9, game.getGroupId());
			ps.setInt(10, game.getLives());

			// Vložím i výchozí hodnoty pro časovaš, kdyby se mohla hra obnovit
			if (withOriginalTime) {
				ps.setInt(11, game.getOriginal_hh());
				ps.setInt(12, game.getOriginal_mm());
				ps.setInt(13, game.getOriginal_ss());
				ps.setInt(14, game.getId());
			}
			else ps.setInt(11, game.getId()); 
			
			ps.execute();

			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}
		
		closeConnection();
		
		return true;
	}
	



	@Override
	public List<Game> getAllGames() {
		final List<Game> gamesList = new ArrayList<>();
		
		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_GAMES);

		if (rs != null) {
			try {
				while (rs.next())
					gamesList.add(new Game(rs.getInt("ID"), rs.getDate("Date"), rs.getString("Comment"),
							rs.getString("KindOfGame"), rs.getInt("HH"), rs.getInt("MM"), rs.getInt("SS"),
							rs.getInt("Score"), rs.getInt("UserID"), rs.getInt("GroupID"), rs.getInt("Original_HH"),
							rs.getInt("Original_MM"), rs.getInt("Original_SS"), rs.getInt("Lives")));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return gamesList;
	}




	@Override
	public boolean deleteGame(final int id) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_GAME_BY_ID + id);
		
		closeConnection();
		
		return deleted;
	}

	
	
	
	@Override
	public void updateCommentInGame(final int id, final String comment) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_COMMENT_IN_GAME_BY_ID);

			ps.setString(1, comment);
			ps.setInt(2, id);			

			ps.execute();

			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}

		closeConnection();

    }
	
	
	
	
	
	@Override
	public boolean deleteGamesByKindOfGame(final String kindOfGame) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_GAME_BY_KIND_OF_GAME + kindOfGame + "'");
		
		closeConnection();
		
		return deleted;
	}
	
	
	@Override
	public boolean deleteAllGames() {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_ALL_GAMES);
		
		closeConnection();
		
		return deleted;
	}
	
	
	
	
	
	
	



	@Override
	public boolean addKindOfGame(final KindOfGame kindOfGame) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_ADD_KINDOFGAME);
			
			ps.setString(1, kindOfGame.getKind());
			ps.setString(2, kindOfGame.getComment());
			
			ps.execute();
			
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}
		
		closeConnection();
		
		return true;
	}




	@Override
	public KindOfGame getKindOfGame(final String kind) {
		KindOfGame kindOfGame = null;

		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_KINDOFGAME_BY_KIND + kind + "'");

		if (rs != null) {
			try {
				while (rs.next())
					kindOfGame = new KindOfGame(rs.getString("Kind"), rs.getString("Comment"));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return kindOfGame;
	}

	
	
	
	@Override
	public boolean updateKindOfGame(final KindOfGame kindOfGame) {
		try {
			if (kindOfGame.getNewKind() != null) {
				/*
				 * Zde se má aktualizovat i typ hry, tak nejprve vytvořím nový typ hry, který je v proměnné newKind,
				 * pak všechny hry, kde se nachází původní typ hry (kind) přepíšu na newkind a pak tu původní
				 * položku v DB (kind) smažu.
				 */				
				addKindOfGame(new KindOfGame(kindOfGame.getNewKind(), kindOfGame.getComment()));
				
				// Aktualizuje všechny hry na nový typ hry:
				createConnection();
				final PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_KIND_OF_GAME_ALL_ROW);
				
				ps.setString(1, kindOfGame.getNewKind());
				ps.setString(2, kindOfGame.getKind());
				
				ps.execute();
				ps.close();								
				closeConnection();
				
				// Smažu ten původní typ, který se výše přepsal:
				deleteKindOfGame(kindOfGame.getKind());				
			}
			
			else {
				// Zde se nemusí přepsat i samotný typ hry, pouze komentář:
				createConnection();
				
				final PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_KINDSOFGAMES);
				
				ps.setString(1, kindOfGame.getComment());
				ps.setString(2, kindOfGame.getKind());
				
				ps.execute();
				
				ps.close();
				
				closeConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}
		
		return true;
	}
	
	



	@Override
	public List<KindOfGame> getAllKindsOfGames() {
		final List<KindOfGame> kindsOfGamesList = new ArrayList<KindOfGame>();

		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_KINDSOFGAMES);

		if (rs != null) {
			try {
				while (rs.next())
					kindsOfGamesList.add(new KindOfGame(rs.getString("Kind"), rs.getString("Comment")));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return kindsOfGamesList;
	}




	@Override
	public boolean deleteKindOfGame(final String kind) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_KINDOFGAME_BY_KIND + kind + "'");
		
		closeConnection();
		
		return deleted;
	}




	@Override
	public boolean addKindOfUser(final KindOfUser kindOfUser) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_ADD_KINDOFUSER);
			
			ps.setString(1, kindOfUser.getKind());
			ps.setString(2, kindOfUser.getComment());
			
			ps.execute();
			
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}
		
		closeConnection();
		
		return true;
	}




	@Override
	public KindOfUser getKindOfUser(final String kind) {
		KindOfUser kindOfUser = null;

		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_KINDOFUSER_BY_KIND + kind + "'");

		if (rs != null) {
			try {
				while (rs.next())
					kindOfUser = new KindOfUser(rs.getString("Kind"), rs.getString("Comment"));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return kindOfUser;
	}



	

	@Override
	public boolean updateKindOfUser(final KindOfUser kindOfUser) {
		try {
			/*
			 * Nejprve otestuji, zda se má přepsat i typ uživatele nebo ne. Pokud bude proměnná newKind naplněna, pak
			 * se musí přepsat i typ uživatele, což se provede tak, že vytvořím nový typ, uživatele, projde všechny uživatele, kteří
			 * mají ten původní typ (kind) a přepíšu jej na novy (newKind) a pak ten původní typ uživatele (kind) smažu z DB.
			 */
			if (kindOfUser.getNewKind() != null) {
				// Vytvořím nový typ:
				addKindOfUser(new KindOfUser(kindOfUser.getNewKind(), kindOfUser.getComment()));
				
				// Přepíšu ten původní na nový:
				createConnection();
				final PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_USERS_BY_NEW_KIND_OF_USER);
		
				ps.setString(1, kindOfUser.getNewKind());
				ps.setString(2, kindOfUser.getKind());
				
				ps.execute();				
				ps.close();				
				closeConnection();
				
				// Smažu původní:
				deleteKindOfUser(kindOfUser.getKind());
			}
			
			else {
				// Zde se má přepsat puze koemntář:
				createConnection();
				
				final PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_KIND_OF_USER);
				
				ps.setString(1, kindOfUser.getComment());
				ps.setString(2, kindOfUser.getKind());
				
				ps.execute();
				
				ps.close();
				
				closeConnection();
			}
			

		} catch (SQLException e) {
			e.printStackTrace();
			
			// Zde došlo k nějaké chybě, tak vrátím false, nepovedlo se přidat uživatele.
			return false;
		}
		
		return true;
	}
	
	
	
	

	@Override
	public List<KindOfUser> getAllKindsOfUsers() {
		final List<KindOfUser> kindsOfUsersList = new ArrayList<>();

		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_KINDSOFUSERS);

		if (rs != null) {
			try {
				while (rs.next())
					kindsOfUsersList.add(new KindOfUser(rs.getString("Kind"), rs.getString("Comment")));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return kindsOfUsersList;
	}




	@Override
	public boolean deleteKindOfUser(final String kind) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_KINDOFUSER_BY_KIND + kind + "'");
		
		closeConnection();
		
		return deleted;
	}
	
	
	
	
	@Override
	public List<String> getAllKindOfUser() {
		final List<String> kindOfUsersList = new ArrayList<>();

		createConnection();

		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_KINDS_OF_USER_ONLY_KINDS);

		if (rs != null) {
			try {
				while (rs.next())
					kindOfUsersList.add(rs.getString("Kind"));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

		return kindOfUsersList;
	}




	@Override
	public boolean addGroup(final Group group) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_ADD_GROUP);
			
			ps.setString(1, group.getGroupName());
			ps.setString(2, group.getComment());
			
			ps.execute();
			
			ps.close();
		} catch (SQLException e) {			
			e.printStackTrace();
			return false;
		}
		
		closeConnection();
		
		return true;
	}




	@Override
	public Group getGroup(final int groupId) {
		Group group = null;
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_GROUP_BY_GROUPID + groupId);
		
		if (rs != null) {
			try {
				while (rs.next())
					group = new Group(rs.getInt("ID"), rs.getString("GroupName"), rs.getString("Comment"));
				
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return group;
	}

	
	
	
	

	@Override
	public boolean updateGroup(final Group group) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_GROUP_BY_ID);
			
			
			ps.setString(1, group.getGroupName());
			ps.setString(2, group.getComment());
			ps.setInt(3, group.getId());
			
			ps.execute();
			
			ps.close();
		} catch (SQLException e) {			
			e.printStackTrace();
			return false;
		}
		
		closeConnection();
		
		return true;
	}
	
	
	



	@Override
	public List<Group> getAllGroups() {
		final List<Group> groupsList = new ArrayList<>();
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_GROUPS);
		
		if (rs != null) {
			try {
				while (rs.next())
					groupsList.add(new Group(rs.getInt("ID"), rs.getString("GroupName"), rs.getString("Comment")));
				
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return groupsList;
	}




	@Override
	public boolean deleteGroup(final int groupId) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_GROUP_BY_GROUPID + groupId);
		
		closeConnection();
		
		return deleted;
	}

	
	
	
	@Override
	public boolean deleteAllGroups() {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_ALL_GROUPS);
		
		closeConnection();
		
		return deleted;
	}
	
	
	
	





	@Override
	public boolean addWord(final Word word) {
		createConnection();
		
		try {
			final PreparedStatement ps = connection.prepareStatement(SQL_ADD_WORD);
			
			ps.setString(1, word.getComment());
			ps.setString(2, word.getWord());
			ps.setInt(3, word.getGroupId());
			
			ps.execute();
			
			ps.close();
		} catch (SQLException e) {			
			e.printStackTrace();
			return false;
		}
		
		closeConnection();
		
		return true;
	}




	@Override
	public Word getWord(final String word) {
		Word w_word = null;
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_WORD_BY_WORD + word + "'");
		
		if (rs != null) {
			try {
				while (rs.next())
					w_word = new Word(rs.getString("Comment"), rs.getString("Word"), rs.getInt("GroupID"));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return w_word;
	}


	
	
	
	@Override
	public boolean updateWord(final Word word) {
		createConnection();
		
		try {
			/*
			 * Logická proměnná, která bude true v případě, že se má aktualizovat slovo
			 * samotné.
			 */
			final boolean updateWord = word.getNewWord() != null;
			
			
			final PreparedStatement ps;

			if (updateWord)
				ps = connection.prepareStatement(SQL_UPDATE_WORD_INCLUDE_WORD);
			else
				ps = connection.prepareStatement(SQL_UPDATE_WORD_WITHOUT_WORD);

			ps.setString(1, word.getComment());

			
			
			if (updateWord) {
				ps.setString(2, word.getNewWord());
				ps.setInt(3, word.getGroupId());
				ps.setString(4, word.getWord());
			}

			else {
				ps.setInt(2, word.getGroupId());
				ps.setString(3, word.getWord());
			}
			
			ps.execute();
			
			ps.close();
		} catch (SQLException e) {			
			e.printStackTrace();
			return false;
		}
		
		closeConnection();
		
		return true;
	}
	
	
	
	


	@Override
	public List<Word> getAllWords() {
		final List<Word> wordsList = new ArrayList<>();
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_ALL_WORDS);
		
		if (rs != null) {
			try {
				while (rs.next())
					wordsList.add(new Word(rs.getString("Comment"), rs.getString("Word"), rs.getInt("GroupID")));

				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return wordsList;
	}




	@Override
	public boolean deleteWord(final String word) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_WORD_BY_WORD + word + "'");
		
		closeConnection();
		
		return deleted;
	}




	@Override
	public List<Word> getWordsByGroupId(final int groupId) {
		final List<Word> wordsList = new ArrayList<>();
		
		createConnection();
		
		final ResultSet rs = executeQuerySqlCommand(SQL_GET_WORDS_BY_GROUP_ID + groupId);
		
		if (rs != null) {
			try {
				while (rs.next())
					wordsList.add(new Word(rs.getString("Comment"), rs.getString("Word"), rs.getInt("GroupID")));
				
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		closeConnection();
		
		return wordsList;
	}
	
	
	
	
	
	@Override
	public boolean deleteAllWords() {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_ALL_WORDS);
		
		closeConnection();
		
		return deleted;
	}
	
	
	
	
	
	@Override
	public boolean deleteWordsByGroupId(final int groupId) {
		createConnection();
		
		final boolean deleted = executeSqlCommand(SQL_DELETE_WORDS_BY_GROUP_ID + groupId);
		
		closeConnection();
		
		return deleted;
	}	
}