package kruncik.jan.gallows.gallowsApp;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Tato třída slouží pouze pro to, aby se při zavření aplikace zjistilo, zda
 * běží rozehraná hra nebo ne, pokud ano, tak aby se nabídlo uložení dané hry
 * před zavřením aplikace.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CloseAppAdapter extends WindowAdapter {

	private final App app;
	
	CloseAppAdapter(final App app) {
		super();
		
		this.app = app;
	}
	
	
	@Override
	public void windowClosing(WindowEvent e) {
		// Zjistím, zda běží nějaká hra a zeptám se uživatele, zda se má uložit:
		app.checkRunningGameAndSave();
	}	
}