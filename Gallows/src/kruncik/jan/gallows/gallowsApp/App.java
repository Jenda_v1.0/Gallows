package kruncik.jan.gallows.gallowsApp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.app.Login;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.Game;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.db_objects.Word;
import kruncik.jan.gallows.dialogs.HelpWordForm;
import kruncik.jan.gallows.dialogs.SaveGameForm;
import kruncik.jan.gallows.dialogs.SelectGameForm;
import kruncik.jan.gallows.dialogs.dataForDialogs.SelectedGame;
import kruncik.jan.gallows.dialogs.userResultsForm.RestoreGameData;

/**
 * Tato třída slouží jako okno aplikace šibenice.
 * 
 * v okne je nastaven BorderLayout jako manažer rozvržení komponent. - Nahore
 * jsou tlačítka pro ovládání hry (start stop, pause)
 *
 * - Uprostřed je panel, který obsahuje také BorderLayout nahore je lable s
 * hadanym slovem a v centru je label pro zobrazeni obrazku, dole je panel s
 * tlačítky
 * 
 * - Dole je akorát label, který zobrazuje bežící čas hry.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class App extends JFrame {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Pomocí této proměnné se budou brát slovíčka z DB.
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	/**
	 * Tato proměnná obsahuje "základní" písmena české abecedy, které se načtou do
	 * tlačítek v okně aplikace, na která může uživatel klikat. Zbytek písmen s
	 * diakritikou se generuje automaticky po kliknutí na dané písmeno bez
	 * diakritiky.
	 */
	private static final String[] LETTERS_ARRAY = new String[] { "A", "B", "C", "Č", "D", "Ď", "E", "F", "G", "H", "I",
			"J", "K", "L", "M", "N", "Ň", "O", "P", "Q", "R", "Ř", "S", "Š", "T", "Ť", "U", "V", "W", "X", "Y", "Z",
			"Ž" };
	
	
	/**
	 * Font pro bale, ve kterém se zobrazuje hádané / hledané slovo. Jedná se o
	 * výchozí styl.
	 */
	private static final Font DEFAULT_FONT_FOR_WORD = new Font("Default style for label word", Font.BOLD, 20);
	
	
	/**
	 * Jedná se o font, který se aplikuje na label, ve kterém se zobrazuje hádané /
	 * hledané slovo. Tento styl se použije v případě, že hráč prohrál a zobrazí se
	 * slovo se zvýrazněním.
	 */
	private static final Font FONT_FOR_GAME_OVER = new Font("Font for game over.", Font.BOLD, 30);
	
	
	/**
	 * Font, který je použit pro labely skóre a lives.
	 */
	private static final Font FONT_FOR_LIVES_SCORE = new Font("Font for lives and score", Font.BOLD, 14);	
	
	
	
	/**
	 * Proměnný random, která slouží pro generování náhodných slovíček z listu
	 * wordsList.
	 */
	private static final Random RANDOM = new Random();
	
	
	/**
	 * Proměnná, ve které se držím hodnotu "maximální počet životů" - pro začátek
	 * nové hry, kdybych ji chtěl někdy změnit, protože ji nastavuji na více
	 * místech.
	 */
	private static final int LIVES = 8;
	
	
	
	
	
	/**
	 * Proměnná, která obsahuje uživatelem zvolené hodnoty pro hru, tyto hodnoty se
	 * nastaví v dialogu SelectGameForm. Hodnoty jako skupina sloviček, typ hry a
	 * případně čas pro časovač.
	 */
	private SelectedGame selectedGame;
	
	
	
	
	
	/**
	 * Tato proměnná bude převážně null, pouze v případě, že se jedná o obnovení
	 * dříve nedohrané hry. Po té, co se hra obnoví se tato proměnná opět nastaví na
	 * null hodnotu.
	 */
	private RestoreGameData gameData;
	
	
	
	/**
	 * Toto proměnná bude ve většíně případů -1, ale nastaví se do ní ID nějaké hry
	 * (Game), která se má načíst v případě, že se jedná o pokračování nedohrané hry
	 * - obnovení nějaké nedohrané hry.
	 */
	private int gameIdForContinue;
	
	
	
	
	
	/**
	 * Tento list obsahuje hrací tlačítka - tlačítka s písmeny.
	 * Potřebuji je zde mít uložené, protože když se generuje nové slovo,
	 * musí se obnovit všechna tlačítka, tak jen projdu list s tlačítky.
	 */
	private final List<JButton> playingButtonsList;
	
	
	
	/**
	 * Proměnná, ve které bude přihlášený uživatel
	 */
	private User user;
	
	
	
	// Abstraktní tlačítka, která budeou zobrazena v okně aplikace v horní
	// části, hned pod menu.
	// abtnStart - Tlačítko pro zahájení hry,
	// abtnStop - Tlačítko pro ukončení hry a možnosti pro uložení
	// abtnPause - Tlačítko pro pause ve hře.
	// aaHelp - tlačítko pro zobrazení okna s nápovědou k hádanému slovu.
	private AbstractAction aaNewGame, aaStart, aaStop, aaPause, aaHelp;
	
	
	
	/**
	 * Proměnná, ve které se budou zobrazovat obrázky šibenice
	 */
	private final JLabel lblImages;
	
	
	
	/**
	 * Proměnná, ve které bude zobrazen čas hry, bude umístěn ve spodní části okna
	 * aplikace
	 */
	private final JLabel lblTime;
	
	
	/**
	 * Panel, ve kterém se bude zobrazovat hadane slovo nebo otazníky ro vyplnění
	 * slova
	 */
	private final JLabel lblWord;
	
	
	
	/**
	 * Do této proměnné se uloží slovo, které se má hádat.
	 */
	private String searchWord;
	
	/**
	 * Tato proměnná bude obsahovat slovo, výše hádané / hledané slovo, které se má
	 * zobrazit v aplikaci, v tomto slově se nahradí znaky, které uživatel ještě
	 * neuhodl za otazníky. 
	 * 
	 * Info:
	 * Za otazníky se nahradí všechny znaky s vyjímkou
	 * mezery, aby uživatel věděl, zda se jedná o jedno nebo více slov.
	 */
	private String searchWordForDisplay;
	
	
	
	/**
	 * Proměnná, která obsahuje skóre uživatele,
	 * Za každé uhodnuté slovo se zvýši hodnota o 1 bod.
	 */
	private int score;
	
	/**
	 * Label, ve kterém se bude zobrazovat skóre.
	 */
	private JLabel lblScore;
	
	
	
	/**
	 * Proměnná, která definuje, kolik má ještě uživatel životů. Tato proměnná je tu
	 * zbytečná, to už ukazují obrázky, ale chtěl jsem zobrazit skóre uživateli v
	 * okně aplikace, a aby to trochu vypadalo, tak jsem tam dal i tyto životy.
	 */
	private int lives;
	
	/**
	 * Label, ve kterém se budou zobrazovat životy.
	 */
	private JLabel lblLives;
	
	
	
	
	// Proměnné pro měření času, do kterých budu ukládat jednotlivé jednotky času
	// (hodiny, minuty sekundy),
	// když bude "běžet hra"
	private int hh, mm, ss;
	
	/**
	 * Timer, který se bude aktualizovat každou vteřinu a nastavovat čas do
	 * proměnných výše
	 */
	private Timer timer;
	
	
	
	/**
	 * Proměnná typu list, do které se uloží při spuštění nové hry slovíčka z DB a
	 * ta se pak generují do hry.
	 */
	private List<Word> wordsList;
	
	
	
	/**
	 * Logická proměnná o tom, zda je pausa ve hře nebo ne. Pokud je hodnota této
	 * proměnné true, pak je pauza ve hře. Tato proměnná je potřeba akorát, když se
	 * klikne na novou hru, tak potřebuji nějak otestovat, zda se hraje nebo ne, a
	 * timer nestačí pokud bude pauza, pak timer stejně nepoběží proto tato
	 * proměnná.
	 */
	private boolean pause;
	
	
	
	
	/**
	 * List, do kterého vložím abstraktní akce - tlačítka, abych jej mohl předat do
	 * menu při vytváření (případně při opakovaném vytváření)
	 */
	private final List<AbstractAction> abstractActionsList;
	
	
	
	/**
	 * Tento list bude obsahovat načtená slova.
	 *  Je zde proto, aby se slova
	 * neopakovala dříve jak po 6. Tj. Pokud je v listu načtených slov z DB více jak
	 * 5 slov, pak se bude hlídat, aby se neopakovalo vždy 5 hádaných slov. Tj. 6e
	 * pokud bylo z DB načteno více jak 5 slov, pak se hlídá, aby se vygenerované
	 * slovo nebylo v prvních pěti pokusech. Pokud je z DB načteno 5 a méně slov,
	 * pak se mohou často opakovat.
	 */
	private final List<String> searchedWordsList;
	
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy, ve kterém se připraví GUI pro aplikaci dle přihlášeného uživtele.
	 * 
	 * @param user - Přihlášený uživatel
	 */
	public App(final User user) {
		super();

		setDialogTitle("", false);
		
		this.user = user;
				
		initGui_1();
		
		gameData = null;
		
		gameIdForContinue = -1;
		
		
		searchedWordsList = new ArrayList<>();
		
		
		/*
		 * Nastavím adaptér, který bude hlídat zavření okna a případné uložení hry:
		 */
		addWindowListener(new CloseAppAdapter(this));
		
		
		
		// Vytvoření a přidání abstraktních tlačítek / akcí do okna a menu aplikace			
		abstractActionsList = createActionsButtons();

		
		
		// Nastavím menu dle typu přihlášeného uživatele:
		if (user.getKindOfUser().equals("Admin"))
			setJMenuBar(new MenuCreator(true, abstractActionsList, this));
		else setJMenuBar(new MenuCreator(false, abstractActionsList, this));
		
		
		
		// Přidání tlačítek do panelu a ten do okna aplikace nahoru, pod menu:
		final JToolBar toolBar = new JToolBar(JToolBar.HORIZONTAL);
		toolBar.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		toolBar.add(aaNewGame);
		toolBar.add(aaStart);
		toolBar.add(aaPause);
		toolBar.add(aaStop);
		toolBar.add(aaHelp);
		add(toolBar, BorderLayout.NORTH);

		
		
		
		
		
		
		
		// Panel s obrázky a tlačítky ve středu okna aplikace, tlacitky pro ovladani hry
		// a labely s popisky:
		final JPanel pnlImagesAndButtons = new JPanel(new BorderLayout());
		
		// Panel, ve kterém budou na centru label s hadaným slovem a nahoře bude panel s
		// zivoty a skore
		final JPanel pnlLivesWordScore = new JPanel(new BorderLayout());
						
		
		searchWordForDisplay = "?";
		lblWord = new JLabel(searchWordForDisplay);
		lblWord.setFont(DEFAULT_FONT_FOR_WORD);
		lblWord.setHorizontalAlignment(JLabel.CENTER);
		pnlLivesWordScore.add(lblWord, BorderLayout.CENTER);
		
		
		// Panel s zivoty a skore
		final JPanel pnlScoreLives = new JPanel(new GridLayout(0, 2, 10, 0));
		// Obraničení panelu - ("bílími mezerami" od okolí):
		pnlScoreLives.setBorder(new EmptyBorder(10, 10, 0, 10));
		
		score = 0;
		lblScore = new JLabel();
		lblScore.setFont(FONT_FOR_LIVES_SCORE);
		setLblScore();
		lblScore.setHorizontalAlignment(JLabel.LEFT);
		pnlScoreLives.add(lblScore);
		
		lives = LIVES;
		lblLives = new JLabel();
		lblLives.setFont(FONT_FOR_LIVES_SCORE);
		setLblLives();
		lblLives.setHorizontalAlignment(JLabel.RIGHT);
		pnlScoreLives.add(lblLives);
		
		pnlLivesWordScore.add(pnlScoreLives, BorderLayout.NORTH);
		
		
		pnlImagesAndButtons.add(pnlLivesWordScore, BorderLayout.NORTH);
		
		
		lblImages = new JLabel();
		lblImages.setHorizontalAlignment(JLabel.CENTER);
		lblImages.setVerticalAlignment(JLabel.CENTER);
		setImage(0);
		pnlImagesAndButtons.add(lblImages, BorderLayout.CENTER);
		
		// Vytvořím si list, kam se budou vkládat tlačítka:
		playingButtonsList = new ArrayList<>();
		pnlImagesAndButtons.add(createPlayingButtons(), BorderLayout.SOUTH);
		
		add(pnlImagesAndButtons, BorderLayout.CENTER);
		
		
				
		
		
		
		
		// Inicializace a přidání labelu s časem do okna aplikace:
		lblTime = new JLabel("00 : 00 : 00");
		lblTime.setHorizontalAlignment(JLabel.CENTER);
		lblTime.setFont(new Font("My Time Style", Font.BOLD, 16));
		add(lblTime, BorderLayout.SOUTH);
		
		
		
		
		
		// Na začátku znepřístupním tlačítka, až po nové hře je zpřístupním:
		enableAllButtons(false);
		
		// Znepřístupním tlačítka pro pauzu, start a stop:
		aaStart.setEnabled(false);
		aaPause.setEnabled(false);
		aaStop.setEnabled(false);
		aaHelp.setEnabled(false);
		
		
		
		
		
		initGui_2();
	}
	

	
	
	
	
	
	
	
	
	/**
	 * Vytvořím si timer dle typu hry. Tj. buď se bude čas inkrementovat nebo
	 * dekremntovat do nuly a až dojde k nule, hra skončila. Při inkrementování času
	 * se hraje, dokud uživatel neprohraje tak, že mu nedojdou životy (tj. neudělá
	 * definovaný počet chyb).
	 * 
	 * @param ascending
	 *            - logická proměnná pro to, zda se mají hodnoty každou vteřinu v
	 *            timeru inkrementovat nebo dekrementovat
	 */
	private void createTimer(final boolean ascending) {
		if (timer != null && timer.isRunning()) {
			timer.stop();
			timer = null;
		}
				
		
		
		final ActionListener ac;
		
		if (ascending) {
			// Zde se budou hodnoty inkremetovat:
			ac = e -> {
				if (ss == 59) {
					if (mm == 59) {
						mm = 0;
						hh++;
					}
					else mm++;

					ss = 0;
				}
				else ss++;

				setDataToLblTime();
			};
		}
		
		else {
			ac = e -> {
				// Zde se musí hodnoty dekrementovat do nuly:
				if (ss == 0) {
					if (ss == mm && ss == hh) {
						// Zde došel čas, tak uživatel "prohrál" - hra skončila, tak jde o získané body
						gameOver(true);
						return;	// Jinak by se pokračovalo a nastavilo by se 0 : 59 : 59, což nechci
					}


					if (mm == 0) {
						mm = 59;

						if (hh > 0)
							hh--;
					}
					else mm--;

					ss = 59;
				}
				else ss--;


				/*
				 * Pokud bude zbývat posledních 15 vteřin času v případě Countdown, tak text s
				 * časem bude svítit červeně.
				 */
				if (ss <= 15 && mm == 0 && mm == hh) {
					if (lblTime.getForeground().getRGB() != Color.RED.getRGB())
						lblTime.setForeground(Color.RED);
				}
				// Pokud zbývá více nez 15 vteřin, tak bude text normálně černý:
				else {
					if (lblTime.getForeground().getRGB() != Color.BLACK.getRGB())
						lblTime.setForeground(Color.BLACK);
				}


				setDataToLblTime();
			};
		}
		
		timer = new Timer(1000, ac);
	}
	
	
	
	
	
	/**
	 * Nastavení hodnot do lblTime (label, který zobrazuje čas pro hru v okně aplikace).
	 * Zobrazuje hodnoty HH : MM : SS ve spodní části okna aplikace
	 */
	private void setDataToLblTime() {
		final String hh_s, mm_s, ss_s;				
		
		if (hh < 10)
			hh_s = "0";
		else hh_s = "";
		
		if (mm < 10)
			mm_s = "0";
		else mm_s = "";
		
		if (ss  < 10)
			ss_s = "0";
		else ss_s = "";
		
		lblTime.setText(hh_s + hh + " : " + mm_s + mm + " : " + ss_s + ss);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která připraví aplikaci pro novou hru (případně ji spustí, pokud uživatel vybere potřebné hodnoty).
	 * 
	 * Pokud už nějaká hra běží, pak se nabídne uložení té aktuální
	 * 
	 * - Načíst z DB slovíčka,
	 * - vygenerovat slovicko asi random cislo dle velikosti listu se slovicky
	 * - nastavit otazníky na slovo do okna aplikace a slovo si uložit do proměnné
	 * - zviditelnit tlačítka
	 * - nastavit score na nulu a zivoty na 8
	 * - spustit timer
	 */
	final void newGame() {
		// Zjistím, zda běží nějaká hra a zeptám se uživatele, zda se má uložit:
		checkRunningGameAndSave();
		
		
		if (selectedGame == null) {
			final SelectedGame selectedGameVar = new SelectGameForm(DATA_MANAGER.getAllGroups(),
					DATA_MANAGER.getAllKindsOfGames(), selectedGame).getGroup(); // pro selected game mohu předat null,
																					// je to jedno

			/*
			 * Pokud se do proměnné výše "selectedGroupFromForm" vrátí null, pak uživatel
			 * dialog zavřel bez výběru skupiny, jinak je tam zvolená skupina slov.
			 */
			
			if (selectedGameVar != null) {
				/*
				 * Zde uživatel nastavil skupinu a nějaký typ hry, popřípadě čas (nezavřel
				 * dialog), tak si tyto hodnoty uložím a dle toho nastavím hodnoty v aplikaci
				 * pro přípravu hry:
				 */
				selectedGame = selectedGameVar; 
			}				
		}
		
		
		/*
		 * Zde musím znovu otestovat, zda se proměnná selectGame nastavila nebo ne,
		 * protože pokud se v podmínce výše dialog zavřel, pak tato proměnná bude stále
		 * null, tudíž mohu skončit, uživatel nechce začít, jinak by zvolil hodnoty z
		 * dialog a potvrdil výběr a ne zrušil bez potvrzení
		 */
		if (selectedGame == null)
			return;
		
		
		
		// Z tohoho listu se budou pokazde generovatnahodne dle jeho velikosti v nasledujici metode generateWord
		// Do listu se načtou slovíčka ze zvolené skupiny:
		wordsList = DATA_MANAGER.getWordsByGroupId(selectedGame.getGroup().getId());
		
		
		/*
		 * Pokud je list se slovíčkami prázdný nebo null (nemělo by nastat), pak požádám
		 * uživatele, aby vybral jinou skupinu se slovíčkami, jinak by neměl jaká
		 * slovíčka hádat.
		 */
		if (wordsList == null || wordsList.isEmpty()) {
			JOptionPane.showMessageDialog(this,
					"Ve zvolené skupině se nevyskytují žádná slovíčka, vyberte prosím jinou.", "Prázdná skupina",
					JOptionPane.INFORMATION_MESSAGE);
			
			/*
			 * Zde nastavím výchozí hodnoty na null, prootže nejsou žádná slovíčka v dané
			 * skupině, pak se při zvolení nové hry musí vybrat nějaká jina, zpráva výše o
			 * tom uživatele informuje.
			 */
			selectedGame = null;
			return;
		}				
		
		// Už není pauza, tak aby nedošlo k nějaké nechtěné chybě.
		pause = false;
		
		/*
		 * V této část už vím, že se bude pokračovat v přípravách pro novou hru, tak
		 * nastavím do titulku dialogu zvolené téma.
		 */
		setDialogTitle(selectedGame.getGroup().getGroupName(), true);
		
		// Při nové hře nastavím label se slovem na výchozí vlastnosti (kdyby nebyly),
		// to nastane, pokud už uživatel jednou prohrál
		lblWord.setForeground(Color.BLACK);
		lblWord.setFont(DEFAULT_FONT_FOR_WORD);
		
		
		/*
		 * Vymažu list s hádanými slovy, aby se mohli načíst nová:
		 */
		searchedWordsList.clear();
		
		
		// Vygenerování slova a nastavení jej do proměnných a do okna obrazovky aplikace
		generateWord();
		
		// Zpřístupním tlačítka (pokud nebyly):
		enableAllButtons(true);
		
		// Zviditelním tlačítka (pro případ, že by nebyly)
		showAllButtons();
		
		
		// Zpřístupním tlačítka pro ovládání hry (pro případ, že by nebyly, například
		// při prvním spuštění aplikace apod.):
		aaStart.setEnabled(false);
		aaPause.setEnabled(true);
		aaStop.setEnabled(true);
		aaHelp.setEnabled(true);
		
		
			
		// Nastavení skóre:		
		if (gameData != null)
			score = gameData.getScore();

		else
			score = 0;
		setLblScore();
		
		
		// Nastavení životů:
		if (gameData != null)
			lives = gameData.getLives();
		else
			lives = LIVES;
		setLblLives();
				
		
		
		
		/*
		 * Nastavím výchozí (první obrázek), nebo, pokud se jedná o pokračování ve hře,
		 * tak se nastaví ten co má, dle životů.
		 */
		setImage();
		
		
		
		
		/*
		 * Nastavím hodnoty pro čas na zvolené hodnoty, bud to budou nuly jako výchozí
		 * hodnoty pro měření nebo nějaký definovaný čas v případě COuntdown:
		 */
		if (gameData != null) {
			hh = gameData.getHh();
			mm = gameData.getMm();
			ss = gameData.getSs();			
		} else {
			hh = selectedGame.getHh();
			mm = selectedGame.getMm();
			ss = selectedGame.getSs();
		}
				
		
		// Zjistím, jaký typ hry je vybrán, buď meření (Measurement) nebo odpočítávání (Countdown):
		if (selectedGame.getKindOfGame().getKind().equals("Measurement"))
			createTimer(true);
		else
			createTimer(false); 
		
		
		/*
		 * Pokud není barva písma pro label, který ukazuje čas ve spodní části obrazovky
		 * černý, pak mu nastavím černou barvu.
		 */
		if (lblTime.getForeground().getRGB() != Color.BLACK.getRGB())
			lblTime.setForeground(Color.BLACK);			
		
		
		
		
		// Uložím si ID proměnné hry, která se má načíst, v případě, že se má po dohrání
		// opět uložit.
		if (gameData != null)
			gameIdForContinue = gameData.getGameId();
		
		
		
		/*
		 * Nastavím následující proměnnou na null hodnotu, protože pokud nebyla, pak se
		 * výše data převzala a hra se obnovila, pak se může pokračovat ve hře, ale
		 * kdyby tato proměnná nebyla null obnovila by se při každém spuštění, což je
		 * špatně.
		 */
		gameData = null;
		
		
		
		// Timer:
		timer.start();
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zjistí, zda běží nějaká hra a pokud ano, nabídne uživateli její
	 * uložení.
	 * 
	 * Tato metoda se volá zejéna v případě, zavření aplikace nebo při kliknutí na
	 * novou hru, kdy se zjistí, zda nějaká hra běží nebo ne a nabínce se uživateli
	 * její uložení.
	 */
	void checkRunningGameAndSave() {
		if ((timer != null && timer.isRunning()) || pause) {
			// Zde nějaká hra běží, tak nabídnu její uložení:
			timer.stop();
			
			saveGame();
		}	
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví titulek dialogu na text: "ŠIbenice (Zvolené_téma)" nebo
	 * jen na "Šibenice", dle toho, zda se ten titulek nastavuje v konstruktoru této
	 * třídy, když ještě není zvolené témma, nebo až po tom, co ho uživatel zvolí.
	 * 
	 * @param title
	 *            - zvolené téma.
	 * @param includeGroup
	 *            - zda se má nastavit titulek dialogu s tím zvoleným tématem /
	 *            skupinou hádaných / hledaných slovíček nebo bez něj.
	 */
	private void setDialogTitle(final String title, final boolean includeGroup) {
		if (includeGroup)
			setTitle("ŠIbenice (" + title + ")");
		
		else setTitle("Šibenice"); 
	}
	
	
	/**
	 * Metoda, která nastaví label, který ukazuje skóre v okně obrazovky na hodnotu,
	 * která je v proměnné score.
	 */
	private void setLblScore() {
		lblScore.setText("Skóre: " + score);
	}
	
	
	/**
	 * Metoda, která nastaví text labelu, který ukazuje životy v okně aplikace na
	 * hodnotu, která je v proměnné lives.
	 */
	private void setLblLives() {
		lblLives.setText("Životy: " + lives);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří hrací tlačítka pro hraní šibenice, jedná se o tlačítka
	 * s abecedou - základní písmena.
	 * 
	 * @return panel s tlačítky coby písmena abecedy, pomocí kterých se budou
	 *         vyplňovat slovíčka
	 */
	private JPanel createPlayingButtons() {
		final JPanel pnlButtons = new JPanel(new GridLayout(0, 11, 10, 10));
		pnlButtons.setPreferredSize(new Dimension(150, 150));

		for (String aLETTERS_ARRAY : LETTERS_ARRAY) {
			// Vytvořím tlačítko a nastavím mu jako text písmeno z abecedy
			final JButton btn = new JButton(aLETTERS_ARRAY);

			/*
			 * Po kliknutí na tlačítko se otestuje, zda hledné slovo obsahuje 
			 * písmeno, na které se kliklo + to samé písmeno s diakritikou,
			 * pokud ano, doplní se text v okně aplikace (nahradí se otazníky).
			 *
			 *  Dále se otestuje, zda ještě není konec hry tak, že uživatel vyhral
			 *  nebo prohrál.
			 *
			 *  Pokud už ve slově nejsou žádné otazníky, tak se vygeneruje další slovo,
			 *  pokud už došli obrázky nebo životy, tak uživatel prohrál.
			 */
			btn.addActionListener(Event -> {
				btn.setVisible(false);

				/*
				 * pokud to pismeno ale neni ve slove, pak se pricte index k sibenici a pokud je to treba 8
				 * pak prohral, jinak se jen zvysi index a zmeni obrazek
				 */

				/*
				 * Projdu hledané / hádané slovo a zjistím, zda obsahuje označené písmeno,
				 * pokud ano, tak jej zrovna nahradím.
				 * To, zda se nějaké písmeno nahradilo si uložím do proměnné "replaced" a dle
				 * ní budu postupovat dále - prohra nebo výhra nebo nic.
				 */
				final boolean replaced = checkWordForLetter(btn.getText());

				// Zobrazím v okně aplikace nově vygenerované slovo:
				lblWord.setText(searchWordForDisplay);

				/*
				 * Pokud uživatel trefil správné písmeno, které se v hádaném / hledaném slově vyskytuje, pak
				 * proměnné replaced bude true a stačí zjistit, zda se ještě nějaké otazníky v hádaném slově vyskytují,
				 * pokud ne, pak uživatel má další bod a může se vygenerovat nové. Ale pokud se ve slově ještě
				 * vyskytují nějaké otazníky, tak nic nedělám, uživatel může hádat dál.
				 *
				 * Pokud se uživatel netrefil do písmena (replaced bude false), pak odečtu život a pokud
				 * už je na určité úrovní, pak uživatel prohrál.
				 * Dle toho životu se zobrazí obrázek s vykreslenou určitou částí šibenice.
				 */

				// Otestuji, zda uživatel trefil písmeno:
				if (replaced) {
					// Zde uživatel trefil písmeno:

					// Otestuji, zda je ještě nějaké písmeno k hádání:
					if (!searchWordForDisplay.contains("?"))
						// Už není písmeno k hádání, tak vygeneruji nové:
						nextWord();
				} else {
					/*
					 * Zde uživatel netrefil písmeno, resp. písmeno, které označil není ve slově,
					 * tak snížím životy, dle toho zobrazím příslušný obrázek a otestuji konec hry.
					 */
					lives--;
					setLblLives();

					/*
					 * Nastavit obrázek: 
					 * Info: 
					 * vždy zneguji aktuální počet životů mínus počet
					 * maximální životů a výsledek je index obrázku, který se má zobrazit.
					 */
					setImage();


					// Pokud došly životy, tak uživtel prohrál:
					if (lives == 0)
						gameOver(false);
				}
			});

			pnlButtons.add(btn);
			playingButtonsList.add(btn);
		}
		
		return pnlButtons;
	}
	
	
	
	
	
	
	
	

	
	
	/**
	 * Metoda, která se zavolá v případě, že uživatel prohdál. Provedou se
	 * následující kroky:
	 * 
	 * - Zastavím timer
	 * - Znepřístupním tlačítka pro označování písmen,
	 * - Znepřístupním tlačítka pro ovládání hry s vyjímkou nové hry
	 * - Zobrazí se hádané slovo, které buď neuhodl, nebo došel čas. 
	 * - Nabídnu uživateli uložení hry (výsledek).
	 * 
	 * @param byTime
	 *            - logická proměnná, pokud je true, znamená to, že nastal konec hry
	 *            tak, že vypršel nastavený čas (v případě typu hry Countdown),
	 *            pokud je tato proměnná false, tak uživatel došli životy (udělal
	 *            určitý počet chyb (x - krát netrefil písmena ve slově))
	 */
	private void gameOver(final boolean byTime) {
		timer.stop();
		
		// Znepřístupním tlačítka pro zadávání písmen pro hádání slova:
		enableAllButtons(false);
				
		aaStart.setEnabled(false);
		aaPause.setEnabled(false);
		aaStop.setEnabled(false);
		aaHelp.setEnabled(false);
		
		
		/*
		 * Nastavím do labelu, kde se zobrazuje hádané / hledané slovo to slovo, které
		 * měl uživatel uhodnout, ale neuhodl, nebo mu došel čas, a daný label zvýrazním
		 * červeně a nastavím mu formát s větším písmem.
		 */
		lblWord.setText(searchWord);
		lblWord.setForeground(Color.RED);
		lblWord.setFont(FONT_FOR_GAME_OVER);
		
		
		
		
		
		final String comment = getCommentToWord(searchWord);
		
		JLabel lblInfo = null;
		
		JPanel pnlText = null;
		if (comment != null) {
			pnlText = new JPanel(new BorderLayout());
			
			lblInfo = new JLabel();
			final JLabel lblWord = new JLabel(searchWord);
			final JLabel lblComment = new JLabel(comment);
			
			lblInfo.setHorizontalAlignment(JLabel.CENTER);
			lblWord.setHorizontalAlignment(JLabel.CENTER);
			lblComment.setHorizontalAlignment(JLabel.CENTER);
			
			pnlText.add(lblInfo, BorderLayout.NORTH);
			pnlText.add(lblWord, BorderLayout.CENTER);
			pnlText.add(lblComment, BorderLayout.SOUTH);
		}
			
		
		
		
		if (byTime) {
			if (pnlText != null) {
				lblInfo.setText("!!! ČAS VYPRŠEL !!!");
				JOptionPane.showMessageDialog(this, pnlText, "GAME OVER", JOptionPane.INFORMATION_MESSAGE);
			}

			else
				JOptionPane.showMessageDialog(this, "!!! ČAS VYPRŠEL !!!", "GAME OVER",
						JOptionPane.INFORMATION_MESSAGE);
		}

		else {
			if (pnlText != null) {
				lblInfo.setText("!!! GAME OVER !!!");
				JOptionPane.showMessageDialog(this, pnlText, "GAME OVER", JOptionPane.INFORMATION_MESSAGE);
			}

			else
				JOptionPane.showMessageDialog(this, "!!! GAME OVER !!!", "GAME OVER", JOptionPane.INFORMATION_MESSAGE);
		}
			
		
			
		saveGame();
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde list s načtenými slovy z DB a až najde to, které je
	 * právě vygenerované, tak vrátí komentář k danému slovu (v parametru této
	 * metody).
	 * 
	 * @param word
	 *            - vygenerované slovo, které hráč neuhodl nebo nestihl uhodnout.
	 * 
	 * @return komentář k výše uvedenému slovu (word)
	 */
	private String getCommentToWord(final String word) {
		// Note: Mohl jsem použít zametodou get getComment, ale musel bych zachytávat
		// vyjímku, což moc nechci:
		final Word wo = wordsList.stream().filter(w -> w.getWord().equalsIgnoreCase(word)).findAny().get();

		if (wo.getComment() != null && !wo.getComment().isEmpty())
			return wo.getComment();

		return null;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, kteá se zeptá uživatele, zda chce uložit hru nebo ne. Pokud ano, tak
	 * zobrazí dialog, s předvyplněnými daty pro ukázku a akorát může doplnit
	 * komentář (volitelný.)
	 */
	private void saveGame() {
		if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, "Přejete si uložit hru?", "Uložit hru",
				JOptionPane.YES_NO_OPTION)) {
			/*
			 * Zde si uzivatel vybral uložit hru, tak předpřipravím objekt pro uložení do DB a předám je jdo formuláře pro 
			 * potenciální doplnění komentáře k danéhře.
			 */
			
			
			// Objekt / proměnná, do které si vložím právě odehranou hru
			final Game game;	
			
			
			if (gameIdForContinue > -1) {
				/*
				 * Do proměnné game si načtu nedohranou hru z DB, abych měl všechny potentiálně
				 * doplněné hodnoty, například komentář apod. - vše co se mohlo upravit a jen
				 * zde přepíšu nově odehraná data, jako jsou životy, čas, skóre apod.
				 */
				game = DATA_MANAGER.getGame(gameIdForContinue);
				
				/*
				 * Nastavím nově odehrané hodnoty (není zde třeba hlídat typ hry, originální -
				 * nastavený čas v případě Countdown by byl již načtený výše z DB, tak jej není
				 * třeba upravovat.):
				 */
				game.setLives(lives);
				game.setScore(score);
				game.setDate(new Date(new java.util.Date().getTime()));
				game.setHh(hh);
				game.setMm(mm);
				game.setSs(ss);
				
				
				// Nastavím následující proměnnou na -1, aby se neukládala pořád ta jedna hra.
				gameIdForContinue = -1;
				
				/*
				 * Jelikož se nějaká hra načetla, tak zde ji musím zrušit, aby se dala na výběr
				 * nová hra, jinak by se pořád pokračovalo v té jedné.
				 */
				selectedGame = null;
			}
			
			
			
			// Zjistím si, o jaký typ hry se jedná:
			else if (selectedGame.getKindOfGame().equals("Measurement"))
				// Zde se jedná hru typu Measurement, tak nepotřebuji uložit nastavený čas pro
				// časovač
				game = new Game(new Date(new java.util.Date().getTime()), null, selectedGame.getKindOfGame().getKind(),
						hh, mm, ss, score, user.getId(), selectedGame.getGroup().getId(), lives);

			// Zde se jedná o typ hry Countdown, tak si musím uložit i nastavený časovač
			else
				game = new Game(new Date(new java.util.Date().getTime()), null, selectedGame.getKindOfGame().getKind(),
						hh, mm, ss, score, user.getId(), selectedGame.getGroup().getId(), selectedGame.getHh(),
						selectedGame.getMm(), selectedGame.getSs(), lives);
			
			
			/*
			 * Do následující proměnné si uložím ten samý objekt jako výše, akorát s
			 * potenciálně doplněnným komentářem. jedná se o "finální" objekt, který se
			 * uloží do DB
			 */
			final Game gameForSave = new SaveGameForm(game, user.getPseudonym()).getEditedGame();
			
			
			

			
			
			/*
			 * Otestuji hodnotu gameForSave, zda je to null nebo ne, protože pokud uživatel
			 * klikl na zavřít dialog bez uložení, pak si to rozmyslel a hra se neuloží, ale
			 * pokud není tato proměnná null, pak uživatel zvolil uložení, pak se získaná
			 * hodnota s potenciálně doplněným komentářem uloži do DB:
			 * 
			 * Ale ještě otestuji, zda je ID hry větší jak -1, pokud ano, pak se hra puze
			 * aktualizuje v DB, protože uživatel dohrál hru, kterou dříve začal, ale
			 * nedokončil.
			 */
			if (gameForSave != null) {
				if (gameForSave.getId() > -1) {
					final boolean updated;
					
					if (gameForSave.getKindOfGame().equals("Measurement"))
						updated = DATA_MANAGER.updateGame(gameForSave, false);

					else
						updated = DATA_MANAGER.updateGame(gameForSave, true);
					
					if (updated)
						JOptionPane.showMessageDialog(this, "Hra byla úspěšně aktualizována.", "Akualizováno",
								JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(this,
								"Při pokusu o aktualizaci hry došlo k chybě, hra nebyla aktualizována!", "Chyba",
								JOptionPane.ERROR_MESSAGE);
				}
				
				
				// Zde se jedná o uložení nové hry, ne aktualizace nedokončené hry:
				else {
					/*
					 * Proměnná, do které si uložím logickou hodnotu o tom, zda se povedlo uložení
					 * nebo ne.
					 */
					final boolean saved;

					/*
					 * Znovu musím otestovat, o jaký typ hry se jedná, protože mám dva různé sql
					 * příkazy pro uložení, jeden ukládá i ty hodnoty pro časovač a druhy ne. V
					 * tomto případě se jedná o uložení hry typu Measurement:
					 */
					if (gameForSave.getKindOfGame().equals("Measurement"))
						// Zde se jedná o uložení do DB hry typu Measurement:
						saved = DATA_MANAGER.addGame(gameForSave, false);

					// Zde se jedná o ulolžeí hry typu Countdown (i s časem nastaveným pro časovač)
					else
						saved = DATA_MANAGER.addGame(gameForSave, true);

					/*
					 * Otestuji, zda se hru podařilo uložit, ale toto by mělo být zbytečné, jen, aby
					 * o tom uživatel věděl, nemělo by zde dojít k chybě.
					 */
					if (saved)
						JOptionPane.showMessageDialog(this, "Hra byla úspěšně uložena do DB.", "Uloženo",
								JOptionPane.INFORMATION_MESSAGE);

					// Toto by nemělo nastat:
					else
						JOptionPane.showMessageDialog(this, "Hru se nepodařilo uložit do DB!", "Chyba",
								JOptionPane.ERROR_MESSAGE);
				}
			}
			// else - Uživatel zabřel dialog bez uložení, tak nic nedělám
		}
		
		else if (gameIdForContinue > -1) {
			/*
			 * Sem se dostanu, pokud uživatel nechce uložit hru, ale potřebuji otestovat,
			 * zda se nejednalo o nějaké pokračování nedohrané hry, pokud ano, pak v
			 * proměnné gameIdForContinue bude její ID, tak to zde musím nastavit na -1, aby
			 * se neukládalo do té jedné hry, a taky musím nastavit selecteGame na null, aby
			 * si pak uživatel vybral úplněn novou hru - nemusí mít stejné nastavení, jako v
			 * té hře co pokračoval.
			 */
			
			// Nastavím následující proměnnou na -1, aby se neukládala pořád ta jedna hra.
			gameIdForContinue = -1;

			/*
			 * Jelikož se nějaká hra načetla, tak zde ji musím zrušit, aby se dala na výběr
			 * nová hra, jinak by se pořád pokračovalo v té jedné.
			 */
			selectedGame = null;
		}
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví obrázek dle aktuálních životů.
	 */
	private void setImage() {
		setImage(-(lives - LIVES));
	}
	
	
	

	/**
	 * Metoda, která nastaví do labelu pro zobrazení obrázků šibenice v okne
	 * aktuální obrázek dle životů
	 * 
	 * @param index
	 *            - Index obrázku, který se má zobrazit - je to dáno dle aktuálních
	 *            životů
	 */
	private void setImage(final int index) {
		lblImages.setIcon(GetIconInterface.getImage("/gallows/Gallows_0" + index + ".png", KindOfPicture.GALLOWS));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zvýší bod uživateli za uhodnuté slovo a vygeneruje další slovo,
	 * zobrazí jej v aplikaci (skryté otazníky) a zobrazí všechna tlačítka.
	 * 
	 * Metoda je volána po kliknutí na nějaké tlačítko s písmenem, když se zjistí,
	 * že žádné další písmeno k hádání není, pak se generuje nové
	 */
	private void nextWord() {
		// Přidám bod uživateli:
		score++;
		setLblScore();
		
		generateWord();
		
		showAllButtons();
	}
	
	
	
	
	
	
	/**
	 * Metoda, která zviditelní všechna tlačítka s písmeny pro hádání slova.
	 */
	private void showAllButtons() {
		playingButtonsList.forEach(b -> b.setVisible(true));
	}
	
	
	
	/**
	 * Metoda, která znepřístupní nebo naopak zpřístupní tlačítka pro označování
	 * písmen pro hádání slova.
	 * 
	 * @param enable
	 *            true - zpřístupnit tlačítka, false - znepřístupnit tlačítka
	 */
	private void enableAllButtons(final boolean enable) {
		playingButtonsList.forEach(b -> b.setEnabled(enable));
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vygeneruje nové slovo, nastaví jej do proměnných searchWord a
	 * searchWordForDisplay a do okna obrazovky.
	 * 
	 * Pokud je velikost listu načtených slov z DB větší než 5, tj. bylo načteno
	 * více jak 5 slov pro hádání, pak si aplikace pamatuje vždy 5 aktuálně
	 * vygenerovaných slov. takže každých 5 slov jdoucích po sobě nemůže být
	 * stejných. Ale pokud je načteno z DB méně jak 5 slov, pak se budou tato slova
	 * často opakovat.
	 */
	private void generateWord() {
		String newWord = wordsList.get(RANDOM.nextInt(wordsList.size())).getWord().toUpperCase();
		
		if (wordsList.size() > 5) {
			/*
			 * Zde je v listu načtených slov z DB pro hádání více jak 5 slov, tak nastavím,
			 * aby se těch 5 slov náhodou neopakovalo po sobně dříve jak po šesté. Pokud
			 * bude v listu slov 5 a méně slov, pak se budou často slova opakovat.
			 */
			// Dokud bude list dříve hádaných slov obsahovat právě vygenerované, pak náhodně
			// vybírám další slova:
			while (searchedWordsList.contains(newWord))
				newWord = wordsList.get(RANDOM.nextInt(wordsList.size())).getWord().toUpperCase();
				
			
			/*
			 * Sem se dostanu v případě, že list dříve vylosovaných slov neobsahuje právě
			 * vygenerované slovo, tak jej přidám, aby se při dalším kole nevylosovalo
			 * stejné slovo jako 5 předchozích slov.
			 */
			searchedWordsList.add(newWord);
			
			/*
			 * V případě, že je velikost listu 5, tj. obshauje 5 vygenerovaných slov, pak
			 * vymažu první vylosované, aby se tam vždy nacházelo puze 5 posledních slov.
			 */
			if (searchedWordsList.size() == 6)
				searchedWordsList.remove(0);
		}
		searchWord = newWord;
		
//		System.out.println("Metoda generateWord, vygenerovano: " + searchWord);
		
		// Nastavím nové slovo do okna obrazovky:
		setQuestionMarksInsteadLetters();
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví proměnnou searchWordForDisplay na text s
	 * počtem otazníků stejný jako hádané slovo a nastaví tuto proměnoou
	 * do lblWord do okna obrazovky.
	 */
	private void setQuestionMarksInsteadLetters() {
		final char[] charArray = searchWord.toCharArray();
		
		searchWordForDisplay = "";
		
		for (final char c : charArray)
			if (c != ' ')
				searchWordForDisplay += "?";
			else searchWordForDisplay += " ";
		
		lblWord.setText(searchWordForDisplay);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkontroluje hádaná / hledané slovo, zda obsahuje zvolený znak,
	 * a zvolený znak s diakritikou (pokud nějakou může obsahovat). 
	 * 
	 * Pokud ano, tak jej zrovna v hledaném slově, které se zobrazuje v okně 
	 * aplikace nahradí.
	 * 
	 * @param letter
	 *            - písmeno, který se testuje, zda se vyskytuje ve hledaném /
	 *            hádaném slově, včetně diakritiky.
	 * 
	 * @return -true, pokud se písmeno (letter) nahradilo v hledaném slově nebo ne.
	 */
	private boolean checkWordForLetter(final String letter) {
		boolean replaced = false;
		
		/*
		 * Projdu hledané slovo - original 
		 * - Zda obsahuje zvolené písmeno, případně jeho verzi z diakritikou,
		 *  	- pokud ano, tak nahradím otazník na stejném indexu v hledaném slově s otazníky, které se zobrazuje
		 */
		
		
		
		// Zjistím, zda se písmeno ve slově nachází:
		if (searchWord.contains(letter)) {
			// Zde se označené písmeno ve slově nachází, tak jej nahradím a uložím si o
			// tom informaci
			findIndexForReplaceLetter(letter);
			replaced = true;
		}
		
		
		/*
		 * Hledané slovo může obsahovat nějaké písmeno (/ na) s diakritikou, která
		 * nejsou na výběr, tak jej zde musím pokrýt, proto projdu celé slovo a zjistím,
		 * zda některé z těch písmen, které nelze označit v okně aplikace neobsahuje:
		 */
		switch (letter) {
		case "A":
			// Písmeno A může mít diakritiku (dlouhé Á), tak zjstím, zda se v hledaném slově
			// nachází nebo ne
			if (searchWord.contains("Á")) {
				findIndexForReplaceLetter("Á");
				replaced = true;
			}
			break;
			
		case "E":
			if (searchWord.contains("Ě")) {
				findIndexForReplaceLetter("Ě");
				replaced = true;
			}
			if (searchWord.contains("É")) {
				findIndexForReplaceLetter("É");
				replaced = true;
			}
			break;
			
		case "I":
			if (searchWord.contains("Í")) {
				findIndexForReplaceLetter("Í");
				replaced = true;
			}
			break;
			
		case "O":
			if (searchWord.contains("Ó")) {
				findIndexForReplaceLetter("Ó");
				replaced = true;
			}
			break;
			
		case "U":
			if (searchWord.contains("Ú")) {
				findIndexForReplaceLetter("Ú");
				replaced = true;
			}
			if (searchWord.contains("Ů")) {
				findIndexForReplaceLetter("Ů");
				replaced = true;
			}
			break;
			
		case "Y":
			if (searchWord.contains("Ý")) {
				findIndexForReplaceLetter("Ý");
				replaced = true;
			}
			break;

		default:
			break;
		}
		
		return replaced;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která projde celé hledané slovo po jednotlivých znacích,
	 * aby se vědělo, písmena na jacích indexích se mají nahradit,
	 * může se jich v jednom slově vyskytovat více, tak projde celé slovo,
	 * a nahradím všechny znaky (letter) másto otazníku.
	 * 
	 * @param letter
	 *            - písmeno, kterým se má nahradit otazník (/ ky) v uživatelem
	 *            hledaném slově v okně aplikace (pokud jej obsahuje)
	 */
	private void findIndexForReplaceLetter(final String letter) {
		/*
		 * Zde se písmeno ve slově nachází, jenže nevím kolikrá, tak projdu všechny znky
		 * v daném slově a pokud se jedná o požadované písmeno, tak jej nahradím.
		 */

		// Pole znaků daného slovy, které projdu:
		final char[] charArray = searchWord.toCharArray();

		for (int i = 0; i < charArray.length; i++)
			if (charArray[i] == letter.charAt(0))
				// Na tomto znaku (i) se nachází písmeno, které se má nahradit,
				// tak jej nahradím:
				replaceLetterInSearchedWordForDisplay(i, letter);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která nahradí písmeno na zadaném indexu za nové (newLetter). Písmeno
	 * se nahradí za otazník v proměnné searchWordForDisplay, která obsahuje hádané
	 * / hledané slovo s otazníky, dokud nejsou vyplněny.
	 * 
	 * @param index
	 *            - index, na kterém se má nahradit otazník za nové písmeno
	 *            (newLetter).
	 * @param newLetter
	 *            - nové písmeno, které má nahradit otazník na indexu (index)
	 */
	private void replaceLetterInSearchedWordForDisplay(final int index, final String newLetter) {
		// Uložím si text od začátku po zadaný index a přidám nové písmeno
		String newSearchWordForDisplay = searchWordForDisplay.substring(0, index) + newLetter;

		// Pokud ještě nejsem na konci slova, přidám zbytek původního textu:
		if ((index + 1) < searchWordForDisplay.length())
			newSearchWordForDisplay += searchWordForDisplay.substring(index + 1);

		// Nově "vytvořený / nahrazený" text vložím do původní proměnné
		searchWordForDisplay = newSearchWordForDisplay;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která se postará o to, aby se načetla a spustile dříve započatá, ale
	 * nedokončená hra.
	 * 
	 * @param contineInGameData
	 *            - Informace o zvolené hře - typ hry a případně čas a slovíčka.
	 * @param gameData
	 *            - data potřebná pro obnovení hry, jako jsou životy, skóre, čas.
	 *            Které zbývají z té nedokončené hry.
	 */
	public final void continueInGame(final SelectedGame contineInGameData, RestoreGameData gameData) {
		// (Nebudu testovat null hodnoty -> nemělo by nastat.)
		
		selectedGame = contineInGameData;
		
		this.gameData = gameData;
		
		newGame();
	}
	
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * Metoda, která vytvoří abstraktní tlačítka pro ovládání hry a přidá je do
	 * listu, který se vrátí, aby bylo možné tyto tlačítka předat jako referencei do
	 * menu okna aplikace.
	 * 
	 * @return - list s vytvořenými abstraktními tlačítky, aby bylo možné je předat
	 *         do menu v okně aplikace
	 */
	private List<AbstractAction> createActionsButtons() {
		final List<AbstractAction> abstractActionsList = new ArrayList<>();
		
		/*
		 * Tlačítko Nová hra:
		 * - zavolám metodu pro novou hru, která zařídí spuštění hry, aby mohl uživatel hrát
		 */
		aaNewGame = new AbstractAction("Nová hra",
				GetIconInterface.getImage("/gameIcons/NewGameIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				newGame();
			}
		};
				
		abstractActionsList.add(aaNewGame);
		
		/*
		 * Tlačítko Start
		 * - Pouze pokud byla hra pozastaveno, 
		 * - zpřístupnit tlačítka pro označování písmen do hádaného slova
		 * - spustit timer
		 * - Zpřístupnit tlačítko Pauza
		 * - Znepřístupnit toto tlačítko
		 */
		aaStart = new AbstractAction("Start",
				GetIconInterface.getImage("/gameIcons/PlayGameIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				// Zpřístupním tlačítka s písmeny:
				enableAllButtons(true);			
				
				pause = false;
				
				if (!timer.isRunning())
					timer.start();
				
				aaPause.setEnabled(true);
				aaStart.setEnabled(false);
				aaHelp.setEnabled(true);
			}
		};
		abstractActionsList.add(aaStart);
		
		
		
		
		/*
		 * Tlačítko Pause
		 * - Zastavím timer
		 * - Znepřístupním tlačítka pro označování písmen
		 * - Znepřístupním tlačítka Pauza (toto tlačítko)
		 */
		aaPause = new AbstractAction("Pauza",
				GetIconInterface.getImage("/gameIcons/PauseGameIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				setPause();
			}
		};
		
		abstractActionsList.add(aaPause);
		
		
		
		
		/*
		 * Tlačítko Stop
		 * - Zastavím timer
		 * - Znepřístupnit tlačítka pro označování písmen
		 * - Znepřístupnit tlačítka stop (toto tlačítko), Pauza a Start 
		 * - Nabídnu uložení hry
		 */
		aaStop = new AbstractAction("Stop",
				GetIconInterface.getImage("/gameIcons/StopGameIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				// Tato část je téměř identickáý, jako v metodě gameOver, akorát bez ukázání výsledného slova.
				if (timer.isRunning())
					timer.stop();
				
				enableAllButtons(false);
				
				aaStop.setEnabled(false);
				aaPause.setEnabled(false);
				aaStart.setEnabled(false);
				aaHelp.setEnabled(false);
				
				// Zeptám se uživatele na uložení hry
				saveGame();
			}
		};
		abstractActionsList.add(aaStop);
		
		
		
		aaHelp = new AbstractAction("Nápověda",
				GetIconInterface.getImage("/gameIcons/HelpIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Získám si slovo, které právě hádám, akorát potřebuji testovat s převodem na
				 * velká písmena, protože jsem již převedl text na velká písmena.
				 */
				final Word word = wordsList.stream().filter(w -> w.getWord().equalsIgnoreCase(searchWord)).findFirst()
						.get();
				
				/*
				 * Nyní Zobrazím dialog, kam předám výše získané hádané slovo a zobrazím k němu
				 * nápovědu (pokud bude k dispozici, tj. komentář je zadán.)
				 */
				new HelpWordForm(word);
			}
		};		
		abstractActionsList.add(aaHelp);
		
		
		return abstractActionsList;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která připraví "zákjladní GUI" pro oknno aplikace šibenice. Připraví
	 * layout, operaci po zavření okna, velikost a ikonu aplikace
	 */
	private void initGui_1() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//		setPreferredSize(new Dimension(635, 700));
		setSize(640, 930);
		setLayout(new BorderLayout());
		
		// Nastavení ikony:
		final URL url = Login.class.getResource("/icons/Gallows.png");
		setIconImage(new ImageIcon(url).getImage());	
	}

	
	
	
	
	
	
	/**
	 * Metoda, která nastaví pauzu do hry, tělo metody, resp. vše co se provede pro
	 * nastavení pauzy ve hře by momhlo být jen po stisknutí tlačítka pause, ale je
	 * to vložené do této metody, protože se volá, resp. je potřeba nastavit pauzu
	 * ve hře i z menu, například když se uživatel rozhodne, že chce změnit hru
	 * apod. Pak se pozastaví hra, aby neběžel čas dál apod.
	 */
	final void setPause() {
		if (timer.isRunning())
			timer.stop();

		pause = true;

		enableAllButtons(false);

		aaPause.setEnabled(false);
		aaStart.setEnabled(true);
		aaHelp.setEnabled(false);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která na konec zviditelní okno aplikace, zarovná velikost okna
	 * velikosti komponentám v okně a nastaví okno na střed obrazovky / monitoru
	 */
	private void initGui_2() {
//		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	
	
	
	
	/**
	 * Setr na proměnnou selectedGame, ve které jsou údaje pro hraní hry šibenice,
	 * například typ hry, slovíčka apod.
	 * 
	 * @param selectedGame
	 *            - informace o hře, kterou chce uživatel hrát.
	 */
	void setSelectedGame(final SelectedGame selectedGame) {
		this.selectedGame = selectedGame;
	}
	
	/**
	 * Getr na proměnnouselectedGame, která obsahuje nastavené parametry pro hru,
	 * nebo null, pokud se jedná o první spuštění.
	 * 
	 * @return výše poposanou referenaci na proměnnou selectedGame
	 */
	SelectedGame getSelectedGame() {
		return selectedGame;
	}
	
	/**
	 * Getr na timer, který měří nebo odpočítává čas hry.
	 * 
	 * @return getr na výše uvedenou proměnnou
	 */
	Timer getTimer() {
		return timer;
	}
	
	/**
	 * Getr na přihlášeného uživatele.
	 * 
	 * @return referencena přihlášeného uživatele.
	 */
	public User getUser() {
		return user;
	}
	
	
	/**
	 * Setr na proměnnou user (přihlášený uživatel). Zde se jedná o to, že se mohl
	 * změnit typ uživatele z admina na běžného uživatele, pak je třeba odebrat z
	 * menu položky pro přístup k DB-
	 * 
	 * @param user
	 *            - přihlášený uživatel (potenciálně se změněnými daty, případně
	 *            typem uživatele)
	 */
	public void setUser(final User user) {		
		this.user = user;

		if (!user.getKindOfUser().equals("Admin"))
			setJMenuBar(new MenuCreator(false, abstractActionsList, this));
		else
			setJMenuBar(new MenuCreator(true, abstractActionsList, this));
	}
}