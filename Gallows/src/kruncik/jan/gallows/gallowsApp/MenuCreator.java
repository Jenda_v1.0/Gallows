package kruncik.jan.gallows.gallowsApp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.app.Login;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.dialogs.AboutAppForm;
import kruncik.jan.gallows.dialogs.AboutAuthorForm;
import kruncik.jan.gallows.dialogs.SelectGameForm;
import kruncik.jan.gallows.dialogs.UserInfoForm;
import kruncik.jan.gallows.dialogs.bestResultsForm.BestResultsForm;
import kruncik.jan.gallows.dialogs.citiesForm.CitiesForm;
import kruncik.jan.gallows.dialogs.dataForDialogs.SelectedGame;
import kruncik.jan.gallows.dialogs.dataForDialogs.UserInfo;
import kruncik.jan.gallows.dialogs.databaseUserForm.UserForm;
import kruncik.jan.gallows.dialogs.groups_wordsForm.GroupWordsForm;
import kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers.EnumKindsOf;
import kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers.KindOfGamesAndUsersForm;
import kruncik.jan.gallows.dialogs.userResultsForm.UserResultsForm;

/**
 * Tato třída slouží jako menu. Obsahuje konstruktor, kterému se pouze předá,
 * zda se mají zobrazit pouze základní tlačítka pro běžné "hraní" - pro běžného
 * uživatele nebo i pro správu DB a uživatelů, tj. když se přihlásí admin, tak
 * se tyto tlačítka zobrazí. Dále list s abstraktními tlačíty, aby bylo možné je
 * přidat do menu.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MenuCreator extends JMenuBar implements ActionListener {

	private static final long serialVersionUID = 1L;



	/**
	 * "komponenta" pro přístup k DB.
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	
	// Položky do menuApp
	private JMenuItem itemSelectGame, itemSetPathToDb, itemLogout, itemClose;
	
	/**
	 * Položka, která bude v menu "Hra", budou zde zobrazeny výsledky všech
	 * uživatelů, tak je možné zjistit, jak na tom daný užvatel je.
	 */
	private JMenuItem itemBestResults;
	
	// Položky do menuUser
	private JMenuItem itemUserInfo, itemResults;
	
	// Položky do menuInfo:
	private JMenuItem itemAboutApp, itemAboutAuthor;
	
	
	
	// Následují položky do menu databáze (pouze pro admina)
	// Položky, do menu pro manipulaci s DB:
	private JMenuItem itemUser, itemGroup, itemCity, itemKindOfUser, itemKindOfGame;
	
	
	
	/**
	 * Reference na instanci třídy App kvůli setování proměnných a volání některých
	 * metod.
	 */
	private final App app;
	
	
	/**
	 * List s abstraktními tlačítky / akcemi pro přidání do menu:
	 */
	private final List<AbstractAction> abstractActionsList;
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param buttonsForAdmin
	 *            - logická proměnná o tom, zda se mají přidat i tlačítka pro
	 *            manipulaci s DB apod. To je pouze pro admina
	 * @param abstractActionsList
	 *            - List s abstraktními tlačítky, která se mají přidat do menu.
	 * @param app
	 *            - reference na instanci třídy App - hlavní okn aplikace, abych
	 *            zbytečně předávat více proměnných, které se setují do proměnných v
	 *            třídě App, navíc potřebuji volat nějaké metody právě v této třídě
	 *            App.
	 */
	MenuCreator(final boolean buttonsForAdmin, final List<AbstractAction> abstractActionsList, final App app) {
		super();

		this.abstractActionsList = abstractActionsList;
		this.app = app;
		
		
		if (buttonsForAdmin) {
			createComponentsForAdmin();
			setIconsToMenuItems(true);
			createKeyShortcut(true);
		}
			

		else {
			createBasicsComponents();
			setIconsToMenuItems(false);
			createKeyShortcut(false);
		}
	}

	
	
	
	/**
	 * Metoda, která vytvoří pouze "základní" tlačítka pro ovládání hry apod.
	 */
	private void createBasicsComponents() {
		final JMenu menuApp = new JMenu("Aplikace");
		
		itemSelectGame = new JMenuItem("Vybrat hru");
		itemSetPathToDb = new JMenuItem("Nastavit DB");
		itemLogout = new JMenuItem("Odhlásit");
		itemClose = new JMenuItem("Zavřít");
		
		
		itemSelectGame.setToolTipText("Otevře dialogové okno pro výběr režimu hry a skupinu slovíček.");
		itemSelectGame.setToolTipText("Otevře oknopro výběr adresáře, kde senachází DB pro aplikaci.");
		itemLogout.setToolTipText("Odhlásí přihlášeného uživatele.");
		itemClose.setToolTipText("Zavře aplikaci");
		
		
		menuApp.add(itemSelectGame);
		menuApp.add(itemSetPathToDb);
		menuApp.addSeparator();
		menuApp.add(itemLogout);
		menuApp.addSeparator();
		menuApp.add(itemClose);
		
		
		itemSelectGame.addActionListener(this);
		itemSetPathToDb.addActionListener(this);
		itemLogout.addActionListener(this);
		itemClose.addActionListener(this);
		
		
		
		// Menu Game:
		final JMenu menuGame = new JMenu("Hra");		
		
		for (int i = 0; i < abstractActionsList.size(); i++) {
			menuGame.add(abstractActionsList.get(i));
			
			if (i < (abstractActionsList.size() - 1))
				menuGame.addSeparator();
		}
		
		itemBestResults = new JMenuItem("Nejlepší výsledky");
		itemBestResults.addActionListener(this);
		
		itemBestResults.setToolTipText("Otevře dialogové okno s přehledem výsledků všech uživatelů.");
		
		menuGame.addSeparator();
		menuGame.add(itemBestResults);
		
		
		
		
		
		// Menu pro uživatele:
		final JMenu menuUser = new JMenu("Uživatel");
		
		itemUserInfo = new JMenuItem("Info");
		itemResults = new JMenuItem("Výsledky");
		
		itemUserInfo.setToolTipText("Otevře dialogové okno s informacemi ohledně uživatelského účtu přihlášeného uživatele.");
		itemResults.setToolTipText("Otevře dialogové okno s výsledky uložených her přihlášeného uživatele.");
		
		menuUser.add(itemUserInfo);
		menuUser.addSeparator();
		menuUser.add(itemResults);
		
		
		itemUserInfo.addActionListener(this);
		itemResults.addActionListener(this);
		
		
		
		
		// Menu pro informace o aplikaci a autorovi:
		
		final JMenu menuInfo = new JMenu("Info");
		
		itemAboutApp = new JMenuItem("Informace");
		itemAboutAuthor = new JMenuItem("Autor");
		
		itemAboutApp.setToolTipText("Otevře dialogové okno s informacemi o tét aplikaci.");
		itemAboutAuthor.setToolTipText("Otevře dialogové okno s informacemi o autoroví této aplikace.");
		
		menuInfo.add(itemAboutApp);
		menuInfo.addSeparator();
		menuInfo.add(itemAboutAuthor);
		
		
		itemAboutApp.addActionListener(this);
		itemAboutAuthor.addActionListener(this);
		
		
		
		add(menuApp);
		add(menuGame);
		add(menuUser);
		add(menuInfo);
	}

	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví tlačítkům v menu ikony.
	 * 
	 * @param databaseItemsIcon
	 *            - logická proměnná, dle které se pozná, zda se mají nastavit i
	 *            ikony pro tlačítka nad položky pro manipulaci s DB. Toto bude
	 *            pouze v případě, že je přihlášen admin.
	 */
	private void setIconsToMenuItems(final boolean databaseItemsIcon) {
		itemSelectGame.setIcon(GetIconInterface.getImage("/icons/SelectGameForm.png", KindOfPicture.TOOLBAR_OR_MENU));
		itemSetPathToDb.setIcon(GetIconInterface.getImage("/icons/DatabaseIcon.png", KindOfPicture.TOOLBAR_OR_MENU));
		
		itemLogout.setIcon(GetIconInterface.getImage("/icons/LogoutIcon.png", KindOfPicture.TOOLBAR_OR_MENU));
		itemClose.setIcon(GetIconInterface.getImage("/icons/CloseIcon.png", KindOfPicture.TOOLBAR_OR_MENU));
		
		itemBestResults.setIcon(GetIconInterface.getImage("/icons/ResultsIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));
		
		itemUserInfo.setIcon(GetIconInterface.getImage("/icons/UserInfoIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));
		itemResults.setIcon(GetIconInterface.getImage("/icons/UserResultsIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));
		
		itemAboutApp.setIcon(GetIconInterface.getImage("/icons/InfoIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));
		itemAboutAuthor.setIcon(GetIconInterface.getImage("/icons/AuthorIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));
		
		if (databaseItemsIcon) {
			itemUser.setIcon(GetIconInterface.getImage("/icons/UsersFormIcon.png", KindOfPicture.TOOLBAR_OR_MENU));
			itemGroup.setIcon(GetIconInterface.getImage("/icons/GroupFormIcon.png", KindOfPicture.TOOLBAR_OR_MENU));
			itemCity.setIcon(GetIconInterface.getImage("/icons/CitiesFormIcon.png", KindOfPicture.TOOLBAR_OR_MENU));
			itemKindOfUser.setIcon(GetIconInterface.getImage("/icons/KindsOfGamesAndUsersIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));
			itemKindOfGame.setIcon(GetIconInterface.getImage("/icons/KindsOfGamesAndUsersIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));			
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří klávesové zkratky k položkám v menu.
	 * 
	 * @param databaseItemsIcon
	 *            - Logkcká proměnná o tom, zda se mají přidat klávesové zkratky i k
	 *            položkám, které se vytvoří pouze v případě, že je přihlášen admin,
	 *            jinak tyto položky budou null.
	 */
	private void createKeyShortcut(final boolean databaseItemsIcon) {
		itemSelectGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		itemSetPathToDb.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));

		itemLogout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_DOWN_MASK));
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));

		itemBestResults.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK));

		itemUserInfo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_DOWN_MASK));
		itemResults.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));

		itemAboutApp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		itemAboutAuthor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));

		if (databaseItemsIcon) {
			itemUser.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
			itemGroup.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK));
			itemCity.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
			itemKindOfUser.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
			itemKindOfGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří tlačítka i pro manipulaci s DB a uživateli, tj. bude
	 * přihlášen admin, tak bude mít nějaké možnosti navíc.
	 */
	private void createComponentsForAdmin() {
		createBasicsComponents();
		
		// Tato položka je pouze pro admina:
		final JMenu menuDb = new JMenu("Databáze");
		
		// Položky, do menu pro manipulaci s DB:
		itemUser = new JMenuItem("Uživatelé");
		itemGroup = new JMenuItem("Skupiny slov");
		itemCity = new JMenuItem("Města");
		itemKindOfUser = new JMenuItem("Typy uživatelů");
		itemKindOfGame = new JMenuItem("Typy her");
		
		itemUser.setToolTipText("Otevře dialogové okno pro manipulaci s uživateli v DB.");
		itemGroup.setToolTipText("Otevře dialogové okno pro manipulaci se skupinami slovíče a slovíčkami v nich.");
		itemCity.setToolTipText("Otevře dialogové okno pro manipulaci s městy v DB.");
		itemKindOfUser.setToolTipText("Otevře dialogové okno pro manipulaci  s typy uživatelů v DB.");
		itemKindOfGame.setToolTipText("Otevře dialogové okno pro manipulaci s typy her v DB.");
		
		menuDb.add(itemUser);
		menuDb.addSeparator();
		menuDb.add(itemGroup);
		menuDb.addSeparator();
		menuDb.add(itemCity);
		menuDb.addSeparator();
		menuDb.add(itemKindOfUser);
		menuDb.addSeparator();
		menuDb.add(itemKindOfGame);
		
		
		itemUser.addActionListener(this);
		itemGroup.addActionListener(this);
		itemCity.addActionListener(this);
		itemKindOfUser.addActionListener(this);
		itemKindOfGame.addActionListener(this);
		
		
		add(menuDb);
	}




	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			// Položky v menuApp
			if (e.getSource() == itemSelectGame) {
				/*
				 * Pauuza se nastaví pouze v případě, že hra běží, což znamena, že timer běží a
				 * ukazuje nějaký čas. Pokud beběží, je buď null, nebo pozastaven, tak jako tak
				 * je to OK, nemusím dávat pauzu, jinak ano.
				 */
				if (app.getTimer() != null && app.getTimer().isRunning())
					app.setPause();
				
				final SelectedGame selectedGame = new SelectGameForm(DATA_MANAGER.getAllGroups(),
						DATA_MANAGER.getAllKindsOfGames(), app.getSelectedGame()).getGroup();
				
				if (selectedGame != null) {
					app.setSelectedGame(selectedGame);
					
					/*
					 * Note: Zde bych mohl uživateli ještě oznámit, že musí kliknout na tlačítko pro
					 * novou hru, aby se zahájila výše nastavená hra, ale už ji zde rovnou zahájím,
					 * aby to nemusel prodlužovat.
					 */
					app.newGame();
				}				
			}
			
			
			else if (e.getSource() == itemSetPathToDb) {
				/*
				 * Nejprve zjistím, zda běží hra, pokud ano, tak ji ukončím a nabídnu uložení.
				 * 
				 * Pak se otevře dialogové okno pro nastavení DB.
				 */
				// Zjistím, zda běží nějaká hra a zeptám se uživatele, zda se má uložit:
				app.checkRunningGameAndSave();
				
				DATA_MANAGER.setDbPath();
			}
			
			
			else if (e.getSource() == itemBestResults)
				/*
				 * zde bude pouze tabulka, kde se budou dat řadit vysledky od nejlepsiho nebo od
				 * nejhorsiho a dle typu hry
				 * 
				 * - takze do DB dat polozku, ze kdyz se ma uzivatel smazat, tak se pouze
				 * nastavi nejaka hodnota - treba deleted na true apod. pak to pri prihlaseni
				 * testovat
				 */
				new BestResultsForm(app.getUser().getKindOfUser().equals("Admin"));
			
			
			
			else if (e.getSource() == itemLogout) {
				// Zavřu okno aplikace:
				app.dispose();
				
				// zobrazím formulář pro přihlášení a až se uživatle přihlásí, tak se znovu otevře okno aplikace.
				SwingUtilities.invokeLater(Login::new);
			}
			
			
			else if (e.getSource() == itemClose) {
				// Zjistím, zda běží nějaká hra a zeptám se uživatele, zda se má uložit:
				app.checkRunningGameAndSave();
				
				// Zavření aplikace:
				System.exit(0);
			}

			
			
			
			
			// MenuUser
			else if (e.getSource() == itemUserInfo) {
				/*
				 * Normalní dialog, kde bude nahore uprostred zobrazen obrazek - oriznut,
				 * pak ty udaje
				 * 
				 * a dole upravit nebo smazat.
				 * 
				 * smazat je jasne, akorat si hlidat ta data, ktera je mozne smazat a ktera ne
				 * a upravit se zobrazi dialog pro editaci uzivatele napsat na to novy dialog
				 *  	- A DO TOHOTO DIALOGU I DOPLNIT MOZNOSTI PRO ADMINA, ABYCH HO MOHL POUZIT I NIZE V 
				 *  	DIALOGU - V TABULCE PRO UZIVATELE - EDITACI
				 */
				final UserInfo info = new UserInfoForm(app.getUser()).showInfo();
				
				if (info.isDeleted()) {
					// Zde byl účet smazán, tak vše pozavírám a znovu otevřu přihlašovací formulář
					app.dispose();
					new Login();
					return;
				}
					
				else if (info.getUser() != null)
					// Nastavím upraveného uživatelel do aplikace:
					app.setUser(info.getUser());
			}

			
			else if (e.getSource() == itemResults)
				/*
				 * dialog, kde budou dve tabulky, v prvni vysledky toho prvniho typu hry a v druhe tabulce 
				 * vysledky fruheho typu hry.
				 * 
				 * dat tabulky pod sebe
				 * 
				 * a pokud budou u hry Measurement zivoty vetsi nez nula, pak pujde obnovit
				 * a pokud budou u hry Countdown zivoty vetsi nez nula a cas, který ubehl (ne originalni) vetsi nez nula,
				 * pak pujde taky obnovit.
				 * 
				 * dale pujde upravit komentar a smazat vysledek a moznosti pro filtrovani vysledku dle datumu odehrani, 
				 * casu (hodin, minut i sekund zvlast, ten originalni a ubely jen kdyz bude oznacena tabulka pro COuntdown typ hry)
				 * a dle zivotu.
				 */
				new UserResultsForm(app.getUser().getId(), app);
			
			
			
			
			// MenuInfo:
			else if (e.getSource() == itemAboutApp)
				/*
				 * Klasicky dialog, kde budou informace o te hre, tabulky
				 * co se stane po kliknuti na jaka tlacitka,
				 * principy hry - kdyz se klikne ne nova hra, tak co se ma zvolit
				 * a popsat ty typy her.
				 * 
				 * a jak se hraje, ze se zvoli ta hra a pak se jen hadaji tlacitka, 
				 * taky popsat menu, co se kde da nastavitg apod.
				 */
				// Zobrazím dialog s informacemi o aplikaci:
				new AboutAppForm();
			
			else if (e.getSource() == itemAboutAuthor)
				// Zobrazím dialo s informacei o autorovi:
				new AboutAuthorForm();
			
			
			
			
			// Menu DB:
			else if (e.getSource() == itemUser)
				/*
				 * Otevře se dialog pro možnosti manipulace s uživateli v DB:
				 */
				new UserForm(app.getUser().getId());
			
			else if (e.getSource() == itemGroup)
				/*
				 * Ohledně možností asi udelat so same jako výše u uzivatelů, ale v tomto okne budou 2 tabulky:
				 * vlevo tabulka Groups, kde budou skupiny slov a vpravo bude tabulka s slovami dle oznacene skupiny
				 */
				new GroupWordsForm();

			
			else if (e.getSource() == itemCity)
				/*
				 * Pro tento dialog a dva dalsi asi udelat jeden dialog akorat s jinymi moznostmi ??? - DOMYSLET!
				 */
				new CitiesForm();

			
			
			else if (e.getSource() == itemKindOfUser)
				new KindOfGamesAndUsersForm("Typy uživatelů", EnumKindsOf.USERS);
			
			else if (e.getSource() == itemKindOfGame)
				new KindOfGamesAndUsersForm("Typy her", EnumKindsOf.GAMES);
		}
	}
}
