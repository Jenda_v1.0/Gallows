package kruncik.jan.gallows.dialogs.citiesForm;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.City;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudAbstractActions;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudMenu;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.KindOfInfoDialog;

/**
 * Tato třída slouží jako dialog, ve kterém je zobrazen přehled všech měst v DB.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CitiesForm extends CrudAbstractActions {

	
	private static final long serialVersionUID = 1L;


	

	/**
	 * Komponenta pro manipulaci s daty v DB.
	 */
	public static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku.
	 */
	private static final String[] TABLE_COLUMNS = {"PSČ", "Název města"};
	
	
	
	
	/**
	 * Jednorozměrné pole, které slouží jako model pro JComboBox, dle kterého se
	 * vyhledávají hodnoty v tabulce.
	 */
	private static final String[] CMB_SEARCH_MODEL = {"PSČ", "Město"};
	
	
	/**
	 * Jednoorzměné pole, které slouží jako model pro JComboBoxu, dle kterého se
	 * řadí hodnoty v tabulce.
	 */
	private static  final String[] CMB_ORDER_MODEL = { "Zobrazit vše", "PSČ - Sestupně", "PSČ - Vzestupně", "Města - Sestupně",
			"Města - Vzestupně" };
	
	
	
	/**
	 * List, který obsahuje veškerá města v DB.
	 */
	private static List<City> allCities;
	
	
	
	
	
	
	
	/**
	 * Model pro tabulku měst.
	 */
	private final CitiesTableModel tableModel;
	
	
	
	
	
	/**
	 * Tabulka měst.
	 */
	private final JTable table;
	
	
	
	
	/**
	 * Akce, která slouží pro vymazání všech měst v DB. Ale vymažou se pouze ta
	 * města, která nejsou aktuálně využívána.
	 */
	private AbstractAction deleteAllCitiesAction;
	
	
	
	
	
	
	public CitiesForm() {
		super("Města");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 870, 500, "/icons/CitiesFormIcon.png", true);
		
		
		allCities = DATA_MANAGER.getAllCities();
		
		
		final List<AbstractAction> abstractActionList = createAbstractActions();
		
		setJMenuBar(new CrudMenu(this, "Město", abstractActionList, KindOfInfoDialog.CITIES_DIALOG));
		
		
		
		
		tableModel = new CitiesTableModel(TABLE_COLUMNS, this);
		
		
		
		
		
		
		
		final JToolBar toolbarCrud = new JToolBar();
		toolbarCrud.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		toolbarCrud.add(createAction);
		toolbarCrud.add(updateAction);
		toolbarCrud.add(deleteAction);
		toolbarCrud.add(deleteAllCitiesAction);
		
		
		
		final JToolBar toolbarFilter = new JToolBar();
		toolbarFilter.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));		
		
		toolbarFilter.add(new JLabel("Seřadit dle: "));
		
		cmbOrder = new JComboBox<>(CMB_ORDER_MODEL);
		cmbOrder.addActionListener(Event -> {
			final String selectedItem = (String) cmbOrder.getSelectedItem();

			if (selectedItem.equals(CMB_ORDER_MODEL[0]))
				tableModel.setCitiesList(allCities);

			else if (selectedItem.equals(CMB_ORDER_MODEL[1]))
				tableModel.sortByPsc(false);

			else if (selectedItem.equals(CMB_ORDER_MODEL[2]))
				tableModel.sortByPsc(true);

			else if (selectedItem.equals(CMB_ORDER_MODEL[3]))
				tableModel.sortByName(false);

			else if (selectedItem.equals(CMB_ORDER_MODEL[4]))
				tableModel.sortByName(true);
		});
		toolbarFilter.add(cmbOrder);
		
		
		toolbarFilter.add(new JLabel("Vyhledat: "));
		
		cmbSearch = new JComboBox<>(CMB_SEARCH_MODEL);
		toolbarFilter.add(cmbSearch);
		
		
		createKeyAdapter();
		txtSearch = new JTextField(20);
		txtSearch.addKeyListener(keyAdapter);
		toolbarFilter.add(txtSearch);
		
				
		btnSearch = new JButton("Vyhledat");
		btnSearch.addActionListener(Event -> checkData());
		toolbarFilter.add(btnSearch);
		
		
		
		final JPanel pnlToolbars = new JPanel(new BorderLayout());
		pnlToolbars.add(toolbarCrud, BorderLayout.NORTH);
		pnlToolbars.add(toolbarFilter, BorderLayout.SOUTH);
		
		add(pnlToolbars, BorderLayout.NORTH);
				
		
		
		tableModel.setCitiesList(allCities);;
				
		table = new JTable(tableModel);
		createPopupMenuMouseAdapter(table, abstractActionList);
		
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		
		
		prepareWindow();
	}

	
	
	
 	
	
	
	
	
	/**
	 * Metoda, která přenačte data v tabulce (načte je znovu z DB) a oznčí původní
	 * řádek (pokd to půjde).
	 */
	final void reloadTableData() {
		// Uložím si označený řádek
		final int selectedRow = table.getSelectedRow();
		
		// přenačtu města z DB:
		allCities = DATA_MANAGER.getAllCities();
		
		// Vložím nová města do tabulky:
		tableModel.setCitiesList(allCities);
		
		// Pokud to jde, označím původní řádek (v případě smazání to vyjít nemusí, navíc
		// se označí ten jebližší, zálěží, jaký se smaže):
		if (selectedRow > -1 && selectedRow < table.getRowCount())
			table.setRowSelectionInterval(selectedRow, selectedRow);
	}
	
	
	
	

	
	


	

	@Override
	protected void checkData() {
		/*
		 * - Zda neni pole prazdne
		 * - Zda odpovida regulárnímu vyrazu dle zvolené typu vyhledavani
		 */
		
		final String searchValue = txtSearch.getText();
		
		if (searchValue.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Není zadáno klíčové slovo pro vyhledání!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		final String selectedItem = (String) cmbSearch.getSelectedItem();		
		
		
		// Vyhledávání dle PSČ:
		if (selectedItem.equals(CMB_SEARCH_MODEL[0])) {
			if (searchValue.matches(REG_EX_PSC)) {
				final int psc = Integer.parseInt(searchValue.replaceAll("\\s", ""));
				
				tableModel
						.setCitiesList(allCities.stream().filter(c -> c.getPsc() == psc).collect(Collectors.toList()));
			} else
				JOptionPane.showMessageDialog(this, "Zadané PSČ neodpovídá požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}
		
		// Vyhledávání dle názvu města:		
		else if (selectedItem.equals(CMB_SEARCH_MODEL[1])) {
			if (searchValue.matches(REG_EX_CITY_NAME)) {
				tableModel.setCitiesList(
						allCities.stream().filter(c -> c.getName().toUpperCase().contains(searchValue.toUpperCase()))
								.collect(Collectors.toList()));
			} else
				JOptionPane.showMessageDialog(this, "Zadaný název města neodpovídá požadované syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}
	}


	
	


	@Override
	protected List<AbstractAction> createAbstractActions() {
		
		createAction = new AbstractAction("Přidat město", GetIconInterface.getImage("/crud/CreateIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				final City newCity = new CityForm("Vytvořit", null, DATA_MANAGER.getAllPsc()).getCity();
				
				if (newCity != null) {
					final boolean created = DATA_MANAGER.addCity(newCity);

					if (!created) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o přidání nového města do DB. Město neblo vytvořeno!",
								"Chyba", JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					reloadTableData();
				}
			}
		};
		
		
		
		updateAction= new AbstractAction("Upravit město", GetIconInterface.getImage("/crud/EditIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				final int selectedRow = table.getSelectedRow();
				
				if (selectedRow > -1) {
					final City selectedCity = tableModel.getCityAtIndex(selectedRow);

					final City editedCity = new CityForm("Upravit", selectedCity, DATA_MANAGER.getAllPsc()).getCity();

					if (editedCity != null) {
						final boolean updated = DATA_MANAGER.updateCity(editedCity);

						if (!updated) {
							JOptionPane.showMessageDialog(null,
									"Došlo k chybě při pokusu o aktualizaci města v DB. Město neblo aktualizováno!",
									"Chyba", JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						reloadTableData();
					}
				} else
					JOptionPane.showMessageDialog(null, "Není označeno žádné město pro editaci!", "Neoznačeno",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		deleteAction = new AbstractAction("Smazat město", GetIconInterface.getImage("/crud/DeleteIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Získám si označené město, zjistím, zda dané PSČ nění využito a pokud není,
				 * smažu jej, pokud je využito, vyhodím oznámení, že nelze smazat dané město.
				 */
				final int selectedRow = table.getSelectedRow();

				if (selectedRow > -1) {
					if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
							"Opravdu chcete smazat označené město?", "Smazat město", JOptionPane.YES_NO_OPTION)) {
						final City selectedCity = tableModel.getCityAtIndex(selectedRow);

						final boolean isPscUsed = DATA_MANAGER.getAllUsers().stream()
								.anyMatch(u -> u.getPsc() == selectedCity.getPsc());

						if (isPscUsed) {
							JOptionPane.showMessageDialog(null, "Označené město je využito, nelze smazat!",
									"Město je využito", JOptionPane.ERROR_MESSAGE);
							return;
						}

						// Zdemohu smazat město (Zde není využito):
						final boolean deleted = DATA_MANAGER.deleteCity(selectedCity.getPsc());

						if (!deleted) {
							JOptionPane.showMessageDialog(null, "Došlo k chybě při pokusu o smazání města '"
									+ selectedCity.getName() + "'. Město nebylo smazáno", "Chyba",
									JOptionPane.ERROR_MESSAGE);
							return;
						}

						reloadTableData();
					}

				} else
					JOptionPane.showMessageDialog(null, "Není označeno žádné město pro smazání!", "Neoznačeno",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		
		
		deleteAllCitiesAction = new AbstractAction("Smazat všechna města",
				GetIconInterface.getImage("/icons/DeleteAllCitiesIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				final List<City> allCitiesList = DATA_MANAGER.getAllCities();
				
				final List<User> allUserList = DATA_MANAGER.getAllUsers();
				
				// Nejprve zjistím, zda se nějaké město používá, tj. zda jej má nějaký uživatel
				// nastaven (psč daného města).
				
				final boolean isSomeCityUsed = allCitiesList.stream()
						.anyMatch(c -> allUserList.stream().anyMatch(u -> u.getPsc() == c.getPsc()));
				
				if (isSomeCityUsed && JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
						"Alespoň jedno město je využíváno, nelze smazat všechna města. Přejete si smazat nepoužívaná města?",
						"Smazat města", JOptionPane.YES_NO_OPTION)) {
					/*
					 * Zde chce uživatel smazat všechna města, která nejsou využívána:
					 * 
					 * Stačí projít všechna města, a zjistit, zda je využíváno uživatelem nebo ne.
					 * Pokud ne, může se smazat.
					 */
					
					allCitiesList.forEach(c -> {
						if (!allUserList.stream().anyMatch(u -> u.getPsc() == c.getPsc())) {
							// Zde není město využíváno, tak je možné jej smazat:
							final boolean deletedCity = DATA_MANAGER.deleteCity(c.getPsc());

							if (!deletedCity) {
								JOptionPane.showMessageDialog(null, "Došlo k chybě při pokusu o smazání města '"
										+ c.getName() + "'. Město nebylo smazáno!", "Chyba", JOptionPane.ERROR_MESSAGE);
								return;
							}
						}
					});
				}
				
				
				else if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
						"Opravdu chcete smazat všechna města?", "Smazat města", JOptionPane.YES_NO_OPTION)) {
					// Zde se žádné město nevyužívá, tak je možné smazat všechna:
					final boolean deleted = DATA_MANAGER.deleteAllCities();
					
					if (!deleted) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o smazání všech měst. Města nebyla smazána!", "Chyba",
								JOptionPane.ERROR_MESSAGE);
						return;
					}				
				}
				
				allCities = DATA_MANAGER.getAllCities();
				
				tableModel.setCitiesList(allCities);
			}
		};
		
		
		
		return new ArrayList<>(Arrays.asList(createAction, updateAction, deleteAction));
	}
}
