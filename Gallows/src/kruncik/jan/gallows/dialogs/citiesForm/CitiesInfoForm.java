package kruncik.jan.gallows.dialogs.citiesForm;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako dialog, který obsahuje informace o dialogu CitiesForm
 * -> dialog pro manipulaci s daty ohledně měst -> tabulka Cities.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CitiesInfoForm extends SourceDialog {
	
	private static final long serialVersionUID = 1L;

	
	public CitiesInfoForm() {
		super("Info");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 500, 500, "/icons/InfoIconForm.png", true);
		
		final JTextArea txaInfo = new JTextArea("Dialog slouží pro CRUD operace nad městami v DB.\n"
				+ "Je možné zobrazit veškerá města v DB, dále je seřadit a vyhledat podle různých kritérií.\n\n"
				+ ""
				+ "Údaje je možné upravit přímo v tabulce dvojlikem levým tlačítka myši na políčku v tabulce, které chceme upravit, nebo na řádek kliknout prvým tlačítkem myši a zvolit operaci nebo označit město v tabulce a zvolit operaci v toolbaru nebo v menu Město.");
		
		txaInfo.setEditable(false);
		
		txaInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		
		txaInfo.setLineWrap(true);
		
		add(new JScrollPane(txaInfo), BorderLayout.CENTER);
		
		
		prepareWindow();
	}

	@Override
	protected void checkData() {
	}
}
