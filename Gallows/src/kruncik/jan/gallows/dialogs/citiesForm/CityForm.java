package kruncik.jan.gallows.dialogs.citiesForm;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import kruncik.jan.gallows.db_objects.City;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako dialog pro vytvoření nebo editaci Města (řádku v
 * tabulce Cities)
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CityForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;



	// Textová pole pro zadání PSČ a názvu města.
	private final JTextField txtPsc, txtName;
	
	
	
	/**
	 * Hodnota v této proměné se vrátí v metodě getCity(), buď bude null nebo v ní
	 * bude město, které se má vytvořit / upravit.
	 */
	private City city;
	
	
	/**
	 * List, který obsahuje všechna PSČ, aby se zjistilo, zda uživatel nezadal
	 * duplicitní PSČ.
	 */
	private final List<Integer> allPsc;
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param title
	 *            - Titulek dialog a text v tlačítko pro potvrzení hodnot (Vytvořit
	 *            / Upravit).
	 * 
	 * @param city
	 *            - Město, které bude buď null, pak se má vytvořit nové pomocí
	 *            tohoto dialogu nebo v něm bude reference na nějaé město, pak se má
	 *            upravit dané město.
	 * 
	 * @param allPsc
	 *            - list se všemi PSČ, aby se mohlo zjistit, zda se nezadala nějaká
	 *            duplicitní hodnota.
	 */
	CityForm(final String title, final City city, final List<Integer> allPsc) {
		super(title);
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 220, "/icons/CityFormIcon.jpg", true);
		
		addWindowListener(new MyWindowAdapter());
		
		createKeyAdapter();
		
		this.city = city;
		this.allPsc = allPsc;
		
		
		final GridBagConstraints gbc = createGbc();
		
		add(new JLabel("*PSČ: "), gbc);
		
		
		txtPsc = new JTextField(20);
		txtPsc.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 0);
		add(txtPsc, gbc);

		
		
		
		
		setGbc(gbc, 0, 1);
		add(new JLabel("*Název města: "), gbc);
		
		txtName = new JTextField(20);
		txtName.addKeyListener(keyAdapter);
		
		setGbc(gbc, 1, 1);
		add(txtName, gbc);
		
		
		
		
		
		// Potenciální naplnění hodnot do polí:
		if (city != null) {
			txtPsc.setText(city.getPscWithSpace());
			txtName.setText(city.getName());
		}
		
		
		
		
		
		
		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText(title);
		btnConfirm.addActionListener(Event -> checkData());
		
		btnCancel.addActionListener(Event -> {
			this.city = null;
			dispose();
		});
		
		gbc.gridwidth = 2;
		setGbc(gbc, 0, 2);
		add(pnlButtons, gbc);
	}

	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí toto okno a po potvrzení nebo zavření dialog vrátí
	 * hodnotu, která bude v proměnné city.
	 * 
	 * @return - null, nebo nově vytvoření či upravené město v dialogu.
	 */
	final City getCity() {
		prepareWindow();
		
		return city;
	}
	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * - Zda není nějaké pole prázdné
		 * - Zda odpovídají regulárním výrazům
		 * - Délka znaků
		 * - Zda Není duplicitní PSČ
		 * - Pak vytvořím nové město nebo nastavím nové hodnoty do existujícího a zavřu dialog
		 */
		
		final String psc = txtPsc.getText();
		
		final String name = txtName.getText();
		
		if (psc.isEmpty() || name.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Alespoň jednoz požadovaných polí je prázdné!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!psc.matches(REG_EX_PSC)) {
			JOptionPane.showMessageDialog(this, "Zadané PSČ neodpovídá požadované syntaxi!", "Chyba syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		// Na počet znaků stačí testovat pouze název města u intu to navdí a už
		// regulární výrazí mají jen 5 číslic.
		if (name.length() > 100) {
			JOptionPane.showMessageDialog(this, "Zadaný název města přesahuje maximální počet znaků!", "Překročen počet znaků",
					JOptionPane.ERROR_MESSAGE);			
			return;
		}

		if (!name.matches(REG_EX_CITY_NAME)) {
			JOptionPane.showMessageDialog(this, "Zadaný název města neodpovídá požadované syntaxi!", "Chyba syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		
		
		final int pscInt = Integer.parseInt(psc.replaceAll("\\s", ""));

		final boolean existPscInDb = allPsc.stream().anyMatch(p -> p == pscInt);

		
		if (existPscInDb) {
			JOptionPane.showMessageDialog(this, "Zadané PSČ se již v DB nachází, zvolte prosím jiné!", "Duplicitní PSČ",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		if (city != null) {
			city.setNewPsc(pscInt);
			city.setName(name);
		}
		
		else city = new City(pscInt, name);
		
		
		
		
		dispose();
	}
	
	
	
	
	
	
	
	/**
	 * Tato třída sloužáí pouze pro to, když se zavře okno dohoto dialogu, tak aby
	 * se nastavila proměnná city na null hodnotu.
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	private class MyWindowAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			/*
			 * Tato metoda se zavolá v případě, že uživatelzavře dialog kliknutím an křížek,
			 * pak musím nastavit proměnnou city na null, aby se nevrátilo nějaké město a
			 * nevytvořilo / neaktualizovalo se v DB.
			 */
			city = null;		
		}
	}
}
