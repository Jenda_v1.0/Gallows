package kruncik.jan.gallows.dialogs.citiesForm;

import java.util.Comparator;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import kruncik.jan.gallows.db_objects.City;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží pouze jako model pro tabulku, kde je zobrazen přehled měst
 * v DB.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CitiesTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	

	/**
	 * List, který obsahuje veškerá města v DB.
	 */
	private List<City> citiesList;
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku.
	 */
	private final String[] columns;
	
	
	/**
	 * Reference na instanci dialogu, kde se nachází tabulka, která obshauje tento
	 * tableModel. Potřebuji ji zde, protože když se aktualizují data v DB, tak chci
	 * aktualizovat i tabulku:
	 */
	private final CitiesForm citiesForm;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param columns
	 *            - Jednorozměrné pole typu String, které obsahuje názvy sloupců pro
	 *            tabulku.
	 */
	CitiesTableModel(final String[] columns, final CitiesForm citiesForm) {
		super();
				
		this.columns = columns;
		this.citiesForm = citiesForm;
	}
	
	
	
	/**
	 * Setr na list hodnot (měst) v tabulce.
	 * 
	 * @param citiesList
	 *            - list s městy v DB, které se mají zobrazit v tabulce.
	 */
	void setCitiesList(List<City> citiesList) {
		this.citiesList = citiesList;
		
		fireTableDataChanged();
	}
	
	
	
	/**
	 * Metoda, která vrátí město na zadaném indexu v listu (v tabulce).
	 * 
	 * @param index
	 *            - Index, ve kterém se má vzít město a vrátit jej (v listu - v
	 *            tabulce)
	 * 
	 * @return město na zadaném indexu.
	 */
	City getCityAtIndex(final int index) {
		return citiesList.get(index);
	}
	
	
	
	
	
	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return citiesList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return citiesList.get(rowIndex).getPscWithSpace();
			
		case 1:
			return citiesList.get(rowIndex).getName();

		default:
			break;
		}
		
		return "?";
	}
	
	
	
	@Override
	public String getColumnName(int columnIndex) {
		return columns[columnIndex];
	}
	
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object objValue = getValueAt(0, columnIndex);
		
		if (objValue != null)
			return objValue.getClass();
		
		return super.getColumnClass(columnIndex);
	}
	
	
	
	
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}


    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final City city = citiesList.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
				/*
				 * - Aby nebylo pole prázdné
				 * - splnovalo regularni vyraz
				 * - delku retezce (int)
				 * - nebyly duplicity
				 */
			final String psc = (String) aValue;			
			
			// Nejprve ještě otestuji, zda zadané psc splňuje syntaxi dle
			// reguálrního výrazu:
			if (!psc.matches(SourceDialog.REG_EX_PSC)) {
				JOptionPane.showMessageDialog(null, "Zadané PSČ nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}

			// Prázdný text:
			if (psc.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Pole pro PSČ nesmí být prázdné!", "Prázdné pole",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

						
			/*
			 * Note:
			 * U přetypování bych mohl mít zachytávání vyjímky, kdyby se to nepovedlo,
			 * ale díky regulárnímu výrazu vím, že je to OK.
			 */
			final int pscInt = Integer.parseInt(psc.replaceAll("\\s", ""));
			
			final boolean existPsc = CitiesForm.DATA_MANAGER.getAllPsc().stream().anyMatch(p -> p == pscInt);
			
			if (existPsc) {
				JOptionPane.showMessageDialog(null, "Zadané PSČ již existuje, zvolte prosím jiné!", "Duplicitní PSČ",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			city.setNewPsc(pscInt);
			break;
			
			
		case 1:
			final String cityName = (String) aValue;			
			
			// Nejprve ještě otestuji, zda zadané psc splňuje syntaxi dle
			// reguálrního výrazu:
			if (!cityName.matches(SourceDialog.REG_EX_CITY_NAME)) {
				JOptionPane.showMessageDialog(null, "Zadaný název města nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}

			// Prázdný text:
			if (cityName.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Pole pro název města nesmí být prázdné!", "Prázdné pole",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			if (cityName.length() > 100) {
				JOptionPane.showMessageDialog(null, "Název města překročil limit maximálního počtu znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			city.setName(cityName);
			
			break;

		default:
			break;
		}
		
		// Aktualizuji město v DB:
		updateCityInDb(city);
		
		// Přenačtu tabulku:
		citiesForm.reloadTableData();
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která aktualizuje město v DB.
	 * 
	 * @param city
	 *            - Město, které se má aktualizovat v DB.
	 */
	private static void updateCityInDb(final City city) {
		final boolean updated = CitiesForm.DATA_MANAGER.updateCity(city);
		
		if (!updated)
			JOptionPane.showMessageDialog(null, "Došlo k chybě při pokusu o aktualizaci města '" + city.getName()
					+ "'. Město nebylo aktualizováno!", "Chyba", JOptionPane.ERROR_MESSAGE);	
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí položky v tabulce dle hodnoty PSČ sestupně nebo
	 * vzestupně dle hodnoty asc v prametru této metody.
	 * 
	 * @param asc
	 *            - logická hodnota o tom, zda se mají hodnoty v list citiesList
	 *            (města v tabulce) seřadit dle PSČ sestupně nebo vzestupně. True =
	 *            vzestupně, false = sestupně.
	 */
	final void sortByPsc(final boolean asc) {
		if (asc)
			citiesList.sort(Comparator.comparingInt(City::getPsc));
		else
			citiesList.sort((a, b) -> Integer.compare(b.getPsc(), a.getPsc()));
		
		fireTableDataChanged();
	}
	
	
	
	
	
	/**
	 * Metoda, která seřadí položky v tabulce dle názvu města sestupně nebo
	 * vzestupně dle hodnoty asc v prametru této metody.
	 * 
	 * @param asc
	 *            - logická hodnota o tom, zda se mají hodnoty v list citiesList
	 *            (města v tabulce) seřadit dle názvu města sestupně nebo vzestupně.
	 *            True = vzestupně, false = sestupně.
	 */
	final void sortByName(final boolean asc) {
		if (asc)
			citiesList.sort(Comparator.comparing(City::getName));
		else
			citiesList.sort((a, b) -> b.getName().compareTo(a.getName()));
		
		fireTableDataChanged();
	}
}
