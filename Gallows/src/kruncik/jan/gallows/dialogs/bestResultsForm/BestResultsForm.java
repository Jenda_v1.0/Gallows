package kruncik.jan.gallows.dialogs.bestResultsForm;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.Game;
import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudAbstractActions;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudMenu;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.KindOfInfoDialog;
import kruncik.jan.gallows.dialogs.userResultsForm.MyTableButtonMouseListener;
import kruncik.jan.gallows.dialogs.userResultsForm.MyTableButtonRenderer;

/**
 * Tento dialog slouží pouze pro zobrazení / reprezentaci výsledků. 
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class BestResultsForm extends CrudAbstractActions {
		
	
	private static final long serialVersionUID = 1L;


	/**
	 * Komponenta pro mannipulaci (v tomto případě pouze pro získávání) dat z DB.
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku.
	 */
	private static final String[] ALL_COLUMNS = { "Pseudonym", "Typ hry", "Skupina slovíček", "Datum vytvoření",
			"Nastavený / zbývající čas", "Skóre" };
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku včetně textu pro
	 * sloupec "Smazat" -> pro smazání hry na daném řádku.
	 */
	private static final String[] ALL_COLUMNS_WITH_DELETE_TEXT = { "Pseudonym", "Typ hry", "Skupina slovíček",
			"Datum vytvoření", "Nastavený / zbývající čas", "Skóre", "Smazat" };
	
	
	
	// Následují komponenty pro filtrování obsahu tybulky.
	
	private static final String[] filterData = {"Sestupně", "Vzestupně", "Dle abecedy", "Dle skupiny"};

	/**
	 * Takový "základní" filtr, kde je možné si vybrat nějaké možnosti filtrování.
	 */
	private final JComboBox<String> cmbFilter;
	
	/**
	 * Komponenta pro výběr typu hry (Measurement, Countdown, vše)
	 */
	private JComboBox<String> cmbKindOfGame;
	
	
	

	
	/**
	 * Model pro cmb, ve kterém půjde zvolit pouze dané sloupce v tomto poli, ze kterých může uživatel
	 * filtrovat výsledky.
	 */
	private static final String[] searchColumntModel = {ALL_COLUMNS[0], ALL_COLUMNS[5]};
	
	/**
	 * Komponenta, ve které se vybere sloupec, ze kterého se budou filtrovat data
	 * dle zadaného textu nebo skóre.
	 */
	private JComboBox<String> cmbFilterColumns;

	
	
	
	
	
	
	
	/**
	 * Tento list bude obsahovat veškerá data, která se budou zobrzovat v tabulce,
	 * tento list se naplní v konstruktoru třídy tak, že se projde list se všemy odehranými hrami,
	 * a doplní se k ní pseudonym a předají se zformátované hodnoty do tabulky.
	 * A tyto hodnoty se vloží pak do tabulky, případně nějak vyfiltrují apod.
	 */
	private static final List<TableValue> ALL_DATA_TO_TABLE = new ArrayList<TableValue>();
	
	
	
	
	
	
	
	// deleteAction -> Akce pro vymazání označenéh hry - výsledku hry
	// deleteMeasurementsAction -> Akce pro vymazání všech her typu Measurements
	// deleteCountdownsAction -> Akce pro vymazání všech her typu Countdown
	// deleteAllAction -> Akce, která vymaže všechny odehrané hry z DB.
	private AbstractAction deleteAction, deleteMeasurementsAction, deleteCountdownsAction, deleteAllAction;
	
	
	
	
	
	
	
	/**
	 * Table model pro tabulku výsledků.
	 */
	private BestResultsTableModel tableModel;
	
	
	
	/**
	 * Tabulka pro hry - výsledky odehraných her.
	 */
	private final JTable table;
	
	
	

	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param isLogginedAdmin
	 *            - logická proměnná o tom, zda je přihlášen admin nebo ne. Pokud
	 *            ano, zobrazí se navíc menu a toolbar s tlačítky pro možnosti
	 *            smazání her.
	 */
	public BestResultsForm(final boolean isLogginedAdmin) {
		super("Nejlepší výsledky");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 1080, 500, "/icons/ResultsIconForm.png", true);
		
		
		
		
		JToolBar toolBarDeleteActions = null;
		
		if (isLogginedAdmin) {
			setJMenuBar(new CrudMenu(this, "Smazat", createAbstractActions(), KindOfInfoDialog.BEST_RESULTS_DIALOG));
			
			toolBarDeleteActions = new JToolBar();
			toolBarDeleteActions.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
			
			toolBarDeleteActions.add(deleteAction);
			toolBarDeleteActions.add(deleteMeasurementsAction);
			toolBarDeleteActions.add(deleteCountdownsAction);
			toolBarDeleteActions.add(deleteAllAction);
			
		}
			
		
		
		
		
		final JToolBar toolBar = new JToolBar(JToolBar.HORIZONTAL);
		toolBar.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		toolBar.add(new JLabel("Typ hry: "));
		
		// Vytvoření a vložení JComboBoxu pro výběr typu hry
		createCmbKindOfGame();
		toolBar.add(cmbKindOfGame);
		
		
		toolBar.add(new JLabel("Filtr: "));
		
		// Takový "základní" filtr pro filtrování výsledků:
		cmbFilter = new JComboBox<>(filterData);
		cmbFilter.addActionListener(Event -> {
			final String selectedItem = (String) cmbFilter.getSelectedItem();
			
			if (selectedItem.equals(filterData[0]))
				tableModel.sortByScore(false);
			
			else if (selectedItem.equals(filterData[1]))
				tableModel.sortByScore(true);
			
			else if (selectedItem.equals(filterData[2]))
				tableModel.sortByPseudonym();
			
			else if (selectedItem.equals(filterData[3]))
				tableModel.sortByGroupName();
		});
		toolBar.add(cmbFilter);
		
		
		toolBar.add(new JLabel("Vyhledávání: "));
		
		// Panl pro vyhledávání dat v tabulce dle zadaného kritéria:
		toolBar.add(createPnlSearch());		
		
		
		
		// Penel s toolbary do okna:
		if (isLogginedAdmin) {
			final JPanel pnlToolbars = new JPanel(new BorderLayout());
			pnlToolbars.add(toolBarDeleteActions, BorderLayout.NORTH);
			pnlToolbars.add(toolBar, BorderLayout.SOUTH);
			
			add(pnlToolbars, BorderLayout.NORTH);
		}
		
		else add(toolBar, BorderLayout.NORTH); 

		
		
		
		if (isLogginedAdmin)
			tableModel = new BestResultsTableModel(ALL_COLUMNS_WITH_DELETE_TEXT, this);
		else
			tableModel = new BestResultsTableModel(ALL_COLUMNS, this);
		
		
		fillAllDataToTableList();
		tableModel.setTableValuesList(ALL_DATA_TO_TABLE);
		
		table = new JTable(tableModel);		
		add(new JScrollPane(table), BorderLayout.CENTER);											
		
		
		if (isLogginedAdmin)
			createPopupMenuMouseAdapter(table, new ArrayList<>(Collections.singletonList(deleteAction)));
		
				
		
		/*
		 * Komponenta pro vykreslení objektů v tabulce, nastavím jí, aby se vykreslovaly
		 * texty na střed buňky v tabulce:
		 */
		final DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(SwingUtilities.CENTER);
		
		for (int i = 0; i < table.getColumnCount(); i++)			
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		
		
		
		
		
		
		if (isLogginedAdmin) {
			// Aby tlačítko v tabulce reagovala na kliknutí:
			table.addMouseListener(new MyTableButtonMouseListener(table));
			
			// Poslední sloupec je tlačítko pro smazání hry - výsledku:
			table.getColumnModel().getColumn(ALL_COLUMNS_WITH_DELETE_TEXT.length - 1)
			.setCellRenderer(new MyTableButtonRenderer());
		}
		
		
		
		
		prepareWindow();
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která naplní list se všemi hodnotami pro tabulku.
	 */
	private static void fillAllDataToTableList() {
		final List<Game> allGamesList = DATA_MANAGER.getAllGames();
		
		final List<User> allUsersList = DATA_MANAGER.getAllUsers();
		
		final List<Group> allGroupsList = DATA_MANAGER.getAllGroups();
		
		ALL_DATA_TO_TABLE.clear();
		
		
		allGamesList.forEach(a -> {
			if (a.getKindOfGame().equals("Measurement"))
				ALL_DATA_TO_TABLE.add(new TableValue(a.getId(), getUserPseudonymById(allUsersList, a.getUserId()),
						a.getKindOfGame(), getGroupNameById(allGroupsList, a.getGroupId()), a.getScore(),
						a.getFormatedDate(), a.getFormatedTime()));

			else if (a.getKindOfGame().equals("Countdown"))	// Podmínka by nemusel být, stačí else
				ALL_DATA_TO_TABLE.add(new TableValue(a.getId(), getUserPseudonymById(allUsersList, a.getUserId()),
						a.getKindOfGame(), getGroupNameById(allGroupsList, a.getGroupId()), a.getScore(),
						a.getFormatedDate(), a.getFormattedOriginalTime()));
		});
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří panel pro filtrování dle zadaného kritérie do textového
	 * pole. Například dle pseudonymu nebo dle skóre.
	 * 
	 * @return panel s komponentami pro filtrování dat v tabulce (JComboBox, textové
	 *         pole a tlačítko pro vyhledání).
	 */
	private JPanel createPnlSearch() {
		final JPanel pnlSearch = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		
		cmbFilterColumns = new JComboBox<>(searchColumntModel);
		pnlSearch.add(cmbFilterColumns);
		
		
		createKeyAdapter();
		txtSearch = new JTextField(20);
		txtSearch.addKeyListener(keyAdapter);
		pnlSearch.add(txtSearch);
		
		
		btnSearch = new JButton("Vyhledat");
		// Na tlačítko pridám akci, která vyfiltruje zadaná data dle zvoleného kritérie
		btnSearch.addActionListener(Event -> checkData());		
		pnlSearch.add(btnSearch);
		
		
		return pnlSearch;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která prohledá list uživatelů načtený z DB - abych nemusel pořád
	 * načítat uživatele z DB, tak jsem si je vložil do listu při staru tohoto
	 * dialogu.
	 * 
	 * Metoda prohledá list s uživateli a vrátí pseudonym uživatele s daným ID.
	 * Nemělo by dojít k žádné chybě. Tj. nemělo by se vrátit null.
	 * 
	 * @param usersList
	 *            - list s uživateli, ze kterého se vezme pseudonym - od uživatele s
	 *            požadovaným ID.
	 * 
	 * @param id
	 *            - ID uživatele u kterého se má vrátit pseudonym
	 * 
	 * @return pseudonymuživatele nebo null -> toto by nastat nemělo.
	 */
	private static String getUserPseudonymById(final List<User> usersList, final int id) {
		return usersList.stream().filter(f -> f.getId() == id).collect(Collectors.toList()).get(0).getPseudonym();
	}
	
	
	
	
	
	
	/**
	 * Metoda, která vrátí název skupniy s daným id.
	 * 
	 * @param groupsList
	 *            - list všech skupin.
	 * @param id
	 *            - ID skupiny, u které se má vrátit její název
	 * @return název skupiny s daným id, nebo null -> nemělo by nastat.
	 */
	private static String getGroupNameById(final List<Group> groupsList, final int id) {
		return groupsList.stream().filter(f -> f.getId() == id).collect(Collectors.toList()).get(0).getGroupName();
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoření instanci JComboBoxu pro výběr typu hry a naplní jej
	 * potřebnými pložkami.
	 */
	private void createCmbKindOfGame() {
		final List<KindOfGame> kindsOfGamesList = DATA_MANAGER.getAllKindsOfGames();
		
		final String[] array = new String[kindsOfGamesList.size() + 1];		
		array[0] = "Vše";
		
		for (int i = 0; i < kindsOfGamesList.size(); i++)
			array[i + 1] = kindsOfGamesList.get(i).getKind();
		
		
		cmbKindOfGame = new JComboBox<>(array);
		
		/*
		 * Nastavím listener na tento JComboBox tak, aby když se změní položka, tak se
		 * vyfiltrují položky dle zvoleného typu nebo všechny položky (hry) a ty se
		 * nastaví do tableModelu - do tabulky.
		 */
		cmbKindOfGame.addActionListener(Event -> {
			final String selectedItem = (String) cmbKindOfGame.getSelectedItem();

			switch (selectedItem) {
				case "Vše":
					tableModel.setTableValuesList(ALL_DATA_TO_TABLE);
					break;
				case "Countdown":
					tableModel.setTableValuesList(filterValuesByKindOfGame("Countdown"));
					break;
				case "Measurement":
					tableModel.setTableValuesList(filterValuesByKindOfGame("Measurement"));
					break;
			}
		});
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří nový list, do kterého vloží pouze ty hry, které
	 * odpovídají zvolenému typu (COuntdown nebo Measurement, případně obojí pod
	 * položkou "Vše").
	 * 
	 * @param kind
	 *            - typ hry, které se mají vyfiltrovat
	 * @return list her typu kind
	 */
	private static List<TableValue> filterValuesByKindOfGame(final String kind) {
		return ALL_DATA_TO_TABLE.stream().filter(f -> f.getKindOfGame().equals(kind)).collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	

	@Override
	protected void checkData() {
		/*
		 * Otestuji, zda není prázdné pole pro zadání kritérie.
		 */
		if (txtSearch.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Nebylo zadáno žádné kritérium!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
			

		/*
		 * Otestuji, zda je zvoleno vyhledávání dle Skóre nebo pseudonymu (uživatelského
		 * jména) uživatele.
		 * 
		 * - Otestuji zadaný text dle regulárního výrazu
		 * - Vyfiltruji všechny hry, kde se shoduje zadané skóre.
		 */
		if (cmbFilterColumns.getSelectedItem().equals(ALL_COLUMNS[0])) {
			if (!txtSearch.getText().matches(REG_EX_PSEUDONYM)) {
				JOptionPane.showMessageDialog(this, "Zadané kritérium neodpovídá požadované syntaxi",
						"Chyba syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			/*
			 * Zde se vyhledává dle pseudonymu, tak zkusím vyfiltrovat data a nastavit do tableModelu 
			 * vyhledaná data, kde se shoduje pseudonym uživatele.
			 */
			
			final String pseudonym = txtSearch.getText();
			
			final List<TableValue> filterDataList = ALL_DATA_TO_TABLE.stream()
					.filter(f -> f.getPseudonym().toUpperCase().contains(pseudonym.toUpperCase()))
					.collect(Collectors.toList());

			tableModel.setTableValuesList(filterDataList);
		}

		// Zde se jedná o vyhledávání dle skore
		else if (cmbFilterColumns.getSelectedItem().equals(ALL_COLUMNS[5])) {
			if (!txtSearch.getText().matches(REG_EX_SCORE)) {
				JOptionPane.showMessageDialog(this, "Zadané kritérium neodpovídá požadované syntaxi",
						"Chyba syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			/*
			 * Zde se jedná o vyhledávání dle skore, tak do table modelu vložím pouze data, kde jsou
			 * požadovaná skóre:
			 */
			final int score = Integer.parseInt(txtSearch.getText().replaceAll("\\s", ""));			
			
			final List<TableValue> filterDataList = ALL_DATA_TO_TABLE.stream().filter(f -> f.getScore() == score)
					.collect(Collectors.toList()); 
			
			tableModel.setTableValuesList(filterDataList);
		}
	}


	
	
	
	
	
	
	
	/**
	 * Metoda, která slouží pro vymazání označené hry z DB a přenačtení hodnot.
	 * 
	 * Info: 
	 * Stačilo by pouze smazat tu jednu položku z listu, ale raději si
	 * přenačtu znovu data z DB.
	 * 
	 * @param rowIndex
	 *            - Index řádku, resp. index položky v listu v tabulce, která se má
	 *            smazat.
	 */
	final void deleteSelectedGame(final int rowIndex) {
		if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, "Opravdu si přejete smazat označenou hru?", "Smazat", JOptionPane.YES_NO_OPTION)) {
			final boolean deleted = DATA_MANAGER.deleteGame(tableModel.getValueAtIndex(rowIndex).getGameId());
			
			if (!deleted) {
				JOptionPane.showMessageDialog(this, "Došlo k chybě při pokusu o smazání hry. Hry nebyla smazána!", "Chyba", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			tableModel.deleteValueAtIndex(rowIndex);;
			
			fillAllDataToTableList();
		}
	}
	
	
	
	
	






	@Override
	protected List<AbstractAction> createAbstractActions() {
		/*
		 * Info:
		 * U metod pro mazání her by bylo možné načíst si všechny hry v DB, pak jej
		 * proiterovat cyklem a kde se napříiklad shoduje typ hry nebo DI pro smazání
		 * apod. Tak tu hru smazat, ale napsal jsem metody přímo pro smazání nad Db,
		 * abych si zde ušetřil cyklus a nechal trochu přehlednější kód.
		 */
		
		deleteAction = new AbstractAction("Smazat",
				GetIconInterface.getImage("/crud/results/DeleteResultIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				final int selectedRow = table.getSelectedRow();

				if (selectedRow > -1)
					deleteSelectedGame(selectedRow);

				else
					JOptionPane.showMessageDialog(null, "Není označena hra, která se má vymazat.", "Neoznačeno",
							JOptionPane.ERROR_MESSAGE);
			}
		};

		
		deleteMeasurementsAction = new AbstractAction("Vymazat hry Measurement", GetIconInterface
				.getImage("/crud/results/DeleteKindsOfResultsIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
						"Opravdu si přejete vymazat veškeré hry typu Measurement?", "Smazat",
						JOptionPane.YES_NO_OPTION)) {
					final boolean deleted = DATA_MANAGER.deleteGamesByKindOfGame("Measurement");

					if (!deleted) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o smazání her typu Measurement. Hry nebyly smazány!", "Chyba",
								JOptionPane.ERROR_MESSAGE);
						return;
					}

					fillAllDataToTableList();

					tableModel.setTableValuesList(ALL_DATA_TO_TABLE);
				}
			}
		};
		
		
		deleteCountdownsAction = new AbstractAction("Vymazat hry Countdown", GetIconInterface
				.getImage("/crud/results/DeleteKindsOfResultsIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
						"Opravdu si přejete vymazat veškeré hry typu Countdown?", "Smazat",
						JOptionPane.YES_NO_OPTION)) {
					final boolean deleted = DATA_MANAGER.deleteGamesByKindOfGame("Countdown");

					if (!deleted) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o smazání her typu Countdown. Hry nebyly smazány!", "Chyba",
								JOptionPane.ERROR_MESSAGE);
						return;
					}

					fillAllDataToTableList();

					tableModel.setTableValuesList(ALL_DATA_TO_TABLE);
				}
			}
		};
		
		
		deleteAllAction = new AbstractAction("Vymazat všechny hry",
				GetIconInterface.getImage("/crud/results/DeleteAllResultsIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
						"Opravdu si přejete vymazat veškeré hry v DB?", "Smazat", JOptionPane.YES_NO_OPTION)) {
					final boolean deleted = DATA_MANAGER.deleteAllGames();

					if (!deleted) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o smazání her. Hry nebyly smazány!", "Chyba",
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					/*
					 * Zde by stačilo puze vyčistit list s daty pomocí metody clear, ale kdyby se to
					 * náhodou nepovedlo, tak aby si toho uživatel všiml, že se nějaké hry načetly:
					 */
					fillAllDataToTableList();
					
					tableModel.setTableValuesList(ALL_DATA_TO_TABLE);
				}
			}
		};
		
		
		return new ArrayList<>(Arrays.asList(deleteAction, deleteMeasurementsAction, deleteCountdownsAction, deleteAllAction));
	}
}