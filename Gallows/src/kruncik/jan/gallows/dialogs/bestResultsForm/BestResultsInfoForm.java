package kruncik.jan.gallows.dialogs.bestResultsForm;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * 
 * @author jan
 *
 */

public class BestResultsInfoForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;



	public BestResultsInfoForm() {
		super("Info");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 500, 500, "/icons/InfoIconForm.png", true);
		
		
		final JTextArea txaInfo = new JTextArea("Dialog obsahuje přehled všech odehraných her všech uživatelů.\n"
				+ "V případě, že je přihlášen uživatel s právy admina, je k dispozici navíc možnost smazat hry. Smazat označenou hru, smazat všechny hry daného typu (Measuremetn nebo Countdown) nebo vymazat veškeré hry v DB.\n\n"
				+ "Je možné zobrazit hry dle uvedených typů nebo všechny a je možné je filtrovat dle různýchkritérií.");
		
		txaInfo.setEditable(false);
		
		txaInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		
		txaInfo.setLineWrap(true);
		
		add(new JScrollPane(txaInfo), BorderLayout.CENTER);
		
		
		prepareWindow();
	}
	
	
	
	@Override
	protected void checkData() {	
	}	
}
