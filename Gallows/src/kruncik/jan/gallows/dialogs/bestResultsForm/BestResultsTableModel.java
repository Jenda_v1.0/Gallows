package kruncik.jan.gallows.dialogs.bestResultsForm;

import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

/**
 * Tato třída slouží jako model pro tabulky s nejlepšími výsledky. Jelikož jsou
 * v aplikaci možné dva typy hry (Countdown a Measurement), tak je zde tato
 * "abstraktní třída" pro společné hodnoty pro zmíněné dva typy her (v aplikaci
 * dvě tabulky) a každá tabulka si pouze doplňuje své hodnoty.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class BestResultsTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = 1L;

	
	private List<TableValue> tableValuesList;
	
	private final String[] columnNames;
	
	
	/**
	 * Reference na dialog s výsledky her pro smazání hry.
	 */
	private final BestResultsForm bestResultsForm;
	
	
	
	BestResultsTableModel(final String[] columnNames, final BestResultsForm bestResultsForm) {
		super();
		
		this.columnNames = columnNames;
		this.bestResultsForm = bestResultsForm;				
	}
	
	
	
	void setTableValuesList(List<TableValue> tableValuesList) {
		this.tableValuesList = tableValuesList;
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která vrátí hodnotu v tabulce na zadaném indexu - řádku.
	 * 
	 * @param index
	 *            - řádek - pozice v listu, kde se má vrátit hodnota.
	 * 
	 * @return vrátí hodnotu na zadaném indexu v listu hodnot v tabulce.
	 */
	TableValue getValueAtIndex(final int index) {
		return tableValuesList.get(index);
	}
	
	
	
	/**
	 * Metoda, která vymaže hodnotu na zadaném indexu v listu - v tabulce.
	 * 
	 * @param index
	 *            - Index hodnoty v listu - v tabulce, která se má smazat.
	 */
	void deleteValueAtIndex(final int index) {
		tableValuesList.remove(index);
		
		fireTableDataChanged();
	}
	
	
	
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	
	@Override
	public int getRowCount() {
		return tableValuesList.size();
	}

	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return tableValuesList.get(rowIndex).getPseudonym();
			
		case 1:
			return tableValuesList.get(rowIndex).getKindOfGame();
			
		case 2:
			return tableValuesList.get(rowIndex).getGroupname();
			
		case 3:
			return tableValuesList.get(rowIndex).getDate();
			
		case 4:
			return tableValuesList.get(rowIndex).getTime();
		
		case 5:
			return tableValuesList.get(rowIndex).getScore();

		case 6:
			return createDeleteGameButton(rowIndex);
			
		default:
			break;
		}
		return null;
	}	

	
	@Override
	public String getColumnName(int columm) {
		return columnNames[columm];
	}
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object objValue = getValueAt(0, columnIndex);

		if (objValue != null)
			return objValue.getClass();

		return super.getColumnClass(columnIndex);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří tlačítko, které vymaže hru na daném řádku - indexu.
	 * 
	 * @param rowIndex
	 *            - Index řádku (pozice v listu), kde se má smazt daná hra.
	 * 
	 * @return tlačítko, které smaže výše popsanou hru.
	 */
	private JButton createDeleteGameButton(final int rowIndex) {
		final JButton btnDelete = new JButton("Smazat");

		btnDelete.addActionListener(Event -> bestResultsForm.deleteSelectedGame(rowIndex));

		return btnDelete;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí položky v listu (v tabulce) dle skóre.
	 * 
	 * @param asc
	 *            - logická proměnná o tom, zda se mají hodnoty v tabulce dle skóře
	 *            seřadit vzestupně nebo sestupně. true = vzestupně, false =
	 *            sestupně
	 */
	void sortByScore(final boolean asc) {
		if (asc)
			tableValuesList.sort(Comparator.comparingInt(TableValue::getScore));
		else
			tableValuesList.sort((a, b) -> Integer.compare(b.getScore(), a.getScore()));
		
		
		fireTableDataChanged();
	}
	
	
	
	/**
	 * Metoda, terá seřadí data v tabulce dle Pseudonymu podle abecedy.
	 */
	void sortByPseudonym() {
		tableValuesList.sort((a, b) -> a.getPseudonym().compareToIgnoreCase(b.getPseudonym()));

		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí data v tabulce dle názvu skupiny - dle abecedy.
	 */
	void sortByGroupName() {
		tableValuesList.sort((a, b) -> a.getGroupname().compareToIgnoreCase(b.getGroupname()));

		fireTableDataChanged();
	}
}