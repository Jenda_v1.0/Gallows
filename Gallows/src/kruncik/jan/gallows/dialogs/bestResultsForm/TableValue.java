package kruncik.jan.gallows.dialogs.bestResultsForm;

/**
 * Tato třída slouží pouze jako "objekt", který se bude vkládat do tabulky s
 * potřebnými hodnotami, protože v tabulce potřebuji i pseudonym a ten v DB v
 * tabulce Games není, tak si vyhledám potřebná data, ty vložím do této třídy, a
 * tuto třídu - její instance s potřebnými hodnotami vložím do tabulky.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class TableValue {

	/**
	 * Tato hodnota se nezobrazuje, ale je potřeba pro smazání dané hry.
	 */
	private final int gameId;

	private final String pseudonym, kindOfGame, groupname;

	private final int score;

	/**
	 * Datum už bude zformátovaná a převedený na datum v požadovaném formátu
	 * nastaveném v objektu Game, to samé pro time.
	 */
	private final String date, time;



	TableValue(final int gameId, final String pseudonym, final String kindOfGame, final String groupName, final int score,
			final String date, final String time) {
		super();

		this.gameId = gameId;
		this.pseudonym = pseudonym;
		this.kindOfGame = kindOfGame;
		this.score = score;
		this.date = date;
		this.time = time;
		this.groupname = groupName;
	}


	int getGameId() {
		return gameId;
	}

	public String getPseudonym() {
		return pseudonym;
	}

	public String getKindOfGame() {
		return kindOfGame;
	}

	public int getScore() {
		return score;
	}

	String getDate() {
		return date;
	}

	String getTime() {
		return time;
	}

	public String getGroupname() {
		return groupname;
	}
}