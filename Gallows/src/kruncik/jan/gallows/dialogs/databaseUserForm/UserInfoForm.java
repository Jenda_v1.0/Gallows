package kruncik.jan.gallows.dialogs.databaseUserForm;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída obsahuje základní informace pro dialog ohldně manipulace s
 * uživateli v DB.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class UserInfoForm extends SourceDialog {

	private static final long serialVersionUID = 1L;

	
	public UserInfoForm() {
		super("Info");

		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 500, 500, "/icons/InfoIconForm.png", true);

		final JTextArea txtInfo = new JTextArea("Dialog slouží pro CRUD operace nad všemi uživateli v DB.\n"
				+ "Je možné zobrazit veškeré uživatele v DB, dále je seřadit a vyhledat podle různých kritérií.\n\n"
				+ "V neposlední řadě je možné některé údaje upravit přímo v tabulce, například dvojlikem levým tlačítkem myši na políčko v tabulce, které chceme upravit.\n"
				+ "Ostatní údaje, které nelze upravit v tabulce, jako je například obrázek je možné upravit pomocí dialogu pro přidání a editaci uživatele. Stačí označit uživatele v tabulce a kliknout na tlačítko upravit uživatele. Stejný postup platí pro smazání uživatele.\n\n"
				+ ""
				+ "Smazat uživatele je možné dvěma způsoby:\n"
				+ "- Buď uživatele pouze označíme jako vymazaného, tj. nastaví se atribut Deleted v DB. Pak je možné daný učet obnovit.\n"
				+ "- Druhý způsob je úplné vymazání z DB. To je možné po kliknutí na tlačítko \"Úplné vymazání\". V tomto případě se vymaže z DB uživatel samotný a veškerá jeho data - odehrané hry apod. Není možné uživatele obnovit.\n\n"
				+ ""
				+ "Pro manipulaci s uživateli je možné využit tlačítka v menu \"Uživatel\" nebo tlačítka v toolbaru, případně kliknout pravým tlačítkem myši na řádek s potřebným uživatele v tabulce a zvolit operaci.");
		
		txtInfo.setEditable(false);
		
		txtInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		
		txtInfo.setLineWrap(true);

		add(new JScrollPane(txtInfo), BorderLayout.CENTER);

		prepareWindow();
	}

	@Override
	protected void checkData() {
	}
}
