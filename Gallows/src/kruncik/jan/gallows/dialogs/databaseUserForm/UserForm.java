package kruncik.jan.gallows.dialogs.databaseUserForm;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.City;
import kruncik.jan.gallows.db_objects.KindOfUser;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.dialogs.RegistrationForm;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudAbstractActions;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudMenu;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.KindOfInfoDialog;

/**
 * Třída slouží jako dialog, který slouží pro reprezentování a manipulaci s
 * tabulkou Users v DB.
 * 
 * TableModelListener je potřeba pro detekkování změn v tabulce (když se některé
 * hodnoty změní, tak se zapíšou změny do DB).
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class UserForm extends CrudAbstractActions {

	
	private static final long serialVersionUID = 1L;




	/**
	 * Komponenta pro manipulaci s daty v DB.
	 */
	public static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	
	
	/**
	 * Regulární výraz pro hodnotu nula nebo jedna, včetně mezer. Jedná se o hodnotu
	 * "Smazáno".
	 */
	private static final String REG_EX_DELETE = "^\\s*(0|1)\\s*$";
	
		
	
	
	
	/**
	 * Regulární výraz pro typ uživatele. Může to být pouze jedno slovo s
	 * podtržítkem, velkými a malými písmeny s diakritikou.
	 * 
	 * Vím, že jsou pouze dva typy (Admin, Player) a já rozhoduji o tom, jaké typy
	 * budou v DB, a i kdyby byli v DB jiné, je to fukt, prootže v aplikaci se
	 * pracuje pouze s těmito dvěma. Ale aby se dalo alespon trochu vyhledavat, i
	 * když by bylo lepší dát comBobox a vyfiltrovat jen jeden z těch dvou typů
	 * (když už)
	 */
	private static final String REG_EX_KIND_OF_USER = "^\\s*[ěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ\\w]+\\s*$";
	
	
	
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku.
	 */
	private static final String[] COLUMNS_NAMES = { "Obrázek", "ID", "Smazán", "Jméno", "Příjmení", "Ban",
			"Typ uživatele", "Uživatelské jméno", "Heslo", "Email", "Telefon", "Ulice", "PSČ", "Město" };
	
	
	
	
	/**
	 * Jednorozměrné pole, které obsahuje položky do komponenty JComboBox pro
	 * seřazení dat v tabulce dle různých sloupců, resp. hodnot ve sloupcích.
	 */
	private static final String[] CMB_ORDERS_MODEL = { "Zobrazit vše", "ID - Sestupně", "ID - Vzestupně",
			"Jméno - Sestupně", "Jméno - Vzestupně", "Příjmení - Sestupně", "Příjmení - Vzestupně", "Smazán - Sestupně",
			"Smazán - Vzestupně", "Typ uživatele - Sestupně", "Typ uživatele - Vzestupně",
			"Uživatelské jméno - Sestupně", "Uživatelské jméno - Vzestupně", "Email - Sestupně", "Email - Vzestupně",
			"Město - Sestupně", "Město - Vzestupně" };
	

		
	
	
	/**
	 * Jednorozměrné pole, které obsahuje hodnoty do pložky cmbSearch. jednotlivé
	 * položky značí sloupec, ve kterém se bude vyhledávat zadaná hodnota.
	 */
	private static final String[] CMB_SEARCH_MODEL = { "ID", "Jméno", "Příjmení", "Smazán", "Typ uživatele",
			"Uživatelské jméno", "Email", "Město", "Telefon" };

	
	
	
	
	/**
	 * Jednorozměrné pole s hodnotami pro komponentu JComboBox cmbDeleted.
	 * 
	 * Tato proměnnná je veřejná, abych k ní mohl přistupovat i z tableModelu
	 * tabulky kvůli získávání a nastavování hodnot.
	 */
	static final String[] CMB_DELETED_MODEL = {"0 - Vytvořen", "1 - Smazán"};
	
	/**
	 * Komponenta pro nastavení toho, zda je uživatelský účet označený v tabulce smazán nebo
	 * ne.
	 */
	private final JComboBox<String> cmbDeleted;

	
	/**
	 * Jednorozměrné pole s hodnotami pro komponentu JComboBox cmbBan.
	 * 
	 * Tato proměnnná je veřejná, abych k ní mohl přistupovat i z tableModelu
	 * tabulky kvůli získávání a nastavování hodnot.
	 */
	static final String[] CMB_BAN_MODEL = {"0 - Povolen", "1 - Blokován"};
	/**
	 * Komponenta pro nastavení toho, zda je uživatelský účet označený v tabulce
	 * zablokován nebo ne.
	 */
	private final JComboBox<String> cmbBan;

	
	/**
	 * Komponenta pro nastavení typu uživatelského účtu označeného v tabulce.
	 */
	private final JComboBox<String> cmbKindOfUser;
	
	
	/**
	 * Komponenta pro nastavení PSČ uživatele daného uživatelského účtu označeného v
	 * tabulce.
	 */
	private final JComboBox<String> cmbPsc;
	
	
	
	
	
	/**
	 * Tabulka uživatelů.
	 */
	private final JTable table;
	
	
	/**
	 * Model dat pro tabulku uživatelů.
	 */
	private final MyTableModel tableModel;
	
	
	
	
	
	/**
	 * List, který obsahuje veškeré uživatle v DB. S tímto listem se nic nedělá,
	 * pouze se z něj filtrují data, která se následně zobrazují v tabulce.
	 */
	private static List<User> ALL_USERS_LIST;
	
	
	
	/**
	 * List, který obsahuje veškerá města v DB.
	 */
	private static List<City> ALL_CITIES_LIST;
	
	
	
	
	/**
	 * ID právě přihlášeného uživatele, potřebuji ho, abych zamezil smazání právě
	 * přihlášeného uživatele.
	 */
	private final int logginedUserId;
	
	
	
	
	
	
	/**
	 * Abstraktní akce, která slouží pro komplentí vymazání uživatele z DB. Nebude
	 * se nastavovat atribut Deleted na 1, ale úplně se z DB vymaže i se všemi jeho
	 * daty. Takže se vymažou nejprve všechny jeho hry a pak samotný živatel.
	 */
	private AbstractAction completeErasureUser;
	
	
	
	
	
	/**
	 * Označit za smazané = nastavit atribut Deleted na 1
	 * markAsCreatedAllUsersAction -> Označí všechny uživatele za vytvořené
	 * markAsDeletedAllUserAction -> Označí se za smazané všichni uživatelé, kromě toho přihlášeného
	 * markAsDeletedAllAdminsAction -> Označí se za smazané všichni admini, kromě toho přihlášeného
	 * markAsDeletedPlayers -> Označí se za smazané všichni hráči
	 */
	private AbstractAction markAsDeletedAllUserAction, markAsCreatedAllUsersAction, markAsDeletedAllAdminsAction,
			markAsDeletedPlayersAction;
	
	
	
	
	
	
	/**
	 * Úplně se z DB vymažou všichn iuživatelů - příslušní uživatelé včetně všech
	 * her atd.
	 * 
	 * compEraseAllUsers -> Vymažou se z DB všichni uživatelé, včetně jejich dat -
	 * odehraných her apod. (Vymažou se všichni uživatelé kroměn přihlášeného
	 * admina)
	 * 
	 * 
	 * compEraseAllAdmins -> Vymažou se všichni admini, včetně všech dat (odehraných
	 * her apod.), vymažou se všichni admini, kromě právě přihlášeného admina.
	 * 
	 * compEraseAllPlayers-> Vymažou se z Db všichni uživatelé typu Player včetně
	 * všech jejich dat - her apod.
	 */
	private AbstractAction compEraseAllUsers, compEraseAllAdmins, compEraseAllPlayers;
	
	
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy, potřebuji si sem před Id přihlášeného uživatele,
	 * abych zajistil, že se nemůže sám smazat.
	 * 
	 * @param logginedUserId
	 *            - ID přihlášeného uživatele.
	 */
	public UserForm(final int logginedUserId) {
		super("Uživatelé");
		
		this.logginedUserId = logginedUserId;
		
		// Při každém otevření dialogu se znovu načtou aktuální data z DB:
		ALL_USERS_LIST = DATA_MANAGER.getAllUsers();
		
		ALL_CITIES_LIST = DATA_MANAGER.getAllCities();
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 1200, 500, "/icons/UsersFormIcon.png", true);
		
		
		final List<AbstractAction> actionsList = createAbstractActions();
		
		createCompleteErasureUserAction();
		actionsList.add(completeErasureUser);
		
		setJMenuBar(new CrudMenu(this, "Uživatel", actionsList, KindOfInfoDialog.USER_INFO_DIALOG));
		
		
		
		
		final JToolBar toolbarCrud = new JToolBar();
		toolbarCrud.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 0));
		
		toolbarCrud.add(createAction);
		toolbarCrud.add(updateAction);
		toolbarCrud.add(deleteAction);
		toolbarCrud.addSeparator();
		toolbarCrud.add(completeErasureUser);
		toolbarCrud.addSeparator();
		toolbarCrud.add(compEraseAllUsers);
		toolbarCrud.add(compEraseAllAdmins);
		toolbarCrud.add(compEraseAllPlayers);
		toolbarCrud.addSeparator();
		toolbarCrud.add(markAsDeletedAllUserAction);
		toolbarCrud.add(markAsCreatedAllUsersAction);
		toolbarCrud.add(markAsDeletedAllAdminsAction);
		toolbarCrud.add(markAsDeletedPlayersAction);
		
		
		
		final JToolBar toolbarFilters = new JToolBar();
		toolbarFilters.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 0));		
		
		toolbarFilters.add(new JLabel("Zobrazit"));
		
		cmbOrder = new JComboBox<>(CMB_ORDERS_MODEL);
		cmbOrder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final String selectedItem = (String) cmbOrder.getSelectedItem();
				
				if (selectedItem.equals(CMB_ORDERS_MODEL[0]))
					tableModel.setUsersList(ALL_USERS_LIST);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[1]))
					tableModel.sortUsersById(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[2]))
					tableModel.sortUsersById(false);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[3]))
					tableModel.sortUsersByFirstName(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[4]))
					tableModel.sortUsersByFirstName(false);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[5]))
					tableModel.sortUsersByLastName(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[6]))
					tableModel.sortUsersByLastName(false);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[7]))
					tableModel.sortUsersByDeleted(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[8]))
					tableModel.sortUsersByDeleted(false);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[9]))
					tableModel.sortUsersByKindOfUser(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[10]))
					tableModel.sortUsersByKindOfUser(false);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[11]))
					tableModel.sortUsersByPseudonym(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[12]))
					tableModel.sortUsersByPseudonym(false);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[13]))
					tableModel.sortUsersByEmail(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[14]))
					tableModel.sortUsersByEmail(false);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[15]))
					tableModel.sortUsersByCity(true);
				else if (selectedItem.equals(CMB_ORDERS_MODEL[16]))
					tableModel.sortUsersByCity(false);
			}
		});
		toolbarFilters.add(cmbOrder);
		
		
		toolbarFilters.add(new JLabel("Vyhledat"));
		
		cmbSearch = new JComboBox<String>(CMB_SEARCH_MODEL);
		toolbarFilters.add(cmbSearch);
		
		createKeyAdapter();
		txtSearch = new JTextField(20);
		txtSearch.addKeyListener(keyAdapter);
		toolbarFilters.add(txtSearch);
		
		
		btnSearch = new JButton("Vyhledat");
		btnSearch.addActionListener(Event -> checkData());
		toolbarFilters.add(btnSearch);
		
		
				
		// Vytvoření panelu pro toolbary a vložení jej do okna dialogu:
		final JPanel pnlToolbars = new JPanel(new BorderLayout());
		pnlToolbars.add(toolbarCrud, BorderLayout.NORTH);
		pnlToolbars.add(toolbarFilters, BorderLayout.SOUTH);
		
		add(pnlToolbars, BorderLayout.NORTH);
		
		
		
		
		
		
		// Tabulka:
		table = new JTable();
		createPopupMenuMouseAdapter(table, actionsList);
		
		tableModel = new MyTableModel(COLUMNS_NAMES, table, ALL_CITIES_LIST, logginedUserId);
		tableModel.setUsersList(ALL_USERS_LIST);

		table.setModel(tableModel);
		
				
		final JScrollPane jspTable = new JScrollPane(table);		
		jspTable.setBorder(BorderFactory.createTitledBorder("Tabulka uživatelů"));
		
		add(jspTable, BorderLayout.CENTER);
		
		
		
		// JComboBoxy do tabulky:
		
		// cmbDeleted:
		cmbDeleted = new JComboBox<>(CMB_DELETED_MODEL);
		table.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(cmbDeleted));
		
		
		
		// cmbBan:
		cmbBan = new JComboBox<>(CMB_BAN_MODEL);
		table.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(cmbBan));
		
		
		
		// cmbKindOfUser:
		final List<KindOfUser> kindsOfUsers = DATA_MANAGER.getAllKindsOfUsers();
		// Pole bude obsahovat pouze konkrétní typy (texty):
		final String[] kindsOfUsersModel = new String[kindsOfUsers.size()];
		
		for (int i = 0; i < kindsOfUsers.size(); i++)
			kindsOfUsersModel[i] = kindsOfUsers.get(i).getKind();
		
		cmbKindOfUser = new JComboBox<>(kindsOfUsersModel);
		table.getColumnModel().getColumn(6).setCellEditor(new DefaultCellEditor(cmbKindOfUser));
		
		
		
		// cmbPsc:
		final List<Integer> pscList = DATA_MANAGER.getAllPsc();
		final String[] pscModel = new String[pscList.size() + 1];
		pscModel[0] = "";
		
		for (int i = 0; i < pscList.size(); i++)
			pscModel[i + 1] = getPscWithSpace(pscList.get(i));
		
		cmbPsc = new JComboBox<>(pscModel);
		table.getColumnModel().getColumn(12).setCellEditor(new DefaultCellEditor(cmbPsc));
		
		
		
		prepareWindow();
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která převede formát psč xxxxx typu int na text ve formátu: xxx xx
	 * tj. za třetím číslem bude mezera.
	 * 
	 * @param psc
	 *            - PSČ typu int
	 * @return psc typu string z mezerou za 3 číslem. Nebo prázdné uvozovky v
	 *         případě, že psč není uvedeno, tj v parametru metody (psc) bude 0 coby
	 *         výchozí hodnoty proměnné typu int.
	 */
	static String getPscWithSpace(final int psc) {
		if (psc > 0) {
			final String pscInText = String.valueOf(psc);

			return pscInText.substring(0, 3) + " " + pscInText.substring(3);
		}

		return "";
	}
	
	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		if (txtSearch.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Není zadáno klíčové slovo pro vyhledání!", "Prázdný text",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		/*
		 * Zde vím, že pole není prázdné, tak pokaždé otestuji regulární výraz a pokud
		 * to bude v pohodě, tak případně odeberu mezery nebo si text převedu na jiný
		 * datový typ apod. a vyfiltruji zadané hodnoty.
		 */
		
		final String selectedItem = (String) cmbSearch.getSelectedItem();		

		final String txtValue = txtSearch.getText();

		// Zde se vyhledává dle ID:
		if (selectedItem.matches(CMB_SEARCH_MODEL[0])) {
			if (txtSearch.getText().matches(REG_EX_ID)) {
				final int id = Integer.parseInt(txtValue.replaceAll("\\s", ""));

				tableModel.setUsersList(
						ALL_USERS_LIST.stream().filter(u -> u.getId() == id).collect(Collectors.toList()));
			}

			else
				JOptionPane.showMessageDialog(this, "Zadané ID neodpovídá požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Jména:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[1])) {
			if (txtSearch.getText().matches(REG_EX_FOR_FIRST_AND_LAST_NAME))
				tableModel.setUsersList(ALL_USERS_LIST.stream()
						.filter(u -> u.getFirstName().toUpperCase().contains(txtValue.toUpperCase()))
						.collect(Collectors.toList()));

			else
				JOptionPane.showMessageDialog(this, "Zadané jméno neodpovídá požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Příjmení:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[2])) {
			if (txtSearch.getText().matches(REG_EX_FOR_FIRST_AND_LAST_NAME))
				tableModel.setUsersList(ALL_USERS_LIST.stream()
						.filter(u -> u.getLastName().toUpperCase().contains(txtValue.toUpperCase()))
						.collect(Collectors.toList()));

			else
				JOptionPane.showMessageDialog(this, "Zadané příjmení neodpovídá požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Smazán:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[3])) {
			if (txtSearch.getText().matches(REG_EX_DELETE))
				tableModel.setUsersList(
						ALL_USERS_LIST.stream().filter(u -> u.getDeleted() == txtValue.replaceAll("\\s", "").charAt(0))
								.collect(Collectors.toList()));

			else
				JOptionPane.showMessageDialog(this, "Zadaná hodnoty coby 'Smazán' neodpovídá požadované syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Typ uživatele:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[4])) {
			if (txtSearch.getText().matches(REG_EX_KIND_OF_USER))
				tableModel.setUsersList(ALL_USERS_LIST.stream()
						.filter(u -> u.getKindOfUser().toUpperCase().contains(txtValue.toUpperCase()))
						.collect(Collectors.toList()));

			else
				JOptionPane.showMessageDialog(this, "Zadaný typ uživatelského účtu neodpovídá požadované syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Pseudonym - uživatelské jméno:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[5])) {
			if (txtSearch.getText().matches(REG_EX_PSEUDONYM))
				tableModel.setUsersList(ALL_USERS_LIST.stream()
						.filter(u -> u.getPseudonym().toUpperCase().contains(txtValue.toUpperCase()))
						.collect(Collectors.toList()));

			else
				JOptionPane.showMessageDialog(this, "Zadané uživatelské jméno neodpovídá požadované syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Email:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[6])) {
			if (txtSearch.getText().matches(REG_EX_EMAIL))
				tableModel.setUsersList(
						ALL_USERS_LIST.stream().filter(u -> u.getEmail().toUpperCase().contains(txtValue.toUpperCase()))
								.collect(Collectors.toList()));

			else
				JOptionPane.showMessageDialog(this, "Zadaný email neodpovídá požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Město:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[7])) {
			if (txtSearch.getText().matches(REG_EX_CITY_NAME)) {
				int cityPsc = -1;
				
				for (final City c : ALL_CITIES_LIST) {
					if (c.getName().toUpperCase().contains(txtValue.toUpperCase())) {
						cityPsc = c.getPsc();
						break;
					}						
				}
				
				

				final List<User> usersListWithPsc = new ArrayList<>();
				
				// Pouze v případě, že se nějaké PSČ dle názvu města najde, tak mohu vyhledávat
				// uživatelel dle PSČ:
				if (cityPsc > -1) {
					for (final User u : ALL_USERS_LIST)
						if (u.getPsc() == cityPsc)
							usersListWithPsc.add(u);

					tableModel.setUsersList(usersListWithPsc);
				}

				tableModel.setUsersList(usersListWithPsc);
			}

			else
				JOptionPane.showMessageDialog(this, "Zadané město neodpovídá požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}

		// Zde se vyhledává dle Telefon:
		else if (selectedItem.matches(CMB_SEARCH_MODEL[8])) {
			if (txtSearch.getText().matches(REG_EX_TELEPHONE_NUMBER)) {
				final int tel = Integer.parseInt(txtValue.replaceAll("\\s", ""));

				tableModel.setUsersList(ALL_USERS_LIST.stream().filter(u -> u.getTelephoneNumber() == tel)
						.collect(Collectors.toList()));
			}

			else
				JOptionPane.showMessageDialog(this, "Zadané telefonní číslo neodpovídá požadované syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}
	}











	@Override
	protected List<AbstractAction> createAbstractActions() {
		createAction = new AbstractAction("Vytvořit",
				GetIconInterface.getImage("/crud/CreateIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				new RegistrationForm(true, true);
				
			
				final int selectedRow = table.getSelectedRow();
				
				// Jelikož nevím, jaké kritérium pro uživatele uživatel právě zvolil, tak pouza
				// aktualiuji tabulku s vybranými užýivateli
				updateActualUsers();
				
				if (selectedRow > -1 && selectedRow <= table.getRowCount())
					table.setRowSelectionInterval(selectedRow, selectedRow);
			}
		};		
		
		
		
		
		
		updateAction = new AbstractAction("Upravit",
				GetIconInterface.getImage("/crud/EditIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() > -1) {
					// Uložím si označený řádek, abych jej mohl znovu označit:
					final int selectedRow = table.getSelectedRow();

					// Získám si uživatele označeného v tabulce pro editaci
					final User selectedUser = tableModel.getUserAtIndex(table.getSelectedRow());

					// Otestuji, zda se nejedná o editace přihlášeného uživatele, to je povolené jen
					// v menu v informacech o účtě:
					if (selectedUser.getId() != logginedUserId) {
						// Otevřu dialog a získám si upravená data ohledně účtu uživatele:
						final User editedUser = new RegistrationForm(true, false).getEditedUser(selectedUser);

						if (editedUser != null) {
							// Aktualizuji uzivatele v DB
							final boolean updated = DATA_MANAGER.updateUser(editedUser);

							if (!updated) {
								JOptionPane.showMessageDialog(null,
										"Nastala chyba při pokusu o upravení dat ohledně uživatelského účtu uživatele '"
												+ editedUser.getPseudonym() + "'.",
										"Chyba", JOptionPane.ERROR_MESSAGE);
								return;
							}

							updateActualUsers();

							if (selectedRow > -1 && selectedRow <= table.getRowCount())
								table.setRowSelectionInterval(selectedRow, selectedRow);
						}
					} else
						JOptionPane.showMessageDialog(null,
								"Vlastní účet lze upravit v informacích ú účtu (Uživatel -> Info).", "Akce zamítnuta",
								JOptionPane.ERROR_MESSAGE);
				} else
					JOptionPane.showMessageDialog(null, "Není zvolen uživatel pro editaci", "Není zvolen uživatel",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		
		
		deleteAction = new AbstractAction("Smazat",
				GetIconInterface.getImage("/crud/DeleteIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				final int selectedRow = table.getSelectedRow();
				
				if (selectedRow > -1) {
					if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
							"Opravdu chcete smazat označený účet?", "Smazat účet", JOptionPane.YES_NO_OPTION)) {
						final int userId = tableModel.getUserAtIndex(selectedRow).getId();

						if (userId == logginedUserId) {
							JOptionPane.showMessageDialog(null, "Nelze smazat sůvj vlastní účet!", "Nelze smazat",
									JOptionPane.ERROR_MESSAGE);
							return;
						}

						DATA_MANAGER.deleteBlockUser(userId);

						/*
						 * Jelikož se "smazal nějaký uživatel" - nastavila se mu hodnota smazáno na 1,
						 * pak musím přenačíst list uživatelů, resp. stačí jen u toho uživatele nastavit
						 * zmíněnou hodnotu a přenačíst tabulku ale pro jistotu přenačtu DB, abych
						 * videěl, zda se změny projevily, kdyby ne. díky tomu, že se data načtou z DB
						 * nově, se to zjistí.
						 * 
						 * (Kvůli tomu, že se to nikde neoznamuje oknem apod. Mohl bych změnit tu
						 * hodnotu v listu, ale kdyby se to v DB neprojevilo, tak by byl uživatel chybně
						 * informován a v tabulce by byla chybná data.)
						 */
						updateActualUsers();

						table.setRowSelectionInterval(selectedRow, selectedRow);
					}

				} else
					JOptionPane.showMessageDialog(null, "Není označen uživatel pro smazání!", "Neoznačeno",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		
		return new ArrayList<>(Arrays.asList(createAction, updateAction, deleteAction));
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci abstraktní akce, která slouží pro kompletní
	 * vymazání uživatele z DB i se všemi k němu patřícími daty.
	 */
	private void createCompleteErasureUserAction() {
		completeErasureUser = new AbstractAction("Úplné vymazání", GetIconInterface.getImage("/crud/CompleteEraseIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				final int selectedRow = table.getSelectedRow();
				
				if (selectedRow > -1) {
					if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
							"Opravdu chcete smazat označený účet i se všemi jeho daty?", "Smazat účet", JOptionPane.YES_NO_OPTION)) {
					
						final int userId = tableModel.getUserAtIndex(selectedRow).getId();

						if (userId == logginedUserId) {
							JOptionPane.showMessageDialog(null, "Nelze smazat sůvj vlastní účet!", "Nelze smazat",
									JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						// Metoda, která vymaže uživatele i s jeho hrami z DB:
						DATA_MANAGER.deleteUserWithAllData(userId);

						
						ALL_USERS_LIST = DATA_MANAGER.getAllUsers();
						tableModel.setUsersList(ALL_USERS_LIST);

						if (selectedRow < table.getRowCount())
							table.setRowSelectionInterval(selectedRow, selectedRow);
					}

				} else
					JOptionPane.showMessageDialog(null, "Není označen uživatel pro 'úplné' vymazání!", "Neoznačeno",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		

		
		
		
		
		
		
		
		
		/*
		 * Pro následující cykly vím, že bych místo znovu načítání uživatelů z DB mohl
		 * projít list aktuálně načtených uživatelů, ten je ten aktuální, ale kdyby
		 * náhodou se někde něco pokazilo apod. (Nemělo by.)
		 */
		
		markAsDeletedAllUserAction = new AbstractAction("Smazat veškeré uživatele",
				GetIconInterface.getImage("/icons/DeleteGroupOfUsersIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Kromě přihlášeného uživatele označím všechny uživatele jako smazané:
				 */
				DATA_MANAGER.getAllUsers().stream().filter(u -> u.getId() != logginedUserId)
						.forEach(u -> DATA_MANAGER.deleteBlockUser(u.getId()));

				reloadTableData();
			}
		};
		markAsDeletedAllUserAction.putValue(AbstractAction.SHORT_DESCRIPTION,
				"Označí veškeré uživatele v DB za smazané s vyjímkou aktuálně přihlášeného uživatele.");
		
		
		
		
		markAsCreatedAllUsersAction = new AbstractAction("Obnovit smazané uživatele",
				GetIconInterface.getImage("/icons/RestoreAccountIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Načtu si všechny uživatele, projdu je a pokud se nejedná o aktuálně
				 * přihlášeného uživatele a je to uživatel s atributem Deleted nastaveným na 1,
				 * pak jej onovím, tj. nastavím atribut Deleted na 0.
				 */
				DATA_MANAGER.getAllUsers().stream().filter(u -> u.getId() != logginedUserId && u.getDeleted() == '1')
						.forEach(u -> DATA_MANAGER.restoreUser(u.getId()));

				reloadTableData();
			}
		};
		markAsCreatedAllUsersAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Označí veškeré uživatele jako vytvořené (pokud byly smazané).");;
		
		
		
		markAsDeletedAllAdminsAction = new AbstractAction("Vymazat veškeré adminy",
				GetIconInterface.getImage("/icons/DeleteAdminsIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Kromě přihlášeného uživatele oznaším všechny uživatele typu Admin jako
				 * smazané:
				 */
				DATA_MANAGER.getAllUsers().stream()
						.filter(u -> u.getId() != logginedUserId && u.getKindOfUser().equals("Admin"))
						.forEach(u -> DATA_MANAGER.deleteBlockUser(u.getId()));

				reloadTableData();
			}
		};
		markAsDeletedAllAdminsAction.putValue(AbstractAction.SHORT_DESCRIPTION,
				"Označí veškeré uživatele typu Admin s vyjímkou přihlášeného uživatele za smazané.");
		
		
		
		
		markAsDeletedPlayersAction = new AbstractAction("Vymazat veškeré hráče",
				GetIconInterface.getImage("/icons/DeleteAllPlayersIcon.jpg", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Označím všechny uživatele typu Player jako smazané:
				 */
				DATA_MANAGER.getAllUsers().stream().filter(u -> u.getKindOfUser().equals("Player"))
						.forEach(u -> DATA_MANAGER.deleteBlockUser(u.getId()));

				reloadTableData();
			}
		};

		markAsDeletedPlayersAction.putValue(AbstractAction.SHORT_DESCRIPTION,
				"Označí veškeré uživatele typu Player jako smazané.");
		
		
		
		
		
		compEraseAllUsers = new AbstractAction("Úplně vymazat všechny uživatele",
				GetIconInterface.getImage("/icons/EraseAllUsersIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Projdu všechny uživatele a pokud se nejdená o aktuálně přihlášeného
				 * uživatele, tak vymažu nejprve všechny hry daného uživatele a pak samotného
				 * uživatele. Data již nelze obnovit - úplně se vymžou z DB.
				 */
				DATA_MANAGER.getAllUsers().stream().filter(u -> u.getId() != logginedUserId)
						.forEach(u -> DATA_MANAGER.deleteUserWithAllData(u.getId()));

				reloadTableData();
			}
		};

		compEraseAllUsers.putValue(AbstractAction.SHORT_DESCRIPTION,
				"Úplně vymaže z DB veškeré uživatele (i s jejich hrami) s vyjímkou aktuálně přihlášeného uživatele.");
		
		
		
		
	
		compEraseAllAdmins = new AbstractAction("Úplně vymazat všechny adminy",
				GetIconInterface.getImage("/icons/EraseAllAdminsIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Projdu všechny uživatele a otestuji, zda se nejedná o aktuálně přihlášeného
				 * uživatele a zároveň je to admin, pak vymažu všechny jeho hry jako u předchozí
				 * akce a pak vymažu samotného uživatele. Tyto data již nepůjde obnovit.
				 */
				DATA_MANAGER.getAllUsers().stream()
						.filter(u -> u.getId() != logginedUserId && u.getKindOfUser().equals("Admin"))
						.forEach(u -> DATA_MANAGER.deleteUserWithAllData(u.getId()));

				reloadTableData();
			}
		};
		compEraseAllAdmins.putValue(AbstractAction.SHORT_DESCRIPTION,
				"Úplně vymaže z DB veškeré uživatele typu Admin (i s jeho hrami) s vyjímkou aktuálně přihlášeného uživatele.");
		
		
		
		
		compEraseAllPlayers = new AbstractAction("Úplně vymazat všechny hráče",
				GetIconInterface.getImage("/icons/EraseAllPlayersIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Zde projdu všechny uživatele v Db a otestuji, zda se jedná o hráče typu
				 * Player, pokud ano, tak nejprve vymažu všechny jeho hry a pak samotného
				 * uživatele. Tyto data již nepůjde obnovit.
				 */
				DATA_MANAGER.getAllUsers().stream().filter(u -> u.getKindOfUser().equals("Player"))
						.forEach(u -> DATA_MANAGER.deleteUserWithAllData(u.getId()));

				reloadTableData();
			}
		};
		compEraseAllPlayers.putValue(AbstractAction.SHORT_DESCRIPTION,
				"Úplně vymaže z DB veškeré uživatele typu Player i s jeho hrami.");
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přenačte data v tabulce. Znovu načte list uživatelů z DB a
	 * vloží jej do tabulky. Při tom znovu označí původní označený řádek (pokud
	 * nějaký byl označen)
	 */
	private void reloadTableData() {
		final int selectedRow = table.getSelectedRow();

		ALL_USERS_LIST = DATA_MANAGER.getAllUsers();

		tableModel.setUsersList(ALL_USERS_LIST);

		if (selectedRow > -1 && selectedRow < table.getRowCount())
			table.setRowSelectionInterval(selectedRow, selectedRow);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která aktualizuje aktuálně vybrané uživatele v tabulce.
	 * 
	 * Nejprve načte všechny uživatelel znovu z DB a pak projde ty uživatele, kteří
	 * se aktuálně nacházejí v tabulce kvůil změněným datům nebo kritérium
	 * vyhledávání.
	 */
	private void updateActualUsers() {
		ALL_USERS_LIST = DATA_MANAGER.getAllUsers();
		
		/*
		 * do tohoto listu vložím ty samé uživatele, které se aktuálně v tabuly
		 * vyskytují akorát již budou nově získané z DB.
		 * 
		 * Je to takto proto, že nevím, jaká data uživatel vyfiltrovat a nechci zobrazit
		 * všechna jako výchozí nastavení, tak to takto prodlužuji.
		 */
		final List<User> updatedUsersList = new ArrayList<>();

		tableModel.getAllUsersInTable().forEach(u -> {
			for (final User us : ALL_USERS_LIST)
				if (us.getId() == u.getId())
					updatedUsersList.add(us);
		});
		tableModel.setUsersList(updatedUsersList);
	}
}
