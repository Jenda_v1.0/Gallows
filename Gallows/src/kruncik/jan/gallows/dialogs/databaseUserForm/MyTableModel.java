package kruncik.jan.gallows.dialogs.databaseUserForm;

import java.awt.Image;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import kruncik.jan.gallows.db_objects.City;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako table model pro tabulku uživatelů.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MyTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = 1L;



	/**
	 * Tato proměnná obsahuje regulární výraz pro telefonní číslo (typu int) načtené z DB.
	 * Jendá se o 9 číslic vedle sebe - bez mezer apod.
	 */
	private static final String REG_EX_TELEPHONE_NUMBER = "^\\s*\\d{9}\\s*$";
	
	
	
	/**
	 * List s uživateli, kteří se nachází v tabulce.
	 */
	private List<User> usersList;
	
	
	/**
	 * Jednorozměrné pole, které obsahujenázvy sloupců.
	 */
	private final String[] columns;
	
	
	/**
	 * Reference na tabulku, protože pokud nějaký uživatel obsahuje obrázek, pak
	 * chci aby se v tabulce zobrazil a daný řádek s obrázkem byl vyšší, tj. aby se
	 * přizpůsobil velikost obrázku, kterou nastavím.
	 * 
	 * Proto potřebuji odkaz na tuto tabulku, kde jsou data v tomto modelu
	 * zobrazována, protože tabulka obsahuje metodu setRowHeight, která umožňuje
	 * nastavení výšky řádku v tabulce.
	 */
	private final JTable table;
	
	
	
	
	/**
	 * List, který obsahuje města z DB, abych nemusel hledat pokaždé název města v
	 * DB, tak si jej pouze najdu v tomto listu.
	 */
	private final List<City> citiesList;
	
	
	/**
	 * ID právě přihlášeného uživatele.
	 */
	private final int logginedUserId;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param columns
	 *            - Jednorozměrné pole, které obsahuje názvy sloupců.
	 * @param table
	 *            - reference na tabulku, ve které jsou zobrazována data v tomto
	 *            modelu, je to potřeba, kvůli nastavení výšky řádků.
	 * @param logginedUserId
	 *            - ID přihlášeného uživate, potřebuji ho, abych zakázal smazání
	 *            právě přihlášeného uživatele.
	 */
	MyTableModel(final String[] columns, final JTable table, final List<City> citiesList,
			final int logginedUserId) {
		super();
		
		this.columns = columns;
		this.table = table;
		this.citiesList = citiesList;
		this.logginedUserId = logginedUserId;
	}
	
	
		
	final void setUsersList(List<User> usersList) {
		this.usersList = usersList;
		
		fireTableDataChanged();
	}
	
	
	
	List<User> getAllUsersInTable() {
		return usersList;
	}
	
	
	
	/**
	 * Metoda, která vrátí objekt - hodnotu na nějakém řádku v tabulce.
	 * 
	 * @param index
	 *            - index řádku v tabulce, na kterém se nachází inforamce o nějakém
	 *            uživateli.
	 * @return Objekt z tabulky na řádku - indexu (vrátí se informace o nějakém
	 *         uživateli).
	 */
	final User getUserAtIndex(final int index) {
		return usersList.get(index);
	}
	
	
	
	
	
	/**
	 * Metoda, která najde a vrátí název města dle PSC daného města.
	 * 
	 * @param psc
	 *            - Psč města, jehož název se má vrátit.
	 * @return - Název města dle PSČ.
	 */
	private String getCityName(final int psc) {
		if (psc > 0)
			return citiesList.stream().filter(c -> c.getPsc() == psc).collect(Collectors.toList()).get(0).getName();
		return "Neuvedeno";
	}
	
	
	
	
	@Override
	public int getColumnCount() {		
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return usersList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			final ImageIcon img = usersList.get(rowIndex).getPicture();
			
			if (img != null && img.getIconHeight() > -1) {
				table.setRowHeight(rowIndex, 150);
				table.getColumnModel().getColumn(columnIndex).setPreferredWidth(150);
				
				return new ImageIcon(img.getImage().getScaledInstance(150, 150, Image.SCALE_SMOOTH));
			}
//				
			// Vrátím "prázdný" obrazek:
			return new ImageIcon();
			
		case 1:
			return usersList.get(rowIndex).getId();
			
		case 2:
			// Získám si hodnotu z objektu uživatele:
			final char deleted = usersList.get(rowIndex).getDeleted();
			
			// Do JComboBoxu nastavím příslušnou hodnotu - ale v tom JComboBoxu to mám jako
			// text s popiskem, tak jej musím trochu upravit:
			if (deleted == '0')
				return UserForm.CMB_DELETED_MODEL[0];
			else
				return UserForm.CMB_DELETED_MODEL[1];
			
		case 3:
			return usersList.get(rowIndex).getFirstName();
						
		case 4:
			return usersList.get(rowIndex).getLastName();
			
		case 5:
			final char ban = usersList.get(rowIndex).getBan();
			
			if (ban == '0')
				return UserForm.CMB_BAN_MODEL[0];
			else
				return UserForm.CMB_BAN_MODEL[1];
			
		case 6:
			return usersList.get(rowIndex).getKindOfUser();
			
		case 7:
			return usersList.get(rowIndex).getPseudonym();
			
		case 8:
			return usersList.get(rowIndex).getPassword();
			
		case 9:
			return usersList.get(rowIndex).getEmail();
			
		case 10:
			final int telephoneNumber = usersList.get(rowIndex).getTelephoneNumber();
			
			if (String.valueOf(telephoneNumber).matches(REG_EX_TELEPHONE_NUMBER)) {
				final String[] telArray = String.valueOf(telephoneNumber).replaceAll("\\s", "").split("");

				// Vrátím číslo ve tvaru: 123 456 789 v typu String:
				return telArray[0] + telArray[1] + telArray[2] + " " + telArray[3] + telArray[4] + telArray[5] + " "
						+ telArray[6] + telArray[7] + telArray[8];
			}
				 
			// Kdyby to náhodou nevyšlo, tak vrátím původní telefonní číslo v Intu
			return usersList.get(rowIndex).getTelephoneNumber();
			
		case 11:
			return usersList.get(rowIndex).getStreet();

		case 12:			
			return UserForm.getPscWithSpace(usersList.get(rowIndex).getPsc());
			
		case 13:
			return getCityName(usersList.get(rowIndex).getPsc());
			
		default:
			break;
		}
		return null;
	}

	
	
	
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 2:
			// Zjistím si, jaký index je zvolen a ten uložím, pravá hodnota, která značí,
			// zda je uživatel smazán nebo ne je jako první ve zvoleném položce v
			// JComboBoxu, pdobně je to i u Ban položky:
			final String itemDeleted = (String) value;
			
			usersList.get(rowIndex).setDeleted(itemDeleted.charAt(0));
			break;
			
		case 3:
			final String firstName = (String) value;
			
			// Nejprve ještě otestuji, zda zadané jméno splňuje syntaxi dle
			// reguálrního výrazu:
			if (!firstName.matches(SourceDialog.REG_EX_FOR_FIRST_AND_LAST_NAME)) {
				JOptionPane.showMessageDialog(null, "Zadané jméno nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}				
			
			
			// Prázdný text:
			if (firstName.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Jméno nesmí být prázdné!",
						"Prázdné pole", JOptionPane.ERROR_MESSAGE);
				return;				
			}
			
			
			// delka řetězce:
			if (firstName.length() > 30) {
				JOptionPane.showMessageDialog(null, "Zadané jméno přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			usersList.get(rowIndex).setFirstName(firstName);
			break;
			
		case 4:
			final String lastName = (String) value;
			
			// Nejprve ještě otestuji, zda příjmení splňuje syntaxi dle
			// reguálrního výrazu:
			if (!lastName.matches(SourceDialog.REG_EX_FOR_FIRST_AND_LAST_NAME)) {
				JOptionPane.showMessageDialog(null, "Zadané příjmení nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}				
			
			
			// Prázdný text:
			if (lastName.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Příjmení nesmí být prázdné!",
						"Prázdné pole", JOptionPane.ERROR_MESSAGE);
				return;				
			}
			
			
			// delka řetězce:
			if (lastName.length() > 40) {
				JOptionPane.showMessageDialog(null, "Zadané příjmení přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			usersList.get(rowIndex).setLastName(lastName);
			break;
			
		case 5:
			// Další možnost je vzít si první hodnotu z jednorozměrného pole coby model
			// JComboBoxu.
			final String itemBan = (String) value;
			
			usersList.get(rowIndex).setBan(itemBan.charAt(0));
			break;
			
		case 6:
			usersList.get(rowIndex).setKindOfUser((String) value);
			break;
			
		case 7:
			// Nejprve si musím otestovat, aby se zadané uživatelské jméno už nevyskytovalo
			// v DB.
			// Není to nezbytné, ale pokud by si čistě náhodou zadalo i stejné heslo, byly
			// by duplicity a nevědělo by se, jaký účet se má načíst.
			
			final String pseudonym = (String) value;
			
			
			// Nejprve ještě otestuji, zda je zadané uživatelské jméno splňuje syntaxe dle
			// reguálrního výrazu:
			if (!pseudonym.matches(SourceDialog.REG_EX_PSEUDONYM)) {
				JOptionPane.showMessageDialog(null, "Zadané uživatelské jméno nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}				
			
			
			// Prázdný text:
			if (pseudonym.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Uživatelské jméno nesmí být prázdné!",
						"Prázdné pole", JOptionPane.ERROR_MESSAGE);
				return;				
			}
			
			
			// delka řetězce:
			if (pseudonym.length() > 30) {
				JOptionPane.showMessageDialog(null, "Zadané uživatelské jméno přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			
			
			boolean existPseudonym = false;
			
			// Mohu si list předávat, ale pak bych si musel hlídat veškeré aktualizace, což
			// nedělám, tak si ty uživatele pokaždé musím načíst, abych měl aktuální data.
			final List<User> all_Users = UserForm.DATA_MANAGER.getAllUsers();

			for (final User u : all_Users)
				if (u.getPseudonym().equalsIgnoreCase(pseudonym) && u.getId() != usersList.get(rowIndex).getId()) {
					existPseudonym = true;
					break;
				}
			
			if (!existPseudonym)
				usersList.get(rowIndex).setPseudonym(pseudonym);
			else
				JOptionPane.showMessageDialog(null, "Zadané uživatelské jméno je již využito, zvolte prosím jiné!",
						"Duplicitní jméno", JOptionPane.ERROR_MESSAGE);
			break;
			
		case 8:
			final String password = (String) value;					
			
			
			// Prázdný text:
			if (password.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Heslo nesmí být prázdné!",
						"Prázdné pole", JOptionPane.ERROR_MESSAGE);
				return;				
			}
			
			
			// delka řetězce:
			if (password.length() > 35) {
				JOptionPane.showMessageDialog(null, "Zadané heslo přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			usersList.get(rowIndex).setPassword((String) value);
			break;
			
		case 9:
			final String email = (String) value;
			
			// Nejprve ještě otestuji, zda je zadané uživatelské jméno splňuje syntaxe dle
			// reguálrního výrazu:
			if (!email.matches(SourceDialog.REG_EX_EMAIL)) {
				JOptionPane.showMessageDialog(null, "Zadaný email nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}				
			
			
			// Prázdný text:
			if (email.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Email nesmí být prázdný!",
						"Prázdné pole", JOptionPane.ERROR_MESSAGE);
				return;				
			}
			
			
			// delka řetězce:
			if (email.length() > 50) {
				JOptionPane.showMessageDialog(null, "Zadaný email přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			usersList.get(rowIndex).setEmail(email);
			break;
			
		case 10:
			// Vezmu si text z tabulky a odeberu z něj mezery:
			final String tel = ((String) value).replaceAll("\\s", "");
			
			// Zjistím, zda je to opravdu telefonní číslo dle regulárního výrazu:
			if (tel.matches(REG_EX_TELEPHONE_NUMBER))
				// Zde je, tak jej převedu na int a uložím:
				usersList.get(rowIndex).setTelephoneNumber(Integer.parseInt(tel));

			// Zde číslo není dle formátu, tak na to upozorním uživatele, že se mohou zadat
			// pouze čísla:
			else
				JOptionPane.showMessageDialog(null, "Telefonní číslo nelze uložit, chybná syntaxe!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
			break;
			
		case 11:
			usersList.get(rowIndex).setStreet((String) value);
			break;
			
		case 12:
			final String pscWithoutSpace = String.valueOf((String) value).replaceAll("\\s", "");
			
			/*
			 * Pokud nebude PSČ zadáno, tj. text bude prázdný, pak nastavím jako psč hodnotu
			 * -1 a tak se pozná, že se psč nemá vkládat do DB, jinak tam dám zadané psč.
			 */
			if (!pscWithoutSpace.isEmpty())
				usersList.get(rowIndex).setPsc(Integer.parseInt(pscWithoutSpace));
			else
				usersList.get(rowIndex).setPsc(-1);
			
			// Zde znovu aktualizuji tabulku, protože chci aby se přenačetlo město
			// automaticky, ne až když to uživatel překlikne			
			fireTableDataChanged();
			break;

		default:
			break;
		}
				
		// Uložení změněných dat do DB:
		saveChangedToDb(rowIndex);
	}
	
	
	
	
	@Override
	public String getColumnName(int columnIndex) {
		return columns[columnIndex];
	}
	
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object objValue = getValueAt(0, columnIndex);

		if (objValue != null)
			return objValue.getClass();

		return super.getColumnClass(columnIndex);
	}
	
	
	
	
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return usersList.get(rowIndex).getId() != logginedUserId && columnIndex >= 2 && columnIndex <= 12;
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zapíše změny do DB. (Aktualizuje uživatele, kde se změnily
	 * nějaká data v DB - zapíše změny do DB).
	 * 
	 * Info: 
	 * Tato metoda mohla být v TableModelListeneru -> tableChanged, ale
	 * potřebuji nejprve ještě nastavit hodnoty do Usera, pak je uložit, tak tuto
	 * metoda zavolám až po tom, co se nastaví.
	 * 
	 * @param rowIndex
	 *            - index v listu uživatelů, kde se zmenily nějaká data v tabulce.
	 */
	private void saveChangedToDb(final int rowIndex) {
		final User user = usersList.get(rowIndex);	
		
		final boolean updated = UserForm.DATA_MANAGER.updateUser(user);
		
		if (!updated)
			JOptionPane.showMessageDialog(null,
					"Došlo k chybě při pokusu o aktualizaci uživatele, změny nebyly zapsány do DB!", "Chyba",
					JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	
	
	// Následují metody pro filtrování dat:
	
	
	/**
	 * Metoda, která seřadí uživatele dle ID vzestupně nebo sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle ID
	 *            sestupně nebo vzestupně. true = Sestupně, false = vzestupně
	 */
	final void sortUsersById(final boolean des) {
		if (des)
			usersList.sort((a, b) -> Integer.compare(b.getId(), a.getId()));
		else
			usersList.sort(Comparator.comparingInt(User::getId));
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí uživatele dle jména vzestupně nebo sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle jména
	 *            sestupně nebo vzestupně. true = Sestupně, false = vzestupně
	 */
	final void sortUsersByFirstName(final boolean des) {
		if (des)
			usersList.sort((a, b) -> b.getFirstName().compareToIgnoreCase(a.getFirstName()));
		else
			usersList.sort((a, b) -> a.getFirstName().compareToIgnoreCase(b.getFirstName()));
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí uživatele dle příjmení vzestupně nebo sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle
	 *            příjmení sestupně nebo vzestupně. true = Sestupně, false =
	 *            vzestupně
	 */
	final void sortUsersByLastName(final boolean des) {
		if (des)
			usersList.sort((a, b) -> b.getLastName().compareToIgnoreCase(a.getLastName()));
		else
			usersList.sort((a, b) -> a.getLastName().compareToIgnoreCase(b.getLastName()));
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí uživatele dle toho, zda jsou smazány nebo ne (0 -
	 * vytvořen, 1 - smazán) vzestupně nebo sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle
	 *            hodnoty "smazáno" sestupně nebo vzestupně. true = Sestupně, false
	 *            = vzestupně
	 */
	final void sortUsersByDeleted(final boolean des) {
		if (des)
			usersList.sort((a, b) -> {
				final int c1 = a.getDeleted();
				final int c2 = b.getDeleted();

				return c1 > c2 ? -1 : c1 == c2 ? 0 : 1;
			});

		else
			usersList.sort((a, b) -> {
				final int c1 = a.getDeleted();
				final int c2 = b.getDeleted();

				return c1 < c2 ? -1 : c1 == c2 ? 0 : 1;
			});
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí uživatele dle typu uživatelského účtu vzestupně nebo
	 * sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle typu
	 *            uživatelského účtu sestupně nebo vzestupně. true = Sestupně, false
	 *            = vzestupně
	 */
	final void sortUsersByKindOfUser(final boolean des) {
		if (des)
			usersList.sort((a, b) -> b.getKindOfUser().compareToIgnoreCase(a.getKindOfUser()));
		else
			usersList.sort((a, b) -> a.getKindOfUser().compareToIgnoreCase(b.getKindOfUser()));
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí uživatele dle uživatelského jména vzestupně nebo
	 * sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle
	 *            uživatelského jména sestupně nebo vzestupně. true = Sestupně,
	 *            false = vzestupně
	 */
	final void sortUsersByPseudonym(final boolean des) {
		if (des)
			usersList.sort((a, b) -> b.getPseudonym().compareToIgnoreCase(a.getPseudonym()));
		else
			usersList.sort((a, b) -> a.getPseudonym().compareToIgnoreCase(b.getPseudonym()));
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí uživatele dle emailové adresy vzestupně nebo sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle
	 *            emailové adresy sestupně nebo vzestupně. true = Sestupně, false =
	 *            vzestupně
	 */
	final void sortUsersByEmail(final boolean des) {
		if (des)
			usersList.sort((a, b) -> b.getEmail().compareToIgnoreCase(a.getEmail()));
		else
			usersList.sort((a, b) -> a.getEmail().compareToIgnoreCase(b.getEmail()));
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která seřadí uživatele dle názvu města vzestupně nebo sestupně.
	 * 
	 * @param des
	 *            - logická hodnota o tom, zda se mají uživatelé setřídit dle názvu
	 *            města sestupně nebo vzestupně. true = Sestupně, false = vzestupně
	 */
	final void sortUsersByCity(final boolean des) {
		if (des)
			usersList.sort((a, b) -> {
				final String cityName_1 = getCityName(a.getPsc());
				final String cityName_2 = getCityName(b.getPsc());

				return cityName_2.compareToIgnoreCase(cityName_1);
			});

		else
			usersList.sort((a, b) -> {
				final String cityName_1 = getCityName(a.getPsc());
				final String cityName_2 = getCityName(b.getPsc());

				return cityName_1.compareToIgnoreCase(cityName_2);
			});
		fireTableDataChanged();
	}
}