package kruncik.jan.gallows.dialogs.userResultsForm;

import java.sql.Date;

/**
 * Třída slouží pro vytvoření instance konkrétního typu hry (Countdown). Pouze
 * dědí z abstraktní třídy, protože s typem hry Measurement mají téměř všechny
 * společné atributy, tak abych jej nevytvářel vícekrát.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

class CountdownTableValue extends AbstractTableValue {

	/**
	 * Tato proměnná značí původní nastavený čas na časovač, třeba 5 minut -> tj.
	 * jak dlouho má trvat hra. Pak druhá čas v proměnné time je uplynulý čas, třeba
	 * uživatel hrál jen 2 minuty z těch nastavených 5 - ti.
	 */
	private final String originalTime;
	
	CountdownTableValue(final int gameId, final String groupName, final String dateString, final String comment,
			final String time, final String originalTime, final int lives, final int score, final Date date) {
		super(gameId, groupName, dateString, comment, time, lives, score, date);

		this.originalTime = originalTime;
	}
	
	String getOriginalTime() {
		return originalTime;
	}
}
