package kruncik.jan.gallows.dialogs.userResultsForm;

import java.sql.Date;

/**
 * Tato třída slouží jako objekt, který se vkládá do tabulky, kde jsou hodnoty typu
 * Measurement - hry.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

class MeasurementTableValue extends AbstractTableValue {

	MeasurementTableValue(final int gameId, final String groupName, final String dateString,
			final String comment, final String time, final int lives, final int score, final Date date) {
		super(gameId, groupName, dateString, comment, time, lives, score, date);
	}
}
