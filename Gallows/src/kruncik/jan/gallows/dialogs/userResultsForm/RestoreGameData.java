package kruncik.jan.gallows.dialogs.userResultsForm;

/**
 * Tato třída slouží puze jako "objekt" pro předání hodnot mezi třídami.
 * Obsahuje proměnné pro data, která jsou potřeba pro obnovení již dříve
 * započaté hry.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class RestoreGameData {

	/**
	 * ID hry, ve které se uživatel rozhodl pokračovat.
	 */
	private final int gameId;
	
	private final int score, lives;

	/**
	 * hh, mm, ss. Pokud se jedná o typ hry Measurement, pak je to zatím naměřený
	 * čas, pokud se jedná o typ hry Countdown, pak se jedná o zbývající čas z
	 * nastaveného času.
	 */
	private final int hh, mm, ss;

	RestoreGameData(final int gameId, final int score, final int lives, final int hh, final int mm,
			final int ss) {
		super();

		this.gameId = gameId;
		this.score = score;
		this.lives = lives;
		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
	}

	
	public int getGameId() {
		return gameId;
	}
	
	public int getScore() {
		return score;
	}

	public int getLives() {
		return lives;
	}

	public int getHh() {
		return hh;
	}

	public int getMm() {
		return mm;
	}

	public int getSs() {
		return ss;
	}
}
