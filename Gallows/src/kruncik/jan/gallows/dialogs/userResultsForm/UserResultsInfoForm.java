package kruncik.jan.gallows.dialogs.userResultsForm;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tatot třída slouží jako dialog, který zobrazuje pouze základní informace o
 * dialogu s výsledky her přihlášeného uživatele do apliakce.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class UserResultsInfoForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;

	
	UserResultsInfoForm() {
		super("Info");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 500, 500, "/icons/InfoIconForm.png", true);
		
		
		final JTextArea txtInfo = new JTextArea("Dialog s výsledky obsahuje odehrané hry typu Countdown a Measurement aktuálně přihlášeného uživatele.\n"
				+ "Výsledky lze seřadit abecedně dle názvu skupiny, dle datumu (od nebo po nejstarší) nebo dle dosaženého skóre a životů, vždy vzestupně nebo sestupně.\n"
				+ "Mezi výsledky lze vyhledávat (/ filtrovat výsledky) dle skupiny slovíček, dosaženého skóre nebo zbývajících životů.\n\n"
				+ "Výše zmíněné dva typy her se od sebe liší tak, že hry typu Countdown mají nastavený nějaký čas, během kterého musí hráč uhodnout co nejvíce slovíček a prohraje, resp. hra skončí když hráči dojdou životy nebo nastavený čas.\n"
				+ "Druhý typ hry (Measurement) je typ hry, který pouze měří čas hráči dokud mu nedojdou životy.\n\n"
				+ "Pokračovat ve hře lze v případě\n:"
				+ "- Measurement:\n"
				+ "\t- Počet životů je > (větší) než nula.\n"
				+ "- Countdown\n"
				+ "\t- Počet životů je > (větší) než nula.\n"
				+ "\t- Nastavený čas ještě nevypršel, tj. zbývající čas je > (větší) než nula.");
		
		txtInfo.setEditable(false);
		
		txtInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		
		txtInfo.setLineWrap(true);
		
		add(new JScrollPane(txtInfo), BorderLayout.CENTER);
		
		
		
		prepareWindow();
	}

	@Override
	protected void checkData() {
		/*
		 * Tato metoda není v tomto dialogu potřeba.
		 */
	}
}
