package kruncik.jan.gallows.dialogs.userResultsForm;

import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Tato třída slouží jako menu, které obsauje pouze položky pro zavření a
 * položku pro otevření dialogu s informacemi o "tomto" dialogu s výsledky
 * odehrených her přihlášeného uživatele.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

class MyMenu extends JMenuBar {

	
	private static final long serialVersionUID = 1L;

	
	
	MyMenu(final JDialog userResultsForm) {
		super();
		
		final JMenu menuDialog = new JMenu("Dialog");
		
		final JMenuItem itemInfo = new JMenuItem("Info");
		final JMenuItem itemClose = new JMenuItem("Zavřít");
		
		itemInfo.addActionListener(Event -> new UserResultsInfoForm());
		itemClose.addActionListener(Event -> userResultsForm.dispose());
		
		
		menuDialog.add(itemInfo);
		menuDialog.addSeparator();
		menuDialog.add(itemClose);
		
		add(menuDialog);
	}
}
