package kruncik.jan.gallows.dialogs.userResultsForm;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JTable;

/**
 * Tato třída je zde potřeba, aby šlo klikat na tlačítka - komponenty Jbutton v
 * tabulce Jtable, resp. aby mohlo to tlačítko reagovat na kliknutí.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MyTableButtonMouseListener extends MouseAdapter {

    private final JTable table;

    public MyTableButtonMouseListener(final JTable table) {
    	super();
    	
        this.table = table;
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        int column = table.getColumnModel().getColumnIndexAtX(e.getX()); // get the coloum of the button
        int row    = e.getY()/table.getRowHeight(); //get the row of the button

                /*Checking the row or column is valid or not*/
		if (row < table.getRowCount() && row >= 0 && column < table.getColumnCount() && column >= 0) {
			final Object value = table.getValueAt(row, column);
			if (value instanceof JButton)
				/* perform a click event */
				((JButton) value).doClick();
		}
    }
}
