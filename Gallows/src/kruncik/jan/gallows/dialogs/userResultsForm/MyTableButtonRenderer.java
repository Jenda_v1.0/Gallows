package kruncik.jan.gallows.dialogs.userResultsForm;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Tato třída je zde potřeba, aby bylo možné vykreslit komponentu JButton v
 * Jtable.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MyTableButtonRenderer implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		return (JButton) value;
	}
}