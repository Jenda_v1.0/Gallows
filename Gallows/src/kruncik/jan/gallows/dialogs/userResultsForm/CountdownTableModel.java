package kruncik.jan.gallows.dialogs.userResultsForm;

import java.util.List;

import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.gallowsApp.App;

/**
 * Třída, která slouží jako table model pro tabulku, kde jsou zobrazeny hry typu
 * Countdown odehrané přihlášeným uživatelem.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CountdownTableModel extends MyAbstractTableModel {

	
	private static final long serialVersionUID = 1L;




	CountdownTableModel(final String[] columns, final List<Group> allGroupsList,
			final List<KindOfGame> kindsOfGameList, final UserResultsForm resultsForm, final App app) {
		super(columns, allGroupsList, kindsOfGameList, resultsForm, app);
	}	
	
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return valuesList.get(rowIndex).getGroupName();
			
		case 1:
			return valuesList.get(rowIndex).getDateString();
			
		case 2:
			return valuesList.get(rowIndex).getComment();

		case 3:
			return ((CountdownTableValue) valuesList.get(rowIndex)).getOriginalTime();
			
		case 4:
			return valuesList.get(rowIndex).getTime();
			
		case 5:
			return valuesList.get(rowIndex).getLives();
			
		case 6:
			return valuesList.get(rowIndex).getScore();
			
		case 7:
			return createContinueButton(rowIndex);

		case 8:
			return createDeleteButton(rowIndex);
			
		default:
			break;
		}
		
		return "?";
	}
	
	
	
	
	@Override
	public Class<?> getColumnClass(int column) {
		if (getValueAt(0, column) != null)
			return getValueAt(0, column).getClass();
					
		return super.getColumnClass(column);
	}
}
