package kruncik.jan.gallows.dialogs.userResultsForm;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.dialogs.dataForDialogs.SelectedGame;
import kruncik.jan.gallows.gallowsApp.App;

/**
 * Tato třída slouží puze pro deklaraci proměnných a nějakých metod pro nastavení
 * hodnot do modelu a následně to tabulky.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class MyAbstractTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	
	
	
	
	/**
	 * List s hodnotami pro tabulku.
	 */
	List<AbstractTableValue> valuesList;
	
	
	/**
	 * Proměnná, která obsahuje názvy sloupců
	 */
	private final String[] columns;

	
	
	
	
	// Proměnné pro obnovení hry:
	
	/**
	 * List, který obsahuje všechny hry, a pro obnovení se vybere ta jedna zvolená.
	 */
	private final List<Group> allGroupsList;
	
	/**
	 * List, který obsahuje typy her, tento objekt se předá do "hlavní" aplikace pro
	 * hraní hry - kvůli nastavení času apod..
	 */
	private final List<KindOfGame> kindsOfGameList;
	
	
	
	/**
	 * Reference na instanci dialogu s výsledky, aby jej bylo možné zavřít, pokud se
	 * uživatel rozhorne obnovit hru.
	 */
	private final UserResultsForm resultsForm;
	
	
	
	
	/**
	 * Reference na "hlavní" aplikace coby hru šibenice, abych tam mohl zavolat
	 * metodu pro pokračování ve hře, resp. obnovení hry -> obnovení původních dat
	 * jako jsou životy, čas, skóre apod.
	 */
	private final App app;
	
	
	
	
	MyAbstractTableModel(final String[] columns, final List<Group> allGroupsList,
			final List<KindOfGame> kindsOfGameList, final UserResultsForm resultsForm, final App app) {
		super();

		this.columns = columns;
		this.allGroupsList = allGroupsList;
		this.kindsOfGameList = kindsOfGameList;
		this.resultsForm = resultsForm;
		this.app = app;
	}
	
	
	/**
	 * Setr na list s hodnotami pro tabulku, který ji při nastavení dat zrovna
	 * aktualizuje (tabulku).
	 * 
	 * @param valuesList
	 *            - list s hodnotami pro vložení do tabulky.
	 */
	void setValuesList(List<AbstractTableValue> valuesList) {
		this.valuesList = valuesList;
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která vrátí položku na zadaném indexu v listu položek v tabulce.
	 * 
	 * @param index
	 *            - pozice prvku v listu.
	 * @return prvek v listu.
	 */
	AbstractTableValue getValueAtIndex(final int index) {
		return valuesList.get(index);
	}
	
	
	/**
	 * Metoda, která vrátí velikost listu s položkami v tabulce.
	 * 
	 * @return velikost listu s položkami v tabulce.
	 */
	int getValuesListSize() {
		return valuesList.size();
	}
	
	
	@Override
	public int getColumnCount() {
		/*
		 * To +1 je tlačítko pro smazání hry -> uživatel může smazat svou hru, když bude
		 * chtít.
		 */
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return valuesList.size();
	}

	
	@Override
	public String getColumnName(int column) {
		return columns[column];
	}
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 2;
	}
	
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex ) {
		if (columnIndex == 2) {
			final String comment = (String) value;

			if (comment.length() > 255) {
				JOptionPane.showMessageDialog(null, "Zadaný komentář přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			valuesList.get(rowIndex).setComment((String) value);
		}
			
		
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		/*
		 * Tuto metodu si doplním v potomcích dle potřeby, každý potomek má trochu jiné
		 * hodnoty, resp. jiný počet.
		 */
		return null;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci tlačítka pro pokračování. A doplní do tlačítka
	 * událost po kliknutí a sice, že pokud se na to tlačítko klikne, tak půjde
	 * obnovit hra - obnoví se.
	 * 
	 * Pokud nebudou splněny podmínky pro pokračování ve hře, tj. nesou životy, nebo
	 * čas (dle typu hry), pak tlačítko bude zablokované vůči kliknutí, tj. nepůjde
	 * hra obnovit, resp. v ní pokračovat.
	 * 
	 * @param index
	 *            - Index pozice dané hodnoty v tabulce, resp. v listu (konkrétní
	 *            hra, která se má obnovit).
	 * 
	 * @return výše popsané, vytvořené a připravené tlačítko.
	 */
	final JButton createContinueButton(final int index) {
		/*
		 * Tlačítko, které slouží pro pokračování v uložené hře (je - li to možné).
		 * Toto tlačítko bude povolené puze v případě, že:
		 * Measurement:
		 * 	- životy jsou větší než nula.
		 * 
		 * Countdown
		 * 	- Životy jsou větší než nula a zbývající čas ještě nevyprčel - větší než nula. 
		 */
		final JButton btnContinue = new JButton("Pokračovat");
		
		final AbstractTableValue value = valuesList.get(index);
		
		if (value instanceof MeasurementTableValue) {
			/*
			 * Zde se má načíst hra typu Measurement
			 */
			
			if (value.getLives() > 0)
				btnContinue.addActionListener(Event -> restoreGame(value, "Measurement"));
			else
				btnContinue.setEnabled(false);
		}
		
		else if (value instanceof CountdownTableValue) {
			/*
			 * Zde se má načíst hra typu Countdown.
			 */						
			final String[] dateArray = value.getTime().replaceAll("\\s", "").split(":");
			
			final int hh, mm, ss;
			hh = Integer.parseInt(dateArray[0]);
			mm = Integer.parseInt(dateArray[1]);
			ss = Integer.parseInt(dateArray[2]);			
			
			if (value.getLives() > 0 && (hh > 0 || mm > 0 || ss > 0))
				btnContinue.addActionListener(Event -> restoreGame(value, "Countdown"));
			else
				btnContinue.setEnabled(false);
		}
		
		return btnContinue;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří tlačítko, které smaže hru na příslušném řádku.
	 * 
	 * @param rowIndex
	 *            - Index řádku, resp. pozice hry v listu zobrazených her, která se
	 *            má smazat z DB.
	 * @return - výše popsané tlačítko, které smaže označenou hru.
	 */
	final JButton createDeleteButton(final int rowIndex) {
		/*
		 * Tlačítko, které po kliknutí na něj smaže hru z DB a přenačte tabulku s daty.
		 * 
		 * - Smaže se hra z DB.
		 * 
		 * - Smaže se hra z listu her v listu v tabulce (není třeba jej znovu přenačítat
		 * apod. stačí puze odebrat tu jednu položku.)
		 * 
		 * - Pak se přenačtou veškeré hry z DB do aplikace -> do dialogu s výsledky, aby
		 * když uživatel něco vyhledá apod. Tak aby se pracovalo s již aktuálními daty.
		 */
		final JButton btnDeleteGame = new JButton("Smazat");
		
		btnDeleteGame.addActionListener(Event -> {
			if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, "Opravdu si přejete smazat tuto hru?",
					"Smazat", JOptionPane.YES_NO_OPTION)) {
				final boolean deleted = UserResultsForm.deleteGameFromDb(valuesList.get(rowIndex).getGameId());

				if (!deleted) {
					JOptionPane.showMessageDialog(null,
							"Doělo k chybě při pokusu o smazání hry z DB. Hry nebyla smazána!", "Chyba",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				valuesList.remove(rowIndex);

				resultsForm.reloadGamesFromDb();

				fireTableDataChanged();
			}
		});
		
		return btnDeleteGame;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která obnoví zvolenou hru a započne její pokračování.
	 * 
	 * @param value
	 *            - označená hra v tabulce, ve které chce uživatel pokračovat.
	 * @param kind
	 *            typ hry - kvůli načtení typu této hry (celého objektu) z DB
	 */
	private void restoreGame(final AbstractTableValue value, final String kind) {
		
		final Group group = getGroupByGroupName(value.getGroupName());
		final KindOfGame kindOfGame = getKindOfGameByKind(kind);
		
		/*
		 * Jeden z objektů, který se předá do "šibenice" - třídy App, aby bylo možné
		 * obnovit hru:
		 */
		final SelectedGame continueInGame = new SelectedGame(group, kindOfGame);
		
		/*
		 * Do této proměnné vložím zbylá data, která jsou potřeba pro obnovení hry, jako
		 * jsou například životy, skóre, čas apod.
		 */
		final String[] timeArray = value.getTime().replaceAll("\\s", "").split(":");
		final int hh = Integer.parseInt(timeArray[0]);
		final int mm = Integer.parseInt(timeArray[1]);
		final int ss = Integer.parseInt(timeArray[2]);
		
		final RestoreGameData restoreGameData = new RestoreGameData(value.getGameId(), value.getScore(), value.getLives(), hh,
				mm, ss);
		
		app.continueInGame(continueInGame, restoreGameData);
		
		resultsForm.dispose();		
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Tato metoda je zde jen pro "urychlení" načítání, abych nemusel znovu načítat
	 * z DB nějaká data, která již mám načtený, jinak nad DB je tato metoda, ale
	 * list skupin již mám načtený ve formuláři, tak si zde pouze vyberu tu jednu
	 * dle názvu, jinak by tato metoda nebyl ptřeba. (Neřeším zde "chyby" ve smyslu,
	 * že se skupina dle názvu nenajde apod. -> toto by nastat nemělo.)
	 * 
	 * @param groupName
	 *            - Název skupiny, která se má vrátit (její objekt)
	 * @return skupinu slov s daným názvem (groupName)
	 */
	private Group getGroupByGroupName(final String groupName) {
		return allGroupsList.stream().filter(g -> g.getGroupName().equals(groupName)).collect(Collectors.toList())
				.get(0);
	}
	
	
	
	/**
	 * Metoda, která vrátí Typ hry (objekt z DB) dle typu hry coby názvu -> kind.
	 * Tato metoda je podobná jako metoda výše (getGroupByGroupName), tj. taky zde
	 * není potřeba, jen abych znovu nenačital z DB něco co již mám načtené, tak si
	 * to do této třídy pouze "předám a najdu".
	 * 
	 * (Také zde neřeším, že se objekt nenajde -> nemělo by k tomu dojít)
	 * 
	 * @param kind
	 *            - typ hry (název typu hry), jejíž objekt se má vrátit.
	 * @return Kind of game dle typu hry - názvu.
	 */
	private KindOfGame getKindOfGameByKind(final String kind) {
		return kindsOfGameList.stream().filter(k -> k.getKind().equals(kind)).collect(Collectors.toList()).get(0);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí pložky v tabulce dle názvu skupiny.
	 * 
	 * @param des
	 *            - zda se mají položky seřadit dle názvu skupiny slovíček sestupně
	 *            nebo vzestupně. true = sestupně, false = vzestupně.
	 */
	final void sortByGroupName(final boolean des) {
		if (des)
			valuesList.sort((a, b) -> a.getGroupName().compareToIgnoreCase(b.getGroupName()));
		else
			valuesList.sort((a, b) -> b.getGroupName().compareToIgnoreCase(a.getGroupName()));
		
		fireTableDataChanged();
	}
	
	
	
	
	
	/**
	 * Metoda, která seřadí pložky v tabulce dle dosaženého skóre.
	 * 
	 * @param des
	 *            - zda se mají položky seřadit dle dosaženého skóre sestupně nebo
	 *            vzestupně. true = sestupně, false = vzestupně.
	 */
	final void sortByScore(final boolean des) {
		if (des)
			valuesList.sort((a, b) -> Integer.compare(b.getScore(), a.getScore()));
		else
			valuesList.sort(Comparator.comparingInt(AbstractTableValue::getScore));
		
		fireTableDataChanged();
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí pložky v tabulce dle zbývajících životů.
	 * 
	 * @param des
	 *            - zda se mají položky seřadit dle zbývajících životů sestupně nebo
	 *            vzestupně. true = sestupně, false = vzestupně.
	 */
	final void sortByLives(final boolean des) {
		if (des)
			valuesList.sort((a, b) -> a.getLives() > b.getLives() ? -1 : a.getLives() == b.getScore() ? 0 : 1);
		else
			valuesList.sort((a, b) -> a.getLives() < b.getLives() ? -1 : a.getLives() == b.getScore() ? 0 : 1);

		fireTableDataChanged();
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí pložky v tabulce dle datumu vytvoření.
	 * 
	 * @param des
	 *            - zda se mají položky seřadit dle datumu vytvoření sestupně nebo
	 *            vzestupně. true = sestupně, false = vzestupně.
	 */
	final void sortByDate(final boolean des) {
		if (des)
			valuesList.sort((a, b) -> b.getDate().compareTo(a.getDate()));
		else
			valuesList.sort(Comparator.comparing(AbstractTableValue::getDate));

		fireTableDataChanged();
	}
}
