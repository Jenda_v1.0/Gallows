package kruncik.jan.gallows.dialogs.userResultsForm;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;

import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.Game;
import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.dialogs.SourceDialog;
import kruncik.jan.gallows.gallowsApp.App;

/**
 * Dialog, ve kterém jsou zobrazeny výsledky přihlášeného uživatele v aplikaci.
 * (Výsledky obou her -> Measuremetn a Countdown)
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class UserResultsForm extends SourceDialog implements ActionListener, TableModelListener {

	
	private static final long serialVersionUID = 1L;

	

	/**
	 * Komponenta pro manipulaci s daty v DB.
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	/**
	 * Regulární výraz pro název skupiny. Může obsahovat velká a malá písmena s /
	 * bez diakritiky, podtržítka a číslice a mezery.
	 */
	private static final String REG_EX_GROUP_NAME = "^[ěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ \\d\\w]+$";
	
	
	/**
	 * Regulární výraz pro počet životů. Projde pouze číslo v intervalu <0, 8> (může
	 * být max 8 životů) (včetně mezer kolem čísla.)
	 */
	private static final String REG_EX_LIVES = "^\\s*[0-8]{1}\\s*$";
	
	
	/**
	 * Proměnná, která obsahuje názvy sloupců pro tabulku Measurement.
	 */
	private static final String[] MEASUREMENT_TABLE_COLUMNS = { "Skupina slovíček", "Datum vytvoření", "Komentář",
			"Změřený čas", "Počet životů", "Skóre", "Pokračovat ve hře", "Smazat hru" };

	/**
	 * Proměnná, která obsahuje názvy sloupců pro tabulku Countdown.
	 */
	private static final String[] COUNTDOWN_TABLE_COLUMNS = { "Skupina slovíček", "Datum vytvoření", "Komentář",
			"Nastavený čas", "Zbývající čas", "Počet životů", "Skóre", "Pokračovat ve hře", "Smazat hru" };

	
	
	
	
	
	/**
	 * Model pro cmbOrderBy.
	 */
	private static final String[] ORDER_BY_MODEL = { "Vše", "Skupina - sestupně", "Skupina - vzestupně",
			"Skóre - sestupně", "Skóre - vzestupně", "Datum - sestupně", "Datum - vzestupně", "Životy - sestupně",
			"Životy - vzestupně" };
		
	/**
	 * komponenta, která obsahuje možnosti, dle kterých lze seřadit položky v
	 * tabulkách.
	 */
	private final JComboBox<String> cmbOrderBy;
	
	
	/**
	 * Model pro cmbSearch.
	 */
	private static final String[] SEARCH_MODEL = {"Skupina slovíček", "Skóre", "Životy"};
	
	/**
	 * Komponenta, ve které se vybere, dle čeho chce uživatel vyhledávat data v tabulce.
	 */
	private JComboBox<String> cmbSearch;
	
	
	/**
	 * Textové pole pro zadání hodnoty, která se má vyhledat v tabulce.
	 */
	private JTextField txtSearch;
	
	/**
	 * Tlačítko, které spustí vyhledávání dat v tabulce dle zadaného kritéria v
	 * textovém poli txtSearch.
	 */
	private JButton btnSearch;
	
	
	
	
	
	// Modely pro tabulky s hodnotami pro typy her Countdown a Measurement.
	private final MyAbstractTableModel measurementTableModel, countdownTableModel;
	
	
	
	
	
	/**
	 * List, který obsahuje veškeré hry načtené z DB
	 */
	private List<Game> allGames;
	
	/**
	 * List, který obsahuje veškeré skupiny slovíček, abych mohl získatnázvy těch
	 * skupin.
	 */
	private static final List<Group> ALL_GROUPS_LIST = DATA_MANAGER.getAllGroups();
	
	
	/**
	 * Proměnná, která obsahuje všechny hodnoty - odehrané hry přihlášeného
	 * uživatele převedené do objektů pro zobrazení v tabulce.
	 */
	private static final List<AbstractTableValue> ALL_TABLE_VALUES = new ArrayList<AbstractTableValue>();
	
	
	
	
	
	// Tabulky pro reprezentaci dat:
	private final JTable tableMeasurement, tableCountdown;
	
	
	
	
	
	
	/**
	 * ID přihlášeného uživatele.
	 */
	private final int userId;
	
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param userId
	 *            - ID přihlášeného uživatele do aplikace, aby se mohli načíst jeho
	 *            odehrané hry.
	 * @param app
	 *            - reference na "hlavní" okno aplikace, abych mohl předat tuto
	 *            referenci do dialogu kvůli obnově hry - pokračování ve hře.
	 */
	public UserResultsForm(final int userId, final App app) {
		super("Výsledky");

		this.userId = userId;
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 975, 500, "/icons/UserResultsIconForm.png", true);		
		
		setJMenuBar(new MyMenu(this));		
		
		allGames = DATA_MANAGER.getAllGamesByUserId(userId);
		
		
		setAllGamesToListForTables();
		
		
		createKeyAdapter();
		
		
		
		final JToolBar toolBar = new JToolBar();
		toolBar.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		toolBar.add(new JLabel("Seřadit"));
		
		cmbOrderBy = new JComboBox<>(ORDER_BY_MODEL);
		cmbOrderBy.addActionListener(this);
		toolBar.add(cmbOrderBy);
		
		toolBar.add(createPnlSearch());
		
		add(toolBar, BorderLayout.NORTH);
		
		
		
		
		
		// Renderer, který nastaví hodnoty na střed buňky:
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(SwingUtilities.CENTER);
		
		
		
		// List, do kterého se načtu typy her, abych je mohl předat pak dále do dialogu
		// / formuláře.
		final List<KindOfGame> kindsOfGamesList = DATA_MANAGER.getAllKindsOfGames();
		
		
		measurementTableModel = new MeasurementTableModel(MEASUREMENT_TABLE_COLUMNS, ALL_GROUPS_LIST, kindsOfGamesList, this, app);
		measurementTableModel.setValuesList(getAllValues("Measurement"));
		
		countdownTableModel = new CountdownTableModel(COUNTDOWN_TABLE_COLUMNS, ALL_GROUPS_LIST, kindsOfGamesList, this, app);
		countdownTableModel.setValuesList(getAllValues("Countdown"));
		
		
		tableMeasurement = new JTable(measurementTableModel);
		// Aby tabulka reagovala na změnu dat v buňkách:
		tableMeasurement.getModel().addTableModelListener(this);
		// Aby tlačítka v tabulce reagovala na kliknutí:
		tableMeasurement.addMouseListener(new MyTableButtonMouseListener(tableMeasurement));
	
		// Nastavím centrování objektů v buňkách:
		for (int i = 0; i < tableMeasurement.getColumnCount(); i++)
			tableMeasurement.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		
		// Nastavím do předposledního sloupce renderer, aby bylo možné vykreslit komponentu JButton v Jtable:
		tableMeasurement.getColumnModel().getColumn(MEASUREMENT_TABLE_COLUMNS.length - 2)
				.setCellRenderer(new MyTableButtonRenderer());
		
		// Poslední sloupec je tlačítko pro smazání hry:
		tableMeasurement.getColumnModel().getColumn(MEASUREMENT_TABLE_COLUMNS.length - 1)
		.setCellRenderer(new MyTableButtonRenderer());
		
		
		
		tableCountdown = new JTable(countdownTableModel);		
		// Aby tabulka reagovala na změnu dat v buňkách:
		tableCountdown.getModel().addTableModelListener(this);
		
		// Aby tlačítka v tabulce reagovala na kliknutí:
		tableCountdown.addMouseListener(new MyTableButtonMouseListener(tableCountdown));
		
		// Nastavím centrování objektů v buňkách:
		for (int i = 0; i < tableCountdown.getColumnCount(); i++)
			tableCountdown.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		
		// Nastavím do předposledního sloupce renderer, aby bylo možné vykreslit komponentu JButton v Jtable:
		tableCountdown.getColumnModel().getColumn(COUNTDOWN_TABLE_COLUMNS.length - 2)
				.setCellRenderer(new MyTableButtonRenderer());
		
		// Poslední sloupec je také tlačítko - pro smazání hry:
		tableCountdown.getColumnModel().getColumn(COUNTDOWN_TABLE_COLUMNS.length - 1)
		.setCellRenderer(new MyTableButtonRenderer());
		
		
		
		final JScrollPane jspMeasurementTable = new JScrollPane(tableMeasurement);
		jspMeasurementTable.setBorder(BorderFactory.createTitledBorder("Hry typu Measurement"));
		
		final JScrollPane jspCountdownTable = new JScrollPane(tableCountdown);
		jspCountdownTable.setBorder(BorderFactory.createTitledBorder("Hry typu Countdown"));
		
		
		final JSplitPane jspTables = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, jspMeasurementTable,
				jspCountdownTable);
		
		jspTables.setOneTouchExpandable(true);
		jspTables.setResizeWeight(0.5d);
		
		add(jspTables, BorderLayout.CENTER);		
		
		
		
		
		
		prepareWindow();
	}

	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří panel, který obsahuje komponenty pro vyhledávání dat v
	 * tabulce. Jedná se o tlačítko, které zahájí vyhledání, popřípadě stisknutí
	 * klávesy enter v textovém poli po zadání hodnot a JComboBox, který obsahuje
	 * položky, ve kterých lze vyhledávat.
	 * 
	 * @return výše popsaný panel popsnými komponentami.
	 */
	private JPanel createPnlSearch() {
		final JPanel pnlSearch = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		pnlSearch.add(new JLabel("Vyhledat: "));
		
		cmbSearch = new JComboBox<>(SEARCH_MODEL);
		pnlSearch.add(cmbSearch);
		
		txtSearch = new JTextField(20);
		txtSearch.addKeyListener(keyAdapter);
		pnlSearch.add(txtSearch);
		
		btnSearch = new JButton("Vyhledat");
		btnSearch.addActionListener(Event -> checkData());
		pnlSearch.add(btnSearch);
		
		return pnlSearch;
	}
	
	
		
	
	
	
	
	
	
	
	/**
	 * Metoda, která vloží do listu ALL_TABLE_VALUES všechny hodnoty (hry)
	 * přhlášeného uživatele, aby se nemuseli pořád do kola vytvářet, tak budou
	 * všechny v tomto listu, ze kterého se budou pouze filtrovat nějaké hodnoty.
	 */
	private void setAllGamesToListForTables() {
		ALL_TABLE_VALUES.clear();
		
		/*
		 * Rozdělím do listu ALL_TABLE_VALUES hodnoty dle typu, aby se lépe rozpoznávaly
		 * a vkládali do tabulek.
		 */
		
		allGames.stream().filter(g -> g.getKindOfGame().equals("Measurement"))
				.forEach(m -> ALL_TABLE_VALUES.add(
						new MeasurementTableValue(m.getId(), getGroupNameByGroupId(m.getGroupId()), m.getFormatedDate(),
								m.getComment(), m.getFormatedTime(), m.getLives(), m.getScore(), m.getDate())));
		
		
		allGames.stream().filter(f -> f.getKindOfGame().equals("Countdown"))
				.forEach(g -> ALL_TABLE_VALUES.add(new CountdownTableValue(g.getId(),
						getGroupNameByGroupId(g.getGroupId()), g.getFormatedDate(), g.getComment(), g.getFormatedTime(),
						g.getFormattedOriginalTime(), g.getLives(), g.getScore(), g.getDate())));
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která znovu načte data - hry z DB a vloží jej do listu allGames a
	 * přenačte list s hodnotami pro tabulky, aby se při dalším vyhledání
	 * zobrazovaly již aktuální hodnoty.
	 */
	final void reloadGamesFromDb() {
		allGames = DATA_MANAGER.getAllGamesByUserId(userId);
		
		setAllGamesToListForTables();
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která z DB vymaže hru s daným ID.
	 * 
	 * @param gameId
	 *            - ID hry, která se má vymazat z DB.
	 * 
	 * @return true, pokud se hra úspěšně vymaže, false, pokud dojde k nějaé chybě.
	 */
	static boolean deleteGameFromDb(final int gameId) {
		return DATA_MANAGER.deleteGame(gameId);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která z listu her danéh uživatele vybere pouze hry typu kindOfGame
	 * (Measurement nebo Countdown) (jedná se o instance tříd MeasurementTableValue
	 * nebo CountdownTableValue) a vrátí je v daném listu typu AbstractTableValue.
	 * 
	 * @return list typu AbstractTableValue, který obsahuje hry typu
	 *         MeasurementTableValue nebo CountdownTableValue s potřebnými hodnotami
	 *         pro tabulku.
	 */
	private List<AbstractTableValue> getAllValues(final String kindOfGame) {
		final List<AbstractTableValue> values = new ArrayList<>();

		if (kindOfGame.equals("Measurement"))
			ALL_TABLE_VALUES.stream().filter(g -> g instanceof MeasurementTableValue).forEach(values::add);

		else if (kindOfGame.equals("Countdown")) // Podmínka zde být už nemusí, jen abych poznal hodnoty.
			ALL_TABLE_VALUES.stream().filter(g -> g instanceof CountdownTableValue).forEach(values::add);

		return values;
	}
	
	
	
	
	
	
	
	
	

	
	/**
	 * Metoda, která vrátí název skupiny dle ID dané skupiny.
	 * 
	 * @param id
	 *            - ID skuipny, ze které se má vzít název skupiny.
	 * @return název skupiny s daným ID.
	 */
	private static String getGroupNameByGroupId(final int id) {
		return ALL_GROUPS_LIST.stream().filter(a -> a.getId() == id).collect(Collectors.toList()).get(0).getGroupName();
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * Metoda se zavolá po kliknutí na tlačítko vyhledat nebo stisknutí enteru v
		 * textovém poli pro vyhledání řetezce v tabulce výsledků odehraných her
		 * přihlášeného uživatele.
		 * 
		 * Postup:
		 * - Zjistím, zda není prázdé pole pro zadání vyhledávácího řetězce.
		 * - Otestuji, jaká položka je zvolená v JComboBoxu a dle toho otestuji zadaný
		 * text na regulární výrazy.
		 * - Pak přetypuji zadanou hodnotu na potřebný typ a vyhledám data v listu.
		 */
		
		if (txtSearch.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Pole pro zadání textového řetězce pro vyhledání výsledku je prázdné!",
					"Prázdné pole", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		// Zvolená položka pro vyhledání:
		final String selectedItem = (String) cmbSearch.getSelectedItem();
		
		// Zde je zvoleno vyhledávíní dle názvu skupina slovíček:
		if (selectedItem.equals(SEARCH_MODEL[0])) {
			if (txtSearch.getText().matches(REG_EX_GROUP_NAME)) {
				/*
				 * Nastvím do modelů tabulek hodnoty (výsledky her), kde se shodují názvy skupin
				 * slovíček a instance hodnot pro dané tabulky, tj. tabulka pro zobrazení
				 * výsledků typu Countdown bude obsahovat jen hodnoty typu CountdownTableValue
				 * apod. pro tabulku typu Measurement.
				 */								
				measurementTableModel.setValuesList(
						ALL_TABLE_VALUES.stream()
								.filter(g -> g.getGroupName().toUpperCase().contains(txtSearch.getText().toUpperCase())
										&& g instanceof MeasurementTableValue)
								.collect(Collectors.toList()));

				countdownTableModel.setValuesList(ALL_TABLE_VALUES.stream().filter(
						g -> g.getGroupName().equalsIgnoreCase(txtSearch.getText()) && g instanceof CountdownTableValue)
						.collect(Collectors.toList()));
			}
			else
				JOptionPane.showMessageDialog(this, "Název skupiny není zadán v požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}
		

		// Zde je zvoleno vyhledávíní dle dosaženého skóre
		else if (selectedItem.equals(SEARCH_MODEL[1])) {
			if (txtSearch.getText().matches(REG_EX_SCORE)) {
				final int score = Integer.parseInt(txtSearch.getText().replaceAll("\\s", ""));
				
				measurementTableModel.setValuesList(ALL_TABLE_VALUES.stream()
						.filter(g -> g.getScore() == score && g instanceof MeasurementTableValue)
						.collect(Collectors.toList()));
				
				countdownTableModel.setValuesList(
						ALL_TABLE_VALUES.stream().filter(g -> g.getScore() == score && g instanceof CountdownTableValue)
								.collect(Collectors.toList()));
			}
			else
				JOptionPane.showMessageDialog(this, "Skóre není zadáno v požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}
		
		
		// Zde je zvoleno vyhledávíní dle zbývajících životů:
		else if (selectedItem.equals(SEARCH_MODEL[2])) {
			if (txtSearch.getText().matches(REG_EX_LIVES)) {
				final int lives = Integer.parseInt(txtSearch.getText().replaceAll("\\s", ""));
				
				measurementTableModel.setValuesList(ALL_TABLE_VALUES.stream()
						.filter(g -> g.getLives() == lives && g instanceof MeasurementTableValue)
						.collect(Collectors.toList()));
				
				countdownTableModel.setValuesList(
						ALL_TABLE_VALUES.stream().filter(g -> g.getLives() == lives && g instanceof CountdownTableValue)
								.collect(Collectors.toList()));
			}
			else
				JOptionPane.showMessageDialog(this, "Počet životů není zadáno v požadované syntaxi!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}
	}





	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JComboBox<?> && e.getSource() == cmbOrderBy) {
			final String selectedItem = (String) cmbOrderBy.getSelectedItem();

			if (selectedItem.equals(ORDER_BY_MODEL[0])) {
				// Zde se má zobrazit vše
				measurementTableModel.setValuesList(getAllValues("Measurement"));
				countdownTableModel.setValuesList(getAllValues("Countdown"));
			} else if (selectedItem.equals(ORDER_BY_MODEL[1])) {
				measurementTableModel.sortByGroupName(true);
				countdownTableModel.sortByGroupName(true);
			} else if (selectedItem.equals(ORDER_BY_MODEL[2])) {
				measurementTableModel.sortByGroupName(false);
				countdownTableModel.sortByGroupName(false);
			} else if (selectedItem.equals(ORDER_BY_MODEL[3])) {
				measurementTableModel.sortByScore(true);
				countdownTableModel.sortByScore(true);
			} else if (selectedItem.equals(ORDER_BY_MODEL[4])) {
				measurementTableModel.sortByScore(false);
				countdownTableModel.sortByScore(false);
			} else if (selectedItem.equals(ORDER_BY_MODEL[5])) {
				measurementTableModel.sortByDate(true);
				countdownTableModel.sortByDate(true);
			} else if (selectedItem.equals(ORDER_BY_MODEL[6])) {
				measurementTableModel.sortByDate(false);
				countdownTableModel.sortByDate(false);
			} else if (selectedItem.equals(ORDER_BY_MODEL[7])) {
				measurementTableModel.sortByLives(true);
				countdownTableModel.sortByLives(true);
			} else if (selectedItem.equals(ORDER_BY_MODEL[8])) {
				measurementTableModel.sortByLives(false);
				countdownTableModel.sortByLives(false);
			}
		}
	}







	@Override
	public void tableChanged(TableModelEvent e) {
		/*
		 * Zjistím, v jaké tabulce došlo ke změně komentáře a daný komentář aktualizuji
		 * v DB.
		 * 
		 * (Nic jiného než komentář nelze změnit, tak si zjistím ID hry a aktualizuji v
		 * dané hře komentář)
		 */
		
		if (e.getFirstRow() < 0)
			return;		
		
		
		if (e.getSource() == tableMeasurement.getModel()) {
			// Pokud není označen řádek, pak není třeba pokračovat, jedná se o aktualizaci
			// dat v tabulce
			// Pokud je list s hodnotami prázdný, dojde k chybě -* například, pokud se
			// vymaže poslední pložka
			if (tableMeasurement.getSelectedRow() < 0 || measurementTableModel.getValuesListSize() == 0)
				return;

			final AbstractTableValue value = measurementTableModel.getValueAtIndex(e.getFirstRow());

			if (value.getComment() != null)
				DATA_MANAGER.updateCommentInGame(value.getGameId(), value.getComment());
		}
		
		else if (e.getSource() == tableCountdown.getModel()) {
			// Pokud není označen řádek, pak není třeba pokračovat, jedná se o aktualizaci
			// dat v tabulce
			// Pokud je list s hodnotami prázdný, dojde k chybě -* například, pokud se
			// vymaže poslední pložka
			if (tableCountdown.getSelectedRow() < 0 || countdownTableModel.getValuesListSize() == 0)
				return;
			
			final AbstractTableValue value = countdownTableModel.getValueAtIndex(e.getFirstRow());

			if (value.getComment() != null)
				DATA_MANAGER.updateCommentInGame(value.getGameId(), value.getComment());
		}
	}
}
