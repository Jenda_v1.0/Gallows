package kruncik.jan.gallows.dialogs.userResultsForm;

import java.sql.Date;

/**
 * Abstraktní třída, která obsahuje některé hodnoty, abych je nemusel deklarovat
 * vícekrát stejně.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class AbstractTableValue {

	/**
	 * Tato proměnná je zde potřeba, abych věděl, kde se má aktualizovat komentář
	 * (pokud jej uživatel v tabulce změní.)
	 */
	private final int gameId;
	
	private final String groupName, dateString, time;
	
	private String comment;
	
	private final int lives, score;
	
	/**
	 * Tento datum je stejný, jako v proměnné dateString, akorát v proměnné
	 * dateString se jedná o zformátovaný čas pro dny, měsíce a roky. V této
	 * proměnné date se jedná o čas typu sql, aby se mohl porovnávat snáze.
	 */
	private final Date date;

	
	
	AbstractTableValue(int gameId, String groupName, String dateString, String comment, String time, int lives,
			int score, Date date) {
		super();
		
		this.gameId = gameId;
		this.groupName = groupName;
		this.dateString = dateString;
		this.comment = comment;
		this.time = time;
		this.lives = lives;
		this.score = score;
		this.date = date;
	}
	
	int getGameId() {
		return gameId;
	}

	public String getGroupName() {
		return groupName;
	}

	String getDateString() {
		return dateString;
	}

	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}

	String getTime() {
		return time;
	}

	public int getLives() {
		return lives;
	}

	public int getScore() {
		return score;
	}

	Date getDate() {
		return date;
	}
}
