package kruncik.jan.gallows.dialogs.userResultsForm;

import java.util.List;

import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.gallowsApp.App;

/**
 * Tato třída slouží jako model pro tabulku, kde jsou zobrazeny data z odehraných her přihlášeného uživatele
 * hry typu Measurement. 
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MeasurementTableModel extends MyAbstractTableModel {

	private static final long serialVersionUID = 1L;




	MeasurementTableModel(final String[] columns, final List<Group> allGroupsList,
			final List<KindOfGame> kindsOfGameList, final UserResultsForm resultsForm, final App app) {
		super(columns, allGroupsList, kindsOfGameList, resultsForm, app);
	}
	
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return valuesList.get(rowIndex).getGroupName();
			
		case 1:
			return valuesList.get(rowIndex).getDateString();
			
		case 2:
			return valuesList.get(rowIndex).getComment();
			
		case 3:
			return valuesList.get(rowIndex).getTime();
			
		case 4:
			return valuesList.get(rowIndex).getLives();
			
		case 5:
			return valuesList.get(rowIndex).getScore();
			
		case 6:
			return createContinueButton(rowIndex);

		case 7:
			return createDeleteButton(rowIndex);
			
		default:
			break;
		}
		
		return "?";
	}
	
	
	
	
	@Override
	public Class<?> getColumnClass(int column) {
		if (getValueAt(0, column) != null)
			return getValueAt(0, column).getClass();
					
		return super.getColumnClass(column);
	}
}
