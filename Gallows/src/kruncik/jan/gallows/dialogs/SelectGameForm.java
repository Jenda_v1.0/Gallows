package kruncik.jan.gallows.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.dialogs.dataForDialogs.ComboBoxEditor;
import kruncik.jan.gallows.dialogs.dataForDialogs.SelectedGame;

/**
 * Tento dialog se používá pro výběr skupiny otázek a typu hry a nastavení času,
 * pokud se vybral odpočet času jako typ hry (Countdown), tato vybraná skupina
 * otázek a typ hry se pak nastaví do apliace do proměnné selectedGroup a
 * kindOfGame, případně i time.
 * 
 * Tentodialog se zobrazí uživateli v případě, že se kliknena novou hru a není
 * zvolené téma - okruh otázek nebo "typ hry" nebo jej uživatel může sám zvolit
 * v menu.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class SelectGameForm extends SourceDialog {
	
	
	private static final long serialVersionUID = 1L;

	

	/**
	 * Komponenta, ve které si uživatel vybere okruh / skupinu slovíček, které bude
	 * pak při hře hádat.
	 */
	private final JComboBox<Group> cmbGroups;
	
	
	/**
	 * Komponenta, ve které si uživatel vybere typ hry (Odpočítávíní času nebo meření času).
	 */
	private final JComboBox<KindOfGame> cmbKindOfGame;
	
	
	/**
	 * Tato proměnná bude obsahovt zvolenou hodnotu v dialogu (zvolenou skupinu) a
	 * ta se vrátí po zavření dialogu (po potvrzení výběru skupiny)
	 */
	private SelectedGame selectedGame;
	
	
	/**
	 * Komponenta, která Slouží pro nastavení času, který se má odpočítávat, v
	 * případě, že se vybere odpočet času, neb měření.
	 */
	private final JComboBox<Integer> cmbGameTime_HH, cmbGameTime_MM, cmbGameTime_SS;
	
	
	/**
	 * Panel, ve kterém budou zobrazeny komponenty pro nastavení času v případě
	 * odpočítávání času pro hádání slov.
	 */
	private final JPanel pnlTime;
	
	
	
	
	/**
	 * Logická proměnná, dle které se pozná, zda se má zavřít okno a vrátit null
	 * hodnota nebo ne. Pokud se do tohoto dialogu jeden z listů obsahující skupiny
	 * slov nebo typy her nepředá nebo bude prázdný, tak se tato proměnná nastaví na
	 * true. a akorát se uživateli oznámí, že nelze zahájit hru a zavře se tento
	 * dialog.
	 */
	private final boolean closeWindow;
	
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param groupsList
	 *            - list se všemi skupiny slov v DB
	 * @param kindsOfGamesList
	 *            - list všech typů her v DB
	 * @param selectedGameLast
	 *            - proměnná, která bude null, pokud se jedná o úplně novou hru, ale
	 *            pokud se jedná již o opakované nastavení, například pro upravení
	 *            již dříve odehrané hry, pak bude tato proměnná naplněná
	 *            předchozími hodnotami a je pak možné je upravit, aby uživatel
	 *            věděl, co již dříve zvolil, aby mohl například změnit jen skupinu
	 *            slov apod. a nenmusel vše nastavovat znovu.
	 */
	public SelectGameForm(final List<Group> groupsList, final List<KindOfGame> kindsOfGamesList,
			final SelectedGame selectedGameLast) {
		super("Vybrat téma");
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 650, 330, "/icons/SelectGameForm.png", true);
		
		
		int rowIndex = 0;
		
		
		final GridBagConstraints gbc = createGbc();
		
		
		selectedGame = null;		
		
		
		// Komponenty pro výběr skupiny slovíček pro hádání:
		add(new JLabel("Níže zvolte skupinu neboli okruh slovíček:", JLabel.CENTER), gbc);		
		
		/*
		 * Proměnná, do které si uložím index pololžky, která se má nastavit jako
		 * označení v komponentě ComboBox, pokud se jedná již u opakované nastavení hry,
		 * zde se nastaví dříve provedené nastavení.
		 * 
		 * Note: 
		 * Musel jsem to udělat přes tento index, protože samotné nastavení
		 * zvolené položky s parametrem objekt nefungovalo, nevím proč, jako tomu je u
		 * komponenty cmbKindOfGame.
		 */
		int indexForSelectedItem = -1;
		
		// Musím dát hodnota do pole, jinak do nejde v ComboBoxu:
		final Group[] groupArray = new Group[groupsList.size()];
		for (int i = 0; i < groupsList.size(); i++) {
			groupArray[i] = groupsList.get(i);
			
			if (selectedGameLast != null && groupsList.get(i).toString().equals(selectedGameLast.getGroup().toString()))
				indexForSelectedItem = i;
		}
			
		
		cmbGroups = new JComboBox<>(groupArray);
		
		// Nastavení zvolené hodnoty (pokud byla již dříve zvolená)
		if (indexForSelectedItem > -1)
			cmbGroups.setSelectedIndex(indexForSelectedItem);
			
				
		setGbc(gbc, 0, ++rowIndex);
		add(cmbGroups, gbc);
		
		
		
		
		
		
		// Tento panel musím inicializovat zde, ale naplní se až níže - kvůli
		// následující komponentě,
		// která tento panel případně zobrazí
		pnlTime = new JPanel(new BorderLayout());
		
		
		

		// Komponenty pro nastavení typu hry:
		setGbc(gbc, 0, ++rowIndex);
		add(new JLabel("Níže zvolte typ / způsob hry:", JLabel.CENTER), gbc);
		
		
		final KindOfGame[] kindOfGameArray = new KindOfGame[kindsOfGamesList.size()];
		for (int i = 0; i < kindsOfGamesList.size(); i++)
			kindOfGameArray[i] = kindsOfGamesList.get(i);
		
		cmbKindOfGame = new JComboBox<>(kindOfGameArray);
		
		// Nastavení nějakých vlastností kvůli horizontálnímu scrollbaru:
		cmbKindOfGame.setPreferredSize(new Dimension(0, 45));
		cmbKindOfGame.setEditor(new ComboBoxEditor());
		cmbKindOfGame.setEditable(true);
		
		// Nastavení zvolené hodnoty (pokud byla již dříve zvolená)
		if (selectedGameLast != null)
			cmbKindOfGame.setSelectedItem(selectedGameLast.getKindOfGame());
		
		
		/*
		 * Nyní potřebuji zrovna otestovat, zda není zvolet typ Countdown, pokud ne, pak
		 * se zrovna penl pro nastavení času zneviditelnit, protože by něměl jít
		 * nastavit. Tento případ může nastat, pokud se jedná o upravení již dříve
		 * nastavené hry, pak se přednastaví již nastavené hodnoty a uživatel puze
		 * upraví, co potřebuje.
		 */
		if (selectedGameLast != null && !selectedGameLast.getKindOfGame().getKind().equals("Countdown"))
			pnlTime.setVisible(false);
			
		
		
		cmbKindOfGame.addActionListener(Event -> {
			if (((KindOfGame) cmbKindOfGame.getSelectedItem()).getKind().equals("Countdown"))
				pnlTime.setVisible(true);
			
			else if (pnlTime.isVisible())
				pnlTime.setVisible(false);
		});
		
		setGbc(gbc, 0, ++rowIndex);
		add(cmbKindOfGame, gbc);
		
		
		
		
		// Panel pro nastavení času:
		pnlTime.add(new JLabel("Nastavte čas hry:", JLabel.CENTER), BorderLayout.NORTH);
		
		final JPanel pnlTimeComponents = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		
		cmbGameTime_HH = new JComboBox<>(getValuesInArray(24));
		cmbGameTime_MM = new JComboBox<>(getValuesInArray(60));
		cmbGameTime_SS = new JComboBox<>(getValuesInArray(60));
		
		// Nastavení zvolené hodnoty (pokud byla již dříve zvolená)
		if (selectedGameLast != null) {
			cmbGameTime_HH.setSelectedItem(selectedGameLast.getHh());
			cmbGameTime_MM.setSelectedItem(selectedGameLast.getMm());
			cmbGameTime_SS.setSelectedItem(selectedGameLast.getSs());
		}
		
		pnlTimeComponents.add(cmbGameTime_HH);
		pnlTimeComponents.add(new JLabel(" : "));
		pnlTimeComponents.add(cmbGameTime_MM);
		pnlTimeComponents.add(new JLabel(" : "));
		pnlTimeComponents.add(cmbGameTime_SS);
		
		pnlTime.add(pnlTimeComponents, BorderLayout.CENTER);
		
		setGbc(gbc, 0, ++rowIndex);
		add(pnlTime, gbc);
		
		
		
		
		// Zrovna zjistím, jaký typ hry se načetl do JComboBoxu a dle toho buď zobrazím
		// nebo zneviditelním panel pro nastavení času:
		if (((KindOfGame) cmbKindOfGame.getSelectedItem()).getKind().equals("Countdown"))
			pnlTime.setVisible(true);
		else
			pnlTime.setVisible(false);
		
		
		
		

		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText("Vybrat");
		btnConfirm.addActionListener(Event -> {
			// Zjistím si, jaký typ hry je zvolen:
			final KindOfGame kindOfGame = (KindOfGame) cmbKindOfGame.getSelectedItem();
			
			// Pokud je zvoleno meření, stačí jen skupina slovíček a ten zvolený typ hry pro
			// další testování v aplikaci
			if (kindOfGame.getKind().equals("Measurement"))
				selectedGame = new SelectedGame((Group) cmbGroups.getSelectedItem(), kindOfGame);

			else
				// Zde byl zvolen odpočet, tak si navíc musím uložit i nastavený čas:
				selectedGame = new SelectedGame((Group) cmbGroups.getSelectedItem(), kindOfGame,
						(int) cmbGameTime_HH.getSelectedItem(), (int) cmbGameTime_MM.getSelectedItem(),
						(int) cmbGameTime_SS.getSelectedItem());

			dispose();
		});
		
		setGbc(gbc, 0, ++rowIndex);
		add(pnlButtons, gbc);
		
		
		
		if (groupsList.isEmpty() || kindsOfGamesList.isEmpty()) {
			JOptionPane.showMessageDialog(this,
					"Nebyly načteny skupiny slov nebo skupiny (popř. obojí), nelze spustit hru!", "Chyba",
					JOptionPane.ERROR_MESSAGE);

			closeWindow = true;

			dispose();
		} else
			closeWindow = false;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří jednorozměrné pole typu Integer, které bude obsahovat
	 * hodnoty od nuly až po zadanou hodnotu values (bez ní, resp. hodnota values -
	 * 1).
	 * 
	 * Toto pole slouží pro model do komponent JComboBox pro nastavení času.
	 * 
	 * @param values
	 *            - hodnota, která definuje počet hodnot v poli a zároveň hodnoty v
	 *            tom poli budou hodnoty od nuly po values - 1
	 *            
	 * @return - výše popsané jednorozměrné pole.
	 */
	private static Integer[] getValuesInArray(final int values) {
		final Integer[] array = new Integer[values];

		for (int i = 0; i < values; i++)
			array[i] = i;

		return array;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která po zavoláni zviditelní tento dialog a udělá ho tak modálním a
	 * dokud uživatel nepotvrdí výběr skupiny / okruhu otázek a typ hry, pak tato metoda vrátí
	 * zvolenou skupinu se slovíčkami pro hádání a zvolený typ hry.
	 * 
	 * @return vybranou skupinu pro hádání slovíček a zvolený typ hry.
	 */
	public final SelectedGame getGroup() {
		/*
		 * Jestli bude proměnná closeWindow true, pak se vrátí null, jako by se zavřel
		 * tento dialog, protože nelze vybrat hru.
		 */
		if (closeWindow)
			return null;

		prepareWindow();

		return selectedGame;
	}
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * V tomto dialogu není tato metoda potřeba, tak ji nebudu vyplňovat, musel bych
		 * napsat jinou třídu pro základní metody pro dialogy, aby se neopakovali.
		 */
	}
}
