package kruncik.jan.gallows.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.User;

/**
 * Tato třída slouží puze jako dialog pro zadání "základních" údajů pro obnovení
 * smazaného uživatelského účtu.
 * 
 * Pro obnovení původního účtu, který byl smazán (nastaven atribut Deleted na
 * hodnotu 1) stačí zadat pseudonym (uživatelské jméno), email, původní heslo a
 * 2x nové heslo.
 * 
 * Po zadání údajů výše se obnoví účet (nastaví se atribut Deleted na 0) a pokud
 * byly odehrány nějaké hry, tak i k nim se získá přístup.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class RestoreAccountForm extends SourceDialog {

	private static final long serialVersionUID = 1L;

	

	/**
	 * Komponenta pro manipulaci s daty v DB.
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager(); 
	
	
	// Textová pole pro zadání uživatelského jména a emailu:
	private final JTextField txtPseudonym, txtEmail;

	// Textová pole pro zadání původního hesla a 2x nového hesla:
	private final JPasswordField pswOriginalPassword, pswPassword_1, pswPassword_2;

	
	
	public RestoreAccountForm() {
		super("Obnovit účet");

		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 500, "/icons/RestoreAccountIcon.png", true);

		
		final GridBagConstraints gbc = createGbc();
		
		
		createKeyAdapter();
		
		add(new JLabel("*Uživatelské jméno: "), gbc);
		
		
		txtPseudonym = new JTextField(20);
		txtPseudonym.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 0);
		add(txtPseudonym, gbc);
		
		
		
		
		setGbc(gbc, 0, 1);
		add(new JLabel("*Email: "), gbc);
		
		txtEmail = new JTextField(20);
		txtEmail.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 1);
		add(txtEmail, gbc);
		
		
		
		
		
		setGbc(gbc, 0, 2);
		add(new JLabel("*Původní heslo: "), gbc);
		
		pswOriginalPassword = new JPasswordField(20);
		pswOriginalPassword.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 2);
		add(pswOriginalPassword, gbc);
		
		
		
		
		
		setGbc(gbc, 0, 3);
		add(new JLabel("*Nové heslo (1): "), gbc);
		
		pswPassword_1 = new JPasswordField(20);
		pswPassword_1.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 3);
		add(pswPassword_1, gbc);
		
		
		
		
		
		setGbc(gbc, 0, 4);
		add(new JLabel("*Nové heslo (2): "), gbc);
		
		pswPassword_2 = new JPasswordField(20);
		pswPassword_2.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 4);
		add(pswPassword_2, gbc);
		
		
		
		
		
		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText("Obnovit");
		btnConfirm.addActionListener(Event -> checkData());
		
		gbc.gridwidth = 2;
		setGbc(gbc, 0, 5);
		add(pnlButtons, gbc);
		
		
		
		prepareWindow();
	}

	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * - Zda nejsou pole prázdná
		 * - Zda odpovídají požadované syntaxi
		 * - Pak najít v DB uživatele s daným pseudonymem, emailem a heslem,
		 * 		- Pokud jej najdu, nastavit nové heslo a Deleted na 0
		 * 		- Pokud jej nenajdu -> chybová hláška.		
		 */
		
		final String pseudonym = txtPseudonym.getText();
		final String email = txtEmail.getText();
		final String pswOriginal = new String(pswOriginalPassword.getPassword());
		final String password_1 = new String(pswPassword_1.getPassword());
		final String password_2 = new String(pswPassword_2.getPassword());
		
		
		if (pseudonym.isEmpty() || email.isEmpty() || pswOriginal.isEmpty()
				|| password_1.isEmpty() && password_2.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Alespoň jednoz požadovaných polí je prázdné!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!pseudonym.matches(REG_EX_PSEUDONYM)) {
			JOptionPane.showMessageDialog(this, "Zadané uživatelské jméno neodpovídá požadované syntaxi!",
					"Chyba syntaxe", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!email.matches(REG_EX_EMAIL)) {
			JOptionPane.showMessageDialog(this, "Zadaný email neodpovídá požadované syntaxi!", "Chyba syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!password_1.equals(password_2)) {
			JOptionPane.showMessageDialog(this, "Zadaná hesla (heslo 1 a heslo 2) jsou různá!", "Hesla se liší",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (pswOriginal.equals(password_1)) {
			JOptionPane.showMessageDialog(this, "Původní a nové heslo jsou stejná!", "Identická hesla",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		
		final User user = DATA_MANAGER.getUserByPseudonymEmailPassword(pseudonym, email, pswOriginal);

		if (user == null) {
			JOptionPane.showMessageDialog(this, "Uživatelský účet se zadanými daty nebyl nalezen!", "Účet nenalezen",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
	
		
		// Zde byl účet nalezen, tak ten účet obnovím, ale nejprve nastavím nové heslo:		
		user.setPassword(password_1);
		final boolean updated = DATA_MANAGER.updateUser(user);

		if (!updated) {
			JOptionPane.showMessageDialog(this,
					"Došlo k chybě při pokusu o aktualizaci uživatelského účtu. Účet nebyl obnoven!", "Chyba",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		

		// Nyní obnovím účet, tak že nastavím atribut Deleted na 0 (možno udělat i v
		// předchozím kroku, ale už jsem měl dříve připravenou metodu)
		final boolean restored = DATA_MANAGER.restoreUser(user.getId());

		if (restored) {
			JOptionPane.showMessageDialog(this, "Uživatelský účet byl obnoven, můžete se přihlásit.", "Účet obnoven",
					JOptionPane.INFORMATION_MESSAGE);
			dispose();
		} else
			JOptionPane.showMessageDialog(this,
					"Došlo k chybě při pokusu o obnovení úživatelského účtu, účet nebyl obnoven!", "Chyba",
					JOptionPane.ERROR_MESSAGE);
	}
}
