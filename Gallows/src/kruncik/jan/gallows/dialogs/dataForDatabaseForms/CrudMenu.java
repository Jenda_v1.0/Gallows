package kruncik.jan.gallows.dialogs.dataForDatabaseForms;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.dialogs.bestResultsForm.BestResultsInfoForm;
import kruncik.jan.gallows.dialogs.citiesForm.CitiesInfoForm;
import kruncik.jan.gallows.dialogs.databaseUserForm.UserInfoForm;
import kruncik.jan.gallows.dialogs.groups_wordsForm.GroupsWordsInfoForm;
import kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers.KindOfGamesInfoForm;
import kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers.KindOfUsersInfoForm;

/**
 * Tato třída slouží jako menu pro dialogy v menu Databáze. Obsahuje předpřipravený
 * konstruktor pro předání hodnot a referencí na různé objekty, abych si co nejlépe zjednodušil
 * vývoj těchto dialogů pro operaci nad DB.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class CrudMenu extends JMenuBar {

	
	private static final long serialVersionUID = 1L;

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param dialog
	 *            - dialog, ve kterém se nachází toto menu - aby jej bylo možné
	 *            zavřít.
	 * @param nameForCrudMenu
	 *            - název pro menu, kde se nachází Crud operace - abstraktní akce
	 *            pro objekty
	 * @param abstList
	 *            - list s abstraktními tlačítky.
	 * @param kindOfInfoDialog
	 *            - výčtová hodnota, kde se nachází typ dialog s informace, který se
	 *            má otevřít, kdyz se klikne na položku Info.
	 */
	public CrudMenu(final JDialog dialog, final String nameForCrudMenu, final List<AbstractAction> abstList,
			final KindOfInfoDialog kindOfInfoDialog) {
		super();

		final JMenu menuDialog = new JMenu("Dialog");
		final JMenu menuCrudActions = new JMenu(nameForCrudMenu);
		
		
		
		final JMenuItem itemInfo = new JMenuItem("Info");
		final JMenuItem itemClose = new JMenuItem("Zavřít");
		
		
		itemInfo.setToolTipText("Zobrazí dialogové oknos informacemi o tomto dialogu.");
		itemClose.setToolTipText("Zavře tento dialog.");
		
		itemInfo.setIcon(GetIconInterface.getImage("/icons/InfoIconForm.png", KindOfPicture.TOOLBAR_OR_MENU));
		itemClose.setIcon(GetIconInterface.getImage("/icons/CloseDialogIcon.png", KindOfPicture.TOOLBAR_OR_MENU));
		
		
		itemInfo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
		
		
		// Zjistím, jaký dialog s informacemi se má otevřít po kliknutí na položku Info:
		if (kindOfInfoDialog == KindOfInfoDialog.USER_INFO_DIALOG)
			itemInfo.addActionListener(Event -> new UserInfoForm());

		else if (kindOfInfoDialog == KindOfInfoDialog.GROUPS_WORDS_INFO_DIALOG)
			itemInfo.addActionListener(Event -> new GroupsWordsInfoForm());

		else if (kindOfInfoDialog == KindOfInfoDialog.CITIES_DIALOG)
			itemInfo.addActionListener(Event -> new CitiesInfoForm());

		else if (kindOfInfoDialog == KindOfInfoDialog.KINDS_OF_GAMES_DIALOG)
			itemInfo.addActionListener(Event -> new KindOfGamesInfoForm());
		
		else if (kindOfInfoDialog == KindOfInfoDialog.KINDS_OF_USERS_DIALOG)
			itemInfo.addActionListener(Event -> new KindOfUsersInfoForm());

		else if (kindOfInfoDialog == KindOfInfoDialog.BEST_RESULTS_DIALOG)
			itemInfo.addActionListener(Event -> new BestResultsInfoForm());		
		
		
		
		itemClose.addActionListener(Event -> dialog.dispose());
		
		menuDialog.add(itemInfo);
		menuDialog.addSeparator();
		menuDialog.add(itemClose);
		
		
		
		
		
		// Přidání abstraktních tlačítek do menu menuCrudActions
		for (int i = 0; i < abstList.size(); i++) {
			menuCrudActions.add(abstList.get(i));
			
			if (i < abstList.size() - 1)
				menuCrudActions.addSeparator();
		}
		
		
		
		
		
		add(menuDialog);
		add(menuCrudActions);
	}
}
