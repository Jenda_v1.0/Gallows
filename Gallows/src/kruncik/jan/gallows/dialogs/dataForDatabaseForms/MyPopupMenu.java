package kruncik.jan.gallows.dialogs.dataForDatabaseForms;

import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JPopupMenu;

/**
 * Tato třída slouží pouze jako Menu, které se zobrazí po kliknutí pravým
 * tlačítkem myši na řádek v tabulce a budou v tomto menu k dispozici pouze
 * tlačítka coby abstraktní akce nad objektem v tabulce.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MyPopupMenu extends JPopupMenu {

	
	private static final long serialVersionUID = 1L;

	
	public MyPopupMenu(final List<AbstractAction> actionsList) {
		super();
		
		for (int i = 0; i < actionsList.size(); i++) {
			add(actionsList.get(i));
			
			if (i < actionsList.size() - 1)
				addSeparator();
		}
	}
}
