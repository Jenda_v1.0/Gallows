package kruncik.jan.gallows.dialogs.dataForDatabaseForms;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží pouze jako abstraktní třída, dědi z SourceDialog, která
 * obsahuje potřebná data pro dialogy a tato třída (CrudAbstractActins) navíc
 * obsahuje abstraktní akce (jejich deklaraci) a abstraktní metodu pro jejich
 * vytvoření.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class CrudAbstractActions extends SourceDialog {
	
	
	
	private static final long serialVersionUID = 1L;




	/**
	 * Jedná se o abstraktní akce pro CRUD operace nad daty v tabulkách nad DB.
	 * 
	 * Jsou deklarovány zde, abych je nemusel vytvářet v každé třídě, resp. v každém dialogu.
	 * pro operaci nad DB.
	 */
	protected AbstractAction createAction, updateAction, deleteAction;
	
	
	
	
	/**
	 * Komponenta, která slouží jako popupMenu, které se otevře po kliknutí pravým
	 * tlačítkem myši do tabulky.
	 */
	private JPopupMenu popupMenu;
	
	
	
	
	
	
	// Následují komponenty pro řazení dat v tabulce:
	
	/**
	 * Komponenta, která obsahuje položky pro seřazení dat v tabulce.
	 */
	protected JComboBox<String> cmbOrder;
	
	
	
	
	
	// Následují komponenty pro vyhledávání / filtrování dat v tabulce:
	
	/**
	 * Položka pro zvolení sloupce, ve kterém se bude vyhledávat.
	 */
	protected JComboBox<String> cmbSearch;

	/**
	 * Textové pole, do kterého se se zadá řetězec pro vyhledávání dat v tabulce dle
	 * nějakého sloupce.
	 */
	protected JTextField txtSearch;

	/**
	 * Tlačítko, na které se klikne pro spuštění vyhledávání dat v tabulce.
	 */
	protected JButton btnSearch;
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci JPoupMenu, které obsahuje abstraktní akce pro
	 * vytvoření, editace a smazání označeného objektu v tabulce. Dále se toto
	 * popupMenu zobrazí v případě, že se klikne na danou tabulku pravým tlačítkem
	 * myši.
	 * 
	 * @param table
	 *            - Tabulka, kam se má přidat událost na kliknutí pravým tlačítkem
	 *            myši pro zobrazení výše popsaného popupMenu.
	 * 
	 * @param actionsList
	 *            - List, který obsahuje abstraktní akce pro výše uvedené operace.
	 */
	protected final void createPopupMenuMouseAdapter(final JTable table, final List<AbstractAction> actionsList) {
		popupMenu = new MyPopupMenu(actionsList);
		
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					int r = table.rowAtPoint(e.getPoint());

					if (r >= 0 && r < table.getRowCount())
						table.setRowSelectionInterval(r, r);

					int rowindex = table.getSelectedRow();

					if (rowindex < 0)
						return;

					popupMenu.show(table, e.getX(), e.getY());
				}
			}
		});
	}
	
	
	
	
	
	
	
	
	
	public CrudAbstractActions(final String title) {
		super(title);
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří abstraktní akce pro CRUD operace a vrátí jej v listu.
	 * 
	 * @return list s vytvořenými abstraktními akcemi pro CRUD operace nad DB.
	 */
	protected abstract List<AbstractAction> createAbstractActions();
}
