package kruncik.jan.gallows.dialogs.dataForDatabaseForms;

/**
 * Tento výšet obsahuje pouze hodnoty pro to, abych věděl v menu CrudMenu jaký dialog s informacei
 * se má otevřít po kliknutí na položku Info.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum KindOfInfoDialog {

	/**
	 * Jedná se o hodnotu, která značí dialog s informacemi o dialogu pro manipulaci
	 * s uživateli (UserForm). Dialog s informacemi: UserInfoForm
	 */
	USER_INFO_DIALOG,
	
	/**
	 * jedná se o hodnotu, která značí dialog s informacemi o dialogu pro manipulaci
	 * s skupinami slov a slovy samotnými. Dialog s informacemi GroupsWordsInfoForm
	 */
	GROUPS_WORDS_INFO_DIALOG,
	
	/**
	 * jedná se o hodnotu, která značí dialog s informacemi o dialogu pro manipulaci
	 * s městy. Dialog s informacemi CitiesInfoForm
	 */
	CITIES_DIALOG,

	/**
	 * jedná se o hodnotu, která značí dialog s informacemi o dialogu pro manipulaci
	 * s typy uživatelů. Dialog s informacemi KindOfUsersInfoForm
	 */
	KINDS_OF_USERS_DIALOG,
	
	/**
	 * jedná se o hodnotu, která značí dialog s informacemi o dialogu pro manipulaci
	 * s typy her. Dialog s informacemi KindOfGamesInfoForm
	 */
	KINDS_OF_GAMES_DIALOG,
	
	/**
	 * jedná se o hodnotu, která značí dialog s informacemi o dialogu pro
	 * reprezentaci všech výsledků od všech hráčů. Dialog s informacemi
	 * BestResultsInfoForm
	 */
	BEST_RESULTS_DIALOG
}
