package kruncik.jan.gallows.dialogs;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.dialogs.dataForDialogs.UserInfo;

/**
 * Tento dialog slouží pouze jako formulář (dialog) pro zobrazení dat
 * o přihlášeném uživateli, nerozlišuje se zde admin nebo běžný uživatel apod.
 * Každý přihlášený uživatel zde vidí své údajé, nebudu zde u admina zobrazovat navíc
 * třeba ID, typ uživatele apod. Protože to může vidět pak v přehledu uživatelů.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class UserInfoForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;

	

	/**
	 * Komponenta pro manipulaci s DB.
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	/**
	 * Reference na přihlášeného uživatele
	 */
	private User loggedInUser;
	
	
	/**
	 * Logická proměnná, dle které se pozná, zda se smazal uživatel nebo ne.
	 */
	private boolean deleted;
	

	// Labely pro zobrazení dat o uživateli:
	private JLabel lblPicture, lblFirstname, lblLastName, lblPseudonym, lblPassword, lblEmail, lblTelephoneNumber, lblStreet,
			lblPsc, lblCity;
	
	
	private final JButton btnEdit, btnDelete;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param loggedInUser
	 *            - Referencena přihlášeného uživatele, aby se v tomto dialogu mohli
	 *            zobrazit jeho údaje.
	 */
	public UserInfoForm(final User loggedInUser) {
		super("Přehled údajů");
		
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 650, "/icons/UserInfoIconForm.png", true);
		
		/*
		 * Proměnná, do které se bude ukládat index řádku, na který se má přidat nějaká
		 * komponenta.
		 */
		int indexForRow = 1;
		
		final GridBagConstraints gbc = createGbc();
				
		
		if (loggedInUser.getPicture() == null || loggedInUser.getPicture().getIconWidth() == -1)
			lblPicture = new JLabel("Obrázek neuveden", JLabel.CENTER);
		else
			lblPicture = new JLabel(GetIconInterface.getResizedImage(loggedInUser.getPicture(), 150, 150),
					JLabel.CENTER);
		
		gbc.gridwidth = 2;
		add(lblPicture, gbc);
		
		
		
		gbc.gridwidth = 1;
		setGbc(gbc, 0, indexForRow);
		add(new JLabel("Jméno: "), gbc);
				
		
		setGbc(gbc, 1, indexForRow);
		lblFirstname = new JLabel(loggedInUser.getFirstName());
		add(lblFirstname, gbc);		
		
		
		
		
		setGbc(gbc, 0, ++indexForRow);
		add(new JLabel("Příjmení: "), gbc);
		
				
		setGbc(gbc, 1, indexForRow);
		lblLastName = new JLabel(loggedInUser.getLastName());
		add(lblLastName, gbc);		
		
		
		
		
		setGbc(gbc, 0, ++indexForRow);
		add(new JLabel("Uživatelské jméno: "), gbc);
		
				
		setGbc(gbc, 1, indexForRow);
		lblPseudonym = new JLabel(loggedInUser.getPseudonym());
		add(lblPseudonym, gbc);
		
		
		
		
		setGbc(gbc, 0, ++indexForRow);		
		add(new JLabel("Heslo: "), gbc);
		
				
		setGbc(gbc, 1, indexForRow);
		lblPassword = new JLabel(loggedInUser.getPassword());
		add(lblPassword, gbc);
		
		
		
		
		setGbc(gbc, 0, ++indexForRow);
		add(new JLabel("Email: "), gbc);
		
				
		setGbc(gbc, 1, indexForRow);
		lblEmail = new JLabel(loggedInUser.getEmail());
		add(lblEmail, gbc);
		
		
		
		if (loggedInUser.getTelephoneNumber() > 0) {
			setGbc(gbc, 0, ++indexForRow);
			add(new JLabel("Telefonní číslo: "), gbc);
			
					
			setGbc(gbc, 1, indexForRow);
			lblTelephoneNumber = new JLabel(String.valueOf(loggedInUser.getTelephoneNumber()));
			add(lblTelephoneNumber, gbc);
		}

		
		
		
		
		setGbc(gbc, 0, ++indexForRow);
		add(new JLabel("Ulice: "), gbc);
		
				
		setGbc(gbc, 1, indexForRow);
		lblStreet = new JLabel(loggedInUser.getStreet());
		add(lblStreet, gbc);
		
		
		
		if (loggedInUser.getPsc() > 0) {
			setGbc(gbc, 0, ++indexForRow);
			add(new JLabel("PSČ: "), gbc);
			
					
			setGbc(gbc, 1, indexForRow);
			lblPsc = new JLabel(String.valueOf(loggedInUser.getPsc()));
			add(lblPsc, gbc);
		}

		
		
		
		if (loggedInUser.getPsc() > 0) {
			setGbc(gbc, 0, ++indexForRow);
			add(new JLabel("Město: "), gbc);
			
			
			setGbc(gbc, 1, indexForRow);
			lblCity = new JLabel(DATA_MANAGER.getCity(loggedInUser.getPsc()).getName());
			add(lblCity, gbc);			
		}

		
		
		
		
		
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 0));
		
		btnEdit = new JButton("upravit");
		btnDelete = new JButton("Smazat");
		
		btnEdit.addActionListener(Event -> {
			final User editedUser;
			
			if (loggedInUser.getKindOfUser().equals("Admin"))
				editedUser = new RegistrationForm(true, false).getEditedUser(loggedInUser);
			else
				editedUser = new RegistrationForm(false, false).getEditedUser(loggedInUser);
			
			if (editedUser != null) {
				// Nastavím uživatel, aby se vrátil pro nastavení do aplikace:
				this.loggedInUser = editedUser;
				// Nastavím labely v dialogu na změněné hodnoty:
				setValuesToTextLabels(editedUser);
				
				// Aktualizuji uzivatele v DB
				final boolean updated = DATA_MANAGER.updateUser(editedUser);
				
				if (updated)
					JOptionPane.showMessageDialog(this,
							"Uživatel '" + editedUser.getPseudonym() + "' byl úspěšně upraven.", "Údaje upraveny",
							JOptionPane.INFORMATION_MESSAGE);

				else
					JOptionPane.showMessageDialog(this,
							"Nastala chyba při pokusu o upravení dat ohledně uživatelského účtu uživatele '"
									+ editedUser.getPseudonym() + "'.",
							"Chyba", JOptionPane.ERROR_MESSAGE); 
			}				
		});
		
		btnDelete.addActionListener(Event -> {
			if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, "Opravdu se přejete smazat účet?",
					"Opravdu smazat", JOptionPane.YES_NO_OPTION)) {
				deleted = DATA_MANAGER.deleteBlockUser(loggedInUser.getId());

				if (deleted)
					JOptionPane.showMessageDialog(this, "Účet úspěšně smazán!", "Smazán",
							JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(this, "Nastala chyba při pokusu o smazání uživatele!", "Chyba",
							JOptionPane.ERROR_MESSAGE);
			}
			
			// Nastavím proměnnou na null, aby se nevráil nějaký uživatel:
			this.loggedInUser = null;
			dispose();
		});
		
		pnlButtons.add(btnEdit);
		pnlButtons.add(btnDelete);
		
		gbc.gridwidth = 2;
		setGbc(gbc, 0, ++indexForRow);
		add(pnlButtons, gbc);
	}
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí dialog, který ukazuje přehled základních údajů pro
	 * uživatele ohledně jeho účtu a pokud dojde k úpravě některých údajů, tak
	 * uživatele s upravenými údaji i vrátí.
	 * 
	 * @return - null nebo objekt s údaji o uživateli, připadně informace o tom, že
	 *         byl smazán
	 */
	public UserInfo showInfo() {
		prepareWindow();

		return new UserInfo(loggedInUser, deleted);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví nové hodnoty do lablů k danému uživateli, jendá se o
	 * to, že si uživatel změníl některé údaje (mohl změnit), pak je zde musím
	 * zobrazit, na aktuální údaje, aby si nemyslel, že se to nepovedlo apod.
	 * 
	 * @param user
	 *            - upravený uživatel (uživatel s potenciálně upravenými daty)
	 */
	private void setValuesToTextLabels(final User user) {
		if (user.getPicture() == null || user.getPicture().getIconWidth() == -1) {
			lblPicture.setText("Obrázek neuveden");
			lblPicture.setIcon(null);
		}
		else {
			lblPicture.setText("");
			lblPicture.setIcon(GetIconInterface.getResizedImage(user.getPicture(), 150, 150));
		}
			
		
		lblFirstname.setText(user.getFirstName());
		lblLastName.setText(user.getLastName());
		lblPseudonym.setText(user.getPseudonym());
		lblPassword.setText(user.getPassword());
		lblEmail.setText(user.getEmail());
		
		if (user.getTelephoneNumber() > 0)
			lblTelephoneNumber.setText(String.valueOf(user.getTelephoneNumber()));
		
		lblStreet.setText(user.getStreet());
		lblPsc.setText(String.valueOf(user.getPsc()));
		if (user.getPsc() > 0)
			lblCity.setText(DATA_MANAGER.getCity(user.getPsc()).getName());
	}
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * Toto je dialog pouze pro zobrazení dat, tato metoda nebude potřeba.
		 */
	}
}
