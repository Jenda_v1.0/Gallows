package kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.db_objects.KindOfUser;
import kruncik.jan.gallows.db_objects.KindsOfAncestor;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudAbstractActions;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudMenu;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.KindOfInfoDialog;

/**
 * Tato třída slouží jako dialog pro manipulaci s daty z tabulek KindsOfGames a
 * KindsOfUsers.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfGamesAndUsersForm extends CrudAbstractActions {

	
	private static final long serialVersionUID = 1L;

	

	/**
	 * Komponenta pro manipulaci s datyv DB.
	 */
	public static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	/**
	 * Názevy sloupců pro tabulku v případě, že se jedná o reprezentaci typů
	 * uživatelů.
	 */
	private static final String[] USERS_COLUMNS = {"Typ uživatele", "Komentář"};
	
	
	/**
	 * Názvy sloupců pro tabulku v případě, že se jedná o reprezentaci typů her.
	 */
	private static final String[] GAMES_COLUMNS = {"Typ hry", "Komentář"};
	
	
	
	/**
	 * Jednorozměrné pole, které obsahuje položky pro JComboBox, pro seřazení
	 * položek v tabulce.
	 */
	private static final String[] CMB_ORDER_MODEL = { "Zobrazit vše", "Typu - Vzestupně", "Typu - Sestupně" };
	
	
	
	
	
	

	
	/**
	 * Model pro tabulku typů uživatelů nebo typu her.
	 */
	private final UsersGamesTableModel tableModel;
	
	/**
	 * Tabulka, ve které budou zobrazeny typy uživatelů nebo her.
	 */
	private final JTable table;
	
	
	
	
	/**
	 * V této proměnné bude uložena hodnota, která výčtový typ "tohoto formuláře",
	 * konkrétně, zda se je v něm zobrazen typ uživatele nebo her a dle této hodnoty
	 * se dále pozna, zda se mají vytvořit abstraktní akce pro manipulaci s typy
	 * uživatelů nebo s typy her.
	 */
	private final EnumKindsOf kindOfForm;
	
	
	
	
	/**
	 * List, který obsahuje veškeré pložky z DB, buď se jedná o typy her nebo typy
	 * uživatelů.
	 */
	private static List<KindsOfAncestor> ALL_KINDS_OF_LIST;
	
	
	
	
	
	
	

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param title
	 *            - titulek dialogu.
	 * 
	 * @param kind
	 *            - typ dialogu - Zda se jedná o dialog, která obsahuje informace
	 *            ohledně typu uživatele nebo typu her.
	 */
	public KindOfGamesAndUsersForm(final String title, final EnumKindsOf kind) {
		super(title);
		
		kindOfForm = kind;		
				
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 800, 500, "/icons/KindsOfGamesAndUsersIconForm.png", true);
		
		
		final List<AbstractAction> crudActionsList = createAbstractActions();
		
		
		
		// Menu a table model pro tabulku:
		if (kind == EnumKindsOf.GAMES) {
			setJMenuBar(new CrudMenu(this, "Typ hry", crudActionsList, KindOfInfoDialog.KINDS_OF_GAMES_DIALOG));
			tableModel = new UsersGamesTableModel(GAMES_COLUMNS, this, kind);
		}
			
//		else if (kind == EnumKindsOf.USERS) {
		else {
			setJMenuBar(new CrudMenu(this, "Typ uživatele", crudActionsList, KindOfInfoDialog.KINDS_OF_USERS_DIALOG));
			tableModel = new UsersGamesTableModel(USERS_COLUMNS, this, kind);
		}
			
		
		
		
		
		final JToolBar toolbarCrud = new JToolBar();
		toolbarCrud.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));;
		
		toolbarCrud.add(createAction);
		toolbarCrud.add(updateAction);
		toolbarCrud.add(deleteAction);
		
		
		
		
		
		final JToolBar toolbarFilter = new JToolBar();
		toolbarFilter.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));;
		
		toolbarFilter.add(new JLabel("Seřadit dle: "));
		
		cmbOrder = new JComboBox<>(CMB_ORDER_MODEL);
		toolbarFilter.add(cmbOrder);
		
		cmbOrder.addActionListener(Event -> {
			final String selectedItem = (String) cmbOrder.getSelectedItem();

			if (selectedItem.equals(CMB_ORDER_MODEL[0]))
				tableModel.setValuesList(ALL_KINDS_OF_LIST);

			else if (selectedItem.equals(CMB_ORDER_MODEL[1]))
				tableModel.sortByKind(true);

			else if (selectedItem.equals(CMB_ORDER_MODEL[2]))
				tableModel.sortByKind(false);
		});
		
		
		
		toolbarFilter.addSeparator();
		
		
		
		toolbarFilter.add(new JLabel("Vyhledat typ: "));
		
		createKeyAdapter();
		txtSearch = new JTextField(20);
		txtSearch.addKeyListener(keyAdapter);
		toolbarFilter.add(txtSearch);
		
		btnSearch = new JButton("Vyhledat");
		btnSearch.addActionListener(Event -> checkData());
		toolbarFilter.add(btnSearch);
		
		
		
		
		final JPanel pnlToolbars = new JPanel(new BorderLayout());
		pnlToolbars.add(toolbarCrud, BorderLayout.NORTH);
		pnlToolbars.add(toolbarFilter, BorderLayout.SOUTH);
		
		add(pnlToolbars, BorderLayout.NORTH);
		
		
		
		
		
		
		
		// Tabulka:
		table = new JTable(tableModel);
		createPopupMenuMouseAdapter(table, crudActionsList);		
		
		reloadListWithData();
		tableModel.setValuesList(ALL_KINDS_OF_LIST);
		
		
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		
		
		
		
		
		
		prepareWindow();
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která přenačte data v tabulce (načte je znovu z DB) a oznčí původní
	 * řádek (pokd to půjde).
	 */
	final void reloadTableData() {
		// Uložím si označený řádek
		final int selectedRow = table.getSelectedRow();

		
		// přenačtu města z DB:
		reloadListWithData();

		
		// Vložím nová města do tabulky:
		tableModel.setValuesList(ALL_KINDS_OF_LIST);

		
		// Pokud to jde, označím původní řádek (v případě smazání to vyjít nemusí, navíc
		// se označí ten jebližší, zálěží, jaký se smaže):
		if (selectedRow > -1 && selectedRow < table.getRowCount())
			table.setRowSelectionInterval(selectedRow, selectedRow);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která naplní list ALL_KINDS_OF_LIST novými / aktuálními daty z DB dle
	 * hodnoty prměnné kindOfForm. Buď se do zmíněného listu vloží všechny typy
	 * uživatelů v DB nebo všechny typy her v DB.
	 */
	@SuppressWarnings("unchecked")
	private void reloadListWithData() {
		if (kindOfForm == EnumKindsOf.GAMES)
			ALL_KINDS_OF_LIST = (List) DATA_MANAGER.getAllKindsOfGames();
		
		else if (kindOfForm == EnumKindsOf.USERS)
			ALL_KINDS_OF_LIST = (List) DATA_MANAGER.getAllKindsOfUsers();		
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected List<AbstractAction> createAbstractActions() {
		// Zjistím, zda se mají vytvořit abstraktní akce pro manipulaci s typy uživateů
		// nebo her:
		
		if (kindOfForm == EnumKindsOf.GAMES) {
			createAction = new AbstractAction("Přidat",
					GetIconInterface.getImage("/crud/CreateIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final KindOfGame newKindOfGame = (KindOfGame) new KindOfGameAndUserForm("Vytvořit", null,
							kindOfForm, ALL_KINDS_OF_LIST).getKind();

					if (newKindOfGame != null) {
						final boolean created = DATA_MANAGER.addKindOfGame(newKindOfGame);

						if (!created) {
							JOptionPane.showMessageDialog(null,
									"Došlo k chybě při pokusu o přidání nového typu hry do Db. Typ hry nebyl přidán!",
									"Chyba", JOptionPane.ERROR_MESSAGE);
							return;
						}

						reloadTableData();
					}
				}
			};
			
			
			updateAction = new AbstractAction("Upravit",
					GetIconInterface.getImage("/crud/EditIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final int selectedRow = table.getSelectedRow();

					if (selectedRow > -1) {
						final KindOfGame kindOfGame = (KindOfGame) tableModel.getValueAtIndex(selectedRow);

						final KindOfGame editedKindOfGame = (KindOfGame) new KindOfGameAndUserForm("Upravit",
								kindOfGame, kindOfForm, ALL_KINDS_OF_LIST).getKind();

						if (editedKindOfGame != null) {
							final boolean updated = DATA_MANAGER.updateKindOfGame(editedKindOfGame);

							if (!updated) {
								JOptionPane.showMessageDialog(
										null, "Došlo k chybě při pokusu o aktualizaci typu hry '"
												+ editedKindOfGame.getKind() + "'.",
										"Chyba", JOptionPane.ERROR_MESSAGE);
								return;
							}

							reloadTableData();
						}
					}

					else
						JOptionPane.showMessageDialog(null, "Není označen typ hry pro editaci!", "Neoznačeno",
								JOptionPane.ERROR_MESSAGE);
				}
			};
			
			
			
			
			deleteAction = new AbstractAction("Smazat",
					GetIconInterface.getImage("/crud/DeleteIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final int selectedRow = table.getSelectedRow();

					if (selectedRow > -1) {
						if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
								"Opravdu si přejete smazat typ hry??", "Smazat typ hry", JOptionPane.YES_NO_OPTION)) {
							final KindOfGame kindOfGame = (KindOfGame) tableModel.getValueAtIndex(selectedRow);

							final boolean isKindOfGameUsed = DATA_MANAGER.getAllGames().stream()
									.anyMatch(k -> k.getKindOfGame().equals(kindOfGame.getKind()));

							if (isKindOfGameUsed) {
								JOptionPane.showMessageDialog(
										null, "Označený typ hry je využíván, nelze smazat.\nTyp hry '"
												+ kindOfGame.getKind() + "'.",
										"Nelze smazat", JOptionPane.ERROR_MESSAGE);
								return;
							}

							final boolean deleted = DATA_MANAGER.deleteKindOfGame(kindOfGame.getKind());

							if (!deleted) {
								JOptionPane
										.showMessageDialog(null,
												"Došlo k chybě při pokusu o smazání typu hry '" + kindOfGame.getKind()
														+ "'. Typ hry nebyl smazán",
												"Chyba", JOptionPane.ERROR_MESSAGE);
								return;
							}

							reloadTableData();
						}
					}

					else
						JOptionPane.showMessageDialog(null, "Není označen typ hry pro smazání!", "Neoznačeno",
								JOptionPane.ERROR_MESSAGE);
				}
			};
		}

		
		
		
		
		else if (kindOfForm == EnumKindsOf.USERS) {
			createAction = new AbstractAction("Přidat",
					GetIconInterface.getImage("/crud/CreateIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final KindOfUser newKindOfUser = (KindOfUser) new KindOfGameAndUserForm("Vytvořit", null,
							kindOfForm, ALL_KINDS_OF_LIST).getKind();

					if (newKindOfUser != null) {
						final boolean created = DATA_MANAGER.addKindOfUser(newKindOfUser);

						if (!created) {
							JOptionPane.showMessageDialog(null,
									"Došlo k chybě při pokusu o přidání nového typu uživatele do Db. Typ uživatele nebyl přidán!",
									"Chyba", JOptionPane.ERROR_MESSAGE);
							return;
						}

						reloadTableData();
					}
				}
			};
			
			
			updateAction = new AbstractAction("Upravit",
					GetIconInterface.getImage("/crud/EditIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final int selectedRow = table.getSelectedRow();

					if (selectedRow > -1) {
						final KindOfUser kindOfUser = (KindOfUser) tableModel.getValueAtIndex(selectedRow);

						final KindOfUser editedKindOfUser = (KindOfUser) new KindOfGameAndUserForm("Upravit",
								kindOfUser, kindOfForm, ALL_KINDS_OF_LIST).getKind();

						if (editedKindOfUser != null) {
							final boolean updated = DATA_MANAGER.updateKindOfUser(editedKindOfUser);

							if (!updated) {
								JOptionPane.showMessageDialog(null,
										"Došlo k chybě při pokusu o aktualizaci typu uživatele '"
												+ editedKindOfUser.getKind() + "'.",
										"Chyba", JOptionPane.ERROR_MESSAGE);
								return;
							}

							reloadTableData();
						}
					}

					else
						JOptionPane.showMessageDialog(null, "Není označen typ uživatele pro editaci!", "Neoznačeno",
								JOptionPane.ERROR_MESSAGE);
				}
			};
			
			
			
			
			deleteAction = new AbstractAction("Smazat",
					GetIconInterface.getImage("/crud/DeleteIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final int selectedRow = table.getSelectedRow();

					if (selectedRow > -1) {
						if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
								"Opravdu si přejete smazat typ uživatele?", "Smazat typ uživatele",
								JOptionPane.YES_NO_OPTION)) {
							final KindOfUser kindOfUser = (KindOfUser) tableModel.getValueAtIndex(selectedRow);

							final boolean isKindOfUserUsed = DATA_MANAGER.getAllUsers().stream()
									.anyMatch(k -> k.getKindOfUser().equals(kindOfUser.getKind()));

							if (isKindOfUserUsed) {
								JOptionPane.showMessageDialog(null,
										"Označený typ uživatele je využíván, nelze smazat.\nTyp uživatele '"
												+ kindOfUser.getKind() + "'.",
										"Nelze smazat", JOptionPane.ERROR_MESSAGE);
								return;
							}

							final boolean deleted = DATA_MANAGER.deleteKindOfUser(kindOfUser.getKind());

							if (!deleted) {
								JOptionPane.showMessageDialog(
										null, "Došlo k chybě při pokusu o smazání typu uživatele '"
												+ kindOfUser.getKind() + "'. Typ uživatele nebyl smazán",
										"Chyba", JOptionPane.ERROR_MESSAGE);
								return;
							}

							reloadTableData();
						}
					}

					else
						JOptionPane.showMessageDialog(null, "Není označen typ uživatele pro smazání!", "Neoznačeno",
								JOptionPane.ERROR_MESSAGE);
				}
			};
		}
		
		
		return new ArrayList<>(Arrays.asList(createAction, updateAction, deleteAction));
	}

	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * - Zda není pole pro vyhledani prazdné
		 * - Zda odpovida zadany text regulárnímu vyrazu pro typ hry nebo uzivatele
		 * - Vyhledani
		 */

		final String searchValue = txtSearch.getText();

		if (searchValue.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Pole pro zadání vyhledávacího řetězce je prázdné!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!searchValue.matches(REG_EX_KIND_OF_USER_AND_GAME)) {
			JOptionPane.showMessageDialog(this, "Zadaný řetězec pro vyhledání neodpovídá požadované syntaxi!",
					"chyná syntaxe", JOptionPane.ERROR_MESSAGE);
			return;
		}

		tableModel.setValuesList(
				ALL_KINDS_OF_LIST.stream().filter(k -> k.getKind().toUpperCase().contains(searchValue.toUpperCase()))
						.collect(Collectors.toList()));
	}
}
