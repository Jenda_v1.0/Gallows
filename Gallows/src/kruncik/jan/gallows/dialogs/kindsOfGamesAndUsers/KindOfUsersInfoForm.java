package kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží pouze jako dialog, který obsahuje informace o dialogu pro
 * manipulaci s daty v tabulce KindsOfUsers.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfUsersInfoForm extends SourceDialog {
	
	private static final long serialVersionUID = 1L;



	public KindOfUsersInfoForm() {
		super("Info");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 500, 500, "/icons/InfoIconForm.png", true);
		
		
		final JTextArea txaInfo = new JTextArea("Dialog slouží pro CRUD operace nad typy uživatelů v DB.\n"
				+ "Tyto údaje by se neměli jakkoli editovat. S těmito dvěma typy uživatelů se pracuje v aplikaci, pokud dojde k jejich změně, aplikace nebude fungovat správně. Dialog pro jejich editaci jsem vytvořil pouze pro usnadnění práce s těmito daty pro potenciální doplňování - vylepšování aplikace samotné.\n\n"
				+ ""
				+ "V dialogu je možné zobrazit veškeré typy uživatelů v DB (V tabulce KindsOfUsers), dále je seřadit a vyhledat podle různých kritérií.\n\n"
				+ ""
				+ "Údaje je možné upravit přímo v tabulce dvojlikem levým tlačítka myši na políčku v tabulce, které chceme upravit, nebo na řádek kliknout prvým tlačítkem myši a zvolit operaci nebo označit typ uživatele v tabulce a zvolit operaci v toolbaru nebo v menu Typ uživatele.");
		
		txaInfo.setEditable(false);
		
		txaInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		
		txaInfo.setLineWrap(true);
		
		add(new JScrollPane(txaInfo), BorderLayout.CENTER);
		
		
		prepareWindow();
	}

	
	
	@Override
	protected void checkData() {
	}
}
