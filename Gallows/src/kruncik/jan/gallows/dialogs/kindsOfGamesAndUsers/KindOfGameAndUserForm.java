package kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.db_objects.KindOfUser;
import kruncik.jan.gallows.db_objects.KindsOfAncestor;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako formulář pro vytvoření a editaci typů her a uživatelů.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class KindOfGameAndUserForm extends SourceDialog {
	
	
	private static final long serialVersionUID = 1L;

	
	
	private final JTextField txtKind;
	
	private final JTextArea txaComment;
	
	
	
	/**
	 * Proměnná, ve které bude uložen typ uživatele nebo typ hry pro vytvoření nebo
	 * editace. Popřípadě null, pokud se dialog zavře bez potvrzení změn či
	 * vytvoření.
	 */
	private KindsOfAncestor kind;
	
	
	/**
	 * Výčtová proměnná, do které se uloží hodnota, která značí, zda se má vytvořit
	 * nový typ uživatele nebo typ hry, je to potřeba, protože pokud se jedná o
	 * vytvoření nového typu hry nebo uživatele a ne o editace, pak bude proměnná
	 * kind null a nevěděl bych tak jakou hodnotu do ní mám vložit, zda typ
	 * uživatele nebo typ hry, proto tato proměnná.
	 */
	private final EnumKindsOf kindOf;
	
	
	/**
	 * List, který obsahuje veškeré typy uživatelů nebo her. Je to zde potřeba kvůli
	 * testování duplicitních hodnot.
	 */
	private final List<KindsOfAncestor> allKindsOf;
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param title
	 *            - Titulek dialogu (Přidat nebo upravit).
	 * @param kind
	 *            - Typ uživatele nebo hry nebo null, tato hodnota bude naplněna
	 *            pouze v případě, že se jedná o editace typu hry nebo uživatele,
	 *            jinak bude null
	 * @param kindOf
	 *            - Výčtový typ o tom, zda se jedná o vytvoření nebo editaci hery
	 *            nebo uživatele (Dle této hodnoty se pozná, zda se v dialogu
	 *            zachází s daty pro typ uživatele nebo hry).
	 * @param allKindsOf
	 *            - List, který obsahuje všechny typy her nebo uživatelů v DB.
	 *            Potřebuji otestovat duplicity.
	 */
	KindOfGameAndUserForm(final String title, final KindsOfAncestor kind, final EnumKindsOf kindOf, final List<KindsOfAncestor> allKindsOf) {
		super(title);
		
		this.kind = kind;
		this.kindOf = kindOf;
		this.allKindsOf = allKindsOf;
		
		
		addWindowStateListener(new MyWindowAdapter());
		
		
		
		if (kindOf == EnumKindsOf.USERS)
			initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 400, "/icons/KindOfUserIconForm.png", true);
		else if (kindOf == EnumKindsOf.GAMES)
			initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 400, "/icons/KindOfGameIconForm.png", true);
		
		
		
		
		final GridBagConstraints gbc = createGbc();
		
		
		int rowIndex = 0;
		
		
		if (kindOf == EnumKindsOf.USERS)
			add(new JLabel("*Typ uživatele: "), gbc);
		else if (kindOf == EnumKindsOf.GAMES)
			add(new JLabel("*Typ hry: "), gbc);
		
		
		createKeyAdapter();
		txtKind = new JTextField(20);
		txtKind.addKeyListener(keyAdapter);
		
		setGbc(gbc, 1, rowIndex);
		add(txtKind, gbc);
		
		
		
		
		setGbc(gbc, 0, ++rowIndex);		
		add(new JLabel("Komentář: "), gbc);
		
		txaComment = new JTextArea();
		final JScrollPane jspComment = new JScrollPane(txaComment);
		jspComment.setPreferredSize(new Dimension(0, 150));
		
		setGbc(gbc, 1, rowIndex);
		add(jspComment, gbc);
		
		
		
		
		
		
		// Potenciální naplnění dat:
		if (kind != null) {
			txtKind.setText(kind.getKind());
			txaComment.setText(kind.getComment());
		}
		
		
		
		
		
		
		
		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText(title);
		btnConfirm.addActionListener(Event -> checkData());
		
		btnCancel.addActionListener(Event -> {
			this.kind = null;
			dispose();
		});
		
		gbc.gridwidth = 2;
		setGbc(gbc, 0, ++rowIndex);
		add(pnlButtons, gbc);
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí tento dialog, který se tak stane modálním, a po zavření
	 * se vrátí hodnota, která bude v proměnné kind buď tam bude null v případě, že
	 * uživatel zavřel dialog bez ptvrzení uložení, nebo typ uživatele nebo typ hry.
	 * 
	 * @return - Typ uživatele nebo typ hry, popř. null, pokud se uživatel rozhodne
	 *         neuložit změny.
	 */
	public KindsOfAncestor getKind() {
		prepareWindow();
		
		return kind;
	}
	
	


	
	
	
	@Override
	protected void checkData() {
		/*
		 * - Prázdná pole
		 * - Regulární výrazy
		 * - Délku řetězců
		 * - duplicity
		 * - Pak vytvořit nové instance nebo naplnit hodnoty.
		 */
		final String kindText = txtKind.getText();
		
		final String commentText = txaComment.getText();
		
		if (kindText.isEmpty()) {
			if (kindOf == EnumKindsOf.USERS)
				JOptionPane.showMessageDialog(this, "Není zadán typ uživatele!", "Prázdné pole",
						JOptionPane.ERROR_MESSAGE);
			else if (kindOf == EnumKindsOf.GAMES)
				JOptionPane.showMessageDialog(this, "Není zadán typ hry!", "Prázdné pole", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!kindText.matches(REG_EX_KIND_OF_USER_AND_GAME)) {
			if (kindOf == EnumKindsOf.USERS)
				JOptionPane.showMessageDialog(this, "Zadaný typ uživatele neodpovídá požadované syntaxi!",
						"Chyba syntaxe", JOptionPane.ERROR_MESSAGE);
			else if (kindOf == EnumKindsOf.GAMES)
				JOptionPane.showMessageDialog(this, "Zadaný typ hry neodpovídá požadované syntaxi!", "Chyba syntaxe",
						JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (kindText.length() > 50) {
			if (kindOf == EnumKindsOf.USERS)
				JOptionPane.showMessageDialog(this, "Zadaný typ uživatele přesahuje povolený počet znaků!",
						"Překročen limit znaků", JOptionPane.ERROR_MESSAGE);
			else if (kindOf == EnumKindsOf.GAMES)
				JOptionPane.showMessageDialog(this, "Zadaný typ hry přesahuje povolený počet znaků!",
						"Překročen limit znaků", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!commentText.isEmpty() && commentText.length() > 255) {
			if (kindOf == EnumKindsOf.USERS)
				JOptionPane.showMessageDialog(this, "Zadaný komentář pro typ uživatele přesahuje povolený počet znaků!",
						"Překročen limit znaků", JOptionPane.ERROR_MESSAGE);
			else if (kindOf == EnumKindsOf.GAMES)
				JOptionPane.showMessageDialog(this, "Zadaný komentář pro typ hrypřesahuje povolený počet znaků!",
						"Překročen limit znaků", JOptionPane.ERROR_MESSAGE);
			return;
		}

		
		final boolean existKindAlready = allKindsOf.stream().anyMatch(k -> k.getKind().equalsIgnoreCase(kindText));

		if (existKindAlready) {
			if (kindOf == EnumKindsOf.USERS)
				JOptionPane.showMessageDialog(this, "Zadaný typ uživatele již existuje, zadejte jiný!!",
						"Duplicitní název", JOptionPane.ERROR_MESSAGE);
			else if (kindOf == EnumKindsOf.GAMES)
				JOptionPane.showMessageDialog(this, "Zadaný typ hry již existuje, zadejte jiný!!", "Duplicitní název",
						JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		
		if (kind != null) {
			kind.setNewKind(kindText);
			kind.setComment(commentText);
		}
		
		
		else {
			if (kindOf == EnumKindsOf.USERS)
				kind = new KindOfUser(kindText, commentText);
			else if (kindOf == EnumKindsOf.GAMES)
				kind = new KindOfGame(kindText, commentText);
		}
		
		
		dispose();
	}
	
	
	
	
	
	
	
	
	/**
	 * Tato třída, která slouží pouze pro to, aby se po zavření dialogu pomocí
	 * kliknutí na křížek nastavila do proměnné kind hodnota null.
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	private class MyWindowAdapter extends WindowAdapter {
		public void windowClosing(java.awt.event.WindowEvent e) {
			/*
			 * Tato metoda se zavolá po kliknutí na křízek v dialogu, tak nastavím proměnnou
			 * kind na hodnotu null, aby se nevrátila žádná hodnota. Uživatel se rozhodl
			 * operaci zrušit, tak nic dělat nebudu.
			 */
			kind = null;
		}
	}
}
