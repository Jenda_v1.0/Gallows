package kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers;

import java.util.Comparator;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import kruncik.jan.gallows.db_objects.KindOfGame;
import kruncik.jan.gallows.db_objects.KindOfUser;
import kruncik.jan.gallows.db_objects.KindsOfAncestor;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží pouze jako model pro tabulku typů uživatelů a typů her.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class UsersGamesTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	

	/**
	 * List, který obsahuje hodnoty v tabulce.
	 */
	private List<KindsOfAncestor> valuesList;
	
	
	private final String[] columns;
	
	
	/**
	 * Reference na dialog, ve kterém je zobrazeny tabulka, která obsahuje tento
	 * tableModel. Potřebuji tuto referenci pro zavolání metody pro načtení
	 * aktuálních hodnot z DB pro tabulku.
	 */
	private final KindOfGamesAndUsersForm kindOfGamesAndUserForm;
	
	
	
	/**
	 * Výčtová proměnná, dle které se pozná, zda se jedná o typ hodnot pro typ
	 * uživatele nebo typ her, například kůvli chybových hláškám apod.
	 */
	private final EnumKindsOf kindOfValue;
	
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param columns
	 *            - Jednorozměrné pole, které obsahuje názvy sloupců
	 * 
	 * @param kindOfGamesAndUserForm
	 *            - Reference na instanci dialogu, ve kterém se nachází tabulka,
	 *            které má tento table model. Abych mohl zavolat při změně dat v
	 *            tabulce metody, která aktualizuje list s hodnotami v tabulce v
	 *            dané třídě.
	 * @param kindOfValue
	 *            - Proměnná, dle které se pozná, zda jsou v listu valuesList a v
	 *            tabulce zobrazeny hodnoty typu her nebo typu uzivatelů (lze brát i
	 *            přes instanceof)
	 */
	UsersGamesTableModel(final String[] columns, final KindOfGamesAndUsersForm kindOfGamesAndUserForm,
			final EnumKindsOf kindOfValue) {
		super();
		
		this.columns = columns;
		this.kindOfGamesAndUserForm = kindOfGamesAndUserForm;
		this.kindOfValue = kindOfValue;
	}
	
	
	
	
	/**
	 * Metoda, která nastaví list s hosdnotami pro tabulku a aktualizuji tyto nová
	 * data v tabulce.
	 * 
	 * @param valuesList
	 *            - list s hodnotami pro tabulku.
	 */
	void setValuesList(List<KindsOfAncestor> valuesList) {
		this.valuesList = valuesList;
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která vrátí hodnotu na zadaném indexu v listu (v řádku v tabulce).
	 * 
	 * @param index
	 *            - hodnota, která značí řádek v tabulce (v listu) na kterém se má
	 *            vrátit hodnota v listu.
	 * 
	 * @return hodnota na indexu v listu.
	 */
	KindsOfAncestor getValueAtIndex(final int index) {
		return valuesList.get(index);
	}
	
	
	
	
	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return valuesList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return valuesList.get(rowIndex).getKind();
			
			
		case 1:
			return valuesList.get(rowIndex).getComment();

		default:
			break;
		}
		
		return null;
	}
	
	
	
	
	
	public String getColumnName(int column) {
		return columns[column];
	}
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object objValue = getValueAt(0, columnIndex);

		if (objValue != null)
			return objValue.getClass();

		return super.getColumnClass(columnIndex);
	}
	
	
	
	
	
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	
	
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final KindsOfAncestor value = valuesList.get(rowIndex);		
		
		switch (columnIndex) {
		case 0:
			final String kind = (String) aValue;

			if (kind.isEmpty()) {
				if (kindOfValue == EnumKindsOf.USERS)
					JOptionPane.showMessageDialog(null, "Není zadán typ uživatele.", "Prázdné ple",
							JOptionPane.ERROR_MESSAGE);
				else if (kindOfValue == EnumKindsOf.GAMES)
					JOptionPane.showMessageDialog(null, "Není zadán typ hry.", "Prázdné ple",
							JOptionPane.ERROR_MESSAGE);
				return;
			}

			if (!kind.matches(SourceDialog.REG_EX_KIND_OF_USER_AND_GAME)) {
				if (kindOfValue == EnumKindsOf.USERS)
					JOptionPane.showMessageDialog(null, "Typ uživatele není zadán v požadované syntaxi.",
							"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				else if (kindOfValue == EnumKindsOf.GAMES)
					JOptionPane.showMessageDialog(null, "Typ hry není zadán v požadované syntaxi.", "Chybná syntaxe",
							JOptionPane.ERROR_MESSAGE);
				return;
			}

			if (kind.length() > 50) {
				if (kindOfValue == EnumKindsOf.USERS)
					JOptionPane.showMessageDialog(null, "Typ uživatele přesahuje povolený počet znaků.",
							"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				else if (kindOfValue == EnumKindsOf.GAMES)
					JOptionPane.showMessageDialog(null, "Typ hry přesahuje povolený počet znaků.",
							"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}

			value.setNewKind(kind);
			break;

			
		case 1:
			final String comment = (String) aValue;

			if (comment.length() > 255) {
				JOptionPane.showMessageDialog(null, "Text komentáře přesahuje povolený počet znaků.",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			value.setComment(comment);		
			break;

		default:
			break;
		}
				
		saveChangedToDb(value);
		
		kindOfGamesAndUserForm.reloadTableData();
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která aktualizuje typ hry nebo typ uživatele v DB (dle typu hodnoty
	 * value).
	 * 
	 * @param value
	 *            - Jeden z potomků třídy KindOfAncestor. Buď to bude typ uživatele
	 *            nebo typ hry, jejichž hodnoty se změnily v tabulce, tak se zde
	 *            aktualizují.
	 */
	private void saveChangedToDb(final KindsOfAncestor value) {
		final boolean updated;
		
//		if (value instanceof KindOfGame)
		if (kindOfValue == EnumKindsOf.GAMES)
			updated = KindOfGamesAndUsersForm.DATA_MANAGER.updateKindOfGame((KindOfGame) value);
		else //if (value instanceof KindOfUser) nebo: if (kindOfValue == EnumKindsOf.USERS)
			updated = KindOfGamesAndUsersForm.DATA_MANAGER.updateKindOfUser((KindOfUser) value);
		
		if (!updated) {
			if (kindOfValue == EnumKindsOf.GAMES)
				JOptionPane.showMessageDialog(null, "Došlo k chybě při pokusu o aktualizaci typu hry '"
						+ value.getKind() + "'. Typ hry nebyl aktualizován!", "Chyba", JOptionPane.ERROR_MESSAGE);

			else if (kindOfValue == EnumKindsOf.USERS)
				JOptionPane.showMessageDialog(null, "Došlo k chybě při pokusu o aktualizaci typu uživatele'"
						+ value.getKind() + "'. Typ uživatele nebyl aktualizován!", "Chyba", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí položky v tabulce (v listu) dle proměnné kind vzestupně
	 * nebo sestupně (dle hodnoty asc)
	 * 
	 * @param asc
	 *            - logická proměnná, která určuje, zda se mají seřadit položky v
	 *            valuesList sestupně nebo vzestupně. True = vzestupně, false =
	 *            sestupně.
	 */
	final void sortByKind(final boolean asc) {
		if (asc)
			valuesList.sort(Comparator.comparing(KindsOfAncestor::getKind));
		else
			valuesList.sort((a, b) -> b.getKind().compareTo(a.getKind()));
		
		fireTableDataChanged();
	}
}
