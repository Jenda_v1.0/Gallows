package kruncik.jan.gallows.dialogs.kindsOfGamesAndUsers;

/**
 * Tento výše slouží pouze kvůli tomu, že jsem nechtěl psát dvakrát téměř ten
 * samý dialog, tak jsem pro tabulky KindsOfGames a KindsOfUsers napsal jen
 * jeden dialog, zde jsou pouze ty typy, kdybych se náhodou někdy v budoucnu
 * rozhodl aplikaci rozšířit (nepravděpodobné), šlo by to uděliat ui přes
 * boolean, ale kdyby náhodou.?
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public enum EnumKindsOf {

	USERS, GAMES
}
