package kruncik.jan.gallows.dialogs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.db_objects.Game;

/**
 * Tato třída slouží jako dialog pro zobrazení "základních" detailů ohledně
 * právě odehrané hry a možnost doplnit komentár k dané hře. Tento dialog se
 * zobrazí ale pouze v případě, že uživatel chce uložit hru, jinak ne.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class SaveGameForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;

	

	/**
	 * Proměnná, která se vrátí v případě, že bude chtít uživatel uložit dialog. Do
	 * proměnné se můžná doplní komentář, pokud jej uživatel zadá.
	 */
	private Game game;
	
	
	/**
	 * Textové pole pro zadání komentáře k odehrané hře.
	 */
	private final JTextArea txtCommentArea;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param game
	 *            - odehraná hra se základními informacemi
	 * @param userName
	 *            - uživatelské jméno právě přihlášeného hráče (hráče, který práve
	 *            odehrál tuto hru)
	 */
	public SaveGameForm(final Game game, final String userName) {
		super("Uložit hru");
		
		this.game = game;
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 600, "/icons/SaveGameForm.jpg", true);
		
		final GridBagConstraints gbc = createGbc();
		
		addWindowListener(new MyWindowAdapter());
		
		
		/*
		 * Proměnná, která definuje index řádku, kam se má přidat nějaká komponenta.
		 */
		int rowIndex = 0;
		
		add(new JLabel("Uživatel: "), gbc);
		
		setGbc(gbc, 1, rowIndex);
		add(new JLabel(userName), gbc);
		
		
		
		
		setGbc(gbc, 0, ++rowIndex);
		add(new JLabel("Datum a čas: "), gbc);
		
		
		setGbc(gbc, 1, rowIndex);
		add(new JLabel(game.getFormatedDate()), gbc);
		
		
		
		setGbc(gbc, 0, ++rowIndex);
		add(new JLabel("Typ hry: "), gbc);
		
		setGbc(gbc, 1, rowIndex);
		add(new JLabel(game.getKindOfGame()), gbc);
		
		
		
		setGbc(gbc, 0, ++rowIndex);
		add(new JLabel("Skóre: "), gbc);
		
		setGbc(gbc, 1, rowIndex);
		add(new JLabel(String.valueOf(game.getScore())), gbc);
		
		
		
		setGbc(gbc, 0, ++rowIndex);
		add(new JLabel("Zbývající životy: "), gbc);
		
		setGbc(gbc, 1, rowIndex);
		add(new JLabel(String.valueOf(game.getLives())), gbc);
		
		
		
		
		setGbc(gbc, 0, ++rowIndex);
		
		if (game.getKindOfGame().equals("Measurement")) {
			add(new JLabel("Čas: "), gbc);
			
			setGbc(gbc, 1, rowIndex);
			add(new JLabel(game.getFormatedTime()), gbc);
		}
		
		else {
			/*
			 * Zde se jedná o hru typu Countdown, tak zde zobrazím nastavený čas (originální
			 * čas) a uplynulý čas
			 */			
			add(new JLabel("Nastavený čas: "), gbc);
			
			setGbc(gbc, 1, rowIndex);
			add(new JLabel(game.getFormattedOriginalTime()), gbc);
			
			
			setGbc(gbc, 0, ++rowIndex);
			add(new JLabel("Uplynulý čas: "), gbc);
			
			setGbc(gbc, 1, rowIndex);
			add(new JLabel(game.getFormatedTime()), gbc);
		}
		
		
		
		setGbc(gbc, 0, ++rowIndex);
		add(new JLabel("Volitelný komentář:"), gbc);
		
		setGbc(gbc, 1, rowIndex);		
		txtCommentArea = new JTextArea();
		if (game.getComment() != null)
			txtCommentArea.setText(game.getComment());

		final JScrollPane jsp = new JScrollPane(txtCommentArea);
		jsp.setPreferredSize(new Dimension(50, 100));
		add(jsp, gbc);
		
		
		
		
		// Panel s tlačítky:
		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText("Uložit");
		/*
		 * Zde se má hra uložit, tak uložím případný komentář (pokud to půjde a zavřu
		 * dialog a vrátím hodnotu)
		 */
		btnConfirm.addActionListener(Event -> checkData());
		
		btnCancel.addActionListener(Event -> {
			/*
			 * Po kliknutí na toto tlačítko se má dialog zavřít bez uložení, tak si uložím
			 * null hodnotu do proměnné game v této třídě.
			 */
			this.game = null;
			dispose();
		});
		
		gbc.gridwidth = 2;
		setGbc(gbc, 0, ++rowIndex);
		add(pnlButtons, gbc);	
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí tento dialog a nabídne tak uživateli možnost uložit
	 * hru.
	 * 
	 * @return - proměnnou game s informacemi o odehrané hře a potenciálně doplněným
	 *         komentářem nebo null, pokud uživatel dialog zavře bez uložení.
	 */
	public final Game getEditedGame() {
		prepareWindow();
		
		return game;
	}
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * Metoda, která zkontroluje zadaný komentář, pokud je prázdný, není co řešit, ale pokud není,
		 * tak zkontrolujji, zda splňuje počet znaků, dle toho se buď vyhodí chybová hláška
		 */
		if (!txtCommentArea.getText().isEmpty()) {
			if (txtCommentArea.getText().length() > 255) {
				JOptionPane.showMessageDialog(this, "Počet znaků v komentáři je větší než povolená hodnota!",
						"Dlouhý text", JOptionPane.ERROR_MESSAGE);
				return;
			}	
			
			// Zde je komentář v pořádku, tak jej vložím do promenné a tu po zavolání metody
			// dispose vrátím pro uložení:
			game.setComment(txtCommentArea.getText());
		}
		
		dispose();
	}
	
	
	
	
	
	
	/**
	 * Třída, která slouží jako listener na události okna dialogu, konkrétně na
	 * kliknutí na křížek pro zavření dialogu, má akorát jednu implementovanou
	 * metodu a sice pokud se klikne na ten křížek, tak se nastaví proměnná game na
	 * null, aby se nic neuložilo do DB.
	 * 
	 * @author jan
	 *
	 */
	private class MyWindowAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			/*
			 * Tato metoda se zvolá, když kliknu na křížek pro zavření dialogu, v takovém
			 * případě se nemá uložit nic do DB, tak nastavím následující proměnnou na null,
			 * aby se tato null hodnota vrátila s zjistilo se, že se nic nemá ukládat do DB.
			 */
			game = null;
		}
	}
}
