package kruncik.jan.gallows.dialogs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;

/**
 * Tato třída je zde pouze pro společné metody nad dialogy.
 * Abych nemusel psát stejné metody v různých třídách
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public abstract class SourceDialog extends JDialog {

	
	private static final long serialVersionUID = 1L;
		
	
	
	/**
	 * Tento font slouží puze pro text do dialogů s informacemi o nějakém dialogu
	 * nebo aplikaci apod.
	 */
	protected static final Font FONT_FOR_TEXT_IN_INFO_DIALOG = new Font("Font for text in dialogs with information.",
			Font.BOLD, 14);
	
	
	
	
	/**
	 * Tato proměnná obsahuje regulární výraz pro skóre.
	 * Jedná se o výraz, kterýn projdou hodnoty typu int (včetně mezer, které se při testování odeberou)
	 */
	protected static final String REG_EX_SCORE = "^\\s*\\d+[\\s\\d]*\\s*$";
	
	
	
	
	/**
	 * Regulární výraz pro ID (číslo větší než nula, včetně mezer)
	 */
	protected static final String REG_EX_ID = "^\\s*\\d+[\\s\\d]*\\s*$";
	
	
	
	
	
	/**
	 * Regulární výraz pro telefonní číslo, jedná se o syntaxi že může být libovolný
	 * počet mezer kdekoliv v textu, ale musí tam být devět číslic, takže akorát 9
	 * čislic a libovolný počet mezer, žádné jiné znaky nejsou povoleny.
	 */
	protected static final String REG_EX_TELEPHONE_NUMBER = "^\\s*(\\s*\\d){9}\\s*$";
	
	
	/**
	 * Regulární výraz pro emailovou adresu.
	 * Syntaxe může být:
	 * a @ b . c
	 * a - alfanumerické znaky a podtržítka, tečky a procenta
	 * b - alfanumerické znaky a podtržítka a pomlčka
	 * c - 2 - 6 znaků typu malá a velká písmena
	 */
	public static final String REG_EX_EMAIL = "^[\\w\\.%]+@[\\w\\.-]+\\.[a-zA-Z]{2,6}$";
	
	
	/**
	 * Regulární výraz pro jméno a příjmení.
	 * Mohou být ve výraze puze velká a malá písmena s diakritikou bez mezer
	 */
	public static final String REG_EX_FOR_FIRST_AND_LAST_NAME = "^[a-zA-ZěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ]+$";
	
	
	
	/**
	 * Regulární výraz pro uživatelské jméno:
	 * povolené jsou:
	 * mezery, velká a malá písmane s diakritikou a bez diakritiky, číslice, tečky	
	 */
	public static final String REG_EX_PSEUDONYM = "^[ěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ \\w\\.]+$";	
	
	
	
	/**
	 * Regulární výraz pro slovo. Jedná se o slovo nebo více slov s velkými a malými
	 * písmeny s / bez diakritikou a mezerami.
	 */
	public static final String REG_EX_WORD = "^\\s*[ěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ A-Za-z]+\\s*$";
	
	
	/**
	 * regulární výraz pro název skupiny slov (Group). Mohou se zde vyskytovat velká
	 * a malá písmena s diakritikou, číslice a podtržítka a mezery - tj. může to být
	 * více slov.
	 */
	public static final String REG_EX_GROUP_NAME = "^\\s*[ěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ \\d\\w]+\\s*$";
	
	
	
	/**
	 * Regulární výraz pro PSČ. Může obsahovat buď 5 čísel vedle sebe bez mezer nebo
	 * 3 čísla, libovolný počet mezer a 2 čísla.
	 */
	public static final String REG_EX_PSC = "^\\s*(\\d{5}|\\d{3}\\s*\\d{2})\\s*$";
	
	
	/**
	 * Regulární výraz pro název města. Mohou tam být velká a malá písmena s
	 * diakritikou, mezery a uplně na konci mohou a nemusí být čísla - ale ta čísla
	 * u sebe - tj. bez mezer.
	 */
	public static final String REG_EX_CITY_NAME = "^\\s*[ěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ \\w]+\\s*\\d*\\s*$";
	
	
	/**
	 * Regulární výraz pro typ uživatele a typ hry. Jedná se o velká a malá písman s
	 * a bez diakritiky, číslice a podtržítka a mezery.
	 */
	public static final String REG_EX_KIND_OF_USER_AND_GAME = "^\\s*[ěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ \\w]+\\s*$";
	
	
	
	
	
	
	
	/**
	 * Tlačítka pro zavření dialogu nebo potvrzení nějuakých dat a případné zavření
	 * dialogu.
	 */
	protected JButton btnCancel, btnConfirm;
	
	
	
	

	
	

	/**
	 * Proměnná, která se implementuje v 
	 */
	protected KeyAdapter keyAdapter;

	
	
	
	
	
	/**
	 * Metoda, která vytvoří panel s tlačítky pro zavření dialolgu (pouze zavření
	 * dialogu) - tlačítko Zavřít (btnCancel) a druhé tlačítko btnConfirm, u kterého
	 * se musí nastavit text dle potřeby a akce.
	 * 
	 * Obě tlačítka se přidají do panelu, ve kterém jsou zarovnány vpravo.
	 * 
	 * @return panel s tlačítky pro zavření dialogua a druhému tlačítku se musí
	 *         dodefinovat akce a text
	 */
	protected final JPanel createPnlButtons() {
		final JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 0));

		btnConfirm = new JButton();
		btnCancel = new JButton("Zavřít");

		btnCancel.addActionListener(Event -> dispose());

		pnlButtons.add(btnConfirm);
		pnlButtons.add(btnCancel);

		return pnlButtons;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která zkontrluje zadaná data v dialogu a dle toho provede příslušnou
	 * operaci, pro kterou je dialog určen, například pokud je dialog pro
	 * přihlášení, tak zkontroluje data zadaná data coby uživatelské jméno a heslo,
	 * zda je to zadáno a zkusí přihlásit uživatele. Jinak pokud je dialog určin
	 * třeba pro registrace uživatele, tak opět zkontroluje zadaná data ve formuláři
	 * a pokud je to v pořádku, pokusí se vytvořit nového uživatele. Apod pro všechn
	 * yostatní formuláře
	 */
	protected abstract void checkData();

	protected void createKeyAdapter() {
		keyAdapter = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					checkData();
			}
		};
	}
	
	
	
	
	
	
	

	
	


	/**
	 * Konstruktor, který nastaví titulek příslušnému dialogu.
	 * 
	 * @param title - text dialogu
	 */
	public SourceDialog(final String title) {
		setTitle(title);
	}
	
	
	
	
	
	/**
	 * Metoda, která slouží pro výchozí nastaví GUI daného okna (formuláře).
	 * Nastavují se následující parametry okna:
	 * 
	 * @param closeOperation
	 *            - Operace, která se stane, když uživatel klikne na křížek pro
	 *            zavření dialogu
	 * @param layoutManager
	 *            - Manager rozvržení komponent v okně
	 * @param width
	 *            - preferovaná šířka okna
	 * @param height
	 *            - preferovaná výška okna
	 * @param pathToIcon
	 *            - cesta k ikoně, která se má nastavit dialogu
	 * @param modal
	 *            - logická hodnota o tom, zda se má stát dialog modálním po tom, co
	 *            se zviditelní
	 */
	protected final void initGui(final int closeOperation, final LayoutManager layoutManager, final int width,
			final int height, final String pathToIcon, final boolean modal) {
		setDefaultCloseOperation(closeOperation);
		setLayout(layoutManager);
		setPreferredSize(new Dimension(width, height));
		setModal(modal);
		setIconImage(GetIconInterface.getImage(pathToIcon, KindOfPicture.APP_ICON).getImage());
	}
	
	
	
	
	
	/**
	 * Metoda, která "připraví" okno a zobrazí jej. Konkrétně přizpůsobí okno
	 * velikosti komponentám (metoda pack), nastaví pozici okna na střed obrazovky
	 * (monitoru) a zviditelní okno
	 */
	protected final void prepareWindow() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	
	
	
	
	

	/**
	 * Metoda pro nastavení parametru GridBagConstraint pro hodnoty girdx a gridy,
	 * které definují pozici v pomyslné mřížce.
	 * 
	 * @param gbc
	 *            - GridBagConstraint, kterému se mají nastavit výše uvedené
	 *            hodnoty.
	 * 
	 * @param gridx
	 *            - hodnota coby index sloupce v pomyslné tabulce
	 * @param gridy
	 *            - hodnota coby index řádku v pomyslné tabulce
	 */
	protected static void setGbc(final GridBagConstraints gbc, final int gridx, final int gridy) {
		gbc.gridx = gridx;
		gbc.gridy = gridy;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda pro vytvoření komponenty GridBagConstraint.
	 * 
	 * @return GridBagConstraint s před definovanými vlastnostmi
	 */
	protected static GridBagConstraints createGbc() {
		final GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(5, 5, 5, 5);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		gbc.weightx = 0.2;
		gbc.weighty = 0.2;		
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		
		return gbc;
	}
}
