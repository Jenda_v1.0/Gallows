package kruncik.jan.gallows.dialogs;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Tato třída slouží jako dialog, ve kterém jsou zobrazeny - popsány základní
 * informace ohledně zacházení a možností obsluhy této aplikace - hraní hry
 * Šibenice.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class AboutAppForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;
	
	
	public AboutAppForm() {
		super("Základní info");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 500, 600, "/icons/InfoIconForm.png", true);
		
		
		
		final JTextArea txtInfo = new JTextArea("Tento dialog obsahuje základní informace ohledně této aplikace - Šibenice.\n"
				+ "Jedná se o aplikaci, kterou jsem napsal pouze za účelem opakování získávání nových zkušeností.\n\n"
				+ "Hra Šibenice se hraje \"normálně\" jak je známo -> hráč se pokouší uhodnou vygenerované slovo a při každém pokusu u uhodnutí nějakého písmeka v daném slově se mu buď sníží životy nebo odkryje písmeno / písmenka ve slově. Dle toho, zda byl pokud o uhodnutí písmena úspěšný nebo ne se testuje snížení životů, případně konec hry nebo zvýšení budů.\n\n"
				+ ""
				+ "Hra obsahuje 2 \"Herní módy\" a sice Measurement a Countdown.\n"
				+ "- Measurement:\n"
				+ "\tJedná se o typ hry, kdy se hráči měří čas dokud neprohraje, tj. dokud hráči nedojdou životy.\n"
				+ "- Countdown:\n"
				+ "\tJedná se o typ hry, kdy si hráč nastaví čas, například 5 minut. Po spuštění hry bude mít hráč zvolených 5 minut na to, aby uhodl co nejvíce slovíček. Hra skončí v případě, když hráči dojde čas nebo životy.\n\n"
				+ ""
				+ "Uživatel má možnost hru předčasně ukončit a uložit.\n"
				+ "V takovém případě je pak možné hru obnovit po kliknutí na tlačítko \"Pokračovat\" v dialogu s přehledem výsledků přihlášeného uživatele.\n\n"
				+ ""
				+ "Každý přihlášený uživatel má možnost otevřít si dialog, kde jsou zobrazeny výsledky všech uložených her všech uživatelů, které si může jakkoli seřadit, například dle dosaženého skóre apod.\n\n"
				+ ""
				+ "V aplikaci figurují dvě role -> Admin a Player.\n"
				+ "Oba typy uživatelů mohou hrát hru. Uživatel typu Admin má navíc práva manipulovat s daty v DB. Například vytvářet další uživatele stejného typu nebo typu Player. Vytvářet, mazat a editovat nové skupiny slov a slova samotná, stejně tak města, uživatele, typy uživatelů atd. (CRUD operace nad všemi tabulkami v DB.)\n\n"
				+ ""
				+ "Poznámka:\n"
				+ "- Typy uživatelů (Admin a Player) by se neměli jakkoli upravovat, v aplikaci se počítá pouze s těmito dvěma typy. Pokud se upraví či přidají nové apod. Aplikace pak nebude fugnovat správně.\n\n"
				+ ""
				+ "Každý uživatel si může upravit svá data ohledné uživatelského účtu v menu Uživatel -> Info -> Zobrazí dialog, se zadanými daty. Zde je možné svá data upravit nebo smazat účet.");
		
		txtInfo.setEditable(false);
		
		txtInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		
		txtInfo.setLineWrap(true);
		
		add(new JScrollPane(txtInfo), BorderLayout.CENTER);
		
		
		
		prepareWindow();
	}

	@Override
	protected void checkData() {
		/*
		 * V tomto dialogu není tato metoda potřeba, tento dialog slouží pouze pro
		 * ukázku něajkých dat.
		 */
	}
}
