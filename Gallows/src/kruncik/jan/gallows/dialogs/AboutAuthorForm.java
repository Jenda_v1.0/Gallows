package kruncik.jan.gallows.dialogs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;

/**
 * Tato třída slouží jako dialog, ve kterém jsou ukázány základní informace o
 * autorovi této aplikace.
 * 
 * Obrázek	Jméno
 * 			Příjmení
 * 			Email
 * 			Tel
 * Osobní poznámky
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class AboutAuthorForm extends SourceDialog {

	private static final long serialVersionUID = 1L;
	

	public AboutAuthorForm() {
		super("Autor");
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 600, "/icons/AuthorIconForm.png", true);
		
		final GridBagConstraints gbc = createGbc();
		
		
		final JLabel lblImage = new JLabel();
		lblImage.setIcon(GetIconInterface.getImage("/icons/AuthorIcon.png", KindOfPicture.AUTOR_ICON));		
		gbc.gridheight = 4;
		add(lblImage, gbc);
		
		
		
		
		
		gbc.gridheight = 1;
		setGbc(gbc, 1, 0);
		add(new JLabel("Jméno: Jan"), gbc);
		
		setGbc(gbc, 1, 1);
		add(new JLabel("Příjmení: Krunčík"), gbc);
		
		setGbc(gbc, 1, 2);
		add(new JLabel("Email: kruncikjan95@seznam.cz"), gbc);
		
		setGbc(gbc, 1, 3);
		add(new JLabel("Telefon: --- --- ---"), gbc);
		
		
		gbc.gridwidth = 2;
		setGbc(gbc, 0, 4);

		final JTextArea txtInfo = new JTextArea(
				"Tuto aplikaci jsem napsal pouze za účelem opakování a získávání nových zkušeností.");
		
		txtInfo.setEditable(false);
		txtInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		txtInfo.setLineWrap(true);
		final JScrollPane jspInfo = new JScrollPane(txtInfo);
		jspInfo.setPreferredSize(new Dimension(0, 150));
		add(jspInfo, gbc);
		
		
		
		prepareWindow();
	}

	@Override
	protected void checkData() {
		/*
		 * V tomto dialogu není tato metoda potřeba, tento dialog slouží pouze pro
		 * ukázku nějakých dat.
		 */
	}
}