package kruncik.jan.gallows.dialogs.dataForDialogs;

import java.io.File;

/**
 * Toto rozhraní slouží obsahuje metody pro výběr souborů.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public interface MyFileChooserImpl {

	/**
	 * Metoda, která slouží pro zobrazení dialogu pro výběr souborů, má se vybrat obrázek,
	 * který bude sloužit jako "uživatelův profil".
	 * 
	 * @return refernci na File, která obsahuje vybraný obrázek
	 */
	File getUserPicture();
	
	
	/**
	 * Metoda, která zobrazí dialog pro výběr adresáře, kde se nachází databáze pro
	 * aplikaci.
	 * 
	 * @return cestu k adresářk, kde se nachází databáze pro tuto aplikaci.
	 */
	String getPathToDbDir();
}
