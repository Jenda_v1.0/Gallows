package kruncik.jan.gallows.dialogs.dataForDialogs;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.plaf.basic.BasicComboBoxEditor;

/**
 * Tato třída slouží jen pro to, abych měl v JComboBoxu v SelectGameForm dialogu
 * horizontální scrolbar, když je potřeba pro výběr typu hry.
 * 
 * Zdroj: 
 * https://stackoverflow.com/questions/10267032/jscrollpane-on-jcombobox
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class ComboBoxEditor extends BasicComboBoxEditor {

	private final JScrollPane scroller = new JScrollPane();
	// NOTE: editor is a JTextField defined in BasicComboBoxEditor

	public ComboBoxEditor() {
		super();
		scroller.setViewportView(editor);
		scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}

	/** Return a JScrollPane containing the JTextField instead of the JTextField **/
	@Override
	public Component getEditorComponent() {
		return scroller;
	}

	@Override
	protected JTextField createEditorComponent() {
		JTextField editor = new JTextField();
		editor.setBorder(null);
		editor.setEditable(false);
		editor.setFont(new Font("Font For my Editor", Font.BOLD, 12));
		return editor;
	}
}
