package kruncik.jan.gallows.dialogs.dataForDialogs;

import kruncik.jan.gallows.db_objects.KindOfGame;

import kruncik.jan.gallows.db_objects.Group;

/**
 * Tato třída slouží pouze jako "uložný prostor" pro předávání více objektů jako
 * návratová hodnota metody. Do instance této třídy se vloží příslušné hodnoty a
 * předají se.
 * 
 * Jsou zde akorát tři proměnné, jedná pro skupinu slov, ze které se budou
 * generovat slovíčka do šibenice a druhá proměnná pro typ hry (odpočet času,
 * meření času) a třetí proměnná (to budou jako 3 proměnné pro hh, mm, ss) se
 * předá pouze v případě, že se zvolil odpočet času, tak se i nastaví čas, jak
 * dlouho chce uživatel hrát, třeba 5 minut, pak se předá tento čas.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class SelectedGame {
	
	
	/**
	 * Skupina, ze které se budou generovat slovíčka
	 */
	private final Group group;

	/**
	 * Typ hry, který definuje způsob meření času hry - odpočet nebo meření času.
	 */
	private final KindOfGame kindOfGame;
	
	
	
	// nastavený čas, pokud se zvolil odpočet času.
	private final int hh, mm, ss;
	
	

	/**
	 * Konstruktor této třídy, který slouží pro naplnění hodnot: skupiny slov a typ
	 * hry v případě, že se jedná o klasicé meření času - stopování (ne odpočet -
	 * Countdown).
	 * 
	 * @param group
	 *            - zvolená skupina slov
	 * @param kindOfGame
	 *            - typ hry - Measurement
	 */
	public SelectedGame(final Group group, final KindOfGame kindOfGame) {
		super();

		this.group = group;
		this.kindOfGame = kindOfGame;
		
		// Zde nastavím hodnoty pro čas na nulu - výchozí hodnoty, které se pužijí pro
		// meření času při nové hře.
		hh = mm = ss = 0;
	}
	
	
	
	
	/**
	 * Tento konstruktor slouží pro naplnění hodnot v případě, že se zvolil odpočet
	 * času. Je třeba naplnit následující hodnoty:
	 * 
	 * @param group
	 *            - skupina slov, která se budou hádat.
	 * @param kindOfGame
	 *            - typ hry, v tomto případě je to odpočet - Countdown, ale je třeba
	 *            tuto hodnotu dále předávatpro testování, tak si ji i tak uložím, i
	 *            když to není potřeba, (ani v kontruktoru výše)
	 * @param hh
	 *            - počet hodin, jak dlouho semá měřit čas.
	 * @param mm
	 *            - počet minut - jak dlouho se má měřit čas
	 * @param ss
	 *            - počet sekund - jak dlouho se má měřit čas
	 */
	public SelectedGame(final Group group, final KindOfGame kindOfGame, final int hh, final int mm, final int ss) {
		super();

		this.group = group;
		this.kindOfGame = kindOfGame;

		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
	}
	
	

	public Group getGroup() {
		return group;
	}

	public KindOfGame getKindOfGame() {
		return kindOfGame;
	}

	public int getHh() {
		return hh;
	}

	public int getMm() {
		return mm;
	}

	public int getSs() {
		return ss;
	}
}