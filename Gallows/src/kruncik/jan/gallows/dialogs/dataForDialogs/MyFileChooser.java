package kruncik.jan.gallows.dialogs.dataForDialogs;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Třída pro dialog, ve kterém si lze vybrat různé soubory. Tento konkrétní
 * slouží puze pro výběr obrázků.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class MyFileChooser extends JFileChooser implements MyFileChooserImpl {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Kontruktor, který nastaví výchozí cestu k domovskému adresáři. V tomto
	 * adresáři se otevře dialog pro výběr souborů.
	 */
	public MyFileChooser() {
		super(System.getProperty("user.home"));
	}
	
	@Override
	public File getUserPicture() {
		setDialogTitle("Vyberte obrázek");

		resetChoosableFileFilters();

		setFileSelectionMode(FILES_ONLY);

		setFileFilter(new FileNameExtensionFilter("*.jpg", "jpg", "jpeg"));
		setFileFilter(new FileNameExtensionFilter("*.png", "png"));
		setFileFilter(new FileNameExtensionFilter("*.gif", "gif"));
		setFileFilter(new FileNameExtensionFilter("*.rat", "raf"));
		setFileFilter(new FileNameExtensionFilter("*.bmp", "bmp"));

		if (showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			final String path = getSelectedFile().getAbsolutePath();

			if (path.endsWith(".jpg") || path.endsWith(".png") || path.endsWith(".gif") || path.endsWith(".raf")
					|| path.endsWith(".bmp"))
				return getSelectedFile();
		}

		return null;
	}
	
	
	
	
	
	
	
	
	
	@Override
	public String getPathToDbDir() {
		setDialogTitle("Vyberte adresář databáze");
		
		resetChoosableFileFilters();

		setFileSelectionMode(DIRECTORIES_ONLY);

		if (showOpenDialog(this) == JFileChooser.APPROVE_OPTION && getSelectedFile().exists())
			return getSelectedFile().getAbsolutePath();

		return null;
	}
}
