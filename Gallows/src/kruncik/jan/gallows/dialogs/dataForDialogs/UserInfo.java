package kruncik.jan.gallows.dialogs.dataForDialogs;

import kruncik.jan.gallows.db_objects.User;

/**
 * Tato třída slouží pouze jako objekt pro předání výsledků. Nacchází se zde
 * logická proměnná, dle které se pozná, zda byl uživatel smazán a proměnná typu
 * User, která se použije v případě, že byl uživatel upraven (jeho nějaká data).
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class UserInfo {

	private User user;

	private boolean deleted;

	
	public UserInfo(User user, boolean deleted) {
		super();
		this.user = user;
		this.deleted = deleted;
	}

	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}
