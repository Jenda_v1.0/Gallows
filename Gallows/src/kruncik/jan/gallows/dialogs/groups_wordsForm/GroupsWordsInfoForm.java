package kruncik.jan.gallows.dialogs.groups_wordsForm;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží puze jako dialog s informacemi o dialogu GroupWordForm,
 * který obsahuje tabulky pro manipulaci se slovy a skupinami slov v DB.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class GroupsWordsInfoForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;



	public GroupsWordsInfoForm() {
		super("Info");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 500, 500, "/icons/InfoIconForm.png", true);
	
		final JTextArea txaInfo = new JTextArea("Dialog slouží pro CRUD operace nad všemi skupinami slovíček a slovíčkami samotnými v DB.\n"
				+ "Je možné zobrazit veškeré skupiny slov v DB, dále je seřadit a vyhledat podle různých kritérií. Toto platí i pro slovíčka.\n"
				+ "Pro zobrazení slovíček stačí kliknout levým tlačítkem myši na skupinu slov a v tabulce (vpravo) se zobrazí veškerá slovíčka v DB v dané skupině slov.\n\n"
				+ ""
				+ "Některé údaje lze upravit přímo v tabulce, například dvojlikem levým tlačítkem myši na políčko v tabulce, které chceme upravit.\n"
				+ "Dále je možné upravit údaje pomocí dialogů pro vytvoření a editaci skupin slov a slov. Stačí označit danou skupinu slov nebo  slovíčko v tabulce a kliknout na tlačítko pro editace v menu Akce, nebo na tlačítka v toolbaru nebo na daný řádek v tabulce kliknout pravým tlačítkem myši.");
		
		txaInfo.setFont(FONT_FOR_TEXT_IN_INFO_DIALOG);
		
		txaInfo.setEditable(false);
		
		txaInfo.setLineWrap(true);
		
		add(new JScrollPane(txaInfo), BorderLayout.CENTER);
		
		prepareWindow();
	}

	
	
	@Override
	protected void checkData() {
	}
}
