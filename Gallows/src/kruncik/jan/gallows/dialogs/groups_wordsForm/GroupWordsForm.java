package kruncik.jan.gallows.dialogs.groups_wordsForm;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.app.KindOfPicture;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.Word;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudAbstractActions;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.CrudMenu;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.KindOfInfoDialog;
import kruncik.jan.gallows.dialogs.dataForDatabaseForms.MyPopupMenu;

/**
 * Tato třída lsouží jako dialog, pro manipulaci se skupinami slovíček a
 * samotnými slovíčkami v daných skupinách.
 * 
 * Dialog obsahuje dvě tabulky. Tabulka vlevo je tabulka, která obsahuje skupinu
 * slov, po označení nějaké skupiny se v tabulce vpravo zobrazí slova
 * vyskytující se v dané skupině.
 * 
 * V horní části okna jsou dva toolbary, první obsahuje tlačítka pro přidání,
 * upravení a smazání skupiny slov nebo slovíček. (tlačítka pro manipulaci se
 * slovíčky se zpřístupní až v případě, že je označeno nějaké slovíško) Druhý
 * toolbar obsahuje různé filtry pro řazení a hledání skupin a slov dle různých
 * kritérií.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class GroupWordsForm extends CrudAbstractActions {

	
	private static final long serialVersionUID = 1L;



	/**
	 * Komponenta pro manipulaci daty v DB.
	 */
	public static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	
	// Ake pro vytvoření, editace a smazání slovíčka z nějaké skupiny:
	private AbstractAction createWordAction, updateWordAction, deleteWordAction;
	
	
	
	/**
	 * Názvy sloupců pro tabulku, kde jsou zobrazeny skupiny slov.
	 * 
	 * Info: 
	 * ID - čka by nikde být nemuseli, ale aby tam alespoň něco bylo.
	 */
	private static final String[] GROUP_COLUMNS_MODEL = { "ID", "Název skupiny", "Komentář" };
	
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku, kde jsou
	 * zobrazeny slova.
	 */
	private static final String[] WORDS_COLUMNS_MODEL = { "Slovo", "Komentář" };
	
	
	
	
	/**
	 * Jednorozměrné pole, které slouží jako model pro JComboBox cmbOrder, tj.
	 * obsahuje položky do tohoto JComboBoxu pro seřazení dat v tabulce se skupiny
	 * slovíček a tabulku slovíček samotných.
	 */
	private static final String[] CMB_ORDER_GROUPS_MODEL = { "Veškeré skupiny", "ID skupin - Sestupně",
			"ID skupin - Vzestupně", "Název skupin - Sestupně", "Název skupin - Vzestupně", "Slov - Sestupně",
			"Slov - Vzestupně" };
		
	
	
	
	/**
	 * Jednorozměrné pole, které slouží jako model položek pro JComboBox cmbOrder.
	 * Jedná se o položky (sloupce v tabulce) dle kterých se bude vyhledávat dle
	 * zadného kritéria v textovém poli.
	 */
	private static final String[] CMB_SEARCH_MODEL = { "ID skupiny", "Název skupiny", "Slovo",
			"Slovo (ze všech slov)" };
	
	
	
	
	
	// Tabulka se skuipnami slov a tabulka se slovy smaotnými:
	private final JTable tableGroups, tableWords;
	
	
	
	
	
	/**
	 * Model pro tabulku tableGroups.
	 */
	private final GroupsTableModel groupsTableModel;
	
	
	/**
	 * Model pro tabulku tableWords.
	 */
	private final  WordsTableModel wordsTableModel;
	
	
	
	
	
	/**
	 * List, který obsahuje veškeré skupiny slov v DB.
	 */
	private static List<Group> ALL_GROUPS;
	
	
	
	
	
	// deleteAllWordsInGroupAction - Akce, která vymaže všechna slovíčka v dané skupině
	// deleteAllGroupsAction - Akce, která vymaže všechna slovíčka a všechny skupiny sloví.
	private AbstractAction deleteAllWordsInGroupAction, deleteAllGroupsAction;
	
	
	
	
	
	
	
	public GroupWordsForm() {
		super("Skupiny slovíček");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 1000, 500, "/icons/GroupFormIcon.png", true);
		
		ALL_GROUPS = DATA_MANAGER.getAllGroups();
		
		
		final List<AbstractAction> actionsList = createAbstractActions();			
		final List<AbstractAction> actionsWordsList = createAbstractActionsForWords(); 
		
		
		final List<AbstractAction> allActions = new ArrayList<AbstractAction>();
		allActions.addAll(actionsList);
		allActions.addAll(actionsWordsList);
		
		
		
		groupsTableModel = new GroupsTableModel(GROUP_COLUMNS_MODEL);
		wordsTableModel = new WordsTableModel(WORDS_COLUMNS_MODEL);
		
		
		
		setJMenuBar(new CrudMenu(this, "Akce", allActions, KindOfInfoDialog.GROUPS_WORDS_INFO_DIALOG));
		
		
		
		
		// Toolbar s tlačítky:
		final JToolBar toolbarActions = new JToolBar();
		toolbarActions.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		toolbarActions.add(createAction);
		toolbarActions.add(updateAction);
		toolbarActions.add(deleteAction);
		toolbarActions.add(deleteAllGroupsAction);
		
		toolbarActions.addSeparator();
		
		toolbarActions.add(createWordAction);
		toolbarActions.add(updateWordAction);
		toolbarActions.add(deleteWordAction);
		toolbarActions.add(deleteAllWordsInGroupAction);
		
		
		
		
		// Toolbar s filtry:
		final JToolBar toolbarFilters = new JToolBar();
		toolbarFilters.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		
		toolbarFilters.add(new JLabel("Seřadit"));
		
		cmbOrder = new JComboBox<>(CMB_ORDER_GROUPS_MODEL);
		cmbOrder.addActionListener(Event -> {
			final String selectedItem = (String) cmbOrder.getSelectedItem();			

			if (selectedItem.equals(CMB_ORDER_GROUPS_MODEL[0]))
				groupsTableModel.setGroupsList(ALL_GROUPS);

			else if (selectedItem.equals(CMB_ORDER_GROUPS_MODEL[1]))
				groupsTableModel.sortByID(false);

			else if (selectedItem.equals(CMB_ORDER_GROUPS_MODEL[2]))
				groupsTableModel.sortByID(true);

			else if (selectedItem.equals(CMB_ORDER_GROUPS_MODEL[3]))
				groupsTableModel.sortByGroupName(false);

			else if (selectedItem.equals(CMB_ORDER_GROUPS_MODEL[4]))
				groupsTableModel.sortByGroupName(true);

			else if (selectedItem.equals(CMB_ORDER_GROUPS_MODEL[5]))
				wordsTableModel.sortByWord(false);

			else if (selectedItem.equals(CMB_ORDER_GROUPS_MODEL[6]))
				wordsTableModel.sortByWord(true);
		});
		toolbarFilters.add(cmbOrder);
		
		
		
		toolbarFilters.addSeparator();		
		
		toolbarFilters.add(new JLabel("Vyhledat"));
		
		cmbSearch = new JComboBox<>(CMB_SEARCH_MODEL);
		toolbarFilters.add(cmbSearch);
		
		createKeyAdapter();
		txtSearch = new JTextField(20);
		txtSearch.addKeyListener(keyAdapter);
		toolbarFilters.add(txtSearch);
		
		
		btnSearch = new JButton("Vyhledat");
		btnSearch.addActionListener(Event -> checkData());
		toolbarFilters.add(btnSearch);
		
		
		
		
		
		// Panel s toolbary pro vložení do okna aplikace:
		final JPanel pnlToolbars = new JPanel(new BorderLayout());
		pnlToolbars.add(toolbarActions, BorderLayout.NORTH);
		pnlToolbars.add(toolbarFilters, BorderLayout.SOUTH);
		
		add(pnlToolbars, BorderLayout.NORTH);
		
		
		
		
		
		
		// Tabulky:		
		groupsTableModel.setGroupsList(ALL_GROUPS);
		
		
		
		tableGroups = new JTable(groupsTableModel);
		tableWords = new JTable(wordsTableModel);
		
		
		createPopupMenuMouseAdapter(tableGroups, actionsList);
		createPopupMenuMouseAdapterTableWords(tableWords, actionsWordsList);
		
		
		
		tableGroups.getSelectionModel().addListSelectionListener(e -> {
			if (!e.getValueIsAdjusting() && tableGroups.getSelectedRow() > -1) {
				final int groupId = groupsTableModel.getGroupAtIndex(tableGroups.getSelectedRow()).getId();

				final List<Word> wordsList = DATA_MANAGER.getWordsByGroupId(groupId);

				wordsTableModel.setWordsList(wordsList);
			}
		});
		
		
		
		final JScrollPane jspGroupTable = new JScrollPane(tableGroups);
		final JScrollPane jspWordsTable = new JScrollPane(tableWords);
		
		final JSplitPane jspTables = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, jspGroupTable, jspWordsTable);
		
		jspTables.setDividerLocation(600);
		jspTables.setOneTouchExpandable(true);
		
		add(jspTables, BorderLayout.CENTER);
		
		
		
		
		
		prepareWindow();
	}

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instanci JPoupMenu, které obsahuje abstraktní akce pro
	 * vytvoření, editace a smazání označeného slova v tabulce slov. Dále se toto
	 * popupMenu zobrazí v případě, že se klikne na danou tabulku pravým tlačítkem
	 * myši.
	 * 
	 * @param table
	 *            - Tabulka slov, kam se má přidat událost na kliknutí pravým
	 *            tlačítkem myši pro zobrazení výše popsaného popupMenu.
	 * 
	 * @param actionsList
	 *            - List, který obsahuje abstraktní akce pro výše uvedené operace.
	 */
	private void createPopupMenuMouseAdapterTableWords(final JTable table, final List<AbstractAction> actionsList) {
		final JPopupMenu popupMenuWords = new MyPopupMenu(actionsList);
		
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					int r = table.rowAtPoint(e.getPoint());

					if (r >= 0 && r < table.getRowCount())
						table.setRowSelectionInterval(r, r);

					int rowindex = table.getSelectedRow();

					if (rowindex < 0)
						return;

					popupMenuWords.show(table, e.getX(), e.getY());
				}
			}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		if (txtSearch.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Není zadáno vyhledávací kritérium!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		/*
		 * - Otestuji, jaka hodnota se ma vyhledat
		 * - dle toho otestuji zadaný řetězec na regulární výrazy.
		 * - Dle toho pak samotnou hodnotu / ty zkusím vyhledat.
		 */
		
		final String selectedItem = (String) cmbSearch.getSelectedItem();
		
		final String searchValue = txtSearch.getText();				

		if (selectedItem.equals(CMB_SEARCH_MODEL[0])) {
			if (searchValue.matches(REG_EX_ID)) {
				final int groupId = Integer.parseInt(searchValue.replaceAll("\\s", ""));

				groupsTableModel.setGroupsList(
						ALL_GROUPS.stream().filter(g -> g.getId() == groupId).collect(Collectors.toList()));
			} else
				JOptionPane.showMessageDialog(this, "Zadaný výraz neodpovídá syntaxi pro ID skupiny!", "Chybná syntaxe",
						JOptionPane.ERROR_MESSAGE);
		}

		else if (selectedItem.equals(CMB_SEARCH_MODEL[1])) {
			if (searchValue.matches(REG_EX_GROUP_NAME))
				groupsTableModel.setGroupsList(ALL_GROUPS.stream()
						.filter(g -> g.getGroupName().toUpperCase().contains(searchValue.toUpperCase()))
						.collect(Collectors.toList()));
			else
				JOptionPane.showMessageDialog(this, "Zadaný výraz neodpovídá syntaxi pro název skupiny!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}

		else if (selectedItem.equals(CMB_SEARCH_MODEL[2])) {
			if (searchValue.matches(REG_EX_WORD))
				wordsTableModel.setWordsList(wordsTableModel.searchWord(searchValue));
			else
				JOptionPane.showMessageDialog(this, "Zadaný výraz neodpovídá syntaxi pro hádané slovo!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}

		else if (selectedItem.equals(CMB_SEARCH_MODEL[3])) {
			if (searchValue.matches(REG_EX_WORD)) {
				wordsTableModel.setWordsList(DATA_MANAGER.getAllWords().stream()
						.filter(w -> w.getWord().toUpperCase().contains(searchValue.toUpperCase()))
						.collect(Collectors.toList()));

				tableGroups.clearSelection();
			} else
				JOptionPane.showMessageDialog(this, "Zadaný výraz neodpovídá syntaxi pro hádané slovo!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
		}
	}

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která vytvoří instance abstraktních akcí pro přidání, editace a
	 * smazání slov. Tyto akce vrátí v listu.
	 * 
	 * @return - výše popsaný list s abstraktními akcemi.
	 */
	private List<AbstractAction> createAbstractActionsForWords() {		
		createWordAction = new AbstractAction("Přidat slovo",
				GetIconInterface.getImage("/crud/CreateIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {				
				/*
				 * Zjistím, zda je vybraná nějaká skupina slov, pokud ano, načtu si ID té
				 * skupiny, a předvyplním hod pak v dialogu, pro případd, že by se jednalo o
				 * skupinu, do které chce uživatel přidat nové slovo:
				 */
				int groupId = -1;
				if (tableGroups.getSelectedRow() > -1)
					groupId = groupsTableModel.getGroupAtIndex(tableGroups.getSelectedRow()).getId();
					
				final Word newWord = new WordForm("Vytvořit", null, ALL_GROUPS, DATA_MANAGER.getAllWords(), groupId)
						.getWord();
				
				if (newWord != null) {
					final boolean created = DATA_MANAGER.addWord(newWord);
					
					if (!created) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o přidání nového slova do DB. Slovo nebylo přidánol.",
								"Chyba", JOptionPane.ERROR_MESSAGE);
						return;
					}

					/*
					 * Zde se vytvořilo nové slovo a přidalo do DB, tak pokud je označena nějaká
					 * skupina slov, tak ji znovu přenačtu tabulku slov, aby se dané slovo
					 * zobrazilo, ale pokud se jedná o nějaký filtr nebo není zvolena skupina apod.
					 * Tak to nemá smysl.
					 */
					if (groupId > -1)
						wordsTableModel.setWordsList(DATA_MANAGER.getWordsByGroupId(groupId));
				}
			}
		};
		
		
		updateWordAction = new AbstractAction("Upravit slovo",
				GetIconInterface.getImage("/crud/EditIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tableWords.getSelectedRow() > -1) {
					final Word selectedWord = wordsTableModel.getWordAtIndex(tableWords.getSelectedRow());
							
					final Word editedWord = new WordForm("Upravit", selectedWord, ALL_GROUPS, DATA_MANAGER.getAllWords(), -1)
							.getWord();
					
					if (editedWord != null) {
						final boolean created = DATA_MANAGER.updateWord(editedWord);
						
						if (!created) {
							JOptionPane.showMessageDialog(null,
									"Došlo k chybě při pokusu o aktualiuzaci slova v DB. Slovo nebylo aktualizováno.",
									"Chyba", JOptionPane.ERROR_MESSAGE);
							return;
						}

						// Přenačtu upravené slovo z DB:
						final int selectedRow = tableGroups.getSelectedRow();

						if (selectedRow > -1)
							wordsTableModel.setWordsList(DATA_MANAGER
									.getWordsByGroupId(groupsTableModel.getGroupAtIndex(selectedRow).getId()));
					}
				} else
					JOptionPane.showMessageDialog(null, "Není označeno slovo pro editaci!", "Nevybráno",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		deleteWordAction = new AbstractAction("Smazat slovo",
				GetIconInterface.getImage("/crud/DeleteIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tableWords.getSelectedRow() > -1) {
					if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, "Opravdu chce smazat slovo?",
							"Potvrdit smazání", JOptionPane.YES_NO_OPTION)) {
						final String word = wordsTableModel.getWordAtIndex(tableWords.getSelectedRow()).getWord();

						boolean deleted = DATA_MANAGER.deleteWord(word);

						if (!deleted) {
							JOptionPane.showMessageDialog(null,
									"Došlo k chybě při pokusu o smazání slova z DB. Slovo nebylo vymazáno.", "Chyba",
									JOptionPane.ERROR_MESSAGE);

							return;
						}

						// Zde bylo smazáno slovo, tak přenačtu tabulku:
						final int selectedRow = tableGroups.getSelectedRow();

						if (selectedRow > -1)
							wordsTableModel.setWordsList(DATA_MANAGER
									.getWordsByGroupId(groupsTableModel.getGroupAtIndex(selectedRow).getId()));
					}

				} else
					JOptionPane.showMessageDialog(null, "Není označeno slovo pro smazání!", "Nevybráno",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		
		
		deleteAllWordsInGroupAction = new AbstractAction("Vmyzat všechna slovíčka ve skupině",
				GetIconInterface.getImage("/icons/DeleteAllWordsIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Postup:			
				 * Zjistím, zda je označenanějaká skupina slov a pokud ano, tak vymažu všechna
				 * slovíčka v ní a přenačtu tabulku slovíček v dané skupině, vím, že by to mohlo
				 * být tak, že jen vymažu list, ale chci mít jistotu, tak přenačtu znovu ta data
				 * z DB.
				 */
				final int selectedGroup = tableGroups.getSelectedRow();
				
				if (selectedGroup > -1 && JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
						"Opravdu si přejete smazat všechna slovíčka v označené skupině?", "Smazat slovíčka",
						JOptionPane.YES_NO_OPTION)) {
					final int groupId = groupsTableModel.getGroupAtIndex(selectedGroup).getId();
					
					final boolean deleted = DATA_MANAGER.deleteWordsByGroupId(groupId);
					
					if (!deleted) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o smazání všdch slovíček v označené skupině. Slovíčka nebyla smazána!",
								"Chyba", JOptionPane.ERROR_MESSAGE);
						return;
					}				
					
					wordsTableModel.setWordsList(DATA_MANAGER.getWordsByGroupId(groupId));
				}
				
				else
					JOptionPane.showMessageDialog(null, "Není označena skupina, kde se mají vymazat všechna slovíčka!",
							"Neoznačeno", JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		
		
		return new ArrayList<>(Arrays.asList(createWordAction, updateWordAction, deleteWordAction, deleteAllWordsInGroupAction));
	}
	
	
	
	
	
	
	
	


	@Override
	protected List<AbstractAction> createAbstractActions() {
		createAction = new AbstractAction("Vytvořit skupinu",
				GetIconInterface.getImage("/crud/CreateIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

					private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {				
				final Group newGroup = new GroupForm("Vytvořit", null, ALL_GROUPS).getGroup();

				if (newGroup != null) {
					final boolean created = DATA_MANAGER.addGroup(newGroup);

					if (!created) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o přidání nové skupina do DB. Skupina nebyla vytvořena!",
								"Chyba", JOptionPane.ERROR_MESSAGE);
						return;
					}

					// Zde byla vytvořena skupina, tak ji zobrazím i v tabulce:
					ALL_GROUPS = DATA_MANAGER.getAllGroups();
					groupsTableModel.setGroupsList(ALL_GROUPS);
				}
			}
		};

		
		updateAction = new AbstractAction("Upravit skupinu",
				GetIconInterface.getImage("/crud/EditIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

					private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				final int selectedRow = tableGroups.getSelectedRow();

				if (selectedRow > -1) {
					final Group editedGroup = new GroupForm("Upravit", groupsTableModel.getGroupAtIndex(selectedRow),
							ALL_GROUPS).getGroup();

					if (editedGroup != null) {
						final boolean edited = DATA_MANAGER.updateGroup(editedGroup);

						if (!edited) {
							JOptionPane
									.showMessageDialog(null,
											"Došlo k chybě při pokusu o aktualizaci skupiny '" + editedGroup
													+ "'. Skupina nebyla aktualizována!",
											"Chyba", JOptionPane.ERROR_MESSAGE);
							return;
						}

						// Zde byla aktualizována skupina, tak ji zobrazím i v tabulce:
						ALL_GROUPS = DATA_MANAGER.getAllGroups();
						groupsTableModel.setGroupsList(ALL_GROUPS);
					}
				} else
					JOptionPane.showMessageDialog(null, "Není označena skupina pro editaci!", "Neoznačena skupina",
							JOptionPane.ERROR_MESSAGE);
			}
		};

		
		deleteAction = new AbstractAction("Smazat skupinu",
				GetIconInterface.getImage("/crud/DeleteIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

					private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Nejpreve smažu veškerá slova v dané skupině, potom samotnou skupinu:
				 */

				final int selectedRow = tableGroups.getSelectedRow();

				if (selectedRow > -1) {
					if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, "Opravdu chcete smazat skupinu?",
							"Potvrzení", JOptionPane.YES_NO_OPTION)) {
						// Získám si skupinu pro smazání:
						final Group group = groupsTableModel.getGroupAtIndex(selectedRow);
						
						// Nejprve vymažu všechna slova v dané skupině:
						DATA_MANAGER.getAllWords().stream().filter(w -> w.getGroupId() == group.getId())
								.forEach(w -> DATA_MANAGER.deleteWord(w.getWord()));

						// Smažu danou skupinu:
						final boolean deleted = DATA_MANAGER.deleteGroup(group.getId());

						// Pro případ, že by se to nepovedlo, oznámím to uživateli:
						if (!deleted) {
							JOptionPane
									.showMessageDialog(null,
											"Došlo k chybě při pokusu o smazání skupiny s názvem '"
													+ group.getGroupName() + "'. Skupina nebyla smazána!",
											"Chyba", JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						// Zde byla smazána skupina, tak přenačtu list se skupinami
						ALL_GROUPS = DATA_MANAGER.getAllGroups();
						groupsTableModel.setGroupsList(ALL_GROUPS);

						// Odznačím řádky v tabulce skupin:
						tableGroups.clearSelection();
						// Vymažu list slov - bude to "prázdná tabulka"
						wordsTableModel.setWordsList(new ArrayList<Word>());
					}
				} else
					JOptionPane.showMessageDialog(null, "Není označena skupina pro smazání!", "Neoznačena skupina",
							JOptionPane.ERROR_MESSAGE);
			}
		};
		
		
		
		
		
		deleteAllGroupsAction = new AbstractAction("Vymazat všechny skupiny",
				GetIconInterface.getImage("/icons/DeleteAllGroupsIcon.png", KindOfPicture.TOOLBAR_OR_MENU)) {

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Vymažou se všechny skupiny včetně jejich slovíček.
				 * 
				 * Postup:
				 * Zde nemusím řešit, procházení skupina a dle toho mazat slovíčka apod. Stačí
				 * prostě vymazat všechny slovíčka, pak všechny skupiny (v tomto pořadí).
				 */
				
				if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
						"Opravdu si přejete smazat všechny skupiny včetně jejich slovíček?", "Smazat skupiny",
						JOptionPane.YES_NO_OPTION)) {
					final boolean deletedAllWords = DATA_MANAGER.deleteAllWords();

					if (!deletedAllWords) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o smazání všech slovíček ze všech skupin slov. Slovíčka nebyla vymazána!",
								"Chyba", JOptionPane.ERROR_MESSAGE);
						return;
					}

					final boolean deletedGroups = DATA_MANAGER.deleteAllGroups();

					if (!deletedGroups) {
						JOptionPane.showMessageDialog(null,
								"Došlo k chybě při pokusu o smazání všech skupin slov. Skupiny nebyly vymazány.",
								"Chyba", JOptionPane.ERROR_MESSAGE);
						return;
					}

					/*
					 * I když vím, že budou všechna data prázdná, tak pro jistotu přenačtu data z
					 * DB, aby to byla jistot, kdyby se z DB nějaká slovíčka načetla, bylo by něco
					 * špatně.
					 */
					ALL_GROUPS = DATA_MANAGER.getAllGroups();
					groupsTableModel.setGroupsList(ALL_GROUPS);

					// Nastavím prázdný list do slovíček:
					wordsTableModel.setWordsList(new ArrayList<Word>());
				}		
			}
		};
		
		

		return new ArrayList<>(Arrays.asList(createAction, updateAction, deleteAction, deleteAllGroupsAction));
	}
}
