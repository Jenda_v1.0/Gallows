package kruncik.jan.gallows.dialogs.groups_wordsForm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako formulář pro vytvoření a editaci skupiny slov (Group).
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class GroupForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;

	
	
	private final JTextField txtGroupname;
	
	private final JTextArea txaComment;
	
	
	
	/**
	 * List, který obsahuje veškeré skupiny v DB. Je zde potřeba, abych zjistil, zda
	 * se ještě v DB nenachází zadaný název skupiny.
	 */
	private final List<Group> allGroups;
	
	
	
	
	/**
	 * Proměnná, do které se vloží hodnota pro vrácení Group z tohoto dialogu - buď
	 * to bude nová instance, nebo upravená, nebo null hodnota, pokud se uživatel
	 * rozhodl zavřít dialog.
	 */
	private Group group;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param title
	 *            - Titulek dialogu - buď Vytvořit nebo Upravit.
	 * 
	 * @param group
	 *            - Buď null hodnota, pokud se má vytvořit nová skupina nebo nějaká
	 *            skupina, která se má upravit.
	 * 
	 * @param allGroups
	 *            - List se včemi skupina v DB, abych mohl zabránit tomu, aby se
	 *            nenastavil duplicitní název skuipny.
	 */
	GroupForm(final String title, final Group group, final List<Group> allGroups) {
		super(title);
		
		this.group = group;
		this.allGroups = allGroups;
		
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 350, "/icons/GroupFormIcon.png", true);
		
		addWindowListener(new MyWindowAdapter());
		
		
		final GridBagConstraints gbc = createGbc();
		
		
		
		add(new JLabel("*Název skupiny: "), gbc);
		
		createKeyAdapter();
		txtGroupname = new JTextField(20);
		txtGroupname.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 0);
		add(txtGroupname, gbc);
		
		
		
		
		
		
		setGbc(gbc, 0, 2);
		add(new JLabel("Komentář: "), gbc);
				
		txaComment = new JTextArea();
		final JScrollPane jspComment = new JScrollPane(txaComment);
		jspComment.setPreferredSize(new Dimension(0, 150));
		setGbc(gbc, 1, 2);
		add(jspComment, gbc);
		
		
		
		
		// Naplnění dat:
		if (group != null) {
			txtGroupname.setText(group.getGroupName());
			txaComment.setText(group.getComment());
		}
		
		
		
		
		
		
		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText(title);
		btnConfirm.addActionListener(Event -> checkData());
		
		btnCancel.addActionListener(Event -> {
			this.group = null;
			dispose();
		});
		
				
		setGbc(gbc, 0, 3);
		gbc.gridwidth = 2;
		add(pnlButtons, gbc);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí tento dialog / formulář, který se pak stane modálním a
	 * po zavření dialogu vrátí hodnotu, která se bude nacházet v proměnné group,
	 * buď tam bude null hodnota nebo nově vytvořená nebo upravená skupina.
	 * 
	 * @return - výče popsanou hodnotu v proměnné group.
	 */
	public Group getGroup() {
		prepareWindow();
		
		return group;
	}










	@Override
	protected void checkData() {
		/*
		 * - Zda není název skupiny prázdný
		 * - Zda odpovídá regulárnímu výrazu
		 * - Zda odpovídá počtu znaků
		 * - Pokud není komentíř prázdný, tak otestovat jeho délku
		 * - Otestovat, zda název skupiny ještě neexistuje
		 * - Pak buď vytvořit novou skupinu nebo nasetovat nové hodnoty do té, co se sem předala.
		 */
		
		if (txtGroupname.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Pole pro název skupiny je prázdné!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!txtGroupname.getText().matches(REG_EX_GROUP_NAME)) {
			JOptionPane.showMessageDialog(this, "Název skupiny neodpovídá požadované syntaxi!", "Chybná syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (txtGroupname.getText().length() > 50) {
			JOptionPane.showMessageDialog(this, "Název skupiny překročil limit počtu znaků!", "Velký počet znaků",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!txaComment.getText().isEmpty() && txaComment.getText().length() > 255) {
			JOptionPane.showMessageDialog(this, "Komentář překročil limit počtu povolených znaků!",
					"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		

		
		/*
		 * Do následující proměnné si uložím logickou hodnotu o tom, zda je již zadaný
		 * název skupiny využit nebo ne.
		 * 
		 * Ale musím si hlídat, abych netestoval tu samou skupinu při editaci skuipny.
		 */
		final boolean existGroupName;

		if (group != null)
			existGroupName = allGroups.stream().anyMatch(
					g -> g.getGroupName().equalsIgnoreCase(txtGroupname.getText()) && g.getId() != group.getId());

		else
			existGroupName = allGroups.stream()
					.anyMatch(g -> g.getGroupName().equalsIgnoreCase(txtGroupname.getText()));

		
		if (existGroupName) {
			JOptionPane.showMessageDialog(this, "Zadaný název skupiny je již využit, vyberte prosím jiný!!",
					"Duplicitní název", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		/*
		 * Pokud bude group null, pak je třeba vytvořit novou skupinu, pokud není null,
		 * pak se jedná o editaci skupiny, pak do ní puze vložím hodnoty z formuláře:
		 */
		if (group != null) {
			group.setGroupName(txtGroupname.getText());
			group.setComment(txaComment.getText());
		}

		else group = new Group(txtGroupname.getText(), txaComment.getText());
		
		
		
		dispose();
	}
	
	
	
	

	
	
	/**
	 * Tato třída slouží pouze jako listener pro zavření okna, kdy nastaví proměnnou
	 * group na null.
	 * 
	 * @author jan
	 *
	 */
	private class MyWindowAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			/*
			 * Pokud se zavře tento dialog například kliknutím na kříek, tak nastavím
			 * proměnnou group na null, aby se nevrátila nějaká nechtěnná hodnota.
			 */
			group = null;
		}
	}
}
