package kruncik.jan.gallows.dialogs.groups_wordsForm;

/**
 * Tato třída slouží pouze jako objekt pro vložení do JcomboBoxu ve formuláři
 * WordForm.
 * 
 * Aby se v tom JComboBoxu přehledně uživatel vybral skupinu, kam se má
 * přesunout nebo přidat slovo.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class WordCmbInfo {

	private final int groupId;
	
	private final String groupName;
	
	
	WordCmbInfo(final int groupId, final String groupName) {
		super();
		
		this.groupId = groupId;
		this.groupName = groupName;
	}
	
	
	
	int getGroupId() {
		return groupId;
	}
	
	
	public String getGroupName() {
		return groupName;
	}
	
	
	
	@Override
	public String toString() {
		return groupId + " - " + groupName;
	}
}
