package kruncik.jan.gallows.dialogs.groups_wordsForm;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import kruncik.jan.gallows.db_objects.Word;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako table model pro tabulku tableWords, která bude
 * obsahovat veškerá slova v DB, ze zvolené skupiny slov v tabulce tableGroups.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class WordsTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = 1L;

	

	/**
	 * List, který obsahuje veškerá slova v tabulce tableWords.
	 */
	private List<Word> wordsList = new ArrayList<>();
	
	
	/*
	 * Proměnná typu jednorozměrné pole, které obsahuje názvy sloupců pro tabulku.
	 */
	private final String[] columns;
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param columns
	 *            - Jednorozměrné pole, které obsahuje názvy sloupců pro tabulku.
	 */
	WordsTableModel(final String[] columns) {
		super();
		
		this.columns = columns;
	}
	
	
	/**
	 * Setr na list s daty v tabulce.
	 * 
	 * @param wordsList
	 *            - list se slovy, která se nachází ve zvolené skupině slov.
	 */
	void setWordsList(List<Word> wordsList) {
		this.wordsList = wordsList;
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která vrátí slovo (word) na zadaném indexu v listu.
	 * 
	 * @param index
	 *            - index slova v listu (v tabulce)
	 * @return slovo na zadaném indexu v listu (v tabulce)
	 */
	Word getWordAtIndex(final int index) {
		return wordsList.get(index);
	}
	
	
	
	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return wordsList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return wordsList.get(rowIndex).getWord();
			
		case 1:
			return wordsList.get(rowIndex).getComment();

		default:
			break;
		}
		
		return "?";
	}


	
	
	
	public String getColumnName(int column) {
		return columns[column];
	}
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object objValue = getValueAt(0, columnIndex);

		if (objValue != null)
			return objValue.getClass();

		return super.getColumnClass(columnIndex);
	}
	
	
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		final Word word = wordsList.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			/*
			 * Nejprve otestuji, aby nově zadané slovo již nebylo používané, například v
			 * jiné skupině apod.
			 */
			final String newWord = (String) value;
			
			
			// Nejprve ještě otestuji, zda zadané slovo splňuje syntaxi dle
			// reguálrního výrazu:
			if (!newWord.matches(SourceDialog.REG_EX_WORD)) {
				JOptionPane.showMessageDialog(null, "Zadané slovo nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}

			// Prázdný text:
			if (newWord.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Pole pro slovo nesmí být prázdné!", "Prázdné pole",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			// delka řetězce:
			if (newWord.length() > 50) {
				JOptionPane.showMessageDialog(null, "Zadané slovo přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			
			final boolean existWord = GroupWordsForm.DATA_MANAGER.getAllWords().stream()
					.anyMatch(w -> w.getWord().equalsIgnoreCase(newWord));
			
			
			if (!existWord)
				word.setNewWord(newWord);
			else
				JOptionPane.showMessageDialog(null, "Zadané slovo již existuje, zvolte prosím jíné!",
						"Duplicitní hodnota", JOptionPane.ERROR_MESSAGE);
			break;
			
		case 1:
			word.setComment((String) value);
			break;

		default:
			break;
		}
		
		
		final boolean updated = saveChangedToDb(word);
		
		/*
		 * Musím zde otestovat, zda se provedly změny nebo ne.
		 * 
		 * Pokud ano, pak potřebuji znovu přenačíst hodnoty (slova) v listu wordsList,
		 * nebo alespoň toto jedno, které se zaktualizovalo, Prootože se přepisuje
		 * proměnná v daném objektu, tak ji musím znovu načíst s DB, aby se projevily
		 * zmeny v tabulce (pouze kvůli tomu, ze se pri aktualizaci slova dava nove
		 * slovo do jine promenne, nez ktera se zobrazuje v tabulce).
		 */
		if (updated && word.getNewWord() != null) {
			reloadActualWords(word);
			fireTableDataChanged();
		}
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která přenačte slovo v listu
	 * 
	 * @param word
	 *            - původní slovo v listu, které se má přenačíst - aktualizovat.
	 */
	private void reloadActualWords(final Word word) {
		final Word updatedWord = GroupWordsForm.DATA_MANAGER.getWord(word.getNewWord());
		
		word.setWord(updatedWord.getWord());
		word.setNewWord(updatedWord.getNewWord());
		word.setComment(updatedWord.getComment());
	}
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zapíše provedené změny do příslušné tabulky -> do příslušného
	 * řádku v tabulce.
	 * 
	 * @param word
	 *            - Slovo pro hádání, které bylo změněno, a které se má aktualizovat
	 *            v DB.
	 * @return vrátí se logická hodnota o tom, zda se povedlo aktulizovat slovo v DB
	 *         nebo ne.
	 */
	private static boolean saveChangedToDb(final Word word) {
		final boolean updated = GroupWordsForm.DATA_MANAGER.updateWord(word);

		if (!updated)
			JOptionPane.showMessageDialog(null,
					"Došlo k chybě při pokusu o aktualizaci slova '" + word.getWord() + "'. Změny nebyly uloženy.",
					"Nastala chyba", JOptionPane.ERROR_MESSAGE);
		
		return updated;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Meotda, která vyhledá v právě zobrezeném listu slov v tabulce slovo, které se
	 * podobná (ignorují se velikosti písmen) slovu v parametru metody (word)
	 * 
	 * @param word
	 *            - Slovo, které se vyhledá v aktuálně zobrazeném listu slov.
	 * @return vrátí se list s nalezenými slovy.
	 */
	final List<Word> searchWord(final String word) {
		return wordsList.stream().filter(w -> w.getWord().toUpperCase().contains(word.toUpperCase()))
				.collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí slova v listu dle slov abecedně - sestupně nebo
	 * vzestupně dle asc hodnoty.
	 * 
	 * @param asc
	 *            - logická proměnná, která značí, zda se mají hodnoty seřadit dle
	 *            slova sestupně nebo vzestupně. True = vzestupně, false = sestupně.
	 */
	final void sortByWord(final boolean asc) {
		if (asc)
			wordsList.sort((a, b) -> a.getWord().compareToIgnoreCase(b.getWord()));
		else
			wordsList.sort((a, b) -> b.getWord().compareToIgnoreCase(a.getWord()));
		
		fireTableDataChanged();
	}
}
