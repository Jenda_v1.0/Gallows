package kruncik.jan.gallows.dialogs.groups_wordsForm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.db_objects.Word;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako formulář pro vytvoření a editace slova v DB.
 * 
 * Ve formuláři je třeba označit ID skupiny, do které se má slovo přidat, dané
 * slovo a volitelný komentář ke slovu.
 * 
 * Nové slovo ještě nesmí v DB existovat.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class WordForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Komponenta, ve které se vybere skupina, kam se má přidat / přesuout slovo.
	 */
	private final JComboBox<WordCmbInfo> cmbGroup;
		
	private final JTextField txtWord;
	
	private final JTextArea txtComment;
	
	
	/**
	 * List, který obsahuje veškerá slova v DB.
	 */
	private final List<Word> allWords;
	
	
	
	/**
	 * Tato proměnná se vrátí v metodě getWord, buď bude null nebo bude obsahovat
	 * vytvořené nebo upravené slovo.
	 */
	private Word word;
	
	
	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param title
	 *            - titulek dialogu a tlačítka pro potvrzení (Vytvořit nebo
	 *            Upravit).
	 * 
	 * @param word
	 *            - proměná, která bude naplněna pouze v případě, že se jedná o
	 *            upravení slova. Pokud se jedná o vytvoření, pak bude tato proměnná
	 *            null.
	 * 
	 * @param allGroups
	 *            - List, který obsahuje všechny skupiny slov - pro označení
	 *            skuipny, kam se má nové slovo přidat nebo přesunout
	 * 
	 * @param allWords
	 *            - list, který obsahuje veškerá slova, aby se otestovalo, zda nové
	 *            slovo ještě v DB neexistuje.
	 * 
	 * @param selectedGrouId
	 *            - Jedná se o index skupiny slov, předá se pouze v případě, že je
	 *            zvolena nějaká skupina v tabulce, tak se v tomto dialogu
	 *            předvyplní.
	 */
	WordForm(final String title, final Word word, final List<Group> allGroups, final List<Word> allWords,
			final int selectedGrouId) {
		super(title);

		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 500, 500, "/icons/WordFormIcon.png", true);

		
		addWindowFocusListener(new MyWindowAdapter());
		
		this.allWords = allWords;
		this.word = word;
		
		final GridBagConstraints gbc = createGbc();
		
		
		
		add(new JLabel("*Skupina: "), gbc);
		
		
		final WordCmbInfo[] cmbGroupModel = getModelForCmb(allGroups);
		cmbGroup = new JComboBox<>(cmbGroupModel);
		setGbc(gbc, 1, 0);
		add(cmbGroup, gbc);
		
		
		
		
		setGbc(gbc, 0, 1);
		add(new JLabel("*Slovo: "), gbc);
		
		createKeyAdapter();
		txtWord = new JTextField(20);
		txtWord.addKeyListener(keyAdapter);
		setGbc(gbc, 1, 1);
		add(txtWord, gbc);		
		
		
		
		
		
		
		setGbc(gbc, 0, 2);
		add(new JLabel("Komentář"), gbc);
				
		txtComment = new JTextArea();
		final JScrollPane jspComment = new JScrollPane(txtComment);
		jspComment.setPreferredSize(new Dimension(0, 150));
		setGbc(gbc, 1, 2);
		add(jspComment, gbc);	
		
		
		
		
		
		
		
		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText(title);
		btnConfirm.addActionListener(Event -> checkData());
		
		btnCancel.addActionListener(Event -> {
			this.word = null;
			dispose();
		});
		
		
		
		
		// Potenciální naplněnní hodnot ve formuláři.
		if (word != null) {
			for (int i = 0; i < cmbGroupModel.length; i++)
				if (word.getGroupId() == cmbGroupModel[i].getGroupId()) {
					cmbGroup.setSelectedIndex(i);
					break;
				}
			
			txtWord.setText(word.getWord());
			txtComment.setText(word.getComment());			
		}
		
		
		/*
		 * Zde se jedná o označenou skupinu slov v tabulce, tak zde označím danou
		 * skupinu.
		 */
		else if (selectedGrouId > -1) {
			for (int i = 0; i < cmbGroupModel.length; i++)
				if (cmbGroupModel[i].getGroupId() == selectedGrouId) {
					cmbGroup.setSelectedIndex(i);
					break;
				}
		}
		
		
		
		
		
		setGbc(gbc, 0, 3);
		gbc.gridwidth = 2;
		add(pnlButtons, gbc);
	}

	
	
	
	
	
	
	
	
	/**
	 * Metoda, která zobrazí tento formulář a až se klikne uzavře tento dialog, tak
	 * se vrátí hodnota v proměnné word. Buď bude null nebo bude obsahovat nově
	 * vytvořené nebo upravené slovo v tomto formuláři.
	 * 
	 * @return - Null hodnotu, pokud se tento formulář zavře bez potvrzení. Jinak
	 *         Nově vytvořené nebo upravené slovo.
	 */
	public Word getWord() {
		prepareWindow();
		
		return word;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která převede list všech skupin do pole typu WordCmbInfo, aby bylo
	 * možné jej zobrazit v JComboBoxu.
	 * 
	 * @param allGroups
	 *            - list se všemi skupinami.
	 * 
	 * @return jednorozměrné pole typu WordCmbInfo, které slouží jako model pro
	 *         JComboBoxu pro výběr skupiny.
	 */
	private static WordCmbInfo[] getModelForCmb(final List<Group> allGroups) {
		final WordCmbInfo[] arrayModel = new WordCmbInfo[allGroups.size()];
		
		for (int i = 0; i < allGroups.size(); i++) {
			final Group group = allGroups.get(i);
			
			arrayModel[i] = new WordCmbInfo(group.getId(), group.getGroupName());
		}
		
		return arrayModel;
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * - Zjistí se, zda není prázdné pole pro slovo.
		 * - Zda je splněna maximální délka slova.
		 * - Zda splňuje syntaxi dle regulárního výrazu.
		 * - Otestuji, zda zadané slovo ještě neexistuje v DB.
		 * - Pokud jsou podmínky splněny, pak se nastaví proměnná word novým nebo upraveným slovem
		 */
		if (txtWord.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Pole pro slovo je prázdné!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		if (!txtComment.getText().isEmpty() && txtComment.getText().length() > 255) {
			JOptionPane.showMessageDialog(this, "Pole pro Komentář obsahuje delší počet znaků, než je povoleno!",
					"PPřekročen limit", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		
		final String wordFromTextField = txtWord.getText();
		
		if (wordFromTextField.length() > 50) {
			JOptionPane.showMessageDialog(this, "Zadané slovo obsahuje větší počet znaků, než je povoleno!",
					"Překročen počet znaků", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		if (!wordFromTextField.matches(REG_EX_WORD)) {
			JOptionPane.showMessageDialog(this, "Zadané slovo nesplňuje požadovanou syntaxi!", "Chyba syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
				
		
		
		// Zjistím, zda zadané slovo ještě není v DB:
		final boolean existWordInDb = allWords.stream().anyMatch(w -> w.getWord().equalsIgnoreCase(wordFromTextField));
		
		/*
		 * Zde ještě potřebuji otestovat, zda se jedná o upravení nebo vytvořené nového
		 * slova.
		 * 
		 * Pokud se bude vytvářet nové slovo, pak bude word null a v případě, že se
		 * slovo již našlo v DB dojde k oznámení, že si má uživatel napsat jiné slovo,
		 * už jednou v DB je.
		 * 
		 * Ale pokud se bude jednat o editaci slova, může uživatel chtít například jen
		 * přeřadit slovo do jiné skupiny, pak to slovo bude stejné, tak pokud se bude
		 * jedna to editaci, word už eebude null, tak nemusím testovat to samé slovo a
		 * půjde změnit v DB.
		 */
		
		if (existWordInDb && word == null) {
			JOptionPane.showMessageDialog(this, "Zadané slovo již existuje, zvolte prosím jiné!", "Duplicitní slovo",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		
		/*
		 * Když bude proměnná word null, pak musím vytvořit novou instanci, protože se jedná o vytvoření nového slova,
		 * ale pkud už něco bude obsahovat, pak do ní pouze vložím nové hodnoty.
		 */

		final WordCmbInfo cmbInfo = (WordCmbInfo) cmbGroup.getSelectedItem();
		
		if (word != null) {
			// Zde pouze změním hodnoty	
			word.setGroupId(cmbInfo.getGroupId());
			word.setNewWord(wordFromTextField);
			word.setComment(txtComment.getText());
		}
		
		
			// Zde vytvořím novou instanci a naplním její hodnoty:
		else word = new Word(txtComment.getText(), wordFromTextField, cmbInfo.getGroupId());

		
		dispose();
	}
	
	
	
	
	
	
	
	
	/**
	 * Tato třída sloužíá pouze pro to, aby nastavila proměnnou word při zavření
	 * tohoto dialogu na null, aby se nevrátila nějaká nechtěnná hodnota.
	 * 
	 * @author Jan Krunčík
	 * @version 1.0
	 */
	private class MyWindowAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			/*
			 * Tato metoda se zavolá, když uživatel klikně na křížek pro zavření dialogu.
			 * V takovém případě se má zavřít tento dialog bez vracení hodnoty, resp. 
			 * má se vrátit null hodnota.
			 */
			word = null;
		}
	}
}
