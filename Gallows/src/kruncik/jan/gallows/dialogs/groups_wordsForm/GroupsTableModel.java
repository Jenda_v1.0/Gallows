package kruncik.jan.gallows.dialogs.groups_wordsForm;

import java.util.Comparator;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import kruncik.jan.gallows.db_objects.Group;
import kruncik.jan.gallows.dialogs.SourceDialog;

/**
 * Tato třída slouží jako table model pro tabulku, kde jsou reprezentovány
 * skupiny slov.
 * 
 * Pokud se na danou tabulku - na konkrétní řádek klikne, pak se v druhé tabulce
 * zobrazí veškerá slova ve zvolené skupině slov.
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class GroupsTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * List, který obsahuje hodnoty do tabulky.
	 */
	private List<Group> groupsList;
	
	/**
	 * Jednorozměrné pole, které obsahuje názvy sloupců.
	 */
	private final String[] columns;
	
	
	GroupsTableModel(final String[] columns) {
		super();
		
		this.columns = columns;
	}
	
	
	/**
	 * Setr na list s hodnotami do tabulky.
	 * 
	 * @param groupsList
	 *            - list typu Group, který obsahuje hodnoty do tabulky (skupiny
	 *            slov).
	 */
	void setGroupsList(List<Group> groupsList) {
		this.groupsList = groupsList;
		
		fireTableDataChanged();
	}
	
	
	/**
	 * Metoda, která vrátí skupinu slov (Group) na zadaném indexu v listu.
	 * 
	 * @param index
	 *            - index, kde se nachází nějaká skupina slov.
	 * @return Group na výše zadaném indexu.
	 */
	Group getGroupAtIndex(final int index) {
		return groupsList.get(index);
	}
	
	
	
	
	@Override
	public int getColumnCount() {		
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return groupsList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return groupsList.get(rowIndex).getId();
			
		case 1:			
			return groupsList.get(rowIndex).getGroupName();
			
		case 2:
			return groupsList.get(rowIndex).getComment();

		default:
			break;
		}
		return "?";
	}

	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		final Object objValue = getValueAt(0, columnIndex);

		if (objValue != null)
			return objValue.getClass();

		return super.getColumnClass(columnIndex);
	}
	
	
	
	@Override
	public String getColumnName(int columnIndex) {
		return columns[columnIndex];
	}
	
	
	
	
	
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex > 0;
	}
	
	
	
	
	
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final Group group = groupsList.get(rowIndex); 
		
		switch (columnIndex) {
		case 1:
//			Nejprve otestuji, zda nově zadanýnázev skupiny není již využit:
			final String newGroupName = (String) aValue;
			
			// Nejprve ještě otestuji, zda zadaný název skupiny splňuje syntaxi dle
			// reguálrního výrazu:
			if (!newGroupName.matches(SourceDialog.REG_EX_GROUP_NAME)) {
				JOptionPane.showMessageDialog(null, "Zadaný název skupiny nesplňuje podmínky pro správnou syntaxi!",
						"Chybná syntaxe", JOptionPane.ERROR_MESSAGE);
				return;
			}

			// Prázdný text:
			if (newGroupName.replaceAll("\\s", "").isEmpty()) {
				JOptionPane.showMessageDialog(null, "Pole pro název skupiny nesmí být prázdné!", "Prázdné pole",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			// delka řetězce:
			if (newGroupName.length() > 50) {
				JOptionPane.showMessageDialog(null, "Zadaný název skupiny přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			
			final boolean existGroupName = GroupWordsForm.DATA_MANAGER.getAllGroups().stream()
					.anyMatch(g -> g.getGroupName().equalsIgnoreCase(newGroupName) && g.getId() != group.getId()); 
			
			
			if (!existGroupName)
				group.setGroupName(newGroupName);
			else
				JOptionPane.showMessageDialog(null, "Zadaný název skupiny již existuje, zvolte prosím jiný!",
						"Duplicitní hodnota", JOptionPane.ERROR_MESSAGE);
			break;
			
		case 2:
			final String comment = (String) aValue;
			
			// delka řetězce:
			if (comment.length() > 255) {
				JOptionPane.showMessageDialog(null, "Zadaný komentář přesahuje povolený počet znaků!",
						"Překročen limit počtu znaků", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			group.setComment(comment);

			break;

		default:
			break;
		}
		
		saveChangedToDb(group);
	}
	
	
	
	
	
	
	/**
	 * Metoda, která zapíše provedené změny do příslušné tabulky -> do příslušného
	 * řádku v tabulce.
	 * 
	 * @param group
	 *            - Skupina slov, ve které došlo k nějaké změně a která se má
	 *            aktualizovat v DB.
	 */
	private static void saveChangedToDb(final Group group) {
		final boolean updated = GroupWordsForm.DATA_MANAGER.updateGroup(group);

		if (!updated)
			JOptionPane.showMessageDialog(null, "Došlo k chybě při pokusu o aktualizaci skupiny s názvem '"
					+ group.getGroupName() + "'. Změny nebyly uloženy.", "Nastala chyba", JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která seřadí skupiny dle jejich ID sestupně nebo vzestupně dle
	 * hodnoty asc.
	 * 
	 * @param asc
	 *            - logcická proměnná. Pokud bude obsahovat hodnotu true, hodnoty se
	 *            seřadí dle ID vzestupně, pokud false, hodnoty se seřadí sestupně.
	 */
	final void sortByID(final boolean asc) {
		if (asc)
			groupsList.sort(Comparator.comparingInt(Group::getId));
		else
			groupsList.sort((a, b) -> Integer.compare(b.getId(), a.getId()));

		fireTableDataChanged();
	}
	
	
	
	/**
	 * Metoda, která seřadí skupiny dle názvu skuipn sestupně nebo vzestupně dle
	 * hodnoty asc.
	 * 
	 * @param asc
	 *            - logcická proměnná. Pokud bude obsahovat hodnotu true, hodnoty se
	 *            seřadí dle názvu skupin vzestupně, pokud false, hodnoty se seřadí
	 *            sestupně.
	 */
	final void sortByGroupName(final boolean asc) {
		if (asc)
			groupsList.sort((a, b) -> a.getGroupName().compareToIgnoreCase(b.getGroupName()));
		else
			groupsList.sort((a, b) -> b.getGroupName().compareToIgnoreCase(a.getGroupName()));

		fireTableDataChanged();
	}
}
