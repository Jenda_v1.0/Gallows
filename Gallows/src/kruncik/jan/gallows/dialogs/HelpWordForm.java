package kruncik.jan.gallows.dialogs;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import kruncik.jan.gallows.db_objects.Word;

/**
 * Tento dialog slouží jako okno s nápovědou pro hádané slovo
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class HelpWordForm extends SourceDialog {

	
	private static final long serialVersionUID = 1L;

	
	/**
	 * Konstruktor této třídy.
	 * 
	 * @param word
	 *            - hádané slovo, ke kterému se má zobrazit nápověda.
	 */
	public HelpWordForm(final Word word) {
		super("Nápověda");
		
		initGui(DISPOSE_ON_CLOSE, new BorderLayout(), 710, 180, "/gameIcons/HelpIcon.png", false);

		
		final JLabel lblHelp;		
		
		if (word.getComment() != null && !word.getComment().isEmpty())
			lblHelp = new JLabel("Nápověda: " + word.getComment());
		
		else lblHelp = new JLabel("Nápověda není k dispozici."); 
		
		
		
		lblHelp.setHorizontalAlignment(JLabel.CENTER);
		
		add(lblHelp, BorderLayout.CENTER);
		
		
		
		
		prepareWindow();
	}
	
	
	
	
	
	
	@Override
	protected void checkData() {
	}
}
