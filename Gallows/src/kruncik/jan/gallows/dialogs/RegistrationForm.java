package kruncik.jan.gallows.dialogs;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import kruncik.jan.gallows.app.GetIconInterface;
import kruncik.jan.gallows.db.DataManager;
import kruncik.jan.gallows.db.DataManagerImpl;
import kruncik.jan.gallows.db_objects.User;
import kruncik.jan.gallows.dialogs.dataForDialogs.MyFileChooser;

/**
 * Tento dialog slouží jako formulář pro registraci nového uživatele. V
 * konstruktoru se předává jako parametr logická hodnota, zda se tento dialog
 * volá tak, aby bylo na výběr, že se může vytvořit admin nebo uživatel apod. a
 * zda má být účet blokován nebo ne. Tuto možnost má pouze admin
 * 
 * 
 * @author Jan Krunčík
 * @version 1.0
 */

public class RegistrationForm extends SourceDialog {
	
	private static final long serialVersionUID = 1L;


	/**
	 * Proměnná pro manipulaci s daty v DB
	 */
	private static final DataManagerImpl DATA_MANAGER = new DataManager();
	
	
	
	
	/**
	 * Regulární výraz pro adresu,
	 * musí být zadáno alespon název ulice, pak může a nemusí být číslo popisné
	 */
	private static final String REG_EX_STREET = "^[a-zA-ZěščřžýáíéúůňóťĚŠČŘŽÝÁÍÉÚŮŇÓŤ ]+\\s+(\\s*\\d?)+$";
	
	
	
	private JTextField txtFirstName, txtLastName, txtPseudonym, txtStreet, txtEmail, txtTelephoneNumber;
	private JPasswordField txtPassword_1, txtPassword_2;
	private JComboBox<String> cmbKindOfUser, cmbBan, cmbPsc;
	private JButton btnPicture;
	private File filePicture;
	/**
	 * Label pro zobrazení obrázku:
	 */
	private JLabel lblImage;
	
	/**
	 * Label, ve kterém bude zobrazena cesta k obrázku, pokud jej uživtel vybere.
	 */
	private JLabel lblPath;
	
	
	
	
	/**
	 * Proměnná, ve které bude uložena hodnota true, pokud bude přihlášen admin a
	 * mají se tak zobrazit možnosti pro výběr typu uživatele a jeho zablokování.
	 */
	private final boolean showOptionsForCreateAdmin;
	
	
	
	
	/**
	 * Proměnná, která se naplní puze v případě, že se jedná o editaci uživatele.
	 */
	private User user;
	
	
	
	
	/**
	 * Konstruktor této třídy, Obsahuje parametr, dle kterého se pozná, zda se má do
	 * formuláře přidat možnost pro nastavení, jaký typ uživatele se má vytvořit.
	 * 
	 * @param showOptionsForCreateAdmin
	 *            - Logická hodnota, pokud bude true, přidá se do dialogu komponenta
	 *            pro definici toho, jaký typ uživatele se má vytvořit
	 * 
	 * @param showWindow
	 *            - logická proměnná o tom, zda se má zobrzit okno ne ne, pokud se
	 *            jendá o editaci uživatele, bude tato proměnná na false a zobrazí
	 *            se až v metodě getEditedUser.
	 */
	public RegistrationForm(final boolean showOptionsForCreateAdmin, final boolean showWindow) {
		super("Registrace");
		initGui(DISPOSE_ON_CLOSE, new GridBagLayout(), 520, 700, "/icons/RegistrationForm.png", true);
		
		this.showOptionsForCreateAdmin = showOptionsForCreateAdmin;
		
		addWindowListener(new MyWindowAdapter());
		
		loadValues();
		
		createKeyAdapter();
		
		/*
		 * Proměnná, která obsahuje index řádku, kam se má komponenta vložit, je zde
		 * proto, že potřebuji různé indexy pro vložení komponenta pro zablokování účtu
		 * a typu účtu pokud je přihlášen admin a nebo tyto komponenty v dialogu
		 * nebudou, pokud se registruje běžný uživatel, pak bude jiný počet řádků.
		 */
		int indexOfRow = 1;
		
		
		final GridBagConstraints gbc = createGbc();
		
		
		add(new JLabel("* Jméno: "), gbc);
		
		gbc.gridx = 1;
		txtFirstName = new JTextField(20);
		txtFirstName.addKeyListener(keyAdapter);
		add(txtFirstName, gbc);
		
		
		
		
		setGbc(gbc, 0, indexOfRow);
		add(new JLabel("* Příjmení: "), gbc);
		
		setGbc(gbc, 1, indexOfRow);
		txtLastName = new JTextField(20);
		txtLastName.addKeyListener(keyAdapter);
		add(txtLastName, gbc);
		
		
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("* Uživatelské jméno: "), gbc);
		
		setGbc(gbc, 1, indexOfRow);
		txtPseudonym = new JTextField(20);
		txtPseudonym.addKeyListener(keyAdapter);
		add(txtPseudonym, gbc);
		
		
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("* Heslo (1): "), gbc);
		
		setGbc(gbc, 1, indexOfRow);
		txtPassword_1 = new JPasswordField(20);
		txtPassword_1.addKeyListener(keyAdapter);
		add(txtPassword_1, gbc);
		
		
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("* Heslo (2): "), gbc);
		
		setGbc(gbc, 1, indexOfRow);
		txtPassword_2 = new JPasswordField(20);
		txtPassword_2.addKeyListener(keyAdapter);
		add(txtPassword_2, gbc);
		
		
		
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("* Email: "), gbc);
		
		setGbc(gbc, 1, indexOfRow);
		txtEmail = new JTextField(20);
		txtEmail.addKeyListener(keyAdapter);
		add(txtEmail, gbc);
		
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("Ulice: "), gbc);
		
		setGbc(gbc, 1, indexOfRow);
		txtStreet = new JTextField("Ulice čp", 20);
		txtStreet.addKeyListener(keyAdapter);
		add(txtStreet, gbc);
		
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("PSČ: "), gbc);
		
		setGbc(gbc, 1, indexOfRow);		
		add(cmbPsc, gbc);
		
		
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("Telefonní číslo: "), gbc);
		
		setGbc(gbc, 1, indexOfRow);
		txtTelephoneNumber = new JTextField(20);
		txtTelephoneNumber.addKeyListener(keyAdapter);
		add(txtTelephoneNumber, gbc);
		
		
		
		setGbc(gbc, 0, ++indexOfRow);
		add(new JLabel("Obrázek: "), gbc);
		
		lblPath = new JLabel("Nevybrán");
		btnPicture = new JButton("Vybrat");
		btnPicture.addActionListener(Event -> {
			final File file = new MyFileChooser().getUserPicture();

			if (file != null) {
				filePicture = file;
				lblPath.setText(filePicture.getAbsoluteFile().toString());
				
				try {
					lblImage.setIcon(GetIconInterface.getResizedImage(new ImageIcon(file.toURI().toURL()), 150, 150));
					lblImage.setText("");
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		});
		
		final JPanel pnlImage = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
		pnlImage.add(lblPath);
		pnlImage.add(btnPicture);
		
		setGbc(gbc, 1, indexOfRow);
		add(pnlImage, gbc);

		
		
		
		
		
		lblImage = new JLabel("Obrázek nevybrán", JLabel.CENTER);
		gbc.gridwidth = 2;
		setGbc(gbc, 0, ++indexOfRow);
		add(lblImage, gbc);		
		
		
		
		
		
		
		
		
		
		
		
		if (showOptionsForCreateAdmin) {
			setGbc(gbc, 0, ++indexOfRow);
			add(new JLabel("Ban: "), gbc);
			
			setGbc(gbc, 1, indexOfRow);		
			add(cmbBan, gbc);
			
			
			
			setGbc(gbc, 0, ++indexOfRow);
			add(new JLabel("Typ uživatele: "), gbc);
			
			setGbc(gbc, 1, indexOfRow);		
			add(cmbKindOfUser, gbc);
		}
		
		
		
		
		
		// Panel s tlačítky pro registraci nového uživatele a zavření dialogu
		final JPanel pnlButtons = createPnlButtons();
		
		btnConfirm.setText("Registrovat");
		btnConfirm.addActionListener(Event -> checkData());
		
		btnCancel.addActionListener(Event -> {
			user = null;
			dispose();
		});
		
		setGbc(gbc, 0, ++indexOfRow);
		gbc.gridwidth = 2;
		add(pnlButtons, gbc);
		
		
		
		if (showWindow)
			prepareWindow();
	}
	
	
	
	
	
	
	
	
	public final User getEditedUser(final User user) {
		setTitle("Editace");
		
		this.user = user;
		
		txtFirstName.setText(user.getFirstName());
		txtLastName.setText(user.getLastName());
		txtPseudonym.setText(user.getPseudonym());
		txtStreet.setText(user.getStreet());
		txtEmail.setText(user.getEmail());
		txtTelephoneNumber.setText(String.valueOf(user.getTelephoneNumber()));
		
		if (user.getPicture() != null && user.getPicture().getIconWidth() > -1) {
			lblImage.setIcon(GetIconInterface.getResizedImage(user.getPicture(), 150, 150));
			lblImage.setText("");
		}
			
		
		setCmbSelectedValue(cmbPsc, getPscWithSpace(String.valueOf(user.getPsc()).toCharArray()));
		
		
		if (showOptionsForCreateAdmin) {
			if (user.getBan() == '0')
				cmbBan.setSelectedIndex(0);
			else
				cmbBan.setSelectedIndex(1); 

			
			setCmbSelectedValue(cmbKindOfUser, user.getKindOfUser());
		}
		
		btnConfirm.setText("Upravit");
		
		
		prepareWindow();
		
		/*
		 * V této proměnné bude buď null, nebo daný uživatel s upravenými hodnotami.
		 */
		return this.user;
	}
	
	
	
	
	
	
	
	/**
	 * Metoda, která nastaví v daném JComboBoxu (cmb) označenou položku (value).
	 * 
	 * @param cmb
	 *            - JComboBox, ve kterém se má označit položka (value)
	 * @param value
	 *            - položka, která se má označit v cmb výše
	 */
	private static void setCmbSelectedValue(final JComboBox<String> cmb, final String value) {
		int index = -1;
		
		for (int i = 0; i < cmb.getItemCount(); i++) {
			if (cmb.getItemAt(i).equals(value)) {
				index = i;
				break;
			}
		}	
		
		if (index > -1)
			cmb.setSelectedIndex(index);
	}
	
	
	
	
	
	
	
	
	
	@Override
	protected void checkData() {
		/*
		 * Metoda, která zkontroluje data, případně vytvoří nového uživatele nebo vyhodí
		 * nějakou chybovou hlášku s chybou, ke které došlo, například je nějaké pole
		 * prázdné, hesla jsou různá apod. viz kód metody.
		 */
		
		/*
		 * Postup:
		 * - Otestuji, zda jsou všechna požadovaná pole vyplněná
		 * - Otestuji, zda zadané hodnoty splňují požadovanou délku - počet znaků
		 * - Zda všechny zadané hodnoty obsahují validní data dle regulárních výrazů
		 * - Zda jsou helsa stejná
		 * 
		 *  Pokud jsou podmínky výše splněny, pak je možné vytvořit uživatele
		 */
		
		final String password_1 = new String(txtPassword_1.getPassword());
		final String password_2 = new String(txtPassword_2.getPassword());
		
		// Zda jsou požadovaná pole vyplněná:
		if (txtFirstName.getText().isEmpty() || txtLastName.getText().isEmpty() || txtEmail.getText().isEmpty()
				|| password_1.isEmpty() || password_2.isEmpty() || txtPseudonym.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Alespoň jedno z požadovaných polí je prázdné!", "Prázdné pole",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		// Velikost řetězců:
		// Note:
		// Telefonní číslo zde neřeším, co je otestováno regulárními výrazy a mezery se
		// nakonec odebereou
		if (txtFirstName.getText().length() > 30 || txtLastName.getText().length() > 40
				|| txtEmail.getText().length() > 50
				|| txtStreet.getText().length() > 65 || password_1.length() > 35
				|| password_2.length() > 35 || txtPseudonym.getText().length() > 30) {
			JOptionPane.showMessageDialog(this, "Řětezec v nějakém poli je delší než požadované minimum!",
					"Délka řetezce", JOptionPane.ERROR_MESSAGE);
			return;
		}			
		
		
		
		// Regulární výrazy:
		if (!txtFirstName.getText().matches(REG_EX_FOR_FIRST_AND_LAST_NAME)) {
			JOptionPane.showMessageDialog(this, "Jméno není zadáno v požadované syntaxi!", "Chybná syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!txtLastName.getText().matches(REG_EX_FOR_FIRST_AND_LAST_NAME)) {
			JOptionPane.showMessageDialog(this, "Příjmení není zadáno v požadované syntaxi!", "Chybná syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!txtPseudonym.getText().matches(REG_EX_PSEUDONYM)) {
			JOptionPane.showMessageDialog(this, "Uživatelské jméno není zadáno v požadované syntaxi!", "Chybná syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (!txtEmail.getText().matches(REG_EX_EMAIL)) {
			JOptionPane.showMessageDialog(this, "Email není zadáno v požadované syntaxi!", "Chybná syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (!txtStreet.getText().isEmpty() && !txtStreet.getText().matches(REG_EX_STREET)) {
			JOptionPane.showMessageDialog(this, "Ulice není zadáno v požadované syntaxi!", "Chybná syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (!txtTelephoneNumber.getText().isEmpty() && !txtTelephoneNumber.getText().matches(REG_EX_TELEPHONE_NUMBER)) {
			JOptionPane.showMessageDialog(this, "Telefonní číslo není zadáno v požadované syntaxi!", "Chybná syntaxe",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		
		
		
		// Stejná hesla:
		if (!password_1.equals(password_2)) {
			JOptionPane.showMessageDialog(null, "Hesla jsou různá!", "Různá helsa", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		/*
		 * Nyní je možné vytvořit uživatele. Postupně otestujji nepovinná pole, abych
		 * věděl, jaké hodnoty mám použít a jaká ne, pokud bude nějkteré z nepovinných
		 * polí vyplněné, tak zde už vím, že hodnota prošla regulárním výrazem, tak jej
		 * mohu použít.
		 * 
		 * na konec ještě otestuji, zda ještě existuje zvolený obrázek, případně předám
		 * příslušnou hodnotu filePicture aby se načetl do DB.
		 */
		final User newUser;
		
		if (this.user != null)
			newUser = this.user;
		else
			newUser = new User();

		newUser.setFirstName(txtFirstName.getText());
		newUser.setLastName(txtLastName.getText());
		newUser.setPseudonym(txtPseudonym.getText());
		newUser.setPassword(password_1);
		newUser.setEmail(txtEmail.getText());
		newUser.setFileImage(filePicture);
		
		if (filePicture != null && filePicture.exists()) {
			try {
				newUser.setPicture(new ImageIcon(filePicture.toURI().toURL()));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}

		
		if (!txtStreet.getText().isEmpty())
			newUser.setStreet(txtStreet.getText());
		
		if (!txtTelephoneNumber.getText().isEmpty())
			newUser.setTelephoneNumber(Integer.parseInt(txtTelephoneNumber.getText().replaceAll("\\s", "")));
		
		/*
		 * Pro PSC si nejprve vezmu označený text a pak zjistím, zda se nejedná o
		 * hodnotu "prázdný text" pokud ano, tak psc nebudu nastavovat, pokud ne, je
		 * zvolno PSC, tak jej nastavím.
		 */
		final String selectedPsc = (String) cmbPsc.getSelectedItem();
		if (selectedPsc.equals(""))
			newUser.setPsc(-1);
		else
			newUser.setPsc(Integer.parseInt(selectedPsc.replaceAll("\\s", "")));
		
		
		if (showOptionsForCreateAdmin) {
			// Zde použiji hodnoty z formuláře:
			
			// Pro ban si vezmu první znak z textu, tam mám nulu nebo jedničku:
			newUser.setBan(((String) cmbBan.getSelectedItem()).charAt(0));
			
			// Pro typ uživatele si vezmu přímo text z ComboBoxu, protože tam byly načtené
			// přímo hodnoty z DB
			newUser.setKindOfUser((String) cmbKindOfUser.getSelectedItem());
		}
		
		else {
			// Zde se jedná o vytvoření běžného uživatele, tak nastvím výchozí hodnoty:
			newUser.setBan('0');	// Povoleno
			
			newUser.setKindOfUser("Player");
		}
		
		
		

		/*
		 * Byní mám připraveného nového uživatele, tak zjistím, zda se v DB uživatel s
		 * daným jménem ještě nenachází.		 
		 * 
		 * Porměnná, do které si uložím, hodnotu true, pokud se zadaný pseudonym již používá, jinak false
		 */
		boolean isPseudonymUsed = false;		
		
		final List<User> allUsersList = DATA_MANAGER.getAllUsers();
		
		for (final User u : allUsersList)
			if (u.getPseudonym().equals(newUser.getPseudonym()) && u.getId() != newUser.getId()) {
				isPseudonymUsed = true;
				break;
			}
		
		
		
		if (isPseudonymUsed) {
			JOptionPane.showMessageDialog(this,
					"Uživatelské jméno '" + newUser.getPseudonym() + "' je již použito, vyberte si prosím jiné.",
					"Uživatelské jméno využito", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
			
		
		
		/*
		 * Pokud se jedná o editaci uživatele, pak v této proměnné bude uložen uživatel,
		 * který se má upravit, tak do této proměnné vložím upravného uživatele ve
		 * formuláří a vrátím jej, aby se mohl upravit v DB.
		 */
		if (this.user != null) {
			this.user = newUser;
			dispose();
			return;
		}
		
		
		
		
		final boolean addedUser = DATA_MANAGER.addUser(newUser);
		
		if (addedUser) {
			JOptionPane.showMessageDialog(this, "Uživatel '" + newUser.getPseudonym() + "' byl vytvořen, můžete se přihlásit.", "Uživatel vytvořen",
					JOptionPane.INFORMATION_MESSAGE);
			
			dispose();
		}


		else
			JOptionPane.showMessageDialog(this,
					"Nastala chyba při pokusu o přidání uživatele do DB!\nUživatel nebyl přidán!",
					"Selhalo přidání uživatele", JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Metoda, která načte výchozí hodnoty z DB do některých komponent, které je
	 * třeba vybrat.
	 */
	private void loadValues() {
		// Získám si list PSC z DB
		final List<Integer> pscList = DATA_MANAGER.getAllPsc();

		
		/*
		 * Do tohoto pole si budu vkládat všechna PSC načtená z DB, ale první bude
		 * prázdná možnost, kdyby jej uživatel nechtěl zadat a navíc je ještě trochu
		 * rozdělím, že za 3 číslo dám mezeru, aby to bylo trochu přehlednější
		 */
		final String[] pscArray = new String[pscList.size() + 1];
		pscArray[0] = "";
		
		for (int i = 0; i < pscList.size(); i++) {
			final char[] charArray = pscList.get(i).toString().toCharArray();
			
			pscArray[i + 1] = getPscWithSpace(charArray);
		}
		
		cmbPsc = new JComboBox<>(pscArray);
				

		
		/*
		 * Pokud je přihlášen uživatel typu admin, pak zobrazím i možnosti pro
		 * zablokování nového účtu a nastavení typu účtu
		 */
		if (showOptionsForCreateAdmin) {
			// Dále si načtu typy uživatelů:
			final List<String> kindsOfUserList = DATA_MANAGER.getAllKindOfUser();
			final String[] kindsOfUserArray = new String[kindsOfUserList.size()];
			for (int i = 0; i < kindsOfUserList.size(); i++)
				kindsOfUserArray[i] = kindsOfUserList.get(i);
			
			cmbKindOfUser = new JComboBox<>(kindsOfUserArray);

			cmbBan = new JComboBox<>(new String[]{"0 - Povoleno", "1 - Zablokováno"});
		}
	}
	
	
	
	
	
	
	/**
	 * Metoda, která přidá do psc mezeru za první tři čísla.
	 * 
	 * @param charArray
	 *            - pole znaků, které obsahuje psc (každý znak = 1 písmeno z psc)
	 * @return - psc v podobě Stringu s mezerou za 3. písmenem
	 */
	private static String getPscWithSpace(final char[] charArray) {
		String finalString = "";
		for (int q = 0; q < charArray.length; q++) {
			if (q == 3)
				finalString += " ";
			finalString += String.valueOf(charArray[q]);
		}
		
		return finalString;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Tato třída slouží puze k tomu, aby se po zavření tohoto dialogu nastavila proměnná user na null,
	 * protože by se jinal tato hodnota vratila, což nechci.
	 * 
	 * @author jan
	 *
	 */
	class MyWindowAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			user = null;
		}
	}
}