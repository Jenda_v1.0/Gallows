# Šibenice #

## Představení ##

Jedná se o desktopovou aplikaci - hra Šibenice. Aplikaci by mělo být možné spustit téměř na libovolném zařízení s nainstalovanou verzí Javy 1.8 a vyšší. Testováno pouze na zařízeních s OS Window 10 a Linux Mint 18.3 s nainstalovanou verzí (Oracle) Javy 1.8.

*(Jedná se pouze o "zkušební" aplikaci napsanou v minulosti za účelem zkoušení nových funkcí a postupů, algoritmizace, práce s databází apod. Z časových důvodů nebylo do aplikace doplněno nastavení například pro nastavení vlastních obrázků, "šablon" pro nějaké hry, například nastavení výchozí časomíry nebo "vlastních" her, případně nastavení jazyka atd.)*

Je třeba si stáhnout databázi, ke které musíme nastavit cestu v aplikaci. Databáze obsahuje předpřipravené údaje pro hru a výchozí administrátorský účet (přihlašovací údaje jsou pro jméno a heslo "admin").



## Adresáře v branchi ##

* DB - Adresář, který obsahuje databázi pro aplikaci. (K tomuto adresáři nastavujeme cestu.)

* Gallows - Projekt Šibenice, tj. zdrojové kódy, obrázky, vygenerovaná aplikace (na cestě .../Gallows/out/artifacts/Gallows_Jar/zde soubor Gallow.jar) atd.

* Gallows_EA - Schéma databáze vytvořené v prostředí [Enterprise Architect](http://sparxsystems.com).



## Postup ##

Nejprve je třeba stáhnout aplikaci s databází. Poté můžeme spustit aplikaci. Jako první se nám zobrazí okno pro přihlášení, ve kterém musíme jako první krok nastavit cestu ke stažené databázi (adresář DB). To provedeme kliknutím na tlačítko "Nastavit DB".

Poté můžeme zadat přihlašovací údaje. Můžeme využít buď výše zmíněný administrátorský účet nebo zaregistrovat vlastní. *Zadané údaje jsou ukládány do lokální databáze, kterou jsme si stáhli, tudíž můžeme zadat libovolné údaje, které splňují [regulární výrazy](https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html).*

Po zadání přihlašovacích údajů se můžeme přihlásit. Pokud jsme zadali správné údaje, zobrazí se nám hlavní okno aplikace. V případě potřeby je doplněna možnost pro obnovení smazaného účtu. Po kliknutí na toto tlačítko se nám zobrazí dialog, do kterého vyplníme uživatelské jméno, email, původní a nové heslo. Takto můžeme obnovit účet v případě, že jsme jej buď sami smazali nebo označili jako smazaný (může administrátor). Pokud administrátor úplně vymazal příslušný účet i s jeho hrami, pak nelze účet obnovit.



## Uživatelé ##

Aplikace podporuje dva typy uživatelských účtů "Administrátor" (označován jako Admin) a "Hráč" (označován jako Player / běžný uživatel). Uživatelé obou typů mohou hrát hru, ukládat výsledky, prohlížet si veškeré výsledky a mazat vlastní výsledky. Admin má pouze několik možností navíc.

* Admin - Uživatel může vytvářet další účty stejného typu a spravovat veškerá data v DB. Například mazat odehrané hry veškerých uživatelů, vytvářet další uživatele s vyšším počtem možností (ban a typ). Může uživatele z DB úplně vymazat nebo označit za vymazané. Vymazaný uživatel nelze být obnoven, jsou smazána veškerá jeho data. Uživatel označený jako smazaný lze obnovit výše popsaným způsobem, jeho data nejsou v aplikaci nijak zobrazována. Admin má dále k dispozici další možnosti jako je například přidávání dalších slovíček nebo skupin slovíček do DB apod. Může zablokovat a odblokovat uživatelské účty.


* Player - Jedná se v podstatě o běžný uživatelský účet. Může spravovat údaje svého účtu, hrát hru a ukládat, případně mazat výsledky svých odehraných her.



## Menu ##

* Aplikace

    * Vybrat hru - Položka zobrazí dialogové okno, kde si vybereme skupinu hádaných slovíček a typ hry. Buď Measurement nebo Countdown.

        * Measurement - Jedná se o způsob hry, kdy se počítá nejvyšší dosažené skóre za co nejkratší čas.

        * Countdown - Jedná se o způsob hry, kdy si nastavíme časomíru, například dvě minuty. Uživatel poté musí uhodnout co nejvíce slovíček než vyprší nastavený čas.


    * Nastavit DB - Položka zobrazí dialogové okno pro nastavení cesty k adresáři stažené databáze, viz výše.


    * Odhlásit - Odhlášení uživatele a zobrazení dialogového okna pro přihlášení.


    * Zavřít - Ukončení aplikace



* Hra

    * Nová hra - Položka pro zahájení nové hry. Pokud jsme dosud nevybraly typ hry a skupinu slovíček, zobrazí se nám nejprve dialog (popsáno u položky "Vybrat hru"), ve kterém tyto údaje nastavíme. Po potvrzení dialogu bude zahájena nová hra.


    * Start - Ukončení pauzy.


    * Pauza - Zahájení pauzy.


    * Stop - Ukončení a nabídka pro uložení aktuálně rozehrané hry.


    * Nápověda - Zobrazí dialogové okno s nápovědou k hádanému slovíčku.


    * Nejlepší výsledky - Zobrazí dialogové okno, kde jsou zobrazeny výsledky veškerých her v DB. Jsou zde k dispozici různé způsoby pro filtrování a vyhledávání. V případě, že je přihlášen administrátor, je k dispozici položka pro vymazání her.



* Uživatel

    * Info - Zobrazí dialogové okno s přehledem uživatelských údajů (jméno, příjmení, heslo, ...).

    * Výsledky - Zobrazí dialogové okno, kde se nachází přehled odehraných her přihlášeného uživatele.



* Info

    * Informace - Zobrazí dialogové okno obsahující základní informace o využívání aplikace.

    * Autor - Zobrazí dialogové okno s informacemi o autorovi aplikace.



* Databáze (bude zobrazeno pouze v případě, že je přihlášen administrátor)

    * Uživatelé - Zobrazí dialogové okno s přehledem veškerých uživatelů. Přihlášený administrátor zde může například vytvořit nové uživatele, editovat či mazat existující atd. Zde jsou například možnosti pro vymazání či blokování uživatele, dále označení jako vymazaného uživatele atd.


    * Skupiny slov - Zobrazí dialogové okno, které obsahuje přehled skupin slov a ke každé skupině jednotlivá slova. Jedná se o slovíčka, která uživatel hádá při hraní hry. Dále jsou k dispozici možnosti pro řazení a vyhledávání jak skupin, tak jednotlivých slovíček (pro jednotlivé skupiny). Případně CRUD operace pro skupiny a slovíčka.


    * Města - Zobrazí dialogové okno, které obsahuje přehled měst v DB. Jsou zde k dispozici možnosti pro CRUD, řadící a vyhledávací operace.


    * Typy uživatelů - Zobrazí dialogové okno, kde se nachází přehled typů uživatelů (admin a player). Jelikož aplikace podporuje pouze zmíněné dva typy, tento dialog není potřeba. Byl doplněn pouze pro testovací účely a potenciální budoucí rozšíření. Údaje by neměly být editovány.


    * Typy her - Zobrazí dialogové okno s přehledem typů her (measurement a countdown). Podobně jako dialog pro typy uživatelů, tak ani tento dialog není nezbytný a je zde pouze pro testovací účely a potenciální rozšíření. Údaje by neměly být editovány.



## Ovládání ##

Po spuštění aplikace, nastavení DB a přihlášení uživatele stačí kliknout v nástrojové liště na první tlačítko "Nová hra". V případě prvního spuštění hry bude zobrazeno dialogové okno pro výběr typu hry a skupiny slovíček. Po potvrzení dialogu bude zahájena nová hra. Pokud se jedná o opakované zahájení nové hry, může být zobrazen dotaz na uložení rozehrané hry.

Po zahájení hry budou zpřístupněna tlačítka ve spodní části okna aplikace, která značí vybraná slovíčka české abecedy. Neuvedená písmena s diakritikou budou v případě nalezené shody automaticky doplněna na příslušné písmeno. Například po kliknutí na tlačítko reprezentující písmeno "A" bude otestován hádaný řetězec, zda obsahuje písmeno "A" i "Á", případně budou doplněna obě písmena v hádaném slově. Podobně pro ostatní písmena.

V některých případech může být zobrazena nápověda k hádanému slovíčku. Tu zobrazíme během hraní hry kliknutím na poslední tlačítko v nástrojové liště v hlavním okně aplikace. *Nápovědu značí komentář u jednotlivých slovíček v DB.*